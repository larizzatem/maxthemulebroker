<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="images/muleBroker.png" alt="Logo" width="200" height="200">
  </a>

  <h3 align="center">Max The Mule Broker</h3>

  <p align="center">
    An awesome project to share my knowledge with the company
    <br />
    <a href="https://gitlab.com/larizzatem/maxthemulebroker/"><strong>Explore the docs »</strong></a>
    <br />
    <br />
  </p>
</div>

<!-- ABOUT THE PROJECT -->
## About The Project

[![Max The Mule Broker][product-screenshot]](https://gitlab.com/larizzatem/maxthemulebroker/)

A simple exercise is designed to show us how much you can learn about MuleSoft in a short
period of time and also, present your deliverables to our services team. We want to see how your
thinking process is throughout this problem, presenting it to us and doing a short technical demo
of your solution.

<p align="right">(<a href="#top">back to top</a>)</p>


### Built With

This section list major frameworks/libraries used to bootstrap the project.

* [MuleSoft](https://mulesoft.com/)
* [Anypoint Studio](https://www.mulesoft.com/platform/studio)
* [Docker](https://www.docker.com/)

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started

This is an easy restful API to create market orders and check historical orders data.

<p align="right">(<a href="#top">back to top</a>)</p>

## Functionalities

You will have to create a simple REST web service that will access that database and at least
be able to:
1. Retrieve all the "filled" orders data including the net amount of each transaction
(given the quantity of shares and the price).
2. Retrieve all the orders data for a given year.
3. Insert a new "BUY" or "SELL" order into the table with the status "Pending".
4. Validate JSON Schema
5. Error Handling

## Testing Coverage
It is a measure (in percent) of the degree to which the source code of a program is executed when a particular test suite is run.
At the moment, this amazing solution has a **80% of Coverage**

<img src="images/coverage.png" alt="Logo" >

### Prerequisites

As it is a Mule Application with Java background code, you can build your own runnable JAR.
To improve the experience, the solution was containerized and its image uploaded to Docker hub, please check at [larizzatem/maxthemulebroker](https://hub.docker.com/repository/docker/larizzatem/maxthemulebroker)

* You will only need to install docker in your PC or server.

<p align="right">(<a href="#top">back to top</a>)</p>

### Installation

As this solution was containerized and published, you dont need to loose time on building a testing environment.

1. Install docker from the Docker’s [website](https://docs.docker.com/compose/install/)
2. Download docker image from Docker Hub with the next command: 

> docker pull larizzatem/maxthemulebroker:1.0.0

3. Run the amazing MaxTheMuleBroker App with the next command:

> docker run -it --rm -p 8081:8081 larizzatem/maxthemulebroker:1.0.0

Note: If you want to run it in the background or the detached mode from the terminal, you can use the docker run command followed by the -d flag (or detached flag) and followed by the name of the docker image you need to use in the terminal.

4. Just Enjoy!

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- ROADMAP -->
## Roadmap

- [x] Implement required functionalities
- [x] Add request logs
- [x] Body Validations for POST Requests
- [x] Error Handling with json responses
- [x] Unit Tests
- [ ] API Versioning
- [ ] Error Logging
- [ ] Pagination
- [ ] Response Fields Selection
    - [ ] Chinese
    - [ ] Spanish

<!-- ACKNOWLEDGMENTS -->
## Acquired Acknowledgments

I'll list here a list of the most important things I learned for this exercise.

* [Mulesoft](https://mulesoft.com)
* [Munit](https://docs.mulesoft.com/munit)
* [Test Recorder Studio](https://docs.mulesoft.com/munit/2.3/test-recorder)
* [Mule Validations](https://docs.mulesoft.com/mule-runtime/3.9/validations-module)
* [Mule Error Handling](https://docs.mulesoft.com/mule-runtime/4.4/error-handling)






[product-screenshot]: images/screenshot.png
