%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo([
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-12-07T13:06:46",
    "PRICE": 90.00,
    "QUANTITY": 3,
    "STATUS": "Pending",
    "SYMBOL": "TST",
    "TX_NUMBER": "10901",
    "TOTAL_AMOUNT": 270.00
  }
])