%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo([
  {
    "ORDER_DATE": |2021-12-07T13:06:46|,
    "SYMBOL": "TST",
    "QUANTITY": 3,
    "PRICE": 90.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10901,
    "STATUS": "Pending"
  }
])