%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo([
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 1.30,
    "QUANTITY": 1,
    "STATUS": "Canceled",
    "SYMBOL": "AAA",
    "TX_NUMBER": "1",
    "TOTAL_AMOUNT": 1.30
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 1.30,
    "QUANTITY": 1,
    "STATUS": "Canceled",
    "SYMBOL": "AAA",
    "TX_NUMBER": "2",
    "TOTAL_AMOUNT": 1.30
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 1.30,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "3",
    "TOTAL_AMOUNT": 1.30
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 1.30,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "4",
    "TOTAL_AMOUNT": 1.30
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 25.10,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "5",
    "TOTAL_AMOUNT": 251.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "6",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "11",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "13",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-04T13:48:01",
    "PRICE": 613.80,
    "QUANTITY": 6774,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "65",
    "TOTAL_AMOUNT": 4157881.12
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-11-07T21:05:17",
    "PRICE": 224.11,
    "QUANTITY": 4935,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "66",
    "TOTAL_AMOUNT": 1105982.85
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-05T07:22:56",
    "PRICE": 815.15,
    "QUANTITY": 2247,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "67",
    "TOTAL_AMOUNT": 1831642.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-08T01:27:19",
    "PRICE": 380.44,
    "QUANTITY": 7120,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "68",
    "TOTAL_AMOUNT": 2708732.82
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-08T18:33:28",
    "PRICE": 478.60,
    "QUANTITY": 3055,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "69",
    "TOTAL_AMOUNT": 1462123.02
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-10T12:56:36",
    "PRICE": 200.69,
    "QUANTITY": 7009,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "70",
    "TOTAL_AMOUNT": 1406636.23
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-01T19:34:14",
    "PRICE": 274.48,
    "QUANTITY": 6932,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "71",
    "TOTAL_AMOUNT": 1902695.44
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-04T06:24:13",
    "PRICE": 284.01,
    "QUANTITY": 3602,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "72",
    "TOTAL_AMOUNT": 1023004.06
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-09T14:00:34",
    "PRICE": 361.13,
    "QUANTITY": 2484,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "73",
    "TOTAL_AMOUNT": 897046.93
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-31T12:42:32",
    "PRICE": 336.46,
    "QUANTITY": 4858,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "74",
    "TOTAL_AMOUNT": 1634522.64
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-21T16:28:05",
    "PRICE": 635.31,
    "QUANTITY": 5155,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "75",
    "TOTAL_AMOUNT": 3275023.04
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-07T13:09:28",
    "PRICE": 402.92,
    "QUANTITY": 2700,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "76",
    "TOTAL_AMOUNT": 1087884.04
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-18T11:48:32",
    "PRICE": 559.53,
    "QUANTITY": 9952,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "77",
    "TOTAL_AMOUNT": 5568442.85
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-22T11:44:15",
    "PRICE": 344.92,
    "QUANTITY": 1743,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "78",
    "TOTAL_AMOUNT": 601195.58
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-09T16:37:39",
    "PRICE": 150.20,
    "QUANTITY": 8883,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "79",
    "TOTAL_AMOUNT": 1334226.57
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-08T15:12:04",
    "PRICE": 853.14,
    "QUANTITY": 6482,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "80",
    "TOTAL_AMOUNT": 5530053.57
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-14T02:25:02",
    "PRICE": 961.71,
    "QUANTITY": 3234,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "81",
    "TOTAL_AMOUNT": 3110170.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-08T21:40:50",
    "PRICE": 107.85,
    "QUANTITY": 505,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "82",
    "TOTAL_AMOUNT": 54464.25
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-30T04:06:04",
    "PRICE": 641.85,
    "QUANTITY": 6369,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "83",
    "TOTAL_AMOUNT": 4087942.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-15T00:07:04",
    "PRICE": 837.03,
    "QUANTITY": 9384,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "84",
    "TOTAL_AMOUNT": 7854689.79
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-26T14:05:35",
    "PRICE": 523.55,
    "QUANTITY": 6835,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "85",
    "TOTAL_AMOUNT": 3578464.17
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-23T18:20:47",
    "PRICE": 284.88,
    "QUANTITY": 480,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "86",
    "TOTAL_AMOUNT": 136742.40
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-13T09:55:10",
    "PRICE": 742.91,
    "QUANTITY": 5946,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "87",
    "TOTAL_AMOUNT": 4417342.70
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-15T12:15:08",
    "PRICE": 277.43,
    "QUANTITY": 3136,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "88",
    "TOTAL_AMOUNT": 870020.46
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-08T21:29:49",
    "PRICE": 911.17,
    "QUANTITY": 8657,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "89",
    "TOTAL_AMOUNT": 7887998.54
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-17T08:59:00",
    "PRICE": 294.18,
    "QUANTITY": 3503,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "90",
    "TOTAL_AMOUNT": 1030512.51
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-21T02:26:14",
    "PRICE": 374.16,
    "QUANTITY": 7463,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "91",
    "TOTAL_AMOUNT": 2792356.11
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-07T02:59:20",
    "PRICE": 482.14,
    "QUANTITY": 4675,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "92",
    "TOTAL_AMOUNT": 2254004.57
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-07T03:03:11",
    "PRICE": 891.19,
    "QUANTITY": 230,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "93",
    "TOTAL_AMOUNT": 204973.70
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-10T05:23:46",
    "PRICE": 189.31,
    "QUANTITY": 2319,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "94",
    "TOTAL_AMOUNT": 439009.88
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-15T22:47:30",
    "PRICE": 989.21,
    "QUANTITY": 3015,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "95",
    "TOTAL_AMOUNT": 2982468.22
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-05T05:01:13",
    "PRICE": 451.68,
    "QUANTITY": 3943,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "96",
    "TOTAL_AMOUNT": 1780974.21
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-04T10:09:59",
    "PRICE": 493.68,
    "QUANTITY": 8682,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "97",
    "TOTAL_AMOUNT": 4286129.70
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-19T02:10:40",
    "PRICE": 604.44,
    "QUANTITY": 5829,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "98",
    "TOTAL_AMOUNT": 3523280.77
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-06T08:25:16",
    "PRICE": 110.44,
    "QUANTITY": 7818,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "99",
    "TOTAL_AMOUNT": 863419.94
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-04T15:13:56",
    "PRICE": 221.07,
    "QUANTITY": 6298,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "100",
    "TOTAL_AMOUNT": 1392298.91
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-07T01:58:30",
    "PRICE": 899.73,
    "QUANTITY": 3526,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "101",
    "TOTAL_AMOUNT": 3172447.91
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-13T20:37:56",
    "PRICE": 735.36,
    "QUANTITY": 7792,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "102",
    "TOTAL_AMOUNT": 5729925.01
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-16T17:01:48",
    "PRICE": 310.56,
    "QUANTITY": 1941,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "103",
    "TOTAL_AMOUNT": 602796.96
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-24T03:09:34",
    "PRICE": 286.46,
    "QUANTITY": 9146,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "104",
    "TOTAL_AMOUNT": 2619963.08
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-06T13:54:54",
    "PRICE": 524.03,
    "QUANTITY": 5562,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "105",
    "TOTAL_AMOUNT": 2914655.02
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-01T10:30:46",
    "PRICE": 272.54,
    "QUANTITY": 7203,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "106",
    "TOTAL_AMOUNT": 1963105.68
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-31T11:19:56",
    "PRICE": 270.91,
    "QUANTITY": 3191,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "107",
    "TOTAL_AMOUNT": 864473.82
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-29T19:17:26",
    "PRICE": 317.97,
    "QUANTITY": 7924,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "108",
    "TOTAL_AMOUNT": 2519594.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-24T02:36:43",
    "PRICE": 462.72,
    "QUANTITY": 2540,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "109",
    "TOTAL_AMOUNT": 1175308.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-26T22:49:53",
    "PRICE": 614.45,
    "QUANTITY": 9063,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "110",
    "TOTAL_AMOUNT": 5568760.46
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-13T10:42:28",
    "PRICE": 884.51,
    "QUANTITY": 8647,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "111",
    "TOTAL_AMOUNT": 7648358.05
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-07T04:29:54",
    "PRICE": 774.64,
    "QUANTITY": 7403,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "112",
    "TOTAL_AMOUNT": 5734660.03
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-11T22:15:22",
    "PRICE": 566.13,
    "QUANTITY": 1581,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "113",
    "TOTAL_AMOUNT": 895051.54
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-09T09:09:43",
    "PRICE": 884.47,
    "QUANTITY": 5502,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "114",
    "TOTAL_AMOUNT": 4866353.78
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-19T05:35:55",
    "PRICE": 975.83,
    "QUANTITY": 2335,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "115",
    "TOTAL_AMOUNT": 2278563.09
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-03T10:38:00",
    "PRICE": 692.59,
    "QUANTITY": 1740,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "116",
    "TOTAL_AMOUNT": 1205106.65
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-28T18:52:00",
    "PRICE": 616.47,
    "QUANTITY": 1126,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "117",
    "TOTAL_AMOUNT": 694145.19
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-18T14:09:25",
    "PRICE": 933.30,
    "QUANTITY": 1264,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "118",
    "TOTAL_AMOUNT": 1179691.18
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-16T22:44:48",
    "PRICE": 698.42,
    "QUANTITY": 3070,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "119",
    "TOTAL_AMOUNT": 2144149.35
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-05T04:13:10",
    "PRICE": 843.67,
    "QUANTITY": 2388,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "120",
    "TOTAL_AMOUNT": 2014683.92
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-10T19:48:01",
    "PRICE": 723.84,
    "QUANTITY": 901,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "121",
    "TOTAL_AMOUNT": 652179.86
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-09T20:24:43",
    "PRICE": 978.41,
    "QUANTITY": 3880,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "122",
    "TOTAL_AMOUNT": 3796230.70
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-14T11:54:06",
    "PRICE": 116.86,
    "QUANTITY": 6125,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "123",
    "TOTAL_AMOUNT": 715767.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-09T11:19:46",
    "PRICE": 450.89,
    "QUANTITY": 4684,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "124",
    "TOTAL_AMOUNT": 2111968.83
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-05T21:51:38",
    "PRICE": 360.33,
    "QUANTITY": 1118,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "125",
    "TOTAL_AMOUNT": 402848.92
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-07T14:10:19",
    "PRICE": 418.16,
    "QUANTITY": 8914,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "126",
    "TOTAL_AMOUNT": 3727478.27
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-20T21:22:46",
    "PRICE": 732.68,
    "QUANTITY": 4072,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "127",
    "TOTAL_AMOUNT": 2983472.93
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-29T16:19:13",
    "PRICE": 499.88,
    "QUANTITY": 3309,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "128",
    "TOTAL_AMOUNT": 1654102.94
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-03T12:17:56",
    "PRICE": 577.13,
    "QUANTITY": 7967,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "129",
    "TOTAL_AMOUNT": 4597994.75
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-24T01:03:51",
    "PRICE": 832.34,
    "QUANTITY": 9051,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "130",
    "TOTAL_AMOUNT": 7533509.58
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-28T09:52:51",
    "PRICE": 939.78,
    "QUANTITY": 4685,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "131",
    "TOTAL_AMOUNT": 4402869.44
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-17T04:41:22",
    "PRICE": 419.93,
    "QUANTITY": 2837,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "132",
    "TOTAL_AMOUNT": 1191341.39
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-05T16:15:54",
    "PRICE": 822.43,
    "QUANTITY": 7089,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "133",
    "TOTAL_AMOUNT": 5830206.22
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-21T14:42:58",
    "PRICE": 141.85,
    "QUANTITY": 4027,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "134",
    "TOTAL_AMOUNT": 571229.97
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-29T15:33:54",
    "PRICE": 724.33,
    "QUANTITY": 8425,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "135",
    "TOTAL_AMOUNT": 6102480.39
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-18T18:43:08",
    "PRICE": 124.16,
    "QUANTITY": 6283,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "136",
    "TOTAL_AMOUNT": 780097.30
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-25T20:49:09",
    "PRICE": 653.70,
    "QUANTITY": 8951,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "137",
    "TOTAL_AMOUNT": 5851268.81
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-13T03:10:14",
    "PRICE": 217.00,
    "QUANTITY": 2377,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "138",
    "TOTAL_AMOUNT": 515809.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-26T10:58:53",
    "PRICE": 931.02,
    "QUANTITY": 2743,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "139",
    "TOTAL_AMOUNT": 2553787.91
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-18T22:25:45",
    "PRICE": 522.21,
    "QUANTITY": 2073,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "140",
    "TOTAL_AMOUNT": 1082541.38
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-30T00:10:21",
    "PRICE": 376.90,
    "QUANTITY": 7096,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "141",
    "TOTAL_AMOUNT": 2674482.36
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-11T22:36:23",
    "PRICE": 300.29,
    "QUANTITY": 7301,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "142",
    "TOTAL_AMOUNT": 2192417.35
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-01T21:25:56",
    "PRICE": 907.06,
    "QUANTITY": 796,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "143",
    "TOTAL_AMOUNT": 722019.76
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-14T11:16:10",
    "PRICE": 161.27,
    "QUANTITY": 2112,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "144",
    "TOTAL_AMOUNT": 340602.25
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-23T16:27:46",
    "PRICE": 136.92,
    "QUANTITY": 8674,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "145",
    "TOTAL_AMOUNT": 1187644.06
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-16T17:56:14",
    "PRICE": 78.67,
    "QUANTITY": 1786,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "146",
    "TOTAL_AMOUNT": 140504.62
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-17T14:53:13",
    "PRICE": 373.95,
    "QUANTITY": 5828,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "147",
    "TOTAL_AMOUNT": 2179380.67
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-07T05:03:07",
    "PRICE": 747.52,
    "QUANTITY": 6170,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "148",
    "TOTAL_AMOUNT": 4612198.52
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-20T15:20:06",
    "PRICE": 579.41,
    "QUANTITY": 3189,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "149",
    "TOTAL_AMOUNT": 1847738.40
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-06T11:08:28",
    "PRICE": 168.76,
    "QUANTITY": 8100,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "150",
    "TOTAL_AMOUNT": 1366955.96
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-18T15:33:01",
    "PRICE": 159.89,
    "QUANTITY": 9615,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "151",
    "TOTAL_AMOUNT": 1537342.34
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-17T18:08:50",
    "PRICE": 781.35,
    "QUANTITY": 6245,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "152",
    "TOTAL_AMOUNT": 4879530.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-08T20:46:47",
    "PRICE": 652.95,
    "QUANTITY": 1701,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "153",
    "TOTAL_AMOUNT": 1110667.97
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-02T02:08:23",
    "PRICE": 169.91,
    "QUANTITY": 8308,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "154",
    "TOTAL_AMOUNT": 1411612.31
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-14T20:52:30",
    "PRICE": 170.72,
    "QUANTITY": 5351,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "155",
    "TOTAL_AMOUNT": 913522.73
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-06T00:49:02",
    "PRICE": 106.65,
    "QUANTITY": 8117,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "156",
    "TOTAL_AMOUNT": 865678.06
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-06T10:38:18",
    "PRICE": 943.19,
    "QUANTITY": 6930,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "157",
    "TOTAL_AMOUNT": 6536306.72
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-01T19:59:51",
    "PRICE": 439.49,
    "QUANTITY": 3946,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "158",
    "TOTAL_AMOUNT": 1734227.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-23T21:11:49",
    "PRICE": 777.35,
    "QUANTITY": 9074,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "159",
    "TOTAL_AMOUNT": 7053673.68
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-07T17:19:02",
    "PRICE": 567.13,
    "QUANTITY": 8132,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "160",
    "TOTAL_AMOUNT": 4611901.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-02T02:26:07",
    "PRICE": 627.73,
    "QUANTITY": 3962,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "161",
    "TOTAL_AMOUNT": 2487066.18
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-09T20:30:35",
    "PRICE": 676.46,
    "QUANTITY": 2420,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "162",
    "TOTAL_AMOUNT": 1637033.25
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-29T16:30:07",
    "PRICE": 990.22,
    "QUANTITY": 7325,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "163",
    "TOTAL_AMOUNT": 7253361.29
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-30T01:19:25",
    "PRICE": 890.56,
    "QUANTITY": 9584,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "164",
    "TOTAL_AMOUNT": 8535127.02
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-07T12:02:43",
    "PRICE": 773.72,
    "QUANTITY": 9282,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "165",
    "TOTAL_AMOUNT": 7181668.77
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-01T15:41:28",
    "PRICE": 308.58,
    "QUANTITY": 6424,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "166",
    "TOTAL_AMOUNT": 1982317.83
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-03T02:12:32",
    "PRICE": 752.14,
    "QUANTITY": 4702,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "167",
    "TOTAL_AMOUNT": 3536562.35
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-21T17:22:41",
    "PRICE": 356.41,
    "QUANTITY": 6793,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "168",
    "TOTAL_AMOUNT": 2421093.15
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-17T23:05:22",
    "PRICE": 701.21,
    "QUANTITY": 2813,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "169",
    "TOTAL_AMOUNT": 1972503.79
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-02T10:20:52",
    "PRICE": 581.29,
    "QUANTITY": 4915,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "170",
    "TOTAL_AMOUNT": 2857040.24
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-14T22:26:38",
    "PRICE": 74.16,
    "QUANTITY": 2958,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "171",
    "TOTAL_AMOUNT": 219365.29
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-30T00:08:20",
    "PRICE": 634.83,
    "QUANTITY": 3069,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "172",
    "TOTAL_AMOUNT": 1948293.32
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-21T22:36:23",
    "PRICE": 885.87,
    "QUANTITY": 951,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "173",
    "TOTAL_AMOUNT": 842462.37
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-01T15:23:07",
    "PRICE": 950.68,
    "QUANTITY": 5206,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "174",
    "TOTAL_AMOUNT": 4949240.04
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-25T06:22:27",
    "PRICE": 343.90,
    "QUANTITY": 8698,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "175",
    "TOTAL_AMOUNT": 2991242.15
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-08T15:55:04",
    "PRICE": 382.05,
    "QUANTITY": 9829,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "176",
    "TOTAL_AMOUNT": 3755169.33
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-11T17:44:16",
    "PRICE": 788.82,
    "QUANTITY": 8066,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "177",
    "TOTAL_AMOUNT": 6362622.18
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-18T11:02:28",
    "PRICE": 920.49,
    "QUANTITY": 3527,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "178",
    "TOTAL_AMOUNT": 3246568.20
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-29T04:05:13",
    "PRICE": 520.46,
    "QUANTITY": 9825,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "179",
    "TOTAL_AMOUNT": 5113519.72
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-14T10:24:57",
    "PRICE": 295.04,
    "QUANTITY": 5523,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "180",
    "TOTAL_AMOUNT": 1629505.97
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-03T04:57:24",
    "PRICE": 512.62,
    "QUANTITY": 8816,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "181",
    "TOTAL_AMOUNT": 4519257.88
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-18T01:45:46",
    "PRICE": 934.29,
    "QUANTITY": 1075,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "182",
    "TOTAL_AMOUNT": 1004361.73
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-24T02:50:43",
    "PRICE": 618.87,
    "QUANTITY": 7540,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "183",
    "TOTAL_AMOUNT": 4666279.76
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-19T22:53:37",
    "PRICE": 691.15,
    "QUANTITY": 9092,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "184",
    "TOTAL_AMOUNT": 6283936.02
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-28T05:47:47",
    "PRICE": 692.97,
    "QUANTITY": 5097,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "185",
    "TOTAL_AMOUNT": 3532067.94
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-18T02:48:09",
    "PRICE": 683.03,
    "QUANTITY": 883,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "186",
    "TOTAL_AMOUNT": 603115.52
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-21T19:23:34",
    "PRICE": 668.20,
    "QUANTITY": 9703,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "187",
    "TOTAL_AMOUNT": 6483544.72
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-16T01:11:04",
    "PRICE": 518.00,
    "QUANTITY": 5978,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "188",
    "TOTAL_AMOUNT": 3096604.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-21T13:30:25",
    "PRICE": 138.77,
    "QUANTITY": 6454,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "189",
    "TOTAL_AMOUNT": 895621.61
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-03T22:46:19",
    "PRICE": 270.06,
    "QUANTITY": 9306,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "190",
    "TOTAL_AMOUNT": 2513178.34
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-30T11:49:42",
    "PRICE": 791.65,
    "QUANTITY": 2875,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "191",
    "TOTAL_AMOUNT": 2275993.82
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-13T11:24:59",
    "PRICE": 843.40,
    "QUANTITY": 3389,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "192",
    "TOTAL_AMOUNT": 2858282.68
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-31T18:58:26",
    "PRICE": 570.50,
    "QUANTITY": 9518,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "193",
    "TOTAL_AMOUNT": 5430019.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-08T12:24:35",
    "PRICE": 711.49,
    "QUANTITY": 9586,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "194",
    "TOTAL_AMOUNT": 6820343.05
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-29T16:06:35",
    "PRICE": 492.14,
    "QUANTITY": 7593,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "195",
    "TOTAL_AMOUNT": 3736819.13
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-09T18:16:47",
    "PRICE": 437.20,
    "QUANTITY": 2590,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "196",
    "TOTAL_AMOUNT": 1132348.03
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-18T21:06:41",
    "PRICE": 284.24,
    "QUANTITY": 6443,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "197",
    "TOTAL_AMOUNT": 1831358.26
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-05T09:48:46",
    "PRICE": 501.69,
    "QUANTITY": 1832,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "198",
    "TOTAL_AMOUNT": 919096.08
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-22T13:44:54",
    "PRICE": 462.64,
    "QUANTITY": 5443,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "199",
    "TOTAL_AMOUNT": 2518149.60
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-10T21:25:26",
    "PRICE": 345.72,
    "QUANTITY": 5000,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "200",
    "TOTAL_AMOUNT": 1728600.01
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-25T01:41:58",
    "PRICE": 191.40,
    "QUANTITY": 943,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "201",
    "TOTAL_AMOUNT": 180490.19
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-14T00:07:06",
    "PRICE": 406.27,
    "QUANTITY": 1668,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "202",
    "TOTAL_AMOUNT": 677658.34
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-07T18:49:54",
    "PRICE": 548.62,
    "QUANTITY": 8376,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "203",
    "TOTAL_AMOUNT": 4595241.08
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-10T05:32:01",
    "PRICE": 870.61,
    "QUANTITY": 3068,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "204",
    "TOTAL_AMOUNT": 2671031.44
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-11-07T22:06:14",
    "PRICE": 189.81,
    "QUANTITY": 5464,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "205",
    "TOTAL_AMOUNT": 1037121.83
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-06T02:03:12",
    "PRICE": 637.67,
    "QUANTITY": 9033,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "206",
    "TOTAL_AMOUNT": 5760072.96
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-14T01:33:14",
    "PRICE": 369.90,
    "QUANTITY": 7090,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "207",
    "TOTAL_AMOUNT": 2622590.96
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-15T14:53:10",
    "PRICE": 639.57,
    "QUANTITY": 6847,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "208",
    "TOTAL_AMOUNT": 4379135.84
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-17T12:30:07",
    "PRICE": 957.93,
    "QUANTITY": 3548,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "209",
    "TOTAL_AMOUNT": 3398735.61
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-01T19:53:02",
    "PRICE": 273.40,
    "QUANTITY": 9069,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "210",
    "TOTAL_AMOUNT": 2479464.54
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-03T14:43:14",
    "PRICE": 486.35,
    "QUANTITY": 6527,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "211",
    "TOTAL_AMOUNT": 3174406.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-24T14:45:25",
    "PRICE": 470.48,
    "QUANTITY": 4550,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "212",
    "TOTAL_AMOUNT": 2140684.05
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-07T10:12:39",
    "PRICE": 615.36,
    "QUANTITY": 5491,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "213",
    "TOTAL_AMOUNT": 3378941.68
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-25T23:17:48",
    "PRICE": 262.12,
    "QUANTITY": 4099,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "214",
    "TOTAL_AMOUNT": 1074429.86
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-27T13:26:00",
    "PRICE": 845.10,
    "QUANTITY": 7726,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "215",
    "TOTAL_AMOUNT": 6529242.41
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-21T23:30:08",
    "PRICE": 504.47,
    "QUANTITY": 9679,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "216",
    "TOTAL_AMOUNT": 4882765.14
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-24T12:13:34",
    "PRICE": 107.01,
    "QUANTITY": 2657,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "217",
    "TOTAL_AMOUNT": 284325.58
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-14T06:59:48",
    "PRICE": 363.48,
    "QUANTITY": 8071,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "218",
    "TOTAL_AMOUNT": 2933647.17
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-30T16:48:13",
    "PRICE": 948.37,
    "QUANTITY": 2713,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "219",
    "TOTAL_AMOUNT": 2572927.80
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-10T03:15:07",
    "PRICE": 494.77,
    "QUANTITY": 3138,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "220",
    "TOTAL_AMOUNT": 1552588.23
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-02T18:38:49",
    "PRICE": 302.23,
    "QUANTITY": 6575,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "221",
    "TOTAL_AMOUNT": 1987162.32
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-30T01:57:43",
    "PRICE": 884.21,
    "QUANTITY": 6073,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "222",
    "TOTAL_AMOUNT": 5369807.46
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-28T20:15:16",
    "PRICE": 500.27,
    "QUANTITY": 3779,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "223",
    "TOTAL_AMOUNT": 1890520.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-03T11:33:45",
    "PRICE": 535.02,
    "QUANTITY": 2432,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "224",
    "TOTAL_AMOUNT": 1301168.69
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-25T03:09:45",
    "PRICE": 57.80,
    "QUANTITY": 7840,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "225",
    "TOTAL_AMOUNT": 453151.99
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-10T04:47:41",
    "PRICE": 706.92,
    "QUANTITY": 6561,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "226",
    "TOTAL_AMOUNT": 4638102.01
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-17T07:23:11",
    "PRICE": 992.81,
    "QUANTITY": 4070,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "227",
    "TOTAL_AMOUNT": 4040736.69
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-12T12:02:13",
    "PRICE": 665.49,
    "QUANTITY": 8403,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "228",
    "TOTAL_AMOUNT": 5592112.39
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-29T07:49:48",
    "PRICE": 728.36,
    "QUANTITY": 2636,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "229",
    "TOTAL_AMOUNT": 1919956.92
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-10T13:40:30",
    "PRICE": 232.95,
    "QUANTITY": 7475,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "230",
    "TOTAL_AMOUNT": 1741301.23
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-10T17:27:18",
    "PRICE": 371.25,
    "QUANTITY": 8938,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "231",
    "TOTAL_AMOUNT": 3318232.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-03T15:01:38",
    "PRICE": 579.18,
    "QUANTITY": 8590,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "232",
    "TOTAL_AMOUNT": 4975156.14
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-17T09:44:45",
    "PRICE": 723.06,
    "QUANTITY": 6388,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "233",
    "TOTAL_AMOUNT": 4618907.26
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-18T18:42:08",
    "PRICE": 562.32,
    "QUANTITY": 3907,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "234",
    "TOTAL_AMOUNT": 2196984.27
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-10T15:04:03",
    "PRICE": 366.09,
    "QUANTITY": 2310,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "235",
    "TOTAL_AMOUNT": 845667.89
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-07T03:08:23",
    "PRICE": 491.13,
    "QUANTITY": 7906,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "236",
    "TOTAL_AMOUNT": 3882873.82
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-12T14:17:32",
    "PRICE": 708.46,
    "QUANTITY": 5499,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "237",
    "TOTAL_AMOUNT": 3895821.66
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-28T00:38:04",
    "PRICE": 819.05,
    "QUANTITY": 2910,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "238",
    "TOTAL_AMOUNT": 2383435.46
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-03T10:59:36",
    "PRICE": 626.33,
    "QUANTITY": 7353,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "239",
    "TOTAL_AMOUNT": 4605404.62
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-04T17:52:14",
    "PRICE": 801.44,
    "QUANTITY": 6639,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "240",
    "TOTAL_AMOUNT": 5320760.18
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-31T22:16:43",
    "PRICE": 266.75,
    "QUANTITY": 6292,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "241",
    "TOTAL_AMOUNT": 1678391.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-08T11:36:50",
    "PRICE": 510.03,
    "QUANTITY": 6155,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "242",
    "TOTAL_AMOUNT": 3139234.64
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-04T01:26:06",
    "PRICE": 132.16,
    "QUANTITY": 9700,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "243",
    "TOTAL_AMOUNT": 1281952.04
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-20T15:02:06",
    "PRICE": 237.85,
    "QUANTITY": 4005,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "244",
    "TOTAL_AMOUNT": 952589.27
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-01T16:50:15",
    "PRICE": 615.63,
    "QUANTITY": 5797,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "245",
    "TOTAL_AMOUNT": 3568807.14
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-18T09:16:44",
    "PRICE": 422.06,
    "QUANTITY": 7184,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "246",
    "TOTAL_AMOUNT": 3032079.02
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-05T04:12:48",
    "PRICE": 534.50,
    "QUANTITY": 197,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "247",
    "TOTAL_AMOUNT": 105296.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-02T02:57:44",
    "PRICE": 927.84,
    "QUANTITY": 4149,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "248",
    "TOTAL_AMOUNT": 3849608.27
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-11T06:19:03",
    "PRICE": 625.90,
    "QUANTITY": 4238,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "249",
    "TOTAL_AMOUNT": 2652564.30
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-27T12:24:10",
    "PRICE": 881.78,
    "QUANTITY": 417,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "250",
    "TOTAL_AMOUNT": 367702.27
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-02T01:01:02",
    "PRICE": 411.71,
    "QUANTITY": 3473,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "251",
    "TOTAL_AMOUNT": 1429868.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-17T18:40:55",
    "PRICE": 917.79,
    "QUANTITY": 3478,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "252",
    "TOTAL_AMOUNT": 3192073.54
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-14T04:44:51",
    "PRICE": 411.36,
    "QUANTITY": 6430,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "253",
    "TOTAL_AMOUNT": 2645044.71
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-27T23:00:53",
    "PRICE": 772.37,
    "QUANTITY": 6383,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "254",
    "TOTAL_AMOUNT": 4930037.68
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-14T04:22:04",
    "PRICE": 416.61,
    "QUANTITY": 8756,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "255",
    "TOTAL_AMOUNT": 3647837.03
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-26T08:18:14",
    "PRICE": 647.93,
    "QUANTITY": 9464,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "256",
    "TOTAL_AMOUNT": 6132009.45
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-13T12:32:16",
    "PRICE": 950.09,
    "QUANTITY": 506,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "257",
    "TOTAL_AMOUNT": 480745.55
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-21T12:49:38",
    "PRICE": 128.08,
    "QUANTITY": 2757,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "258",
    "TOTAL_AMOUNT": 353116.57
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-11-05T01:18:03",
    "PRICE": 601.28,
    "QUANTITY": 4939,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "259",
    "TOTAL_AMOUNT": 2969722.06
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-04T03:28:34",
    "PRICE": 51.60,
    "QUANTITY": 8825,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "260",
    "TOTAL_AMOUNT": 455369.99
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-13T16:09:42",
    "PRICE": 472.46,
    "QUANTITY": 3856,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "261",
    "TOTAL_AMOUNT": 1821805.73
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-23T19:58:32",
    "PRICE": 240.45,
    "QUANTITY": 2532,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "262",
    "TOTAL_AMOUNT": 608819.39
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-06T22:32:39",
    "PRICE": 103.76,
    "QUANTITY": 4207,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "263",
    "TOTAL_AMOUNT": 436518.33
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-03T08:36:56",
    "PRICE": 732.68,
    "QUANTITY": 715,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "264",
    "TOTAL_AMOUNT": 523866.19
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-25T14:46:22",
    "PRICE": 511.75,
    "QUANTITY": 3580,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "265",
    "TOTAL_AMOUNT": 1832065.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-28T08:14:41",
    "PRICE": 957.66,
    "QUANTITY": 8356,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "266",
    "TOTAL_AMOUNT": 8002206.74
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-19T13:08:45",
    "PRICE": 804.12,
    "QUANTITY": 6237,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "267",
    "TOTAL_AMOUNT": 5015296.41
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-10T07:42:28",
    "PRICE": 840.53,
    "QUANTITY": 6053,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "268",
    "TOTAL_AMOUNT": 5087728.27
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-04T08:43:05",
    "PRICE": 566.72,
    "QUANTITY": 5770,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "269",
    "TOTAL_AMOUNT": 3269974.23
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-26T10:53:15",
    "PRICE": 992.17,
    "QUANTITY": 9086,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "270",
    "TOTAL_AMOUNT": 9014856.46
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-27T00:19:42",
    "PRICE": 522.84,
    "QUANTITY": 5312,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "271",
    "TOTAL_AMOUNT": 2777326.22
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-14T05:15:07",
    "PRICE": 853.80,
    "QUANTITY": 2920,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "272",
    "TOTAL_AMOUNT": 2493095.96
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-29T04:35:53",
    "PRICE": 908.33,
    "QUANTITY": 1648,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "273",
    "TOTAL_AMOUNT": 1496927.87
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-10T10:50:53",
    "PRICE": 723.40,
    "QUANTITY": 5480,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "274",
    "TOTAL_AMOUNT": 3964232.13
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-11-04T06:13:09",
    "PRICE": 71.15,
    "QUANTITY": 6729,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "275",
    "TOTAL_AMOUNT": 478768.36
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-29T22:47:51",
    "PRICE": 527.79,
    "QUANTITY": 468,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "276",
    "TOTAL_AMOUNT": 247005.71
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-03T00:59:14",
    "PRICE": 323.14,
    "QUANTITY": 6990,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "277",
    "TOTAL_AMOUNT": 2258748.70
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-08T16:12:51",
    "PRICE": 237.98,
    "QUANTITY": 161,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "278",
    "TOTAL_AMOUNT": 38314.78
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-06T00:20:13",
    "PRICE": 700.10,
    "QUANTITY": 9624,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "279",
    "TOTAL_AMOUNT": 6737762.17
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-20T04:54:58",
    "PRICE": 753.95,
    "QUANTITY": 8676,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "280",
    "TOTAL_AMOUNT": 6541270.31
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-31T20:26:28",
    "PRICE": 327.52,
    "QUANTITY": 3578,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "281",
    "TOTAL_AMOUNT": 1171866.52
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-20T20:28:02",
    "PRICE": 866.76,
    "QUANTITY": 5825,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "282",
    "TOTAL_AMOUNT": 5048877.06
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-17T22:04:35",
    "PRICE": 836.22,
    "QUANTITY": 5281,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "283",
    "TOTAL_AMOUNT": 4416077.67
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-20T23:38:09",
    "PRICE": 168.78,
    "QUANTITY": 9503,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "284",
    "TOTAL_AMOUNT": 1603916.33
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-11T07:30:16",
    "PRICE": 605.22,
    "QUANTITY": 9251,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "285",
    "TOTAL_AMOUNT": 5598889.95
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-25T21:48:32",
    "PRICE": 219.60,
    "QUANTITY": 7960,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "286",
    "TOTAL_AMOUNT": 1748016.05
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-17T03:38:46",
    "PRICE": 289.42,
    "QUANTITY": 579,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "287",
    "TOTAL_AMOUNT": 167574.19
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-26T17:40:10",
    "PRICE": 261.60,
    "QUANTITY": 2663,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "288",
    "TOTAL_AMOUNT": 696640.82
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-23T03:23:35",
    "PRICE": 141.07,
    "QUANTITY": 5472,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "289",
    "TOTAL_AMOUNT": 771935.08
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-05T23:19:53",
    "PRICE": 459.60,
    "QUANTITY": 9295,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "290",
    "TOTAL_AMOUNT": 4271982.06
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-02T04:29:53",
    "PRICE": 570.31,
    "QUANTITY": 2247,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "291",
    "TOTAL_AMOUNT": 1281486.56
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-20T12:27:56",
    "PRICE": 954.88,
    "QUANTITY": 345,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "292",
    "TOTAL_AMOUNT": 329433.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-01T15:03:02",
    "PRICE": 255.86,
    "QUANTITY": 8212,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "293",
    "TOTAL_AMOUNT": 2101122.33
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-19T09:10:53",
    "PRICE": 574.41,
    "QUANTITY": 9134,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "294",
    "TOTAL_AMOUNT": 5246660.69
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-25T03:36:07",
    "PRICE": 189.66,
    "QUANTITY": 2488,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "295",
    "TOTAL_AMOUNT": 471874.09
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-12T10:44:10",
    "PRICE": 90.70,
    "QUANTITY": 5572,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "296",
    "TOTAL_AMOUNT": 505380.38
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-12T16:53:08",
    "PRICE": 137.53,
    "QUANTITY": 7789,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "297",
    "TOTAL_AMOUNT": 1071221.16
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-24T20:01:10",
    "PRICE": 605.71,
    "QUANTITY": 6304,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "298",
    "TOTAL_AMOUNT": 3818395.98
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-26T00:54:23",
    "PRICE": 84.84,
    "QUANTITY": 396,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "299",
    "TOTAL_AMOUNT": 33596.64
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-14T16:05:19",
    "PRICE": 306.55,
    "QUANTITY": 2507,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "300",
    "TOTAL_AMOUNT": 768520.82
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-29T19:29:43",
    "PRICE": 347.32,
    "QUANTITY": 3990,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "301",
    "TOTAL_AMOUNT": 1385806.83
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-26T18:03:54",
    "PRICE": 488.15,
    "QUANTITY": 3986,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "302",
    "TOTAL_AMOUNT": 1945765.88
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-22T20:18:58",
    "PRICE": 382.09,
    "QUANTITY": 7280,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "303",
    "TOTAL_AMOUNT": 2781615.17
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-31T05:15:58",
    "PRICE": 658.73,
    "QUANTITY": 5711,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "304",
    "TOTAL_AMOUNT": 3762006.92
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-12T05:20:21",
    "PRICE": 860.79,
    "QUANTITY": 7938,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "305",
    "TOTAL_AMOUNT": 6832950.85
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-22T21:37:19",
    "PRICE": 478.19,
    "QUANTITY": 892,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "306",
    "TOTAL_AMOUNT": 426545.48
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-15T07:30:09",
    "PRICE": 791.14,
    "QUANTITY": 9289,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "307",
    "TOTAL_AMOUNT": 7348899.60
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-19T05:03:41",
    "PRICE": 135.05,
    "QUANTITY": 4679,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "308",
    "TOTAL_AMOUNT": 631898.96
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-19T20:32:49",
    "PRICE": 194.40,
    "QUANTITY": 2317,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "309",
    "TOTAL_AMOUNT": 450424.79
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-05T02:18:35",
    "PRICE": 336.13,
    "QUANTITY": 1313,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "310",
    "TOTAL_AMOUNT": 441338.70
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-30T08:32:12",
    "PRICE": 525.97,
    "QUANTITY": 8513,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "311",
    "TOTAL_AMOUNT": 4477582.36
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-03T22:34:57",
    "PRICE": 387.84,
    "QUANTITY": 5014,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "312",
    "TOTAL_AMOUNT": 1944629.74
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-30T06:54:44",
    "PRICE": 414.54,
    "QUANTITY": 7250,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "313",
    "TOTAL_AMOUNT": 3005415.06
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-29T22:53:28",
    "PRICE": 325.62,
    "QUANTITY": 9998,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "314",
    "TOTAL_AMOUNT": 3255548.71
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-04T05:24:56",
    "PRICE": 176.13,
    "QUANTITY": 9805,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "315",
    "TOTAL_AMOUNT": 1726954.70
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-04T01:13:10",
    "PRICE": 258.29,
    "QUANTITY": 8392,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "316",
    "TOTAL_AMOUNT": 2167569.75
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-17T16:00:27",
    "PRICE": 859.85,
    "QUANTITY": 5602,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "317",
    "TOTAL_AMOUNT": 4816879.56
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-17T01:45:12",
    "PRICE": 521.95,
    "QUANTITY": 3409,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "318",
    "TOTAL_AMOUNT": 1779327.59
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-02T10:49:28",
    "PRICE": 631.21,
    "QUANTITY": 4469,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "319",
    "TOTAL_AMOUNT": 2820877.59
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-11T19:59:24",
    "PRICE": 654.84,
    "QUANTITY": 808,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "320",
    "TOTAL_AMOUNT": 529110.74
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-26T14:14:04",
    "PRICE": 302.80,
    "QUANTITY": 856,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "321",
    "TOTAL_AMOUNT": 259196.79
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-15T23:05:13",
    "PRICE": 773.89,
    "QUANTITY": 3886,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "322",
    "TOTAL_AMOUNT": 3007336.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-14T08:12:28",
    "PRICE": 173.88,
    "QUANTITY": 1613,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "323",
    "TOTAL_AMOUNT": 280468.45
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-01T10:07:17",
    "PRICE": 580.76,
    "QUANTITY": 9159,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "324",
    "TOTAL_AMOUNT": 5319180.93
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-06T12:05:18",
    "PRICE": 89.53,
    "QUANTITY": 6125,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "325",
    "TOTAL_AMOUNT": 548371.24
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-19T19:59:41",
    "PRICE": 256.33,
    "QUANTITY": 8814,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "326",
    "TOTAL_AMOUNT": 2259292.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-16T16:40:40",
    "PRICE": 236.76,
    "QUANTITY": 1564,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "327",
    "TOTAL_AMOUNT": 370292.63
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-05T23:27:08",
    "PRICE": 361.94,
    "QUANTITY": 2881,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "328",
    "TOTAL_AMOUNT": 1042749.15
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-25T12:03:42",
    "PRICE": 769.30,
    "QUANTITY": 6039,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "329",
    "TOTAL_AMOUNT": 4645802.63
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-07T12:22:35",
    "PRICE": 68.36,
    "QUANTITY": 1062,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "330",
    "TOTAL_AMOUNT": 72598.32
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-28T06:36:29",
    "PRICE": 869.56,
    "QUANTITY": 2106,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "331",
    "TOTAL_AMOUNT": 1831293.35
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-23T06:18:25",
    "PRICE": 727.97,
    "QUANTITY": 2780,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "332",
    "TOTAL_AMOUNT": 2023756.52
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-23T09:07:14",
    "PRICE": 269.46,
    "QUANTITY": 268,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "333",
    "TOTAL_AMOUNT": 72215.28
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-12T19:40:20",
    "PRICE": 507.36,
    "QUANTITY": 8375,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "334",
    "TOTAL_AMOUNT": 4249139.88
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-22T20:48:29",
    "PRICE": 867.11,
    "QUANTITY": 5989,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "335",
    "TOTAL_AMOUNT": 5193121.70
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-14T20:23:57",
    "PRICE": 553.24,
    "QUANTITY": 3859,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "336",
    "TOTAL_AMOUNT": 2134953.12
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-10T09:30:22",
    "PRICE": 52.50,
    "QUANTITY": 5922,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "337",
    "TOTAL_AMOUNT": 310905.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-17T21:57:50",
    "PRICE": 391.69,
    "QUANTITY": 2669,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "338",
    "TOTAL_AMOUNT": 1045420.62
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-11-09T21:16:12",
    "PRICE": 790.75,
    "QUANTITY": 6915,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "339",
    "TOTAL_AMOUNT": 5468036.25
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-18T16:46:09",
    "PRICE": 556.48,
    "QUANTITY": 7391,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "340",
    "TOTAL_AMOUNT": 4112943.54
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-07T19:45:47",
    "PRICE": 251.70,
    "QUANTITY": 9710,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "341",
    "TOTAL_AMOUNT": 2444006.97
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-13T01:30:50",
    "PRICE": 786.24,
    "QUANTITY": 4445,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "342",
    "TOTAL_AMOUNT": 3494836.76
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-27T08:54:39",
    "PRICE": 173.44,
    "QUANTITY": 2921,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "343",
    "TOTAL_AMOUNT": 506618.25
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-09T10:25:20",
    "PRICE": 888.41,
    "QUANTITY": 6547,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "344",
    "TOTAL_AMOUNT": 5816420.09
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-08T16:03:21",
    "PRICE": 611.09,
    "QUANTITY": 9861,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "345",
    "TOTAL_AMOUNT": 6025958.75
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-05T12:13:09",
    "PRICE": 123.70,
    "QUANTITY": 3164,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "346",
    "TOTAL_AMOUNT": 391386.79
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-06T02:00:06",
    "PRICE": 607.86,
    "QUANTITY": 8630,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "347",
    "TOTAL_AMOUNT": 5245831.67
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-17T20:30:14",
    "PRICE": 876.51,
    "QUANTITY": 3886,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "348",
    "TOTAL_AMOUNT": 3406117.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-20T12:52:06",
    "PRICE": 814.20,
    "QUANTITY": 3122,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "349",
    "TOTAL_AMOUNT": 2541932.44
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-15T04:52:53",
    "PRICE": 220.06,
    "QUANTITY": 7820,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "350",
    "TOTAL_AMOUNT": 1720869.18
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-05T19:38:34",
    "PRICE": 699.95,
    "QUANTITY": 936,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "351",
    "TOTAL_AMOUNT": 655153.21
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-09T19:30:22",
    "PRICE": 938.54,
    "QUANTITY": 6752,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "352",
    "TOTAL_AMOUNT": 6337021.93
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-09T07:18:37",
    "PRICE": 388.82,
    "QUANTITY": 3374,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "353",
    "TOTAL_AMOUNT": 1311878.70
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-16T21:05:28",
    "PRICE": 528.31,
    "QUANTITY": 1041,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "354",
    "TOTAL_AMOUNT": 549970.71
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-10T15:46:58",
    "PRICE": 648.57,
    "QUANTITY": 4203,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "355",
    "TOTAL_AMOUNT": 2725939.74
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-30T04:44:08",
    "PRICE": 56.32,
    "QUANTITY": 8717,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "356",
    "TOTAL_AMOUNT": 490941.44
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-09T00:57:42",
    "PRICE": 845.56,
    "QUANTITY": 9264,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "357",
    "TOTAL_AMOUNT": 7833267.82
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-15T06:40:42",
    "PRICE": 169.14,
    "QUANTITY": 8488,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "358",
    "TOTAL_AMOUNT": 1435660.31
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-17T17:07:17",
    "PRICE": 629.79,
    "QUANTITY": 2529,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "359",
    "TOTAL_AMOUNT": 1592738.85
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-12T08:56:48",
    "PRICE": 816.42,
    "QUANTITY": 9921,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "360",
    "TOTAL_AMOUNT": 8099702.65
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-29T11:12:57",
    "PRICE": 674.28,
    "QUANTITY": 9598,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "361",
    "TOTAL_AMOUNT": 6471739.72
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-13T19:44:19",
    "PRICE": 74.37,
    "QUANTITY": 133,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "362",
    "TOTAL_AMOUNT": 9891.21
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-28T07:19:22",
    "PRICE": 629.58,
    "QUANTITY": 6674,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "363",
    "TOTAL_AMOUNT": 4201817.03
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-11T07:12:26",
    "PRICE": 905.84,
    "QUANTITY": 2125,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "364",
    "TOTAL_AMOUNT": 1924910.06
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-02T22:15:36",
    "PRICE": 675.66,
    "QUANTITY": 5748,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "365",
    "TOTAL_AMOUNT": 3883693.53
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-07T11:07:48",
    "PRICE": 654.93,
    "QUANTITY": 9520,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "366",
    "TOTAL_AMOUNT": 6234933.53
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-23T14:42:36",
    "PRICE": 677.37,
    "QUANTITY": 2567,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "367",
    "TOTAL_AMOUNT": 1738808.78
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-29T19:22:35",
    "PRICE": 657.95,
    "QUANTITY": 6575,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "368",
    "TOTAL_AMOUNT": 4326021.33
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-17T16:14:35",
    "PRICE": 787.35,
    "QUANTITY": 4191,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "369",
    "TOTAL_AMOUNT": 3299783.75
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-17T10:53:34",
    "PRICE": 144.26,
    "QUANTITY": 4448,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "370",
    "TOTAL_AMOUNT": 641668.46
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-10T04:09:26",
    "PRICE": 923.40,
    "QUANTITY": 871,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "371",
    "TOTAL_AMOUNT": 804281.42
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-31T16:01:19",
    "PRICE": 177.48,
    "QUANTITY": 7028,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "372",
    "TOTAL_AMOUNT": 1247329.41
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-17T01:10:23",
    "PRICE": 242.89,
    "QUANTITY": 4414,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "373",
    "TOTAL_AMOUNT": 1072116.46
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-06T23:40:48",
    "PRICE": 214.93,
    "QUANTITY": 4446,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "374",
    "TOTAL_AMOUNT": 955578.75
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-17T01:47:03",
    "PRICE": 159.03,
    "QUANTITY": 2288,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "375",
    "TOTAL_AMOUNT": 363860.64
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-26T04:50:45",
    "PRICE": 694.01,
    "QUANTITY": 451,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "376",
    "TOTAL_AMOUNT": 312998.51
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-31T11:59:40",
    "PRICE": 934.98,
    "QUANTITY": 9199,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "377",
    "TOTAL_AMOUNT": 8600880.84
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-11-05T21:44:33",
    "PRICE": 981.99,
    "QUANTITY": 9108,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "378",
    "TOTAL_AMOUNT": 8943964.83
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-22T01:20:48",
    "PRICE": 645.36,
    "QUANTITY": 2988,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "379",
    "TOTAL_AMOUNT": 1928335.64
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-08T04:15:57",
    "PRICE": 998.03,
    "QUANTITY": 280,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "380",
    "TOTAL_AMOUNT": 279448.41
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-06T07:41:49",
    "PRICE": 825.50,
    "QUANTITY": 1762,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "381",
    "TOTAL_AMOUNT": 1454531.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-13T15:45:45",
    "PRICE": 458.66,
    "QUANTITY": 789,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "382",
    "TOTAL_AMOUNT": 361882.74
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-14T18:48:37",
    "PRICE": 564.93,
    "QUANTITY": 3695,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "383",
    "TOTAL_AMOUNT": 2087416.32
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-29T09:55:17",
    "PRICE": 169.40,
    "QUANTITY": 1139,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "384",
    "TOTAL_AMOUNT": 192946.59
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-11T15:31:46",
    "PRICE": 486.21,
    "QUANTITY": 7443,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "385",
    "TOTAL_AMOUNT": 3618860.97
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-03T06:19:46",
    "PRICE": 755.73,
    "QUANTITY": 8018,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "386",
    "TOTAL_AMOUNT": 6059442.98
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-03T21:20:07",
    "PRICE": 591.28,
    "QUANTITY": 5353,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "387",
    "TOTAL_AMOUNT": 3165122.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-25T01:50:02",
    "PRICE": 272.26,
    "QUANTITY": 5440,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "388",
    "TOTAL_AMOUNT": 1481094.45
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-30T23:49:47",
    "PRICE": 753.95,
    "QUANTITY": 6374,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "389",
    "TOTAL_AMOUNT": 4805677.38
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-12T21:55:25",
    "PRICE": 155.29,
    "QUANTITY": 7941,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "390",
    "TOTAL_AMOUNT": 1233157.84
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-11T11:42:35",
    "PRICE": 548.36,
    "QUANTITY": 7773,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "391",
    "TOTAL_AMOUNT": 4262402.17
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-30T06:33:01",
    "PRICE": 591.35,
    "QUANTITY": 1676,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "392",
    "TOTAL_AMOUNT": 991102.56
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-11-04T10:27:57",
    "PRICE": 290.78,
    "QUANTITY": 494,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "393",
    "TOTAL_AMOUNT": 143645.32
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-18T16:04:36",
    "PRICE": 533.42,
    "QUANTITY": 2257,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "394",
    "TOTAL_AMOUNT": 1203928.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-19T22:36:40",
    "PRICE": 986.10,
    "QUANTITY": 9838,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "395",
    "TOTAL_AMOUNT": 9701251.56
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-09T12:11:24",
    "PRICE": 467.65,
    "QUANTITY": 5488,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "396",
    "TOTAL_AMOUNT": 2566463.17
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-09T00:17:17",
    "PRICE": 900.19,
    "QUANTITY": 9196,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "397",
    "TOTAL_AMOUNT": 8278147.26
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-16T02:21:55",
    "PRICE": 689.66,
    "QUANTITY": 9140,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "398",
    "TOTAL_AMOUNT": 6303492.15
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-01T14:17:07",
    "PRICE": 974.07,
    "QUANTITY": 3053,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "399",
    "TOTAL_AMOUNT": 2973835.73
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-05T22:42:02",
    "PRICE": 465.05,
    "QUANTITY": 1736,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "400",
    "TOTAL_AMOUNT": 807326.78
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-27T17:14:23",
    "PRICE": 121.80,
    "QUANTITY": 6528,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "401",
    "TOTAL_AMOUNT": 795110.42
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-18T20:54:10",
    "PRICE": 153.60,
    "QUANTITY": 2842,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "402",
    "TOTAL_AMOUNT": 436531.22
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-08T10:17:06",
    "PRICE": 115.25,
    "QUANTITY": 7853,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "403",
    "TOTAL_AMOUNT": 905058.25
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-25T13:37:16",
    "PRICE": 453.79,
    "QUANTITY": 7082,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "404",
    "TOTAL_AMOUNT": 3213740.84
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-06T18:07:55",
    "PRICE": 442.86,
    "QUANTITY": 2817,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "405",
    "TOTAL_AMOUNT": 1247536.58
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-17T23:13:44",
    "PRICE": 697.22,
    "QUANTITY": 850,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "406",
    "TOTAL_AMOUNT": 592636.98
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-17T00:39:20",
    "PRICE": 59.25,
    "QUANTITY": 2511,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "407",
    "TOTAL_AMOUNT": 148776.75
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-18T00:01:16",
    "PRICE": 766.32,
    "QUANTITY": 6746,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "408",
    "TOTAL_AMOUNT": 5169594.77
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-15T02:26:28",
    "PRICE": 987.86,
    "QUANTITY": 3985,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "409",
    "TOTAL_AMOUNT": 3936622.04
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-29T06:29:43",
    "PRICE": 208.61,
    "QUANTITY": 6202,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "410",
    "TOTAL_AMOUNT": 1293799.22
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-30T12:41:36",
    "PRICE": 277.48,
    "QUANTITY": 2069,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "411",
    "TOTAL_AMOUNT": 574106.14
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-19T11:28:48",
    "PRICE": 255.71,
    "QUANTITY": 2732,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "412",
    "TOTAL_AMOUNT": 698599.74
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-28T18:22:41",
    "PRICE": 465.24,
    "QUANTITY": 7691,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "413",
    "TOTAL_AMOUNT": 3578160.76
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-20T17:34:19",
    "PRICE": 852.86,
    "QUANTITY": 7652,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "414",
    "TOTAL_AMOUNT": 6526084.61
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-08T08:03:37",
    "PRICE": 752.47,
    "QUANTITY": 1965,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "415",
    "TOTAL_AMOUNT": 1478603.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-09T16:54:21",
    "PRICE": 250.60,
    "QUANTITY": 4805,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "416",
    "TOTAL_AMOUNT": 1204133.03
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-27T19:36:13",
    "PRICE": 210.15,
    "QUANTITY": 9648,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "417",
    "TOTAL_AMOUNT": 2027527.14
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-13T15:07:05",
    "PRICE": 620.72,
    "QUANTITY": 9406,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "418",
    "TOTAL_AMOUNT": 5838492.04
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-14T13:11:58",
    "PRICE": 179.44,
    "QUANTITY": 5712,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "419",
    "TOTAL_AMOUNT": 1024961.29
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-12T15:43:05",
    "PRICE": 382.80,
    "QUANTITY": 7657,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "420",
    "TOTAL_AMOUNT": 2931099.51
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-25T13:56:40",
    "PRICE": 852.99,
    "QUANTITY": 6721,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "421",
    "TOTAL_AMOUNT": 5732945.72
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-07T09:19:52",
    "PRICE": 245.45,
    "QUANTITY": 4156,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "422",
    "TOTAL_AMOUNT": 1020090.19
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-31T07:09:43",
    "PRICE": 908.09,
    "QUANTITY": 5247,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "423",
    "TOTAL_AMOUNT": 4764748.37
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-04T05:31:43",
    "PRICE": 399.00,
    "QUANTITY": 3217,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "424",
    "TOTAL_AMOUNT": 1283583.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-16T14:10:47",
    "PRICE": 904.91,
    "QUANTITY": 1148,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "425",
    "TOTAL_AMOUNT": 1038836.65
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-25T13:19:48",
    "PRICE": 497.34,
    "QUANTITY": 2252,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "426",
    "TOTAL_AMOUNT": 1120009.67
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-10T23:54:55",
    "PRICE": 931.40,
    "QUANTITY": 1618,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "427",
    "TOTAL_AMOUNT": 1507005.24
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-10T20:56:01",
    "PRICE": 573.83,
    "QUANTITY": 5476,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "428",
    "TOTAL_AMOUNT": 3142293.17
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-23T01:30:13",
    "PRICE": 516.35,
    "QUANTITY": 8999,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "429",
    "TOTAL_AMOUNT": 4646633.43
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-04T14:46:31",
    "PRICE": 986.79,
    "QUANTITY": 7409,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "430",
    "TOTAL_AMOUNT": 7311126.95
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-03T19:51:23",
    "PRICE": 226.63,
    "QUANTITY": 2894,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "431",
    "TOTAL_AMOUNT": 655867.23
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-28T16:17:40",
    "PRICE": 659.99,
    "QUANTITY": 5428,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "432",
    "TOTAL_AMOUNT": 3582425.67
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-04T04:41:11",
    "PRICE": 65.22,
    "QUANTITY": 8449,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "433",
    "TOTAL_AMOUNT": 551043.79
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-26T00:16:31",
    "PRICE": 389.99,
    "QUANTITY": 6269,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "434",
    "TOTAL_AMOUNT": 2444847.25
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-13T21:33:33",
    "PRICE": 731.52,
    "QUANTITY": 6339,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "435",
    "TOTAL_AMOUNT": 4637105.40
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-25T17:12:16",
    "PRICE": 280.93,
    "QUANTITY": 746,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "436",
    "TOTAL_AMOUNT": 209573.77
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-14T09:18:55",
    "PRICE": 932.77,
    "QUANTITY": 6259,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "437",
    "TOTAL_AMOUNT": 5838207.55
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-18T23:39:32",
    "PRICE": 733.60,
    "QUANTITY": 2143,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "438",
    "TOTAL_AMOUNT": 1572104.75
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-10T14:38:09",
    "PRICE": 344.15,
    "QUANTITY": 6826,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "439",
    "TOTAL_AMOUNT": 2349167.86
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-11T04:34:56",
    "PRICE": 717.41,
    "QUANTITY": 5420,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "440",
    "TOTAL_AMOUNT": 3888362.05
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-15T17:29:54",
    "PRICE": 259.18,
    "QUANTITY": 2333,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "441",
    "TOTAL_AMOUNT": 604666.92
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-22T09:08:08",
    "PRICE": 671.09,
    "QUANTITY": 8819,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "442",
    "TOTAL_AMOUNT": 5918342.95
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-13T06:50:45",
    "PRICE": 565.82,
    "QUANTITY": 7706,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "443",
    "TOTAL_AMOUNT": 4360208.98
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-12T18:41:51",
    "PRICE": 263.08,
    "QUANTITY": 2659,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "444",
    "TOTAL_AMOUNT": 699529.68
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-28T07:11:36",
    "PRICE": 795.92,
    "QUANTITY": 6969,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "445",
    "TOTAL_AMOUNT": 5546766.36
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-12T11:18:27",
    "PRICE": 282.04,
    "QUANTITY": 8922,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "446",
    "TOTAL_AMOUNT": 2516360.96
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-11T03:42:47",
    "PRICE": 855.35,
    "QUANTITY": 8612,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "447",
    "TOTAL_AMOUNT": 7366273.99
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-13T09:57:47",
    "PRICE": 371.82,
    "QUANTITY": 2040,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "448",
    "TOTAL_AMOUNT": 758512.81
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-20T20:31:28",
    "PRICE": 756.93,
    "QUANTITY": 638,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "449",
    "TOTAL_AMOUNT": 482921.34
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-23T07:33:22",
    "PRICE": 556.08,
    "QUANTITY": 5483,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "450",
    "TOTAL_AMOUNT": 3048986.73
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-18T13:05:40",
    "PRICE": 640.91,
    "QUANTITY": 2257,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "451",
    "TOTAL_AMOUNT": 1446533.81
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-15T00:05:06",
    "PRICE": 57.03,
    "QUANTITY": 6465,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "452",
    "TOTAL_AMOUNT": 368698.94
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-26T11:56:43",
    "PRICE": 517.02,
    "QUANTITY": 6052,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "453",
    "TOTAL_AMOUNT": 3129005.16
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-18T22:08:18",
    "PRICE": 679.67,
    "QUANTITY": 2841,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "454",
    "TOTAL_AMOUNT": 1930942.42
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-11T04:08:17",
    "PRICE": 97.22,
    "QUANTITY": 7124,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "455",
    "TOTAL_AMOUNT": 692595.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-12T02:02:29",
    "PRICE": 998.91,
    "QUANTITY": 3226,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "456",
    "TOTAL_AMOUNT": 3222483.57
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-22T16:01:53",
    "PRICE": 961.05,
    "QUANTITY": 6827,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "457",
    "TOTAL_AMOUNT": 6561088.27
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-17T11:11:00",
    "PRICE": 569.21,
    "QUANTITY": 5899,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "458",
    "TOTAL_AMOUNT": 3357769.92
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-10T13:39:15",
    "PRICE": 817.57,
    "QUANTITY": 4872,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "459",
    "TOTAL_AMOUNT": 3983201.08
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-06T18:58:46",
    "PRICE": 709.73,
    "QUANTITY": 8504,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "460",
    "TOTAL_AMOUNT": 6035543.75
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-11T19:06:06",
    "PRICE": 988.07,
    "QUANTITY": 5719,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "461",
    "TOTAL_AMOUNT": 5650772.37
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-30T05:44:06",
    "PRICE": 530.54,
    "QUANTITY": 9818,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "462",
    "TOTAL_AMOUNT": 5208841.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-06T19:04:15",
    "PRICE": 83.10,
    "QUANTITY": 4277,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "463",
    "TOTAL_AMOUNT": 355418.69
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T06:53:53",
    "PRICE": 455.95,
    "QUANTITY": 2728,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "464",
    "TOTAL_AMOUNT": 1243831.63
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-13T22:23:48",
    "PRICE": 237.20,
    "QUANTITY": 7442,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "465",
    "TOTAL_AMOUNT": 1765242.38
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-22T08:00:56",
    "PRICE": 726.75,
    "QUANTITY": 3836,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "466",
    "TOTAL_AMOUNT": 2787813.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-10T20:05:11",
    "PRICE": 942.93,
    "QUANTITY": 567,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "467",
    "TOTAL_AMOUNT": 534641.31
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-25T20:42:49",
    "PRICE": 190.76,
    "QUANTITY": 8612,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "468",
    "TOTAL_AMOUNT": 1642825.07
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-01T16:01:58",
    "PRICE": 623.07,
    "QUANTITY": 6742,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "469",
    "TOTAL_AMOUNT": 4200737.99
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-07T11:48:45",
    "PRICE": 247.65,
    "QUANTITY": 1346,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "470",
    "TOTAL_AMOUNT": 333336.89
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-19T05:39:44",
    "PRICE": 188.76,
    "QUANTITY": 5802,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "471",
    "TOTAL_AMOUNT": 1095185.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-10T04:54:27",
    "PRICE": 888.83,
    "QUANTITY": 2035,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "472",
    "TOTAL_AMOUNT": 1808769.08
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-25T12:31:54",
    "PRICE": 617.48,
    "QUANTITY": 6006,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "473",
    "TOTAL_AMOUNT": 3708584.76
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-07T09:03:39",
    "PRICE": 530.59,
    "QUANTITY": 8461,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "474",
    "TOTAL_AMOUNT": 4489322.22
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-08T14:18:44",
    "PRICE": 876.54,
    "QUANTITY": 5427,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "475",
    "TOTAL_AMOUNT": 4756982.46
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-07T16:30:37",
    "PRICE": 902.07,
    "QUANTITY": 2866,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "476",
    "TOTAL_AMOUNT": 2585332.64
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-01T01:50:43",
    "PRICE": 270.17,
    "QUANTITY": 4581,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "477",
    "TOTAL_AMOUNT": 1237648.83
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-01T18:17:05",
    "PRICE": 672.13,
    "QUANTITY": 2240,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "478",
    "TOTAL_AMOUNT": 1505571.21
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-19T06:29:36",
    "PRICE": 498.87,
    "QUANTITY": 8556,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "479",
    "TOTAL_AMOUNT": 4268331.68
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-19T11:52:21",
    "PRICE": 583.03,
    "QUANTITY": 519,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "480",
    "TOTAL_AMOUNT": 302592.59
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-15T21:08:48",
    "PRICE": 567.18,
    "QUANTITY": 9302,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "481",
    "TOTAL_AMOUNT": 5275908.29
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-30T07:55:42",
    "PRICE": 429.02,
    "QUANTITY": 3312,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "482",
    "TOTAL_AMOUNT": 1420914.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-25T03:01:12",
    "PRICE": 789.29,
    "QUANTITY": 7518,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "483",
    "TOTAL_AMOUNT": 5933882.05
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-10T10:13:48",
    "PRICE": 519.57,
    "QUANTITY": 1925,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "484",
    "TOTAL_AMOUNT": 1000172.26
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-20T11:41:50",
    "PRICE": 81.86,
    "QUANTITY": 3249,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "485",
    "TOTAL_AMOUNT": 265963.14
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-31T17:07:13",
    "PRICE": 441.35,
    "QUANTITY": 6964,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "486",
    "TOTAL_AMOUNT": 3073561.44
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-07T07:52:54",
    "PRICE": 597.23,
    "QUANTITY": 3155,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "487",
    "TOTAL_AMOUNT": 1884260.59
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-23T19:52:11",
    "PRICE": 608.94,
    "QUANTITY": 772,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "488",
    "TOTAL_AMOUNT": 470101.68
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-13T02:02:05",
    "PRICE": 586.48,
    "QUANTITY": 9121,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "489",
    "TOTAL_AMOUNT": 5349283.90
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-07T07:55:24",
    "PRICE": 647.13,
    "QUANTITY": 3510,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "490",
    "TOTAL_AMOUNT": 2271426.32
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-26T13:48:40",
    "PRICE": 442.16,
    "QUANTITY": 359,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "491",
    "TOTAL_AMOUNT": 158735.44
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-01T01:22:07",
    "PRICE": 857.92,
    "QUANTITY": 1612,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "492",
    "TOTAL_AMOUNT": 1382967.01
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-13T12:00:31",
    "PRICE": 434.11,
    "QUANTITY": 897,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "493",
    "TOTAL_AMOUNT": 389396.66
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-28T16:17:02",
    "PRICE": 969.27,
    "QUANTITY": 7307,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "494",
    "TOTAL_AMOUNT": 7082456.03
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-05T21:31:16",
    "PRICE": 99.23,
    "QUANTITY": 8202,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "495",
    "TOTAL_AMOUNT": 813884.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-17T08:49:03",
    "PRICE": 705.07,
    "QUANTITY": 6158,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "496",
    "TOTAL_AMOUNT": 4341821.11
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-25T16:53:01",
    "PRICE": 212.15,
    "QUANTITY": 2688,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "497",
    "TOTAL_AMOUNT": 570259.18
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-19T18:09:09",
    "PRICE": 534.78,
    "QUANTITY": 6686,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "498",
    "TOTAL_AMOUNT": 3575539.28
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-28T22:37:28",
    "PRICE": 828.83,
    "QUANTITY": 4182,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "499",
    "TOTAL_AMOUNT": 3466167.13
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-15T18:59:25",
    "PRICE": 752.66,
    "QUANTITY": 5204,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "500",
    "TOTAL_AMOUNT": 3916842.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-29T06:56:34",
    "PRICE": 980.01,
    "QUANTITY": 4464,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "501",
    "TOTAL_AMOUNT": 4374764.68
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-02T10:44:34",
    "PRICE": 747.39,
    "QUANTITY": 6798,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "502",
    "TOTAL_AMOUNT": 5080757.32
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-29T07:28:34",
    "PRICE": 971.44,
    "QUANTITY": 3413,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "503",
    "TOTAL_AMOUNT": 3315524.73
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-20T19:12:20",
    "PRICE": 364.44,
    "QUANTITY": 9116,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "504",
    "TOTAL_AMOUNT": 3322235.06
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-06T23:09:29",
    "PRICE": 505.12,
    "QUANTITY": 7545,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "505",
    "TOTAL_AMOUNT": 3811130.36
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-22T17:44:33",
    "PRICE": 712.34,
    "QUANTITY": 1075,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "506",
    "TOTAL_AMOUNT": 765765.53
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-07T12:13:20",
    "PRICE": 440.44,
    "QUANTITY": 7866,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "507",
    "TOTAL_AMOUNT": 3464501.06
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-03T05:28:16",
    "PRICE": 494.71,
    "QUANTITY": 9471,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "508",
    "TOTAL_AMOUNT": 4685398.33
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-09T16:16:11",
    "PRICE": 673.50,
    "QUANTITY": 9886,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "509",
    "TOTAL_AMOUNT": 6658221.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-19T00:14:32",
    "PRICE": 83.12,
    "QUANTITY": 6705,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "510",
    "TOTAL_AMOUNT": 557319.62
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-19T04:01:36",
    "PRICE": 764.27,
    "QUANTITY": 1147,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "511",
    "TOTAL_AMOUNT": 876617.71
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-02T18:24:37",
    "PRICE": 296.26,
    "QUANTITY": 5540,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "512",
    "TOTAL_AMOUNT": 1641280.45
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-11T18:46:34",
    "PRICE": 209.05,
    "QUANTITY": 4022,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "513",
    "TOTAL_AMOUNT": 840799.11
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-23T23:43:31",
    "PRICE": 955.76,
    "QUANTITY": 1771,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "514",
    "TOTAL_AMOUNT": 1692650.98
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-11T15:55:36",
    "PRICE": 174.15,
    "QUANTITY": 2495,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "515",
    "TOTAL_AMOUNT": 434504.23
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-03T18:24:14",
    "PRICE": 66.41,
    "QUANTITY": 6391,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "516",
    "TOTAL_AMOUNT": 424426.33
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-11T10:17:34",
    "PRICE": 513.21,
    "QUANTITY": 9414,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "517",
    "TOTAL_AMOUNT": 4831359.15
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-20T10:54:09",
    "PRICE": 56.84,
    "QUANTITY": 3890,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "518",
    "TOTAL_AMOUNT": 221107.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-03T13:51:33",
    "PRICE": 943.66,
    "QUANTITY": 6851,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "519",
    "TOTAL_AMOUNT": 6465014.48
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-23T00:03:56",
    "PRICE": 119.90,
    "QUANTITY": 5543,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "520",
    "TOTAL_AMOUNT": 664605.71
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-27T22:17:39",
    "PRICE": 574.14,
    "QUANTITY": 4653,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "521",
    "TOTAL_AMOUNT": 2671473.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-19T15:30:53",
    "PRICE": 477.26,
    "QUANTITY": 303,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "522",
    "TOTAL_AMOUNT": 144609.78
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-22T00:14:22",
    "PRICE": 197.69,
    "QUANTITY": 4036,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "523",
    "TOTAL_AMOUNT": 797876.85
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-10T17:40:08",
    "PRICE": 278.25,
    "QUANTITY": 6405,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "524",
    "TOTAL_AMOUNT": 1782191.25
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-19T09:28:55",
    "PRICE": 190.45,
    "QUANTITY": 4935,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "525",
    "TOTAL_AMOUNT": 939870.73
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-25T10:18:52",
    "PRICE": 618.94,
    "QUANTITY": 7493,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "526",
    "TOTAL_AMOUNT": 4637717.44
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-17T19:21:03",
    "PRICE": 703.33,
    "QUANTITY": 1824,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "527",
    "TOTAL_AMOUNT": 1282873.95
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-01T20:57:33",
    "PRICE": 949.91,
    "QUANTITY": 269,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "528",
    "TOTAL_AMOUNT": 255525.78
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-05T01:12:29",
    "PRICE": 541.54,
    "QUANTITY": 3925,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "529",
    "TOTAL_AMOUNT": 2125544.41
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-25T05:42:42",
    "PRICE": 59.63,
    "QUANTITY": 4124,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "530",
    "TOTAL_AMOUNT": 245914.12
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-06T06:12:17",
    "PRICE": 928.40,
    "QUANTITY": 4691,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "531",
    "TOTAL_AMOUNT": 4355124.51
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-16T07:48:53",
    "PRICE": 630.21,
    "QUANTITY": 9503,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "532",
    "TOTAL_AMOUNT": 5988885.84
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-19T01:12:51",
    "PRICE": 636.83,
    "QUANTITY": 818,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "533",
    "TOTAL_AMOUNT": 520926.95
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-19T00:58:33",
    "PRICE": 51.10,
    "QUANTITY": 4698,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "534",
    "TOTAL_AMOUNT": 240067.79
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-20T10:53:41",
    "PRICE": 583.54,
    "QUANTITY": 4569,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "535",
    "TOTAL_AMOUNT": 2666194.16
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-10T05:57:14",
    "PRICE": 645.15,
    "QUANTITY": 2945,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "536",
    "TOTAL_AMOUNT": 1899966.82
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-06T08:38:46",
    "PRICE": 817.07,
    "QUANTITY": 431,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "537",
    "TOTAL_AMOUNT": 352157.17
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-26T07:26:10",
    "PRICE": 412.82,
    "QUANTITY": 7647,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "538",
    "TOTAL_AMOUNT": 3156834.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-01T04:55:00",
    "PRICE": 525.55,
    "QUANTITY": 5786,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "539",
    "TOTAL_AMOUNT": 3040832.23
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-31T21:44:35",
    "PRICE": 485.55,
    "QUANTITY": 3901,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "540",
    "TOTAL_AMOUNT": 1894130.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-11T18:50:21",
    "PRICE": 968.05,
    "QUANTITY": 4615,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "541",
    "TOTAL_AMOUNT": 4467550.69
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-31T06:45:16",
    "PRICE": 860.70,
    "QUANTITY": 6657,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "542",
    "TOTAL_AMOUNT": 5729679.98
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-27T15:47:45",
    "PRICE": 845.92,
    "QUANTITY": 2253,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "543",
    "TOTAL_AMOUNT": 1905857.72
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-30T23:46:51",
    "PRICE": 303.78,
    "QUANTITY": 2743,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "544",
    "TOTAL_AMOUNT": 833268.54
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-20T14:28:52",
    "PRICE": 183.21,
    "QUANTITY": 4698,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "545",
    "TOTAL_AMOUNT": 860720.61
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-05T12:48:55",
    "PRICE": 831.42,
    "QUANTITY": 9222,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "546",
    "TOTAL_AMOUNT": 7667355.08
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-02T02:01:10",
    "PRICE": 308.24,
    "QUANTITY": 2235,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "547",
    "TOTAL_AMOUNT": 688916.38
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-29T13:23:36",
    "PRICE": 804.25,
    "QUANTITY": 7048,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "548",
    "TOTAL_AMOUNT": 5668354.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-14T23:44:18",
    "PRICE": 172.65,
    "QUANTITY": 8300,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "549",
    "TOTAL_AMOUNT": 1432994.95
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-04T18:22:41",
    "PRICE": 121.04,
    "QUANTITY": 3357,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "550",
    "TOTAL_AMOUNT": 406331.28
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-07T13:44:11",
    "PRICE": 123.35,
    "QUANTITY": 3339,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "551",
    "TOTAL_AMOUNT": 411865.64
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-05T15:00:41",
    "PRICE": 620.75,
    "QUANTITY": 1636,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "552",
    "TOTAL_AMOUNT": 1015547.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-16T17:17:14",
    "PRICE": 379.37,
    "QUANTITY": 898,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "553",
    "TOTAL_AMOUNT": 340674.26
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-05T08:22:40",
    "PRICE": 693.54,
    "QUANTITY": 4372,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "554",
    "TOTAL_AMOUNT": 3032156.78
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-18T15:35:39",
    "PRICE": 831.82,
    "QUANTITY": 8890,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "555",
    "TOTAL_AMOUNT": 7394879.87
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-12T04:43:33",
    "PRICE": 826.89,
    "QUANTITY": 9649,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "556",
    "TOTAL_AMOUNT": 7978661.75
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-30T17:28:26",
    "PRICE": 388.18,
    "QUANTITY": 1885,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "557",
    "TOTAL_AMOUNT": 731719.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-20T23:25:16",
    "PRICE": 82.05,
    "QUANTITY": 3174,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "558",
    "TOTAL_AMOUNT": 260426.71
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-02T06:29:46",
    "PRICE": 733.49,
    "QUANTITY": 4344,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "559",
    "TOTAL_AMOUNT": 3186280.52
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-05T16:07:45",
    "PRICE": 830.21,
    "QUANTITY": 3829,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "560",
    "TOTAL_AMOUNT": 3178874.17
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-01T00:53:49",
    "PRICE": 418.20,
    "QUANTITY": 7216,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "561",
    "TOTAL_AMOUNT": 3017731.29
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-07T21:19:31",
    "PRICE": 911.11,
    "QUANTITY": 3709,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "562",
    "TOTAL_AMOUNT": 3379306.94
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-24T08:34:32",
    "PRICE": 952.85,
    "QUANTITY": 3036,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "563",
    "TOTAL_AMOUNT": 2892852.53
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-14T21:13:02",
    "PRICE": 255.10,
    "QUANTITY": 3048,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "564",
    "TOTAL_AMOUNT": 777544.82
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-29T05:14:46",
    "PRICE": 641.07,
    "QUANTITY": 8277,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "565",
    "TOTAL_AMOUNT": 5306136.45
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-18T15:36:48",
    "PRICE": 61.79,
    "QUANTITY": 5493,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "566",
    "TOTAL_AMOUNT": 339412.48
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-24T10:46:57",
    "PRICE": 357.70,
    "QUANTITY": 4254,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "567",
    "TOTAL_AMOUNT": 1521655.85
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-20T13:44:26",
    "PRICE": 482.66,
    "QUANTITY": 2636,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "568",
    "TOTAL_AMOUNT": 1272291.77
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-25T16:01:58",
    "PRICE": 585.55,
    "QUANTITY": 8393,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "569",
    "TOTAL_AMOUNT": 4914521.05
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-30T12:31:01",
    "PRICE": 167.16,
    "QUANTITY": 6761,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "570",
    "TOTAL_AMOUNT": 1130168.78
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-21T02:25:34",
    "PRICE": 938.11,
    "QUANTITY": 336,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "571",
    "TOTAL_AMOUNT": 315204.96
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-28T09:23:41",
    "PRICE": 263.27,
    "QUANTITY": 2260,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "572",
    "TOTAL_AMOUNT": 594990.18
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-07T21:18:57",
    "PRICE": 266.98,
    "QUANTITY": 5588,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "573",
    "TOTAL_AMOUNT": 1491884.30
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-14T01:19:09",
    "PRICE": 646.42,
    "QUANTITY": 1939,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "574",
    "TOTAL_AMOUNT": 1253408.35
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-06T16:40:41",
    "PRICE": 758.60,
    "QUANTITY": 2704,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "575",
    "TOTAL_AMOUNT": 2051254.33
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-25T19:48:15",
    "PRICE": 947.96,
    "QUANTITY": 2688,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "576",
    "TOTAL_AMOUNT": 2548116.54
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-14T19:35:19",
    "PRICE": 832.40,
    "QUANTITY": 8988,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "577",
    "TOTAL_AMOUNT": 7481611.42
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-26T22:20:08",
    "PRICE": 848.52,
    "QUANTITY": 5104,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "578",
    "TOTAL_AMOUNT": 4330846.18
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-14T21:34:25",
    "PRICE": 671.01,
    "QUANTITY": 6402,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "579",
    "TOTAL_AMOUNT": 4295806.08
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-08T11:04:48",
    "PRICE": 332.19,
    "QUANTITY": 4800,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "580",
    "TOTAL_AMOUNT": 1594512.01
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-11T22:03:02",
    "PRICE": 950.34,
    "QUANTITY": 9553,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "581",
    "TOTAL_AMOUNT": 9078598.28
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-08T05:47:51",
    "PRICE": 517.72,
    "QUANTITY": 8570,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "582",
    "TOTAL_AMOUNT": 4436860.15
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-03T15:12:05",
    "PRICE": 480.93,
    "QUANTITY": 8875,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "583",
    "TOTAL_AMOUNT": 4268253.68
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-17T09:05:54",
    "PRICE": 850.58,
    "QUANTITY": 8456,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "584",
    "TOTAL_AMOUNT": 7192504.62
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-18T21:31:22",
    "PRICE": 956.02,
    "QUANTITY": 5209,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "585",
    "TOTAL_AMOUNT": 4979908.28
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-09T15:26:04",
    "PRICE": 747.70,
    "QUANTITY": 7062,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "586",
    "TOTAL_AMOUNT": 5280257.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-27T05:27:01",
    "PRICE": 243.72,
    "QUANTITY": 9482,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "587",
    "TOTAL_AMOUNT": 2310953.05
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-17T18:35:52",
    "PRICE": 811.20,
    "QUANTITY": 1848,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "588",
    "TOTAL_AMOUNT": 1499097.62
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-23T17:10:12",
    "PRICE": 797.68,
    "QUANTITY": 6398,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "589",
    "TOTAL_AMOUNT": 5103556.59
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-30T13:49:56",
    "PRICE": 772.76,
    "QUANTITY": 9794,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "590",
    "TOTAL_AMOUNT": 7568411.54
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-20T13:25:01",
    "PRICE": 75.75,
    "QUANTITY": 5082,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "591",
    "TOTAL_AMOUNT": 384961.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-21T17:28:32",
    "PRICE": 268.68,
    "QUANTITY": 426,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "592",
    "TOTAL_AMOUNT": 114457.68
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-02T09:00:16",
    "PRICE": 619.02,
    "QUANTITY": 9828,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "593",
    "TOTAL_AMOUNT": 6083728.75
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-11T20:51:32",
    "PRICE": 372.57,
    "QUANTITY": 1432,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "594",
    "TOTAL_AMOUNT": 533520.25
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-12T01:19:07",
    "PRICE": 459.99,
    "QUANTITY": 7654,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "595",
    "TOTAL_AMOUNT": 3520763.39
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-28T18:18:13",
    "PRICE": 150.58,
    "QUANTITY": 9142,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "596",
    "TOTAL_AMOUNT": 1376602.38
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-18T21:34:16",
    "PRICE": 795.64,
    "QUANTITY": 3105,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "597",
    "TOTAL_AMOUNT": 2470462.25
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-15T03:51:03",
    "PRICE": 176.53,
    "QUANTITY": 5275,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "598",
    "TOTAL_AMOUNT": 931195.74
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-19T14:44:29",
    "PRICE": 385.62,
    "QUANTITY": 3633,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "599",
    "TOTAL_AMOUNT": 1400957.44
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-02T18:12:46",
    "PRICE": 190.07,
    "QUANTITY": 6482,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "600",
    "TOTAL_AMOUNT": 1232033.79
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-29T04:27:42",
    "PRICE": 323.49,
    "QUANTITY": 9136,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "601",
    "TOTAL_AMOUNT": 2955404.55
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-16T12:21:54",
    "PRICE": 227.75,
    "QUANTITY": 7335,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "602",
    "TOTAL_AMOUNT": 1670546.25
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-29T00:07:15",
    "PRICE": 878.68,
    "QUANTITY": 2801,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "603",
    "TOTAL_AMOUNT": 2461182.66
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-11T14:16:30",
    "PRICE": 441.15,
    "QUANTITY": 9909,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "604",
    "TOTAL_AMOUNT": 4371355.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-20T01:27:57",
    "PRICE": 409.92,
    "QUANTITY": 5179,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "605",
    "TOTAL_AMOUNT": 2122975.75
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-13T07:26:21",
    "PRICE": 167.35,
    "QUANTITY": 6322,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "606",
    "TOTAL_AMOUNT": 1057986.74
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-07T05:14:37",
    "PRICE": 821.67,
    "QUANTITY": 4806,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "607",
    "TOTAL_AMOUNT": 3948945.94
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-25T08:28:16",
    "PRICE": 915.84,
    "QUANTITY": 9656,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "608",
    "TOTAL_AMOUNT": 8843351.30
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-10T16:45:33",
    "PRICE": 361.21,
    "QUANTITY": 901,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "609",
    "TOTAL_AMOUNT": 325450.20
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-08T12:30:44",
    "PRICE": 681.30,
    "QUANTITY": 5458,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "610",
    "TOTAL_AMOUNT": 3718535.33
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-19T15:47:53",
    "PRICE": 655.49,
    "QUANTITY": 8667,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "611",
    "TOTAL_AMOUNT": 5681131.75
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-10T23:36:50",
    "PRICE": 552.69,
    "QUANTITY": 269,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "612",
    "TOTAL_AMOUNT": 148673.61
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-03T12:50:30",
    "PRICE": 171.01,
    "QUANTITY": 1587,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "613",
    "TOTAL_AMOUNT": 271392.86
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-16T12:17:22",
    "PRICE": 720.28,
    "QUANTITY": 1766,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "614",
    "TOTAL_AMOUNT": 1272014.53
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-06T19:42:12",
    "PRICE": 805.82,
    "QUANTITY": 4567,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "615",
    "TOTAL_AMOUNT": 3680179.97
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-01T03:58:59",
    "PRICE": 151.98,
    "QUANTITY": 9355,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "616",
    "TOTAL_AMOUNT": 1421772.86
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-18T23:54:34",
    "PRICE": 265.24,
    "QUANTITY": 6532,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "617",
    "TOTAL_AMOUNT": 1732547.62
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-03T12:20:49",
    "PRICE": 454.76,
    "QUANTITY": 9052,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "618",
    "TOTAL_AMOUNT": 4116487.61
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-06T07:52:05",
    "PRICE": 829.71,
    "QUANTITY": 2648,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "619",
    "TOTAL_AMOUNT": 2197072.14
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-04T19:22:35",
    "PRICE": 586.07,
    "QUANTITY": 2867,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "620",
    "TOTAL_AMOUNT": 1680262.71
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-23T18:25:32",
    "PRICE": 623.79,
    "QUANTITY": 8601,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "621",
    "TOTAL_AMOUNT": 5365217.60
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-04T09:34:09",
    "PRICE": 762.47,
    "QUANTITY": 610,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "622",
    "TOTAL_AMOUNT": 465106.68
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-20T15:41:00",
    "PRICE": 836.66,
    "QUANTITY": 7405,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "623",
    "TOTAL_AMOUNT": 6195467.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-14T03:30:39",
    "PRICE": 239.30,
    "QUANTITY": 3980,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "624",
    "TOTAL_AMOUNT": 952414.01
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-16T12:15:47",
    "PRICE": 756.40,
    "QUANTITY": 3930,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "625",
    "TOTAL_AMOUNT": 2972652.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-30T04:07:09",
    "PRICE": 247.83,
    "QUANTITY": 7649,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "626",
    "TOTAL_AMOUNT": 1895651.68
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-28T15:22:30",
    "PRICE": 510.08,
    "QUANTITY": 8476,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "627",
    "TOTAL_AMOUNT": 4323437.97
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-14T04:36:07",
    "PRICE": 515.54,
    "QUANTITY": 4605,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "628",
    "TOTAL_AMOUNT": 2374061.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-11-04T15:24:41",
    "PRICE": 651.50,
    "QUANTITY": 8982,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "629",
    "TOTAL_AMOUNT": 5851773.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-27T04:40:43",
    "PRICE": 832.13,
    "QUANTITY": 2787,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "630",
    "TOTAL_AMOUNT": 2319146.32
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-17T17:44:54",
    "PRICE": 689.58,
    "QUANTITY": 7516,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "631",
    "TOTAL_AMOUNT": 5182883.41
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-04T05:02:19",
    "PRICE": 234.90,
    "QUANTITY": 5461,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "632",
    "TOTAL_AMOUNT": 1282788.87
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-16T09:15:42",
    "PRICE": 550.17,
    "QUANTITY": 3016,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "633",
    "TOTAL_AMOUNT": 1659312.67
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-25T07:37:48",
    "PRICE": 316.68,
    "QUANTITY": 2105,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "634",
    "TOTAL_AMOUNT": 666611.38
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-24T16:31:59",
    "PRICE": 645.41,
    "QUANTITY": 413,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "635",
    "TOTAL_AMOUNT": 266554.32
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-24T21:07:58",
    "PRICE": 822.05,
    "QUANTITY": 4729,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "636",
    "TOTAL_AMOUNT": 3887474.39
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-17T05:22:00",
    "PRICE": 520.20,
    "QUANTITY": 3415,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "637",
    "TOTAL_AMOUNT": 1776483.04
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-05T04:15:40",
    "PRICE": 108.07,
    "QUANTITY": 4409,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "638",
    "TOTAL_AMOUNT": 476480.63
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-24T15:12:13",
    "PRICE": 422.55,
    "QUANTITY": 898,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "639",
    "TOTAL_AMOUNT": 379449.89
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-22T05:43:19",
    "PRICE": 409.59,
    "QUANTITY": 4810,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "640",
    "TOTAL_AMOUNT": 1970127.88
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-30T05:33:34",
    "PRICE": 359.52,
    "QUANTITY": 7008,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "641",
    "TOTAL_AMOUNT": 2519516.08
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-09T20:00:11",
    "PRICE": 583.18,
    "QUANTITY": 153,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "642",
    "TOTAL_AMOUNT": 89226.54
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-16T00:40:25",
    "PRICE": 183.63,
    "QUANTITY": 6051,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "643",
    "TOTAL_AMOUNT": 1111145.16
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-26T14:34:03",
    "PRICE": 533.88,
    "QUANTITY": 9783,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "644",
    "TOTAL_AMOUNT": 5222948.09
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-13T18:47:52",
    "PRICE": 904.03,
    "QUANTITY": 8133,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "645",
    "TOTAL_AMOUNT": 7352476.23
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-02T22:33:02",
    "PRICE": 150.64,
    "QUANTITY": 7023,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "646",
    "TOTAL_AMOUNT": 1057944.72
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-17T03:33:41",
    "PRICE": 52.76,
    "QUANTITY": 6354,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "647",
    "TOTAL_AMOUNT": 335237.03
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-06T05:19:39",
    "PRICE": 358.93,
    "QUANTITY": 6881,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "648",
    "TOTAL_AMOUNT": 2469797.28
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T00:43:06",
    "PRICE": 403.15,
    "QUANTITY": 3183,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "649",
    "TOTAL_AMOUNT": 1283226.43
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-21T00:53:14",
    "PRICE": 175.03,
    "QUANTITY": 1259,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "650",
    "TOTAL_AMOUNT": 220362.77
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-28T09:14:50",
    "PRICE": 223.27,
    "QUANTITY": 7565,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "651",
    "TOTAL_AMOUNT": 1689037.58
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-07T16:59:01",
    "PRICE": 753.41,
    "QUANTITY": 5648,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "652",
    "TOTAL_AMOUNT": 4255259.53
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-22T18:08:57",
    "PRICE": 693.82,
    "QUANTITY": 248,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "653",
    "TOTAL_AMOUNT": 172067.36
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-26T19:11:18",
    "PRICE": 319.79,
    "QUANTITY": 8279,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "654",
    "TOTAL_AMOUNT": 2647541.48
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-26T22:48:24",
    "PRICE": 215.93,
    "QUANTITY": 6164,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "655",
    "TOTAL_AMOUNT": 1330992.47
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-23T14:08:58",
    "PRICE": 966.49,
    "QUANTITY": 3565,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "656",
    "TOTAL_AMOUNT": 3445536.82
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-05T04:57:57",
    "PRICE": 218.22,
    "QUANTITY": 1708,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "657",
    "TOTAL_AMOUNT": 372719.76
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-10T14:50:32",
    "PRICE": 685.55,
    "QUANTITY": 1077,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "658",
    "TOTAL_AMOUNT": 738337.34
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-26T19:45:39",
    "PRICE": 253.19,
    "QUANTITY": 3367,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "659",
    "TOTAL_AMOUNT": 852490.74
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-30T10:21:39",
    "PRICE": 799.98,
    "QUANTITY": 393,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "660",
    "TOTAL_AMOUNT": 314392.13
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-09T15:53:47",
    "PRICE": 678.77,
    "QUANTITY": 5505,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "661",
    "TOTAL_AMOUNT": 3736628.96
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-15T14:22:34",
    "PRICE": 489.35,
    "QUANTITY": 5349,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "662",
    "TOTAL_AMOUNT": 2617533.18
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-06T15:03:00",
    "PRICE": 300.94,
    "QUANTITY": 6475,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "663",
    "TOTAL_AMOUNT": 1948586.52
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-14T22:13:22",
    "PRICE": 768.08,
    "QUANTITY": 3851,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "664",
    "TOTAL_AMOUNT": 2957876.15
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-07T08:14:51",
    "PRICE": 493.53,
    "QUANTITY": 6872,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "665",
    "TOTAL_AMOUNT": 3391538.15
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-08T12:03:49",
    "PRICE": 551.77,
    "QUANTITY": 6559,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "666",
    "TOTAL_AMOUNT": 3619059.56
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-30T14:39:02",
    "PRICE": 661.59,
    "QUANTITY": 9964,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "667",
    "TOTAL_AMOUNT": 6592083.03
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-03T02:59:02",
    "PRICE": 769.02,
    "QUANTITY": 1845,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "668",
    "TOTAL_AMOUNT": 1418841.94
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-23T00:15:19",
    "PRICE": 733.97,
    "QUANTITY": 2199,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "669",
    "TOTAL_AMOUNT": 1613999.97
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-22T18:34:56",
    "PRICE": 900.23,
    "QUANTITY": 2750,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "670",
    "TOTAL_AMOUNT": 2475632.45
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-07T06:20:04",
    "PRICE": 331.81,
    "QUANTITY": 4591,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "671",
    "TOTAL_AMOUNT": 1523339.70
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-30T09:25:43",
    "PRICE": 214.89,
    "QUANTITY": 1288,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "672",
    "TOTAL_AMOUNT": 276778.32
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-17T06:32:41",
    "PRICE": 583.78,
    "QUANTITY": 9873,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "673",
    "TOTAL_AMOUNT": 5763660.23
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-26T12:14:05",
    "PRICE": 750.84,
    "QUANTITY": 1349,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "674",
    "TOTAL_AMOUNT": 1012883.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-04T18:22:04",
    "PRICE": 291.20,
    "QUANTITY": 3194,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "675",
    "TOTAL_AMOUNT": 930092.84
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-25T20:01:12",
    "PRICE": 183.26,
    "QUANTITY": 2274,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "676",
    "TOTAL_AMOUNT": 416733.23
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-28T11:19:24",
    "PRICE": 847.83,
    "QUANTITY": 6540,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "677",
    "TOTAL_AMOUNT": 5544808.31
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-28T12:27:19",
    "PRICE": 266.83,
    "QUANTITY": 6160,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "678",
    "TOTAL_AMOUNT": 1643672.72
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-11T23:08:42",
    "PRICE": 434.51,
    "QUANTITY": 2935,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "679",
    "TOTAL_AMOUNT": 1275286.88
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-18T08:32:54",
    "PRICE": 740.94,
    "QUANTITY": 6411,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "680",
    "TOTAL_AMOUNT": 4750166.36
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-25T11:58:49",
    "PRICE": 888.02,
    "QUANTITY": 6851,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "681",
    "TOTAL_AMOUNT": 6083825.15
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-09T08:30:23",
    "PRICE": 496.22,
    "QUANTITY": 4389,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "682",
    "TOTAL_AMOUNT": 2177909.59
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-24T03:50:18",
    "PRICE": 443.37,
    "QUANTITY": 4405,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "683",
    "TOTAL_AMOUNT": 1953044.83
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-04T10:13:45",
    "PRICE": 975.53,
    "QUANTITY": 9652,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "684",
    "TOTAL_AMOUNT": 9415815.84
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-12T08:55:39",
    "PRICE": 860.29,
    "QUANTITY": 5269,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "685",
    "TOTAL_AMOUNT": 4532867.89
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-30T01:51:26",
    "PRICE": 888.46,
    "QUANTITY": 5625,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "686",
    "TOTAL_AMOUNT": 4997587.62
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-04T20:58:03",
    "PRICE": 191.31,
    "QUANTITY": 1478,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "687",
    "TOTAL_AMOUNT": 282756.18
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-01T19:12:12",
    "PRICE": 63.98,
    "QUANTITY": 1681,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "688",
    "TOTAL_AMOUNT": 107550.38
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-25T01:16:15",
    "PRICE": 846.83,
    "QUANTITY": 1066,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "689",
    "TOTAL_AMOUNT": 902720.80
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-26T20:10:09",
    "PRICE": 502.61,
    "QUANTITY": 8731,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "690",
    "TOTAL_AMOUNT": 4388287.78
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-08T11:42:28",
    "PRICE": 912.78,
    "QUANTITY": 1632,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "691",
    "TOTAL_AMOUNT": 1489657.01
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-17T00:34:52",
    "PRICE": 898.22,
    "QUANTITY": 5656,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "692",
    "TOTAL_AMOUNT": 5080332.15
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-11T12:54:52",
    "PRICE": 922.62,
    "QUANTITY": 7661,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "693",
    "TOTAL_AMOUNT": 7068191.78
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-09T17:29:09",
    "PRICE": 348.07,
    "QUANTITY": 9079,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "694",
    "TOTAL_AMOUNT": 3160127.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-05T08:01:28",
    "PRICE": 540.71,
    "QUANTITY": 951,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "695",
    "TOTAL_AMOUNT": 514215.23
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-04T22:41:57",
    "PRICE": 580.37,
    "QUANTITY": 7366,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "696",
    "TOTAL_AMOUNT": 4275005.38
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-07T08:23:53",
    "PRICE": 865.63,
    "QUANTITY": 9167,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "697",
    "TOTAL_AMOUNT": 7935230.25
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-15T11:31:25",
    "PRICE": 107.65,
    "QUANTITY": 2572,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "698",
    "TOTAL_AMOUNT": 276875.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-12T16:46:22",
    "PRICE": 238.94,
    "QUANTITY": 7282,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "699",
    "TOTAL_AMOUNT": 1739961.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-10T06:47:12",
    "PRICE": 624.44,
    "QUANTITY": 8367,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "700",
    "TOTAL_AMOUNT": 5224689.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-25T20:25:16",
    "PRICE": 152.99,
    "QUANTITY": 1626,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "701",
    "TOTAL_AMOUNT": 248761.75
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-23T00:16:55",
    "PRICE": 790.06,
    "QUANTITY": 9536,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "702",
    "TOTAL_AMOUNT": 7534012.14
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-15T03:00:41",
    "PRICE": 400.52,
    "QUANTITY": 9857,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "703",
    "TOTAL_AMOUNT": 3947925.53
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-08T22:25:50",
    "PRICE": 667.43,
    "QUANTITY": 4856,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "704",
    "TOTAL_AMOUNT": 3241040.04
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-23T16:31:44",
    "PRICE": 347.79,
    "QUANTITY": 8771,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "705",
    "TOTAL_AMOUNT": 3050466.16
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-30T04:46:56",
    "PRICE": 181.08,
    "QUANTITY": 7057,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "706",
    "TOTAL_AMOUNT": 1277881.57
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-01T16:11:23",
    "PRICE": 613.36,
    "QUANTITY": 9344,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "707",
    "TOTAL_AMOUNT": 5731235.70
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-30T10:52:36",
    "PRICE": 123.89,
    "QUANTITY": 5831,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "708",
    "TOTAL_AMOUNT": 722402.59
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-08T16:55:35",
    "PRICE": 225.64,
    "QUANTITY": 5610,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "709",
    "TOTAL_AMOUNT": 1265840.40
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-30T07:19:03",
    "PRICE": 710.46,
    "QUANTITY": 4839,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "710",
    "TOTAL_AMOUNT": 3437916.05
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-14T05:46:25",
    "PRICE": 50.02,
    "QUANTITY": 448,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "711",
    "TOTAL_AMOUNT": 22408.96
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-17T18:32:24",
    "PRICE": 953.12,
    "QUANTITY": 6254,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "712",
    "TOTAL_AMOUNT": 5960812.45
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-04T12:19:19",
    "PRICE": 732.35,
    "QUANTITY": 294,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "713",
    "TOTAL_AMOUNT": 215310.89
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-10T20:27:09",
    "PRICE": 141.01,
    "QUANTITY": 7883,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "714",
    "TOTAL_AMOUNT": 1111581.79
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-16T14:20:32",
    "PRICE": 567.86,
    "QUANTITY": 6400,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "715",
    "TOTAL_AMOUNT": 3634303.91
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-11T05:04:30",
    "PRICE": 756.54,
    "QUANTITY": 8982,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "716",
    "TOTAL_AMOUNT": 6795242.08
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-11-01T20:58:28",
    "PRICE": 427.56,
    "QUANTITY": 2496,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "717",
    "TOTAL_AMOUNT": 1067189.75
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-15T21:30:23",
    "PRICE": 209.69,
    "QUANTITY": 593,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "718",
    "TOTAL_AMOUNT": 124346.17
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-17T17:12:05",
    "PRICE": 859.42,
    "QUANTITY": 7685,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "719",
    "TOTAL_AMOUNT": 6604642.57
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-26T06:30:01",
    "PRICE": 114.04,
    "QUANTITY": 819,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "720",
    "TOTAL_AMOUNT": 93398.76
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-07T21:49:06",
    "PRICE": 709.57,
    "QUANTITY": 7594,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "721",
    "TOTAL_AMOUNT": 5388474.64
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-27T06:50:42",
    "PRICE": 561.76,
    "QUANTITY": 9296,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "722",
    "TOTAL_AMOUNT": 5222121.05
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-02T10:44:10",
    "PRICE": 109.91,
    "QUANTITY": 7395,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "723",
    "TOTAL_AMOUNT": 812784.48
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-14T15:59:35",
    "PRICE": 735.03,
    "QUANTITY": 8475,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "724",
    "TOTAL_AMOUNT": 6229379.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-22T18:08:58",
    "PRICE": 565.47,
    "QUANTITY": 9160,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "725",
    "TOTAL_AMOUNT": 5179704.93
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-13T23:34:59",
    "PRICE": 688.31,
    "QUANTITY": 1704,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "726",
    "TOTAL_AMOUNT": 1172880.24
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-21T07:40:09",
    "PRICE": 227.83,
    "QUANTITY": 9677,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "727",
    "TOTAL_AMOUNT": 2204710.93
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-18T07:05:36",
    "PRICE": 311.90,
    "QUANTITY": 5355,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "728",
    "TOTAL_AMOUNT": 1670224.47
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-11T05:45:53",
    "PRICE": 618.81,
    "QUANTITY": 809,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "729",
    "TOTAL_AMOUNT": 500617.29
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-10T19:45:12",
    "PRICE": 675.47,
    "QUANTITY": 7974,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "730",
    "TOTAL_AMOUNT": 5386197.55
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-07T04:31:10",
    "PRICE": 121.97,
    "QUANTITY": 4710,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "731",
    "TOTAL_AMOUNT": 574478.71
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-03T15:47:54",
    "PRICE": 307.85,
    "QUANTITY": 8262,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "732",
    "TOTAL_AMOUNT": 2543456.75
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-22T19:56:18",
    "PRICE": 50.67,
    "QUANTITY": 9976,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "733",
    "TOTAL_AMOUNT": 505483.90
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-18T06:06:14",
    "PRICE": 139.98,
    "QUANTITY": 2619,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "734",
    "TOTAL_AMOUNT": 366607.61
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-06T13:26:04",
    "PRICE": 219.23,
    "QUANTITY": 2057,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "735",
    "TOTAL_AMOUNT": 450956.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-24T06:18:26",
    "PRICE": 712.14,
    "QUANTITY": 2732,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "736",
    "TOTAL_AMOUNT": 1945566.52
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-05T02:45:33",
    "PRICE": 258.78,
    "QUANTITY": 3161,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "737",
    "TOTAL_AMOUNT": 818003.58
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-05T22:43:57",
    "PRICE": 727.86,
    "QUANTITY": 8230,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "738",
    "TOTAL_AMOUNT": 5990287.68
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-21T13:25:34",
    "PRICE": 729.26,
    "QUANTITY": 4971,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "739",
    "TOTAL_AMOUNT": 3625151.51
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-22T19:51:41",
    "PRICE": 163.54,
    "QUANTITY": 8745,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "740",
    "TOTAL_AMOUNT": 1430157.24
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-28T05:35:42",
    "PRICE": 606.19,
    "QUANTITY": 8888,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "741",
    "TOTAL_AMOUNT": 5387816.74
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-26T21:36:32",
    "PRICE": 453.99,
    "QUANTITY": 9465,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "742",
    "TOTAL_AMOUNT": 4297015.26
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-10T00:46:37",
    "PRICE": 418.49,
    "QUANTITY": 4037,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "743",
    "TOTAL_AMOUNT": 1689444.09
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-06T17:36:18",
    "PRICE": 708.86,
    "QUANTITY": 9321,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "744",
    "TOTAL_AMOUNT": 6607283.92
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-12T21:51:19",
    "PRICE": 450.94,
    "QUANTITY": 3155,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "745",
    "TOTAL_AMOUNT": 1422715.71
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-24T10:05:28",
    "PRICE": 85.24,
    "QUANTITY": 8833,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "746",
    "TOTAL_AMOUNT": 752924.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-20T16:11:50",
    "PRICE": 481.30,
    "QUANTITY": 2614,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "747",
    "TOTAL_AMOUNT": 1258118.17
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-09T10:10:44",
    "PRICE": 363.10,
    "QUANTITY": 2145,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "748",
    "TOTAL_AMOUNT": 778849.51
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-07T10:21:14",
    "PRICE": 888.03,
    "QUANTITY": 9289,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "749",
    "TOTAL_AMOUNT": 8248910.94
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-17T20:17:47",
    "PRICE": 196.62,
    "QUANTITY": 8545,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "750",
    "TOTAL_AMOUNT": 1680117.86
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-02T01:44:13",
    "PRICE": 586.00,
    "QUANTITY": 6628,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "751",
    "TOTAL_AMOUNT": 3884008.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-12T06:04:01",
    "PRICE": 810.00,
    "QUANTITY": 4472,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "752",
    "TOTAL_AMOUNT": 3622320.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-05T18:50:46",
    "PRICE": 853.82,
    "QUANTITY": 1876,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "753",
    "TOTAL_AMOUNT": 1601766.33
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-08T23:56:04",
    "PRICE": 87.74,
    "QUANTITY": 2149,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "754",
    "TOTAL_AMOUNT": 188553.26
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-11-05T17:38:41",
    "PRICE": 472.85,
    "QUANTITY": 9587,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "755",
    "TOTAL_AMOUNT": 4533213.01
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-02T10:11:38",
    "PRICE": 457.85,
    "QUANTITY": 1357,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "756",
    "TOTAL_AMOUNT": 621302.46
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-12T04:03:22",
    "PRICE": 810.83,
    "QUANTITY": 8588,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "757",
    "TOTAL_AMOUNT": 6963408.19
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-10T19:38:07",
    "PRICE": 470.33,
    "QUANTITY": 3521,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "758",
    "TOTAL_AMOUNT": 1656031.88
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-03T16:47:32",
    "PRICE": 829.98,
    "QUANTITY": 5200,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "759",
    "TOTAL_AMOUNT": 4315895.90
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-06T06:34:14",
    "PRICE": 845.28,
    "QUANTITY": 9867,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "760",
    "TOTAL_AMOUNT": 8340378.05
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-23T16:51:29",
    "PRICE": 115.99,
    "QUANTITY": 7605,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "761",
    "TOTAL_AMOUNT": 882103.93
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-12T11:46:22",
    "PRICE": 900.87,
    "QUANTITY": 6005,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "762",
    "TOTAL_AMOUNT": 5409724.32
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-17T06:37:28",
    "PRICE": 984.66,
    "QUANTITY": 6430,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "763",
    "TOTAL_AMOUNT": 6331363.63
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-07T02:12:29",
    "PRICE": 108.68,
    "QUANTITY": 3638,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "764",
    "TOTAL_AMOUNT": 395377.84
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-03T11:43:40",
    "PRICE": 580.08,
    "QUANTITY": 4658,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "765",
    "TOTAL_AMOUNT": 2702012.72
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-22T20:08:02",
    "PRICE": 103.37,
    "QUANTITY": 1732,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "766",
    "TOTAL_AMOUNT": 179036.84
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-20T07:57:46",
    "PRICE": 428.28,
    "QUANTITY": 8931,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "767",
    "TOTAL_AMOUNT": 3824968.67
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-15T15:38:58",
    "PRICE": 984.23,
    "QUANTITY": 8491,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "768",
    "TOTAL_AMOUNT": 8357096.76
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-04T10:20:18",
    "PRICE": 622.92,
    "QUANTITY": 3758,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "769",
    "TOTAL_AMOUNT": 2340933.30
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-06T09:11:42",
    "PRICE": 820.95,
    "QUANTITY": 1522,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "770",
    "TOTAL_AMOUNT": 1249485.92
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-09T13:02:40",
    "PRICE": 669.41,
    "QUANTITY": 2265,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "771",
    "TOTAL_AMOUNT": 1516213.59
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-06T06:54:35",
    "PRICE": 160.54,
    "QUANTITY": 228,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "772",
    "TOTAL_AMOUNT": 36603.12
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-19T16:10:01",
    "PRICE": 818.96,
    "QUANTITY": 8214,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "773",
    "TOTAL_AMOUNT": 6726937.62
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-23T08:41:28",
    "PRICE": 570.87,
    "QUANTITY": 1221,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "774",
    "TOTAL_AMOUNT": 697032.26
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-14T08:58:46",
    "PRICE": 320.67,
    "QUANTITY": 6330,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "775",
    "TOTAL_AMOUNT": 2029841.18
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-30T00:13:42",
    "PRICE": 932.04,
    "QUANTITY": 7662,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "776",
    "TOTAL_AMOUNT": 7141290.31
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-30T17:01:54",
    "PRICE": 749.36,
    "QUANTITY": 833,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "777",
    "TOTAL_AMOUNT": 624216.87
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-06T08:28:25",
    "PRICE": 575.50,
    "QUANTITY": 6377,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "778",
    "TOTAL_AMOUNT": 3669963.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-29T06:29:03",
    "PRICE": 640.48,
    "QUANTITY": 6245,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "779",
    "TOTAL_AMOUNT": 3999797.48
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-06T18:01:07",
    "PRICE": 843.43,
    "QUANTITY": 8343,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "780",
    "TOTAL_AMOUNT": 7036736.43
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-25T20:33:21",
    "PRICE": 648.95,
    "QUANTITY": 9586,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "781",
    "TOTAL_AMOUNT": 6220834.82
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-27T14:02:50",
    "PRICE": 497.16,
    "QUANTITY": 8512,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "782",
    "TOTAL_AMOUNT": 4231825.95
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-07T06:11:13",
    "PRICE": 99.38,
    "QUANTITY": 5199,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "783",
    "TOTAL_AMOUNT": 516676.61
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-07T00:18:57",
    "PRICE": 456.50,
    "QUANTITY": 2654,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "784",
    "TOTAL_AMOUNT": 1211551.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-08T03:57:59",
    "PRICE": 674.58,
    "QUANTITY": 2471,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "785",
    "TOTAL_AMOUNT": 1666887.22
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-19T08:30:54",
    "PRICE": 494.76,
    "QUANTITY": 9026,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "786",
    "TOTAL_AMOUNT": 4465703.85
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-05T21:40:37",
    "PRICE": 423.60,
    "QUANTITY": 9409,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "787",
    "TOTAL_AMOUNT": 3985652.46
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-06T14:42:09",
    "PRICE": 756.33,
    "QUANTITY": 1215,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "788",
    "TOTAL_AMOUNT": 918940.97
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-04T13:09:04",
    "PRICE": 657.53,
    "QUANTITY": 5261,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "789",
    "TOTAL_AMOUNT": 3459265.48
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-02T02:56:41",
    "PRICE": 579.66,
    "QUANTITY": 1046,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "790",
    "TOTAL_AMOUNT": 606324.33
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-07T13:01:45",
    "PRICE": 927.59,
    "QUANTITY": 9114,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "791",
    "TOTAL_AMOUNT": 8454055.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-06T18:17:38",
    "PRICE": 372.61,
    "QUANTITY": 3474,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "792",
    "TOTAL_AMOUNT": 1294447.09
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-26T10:48:43",
    "PRICE": 342.88,
    "QUANTITY": 6412,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "793",
    "TOTAL_AMOUNT": 2198546.59
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-09T18:46:09",
    "PRICE": 985.65,
    "QUANTITY": 3881,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "794",
    "TOTAL_AMOUNT": 3825307.74
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-09T16:55:32",
    "PRICE": 678.73,
    "QUANTITY": 1878,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "795",
    "TOTAL_AMOUNT": 1274654.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-30T09:31:49",
    "PRICE": 190.29,
    "QUANTITY": 1521,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "796",
    "TOTAL_AMOUNT": 289431.08
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-08T18:47:12",
    "PRICE": 478.65,
    "QUANTITY": 1289,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "797",
    "TOTAL_AMOUNT": 616979.84
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-23T16:52:10",
    "PRICE": 905.27,
    "QUANTITY": 4108,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "798",
    "TOTAL_AMOUNT": 3718849.24
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-08T09:21:27",
    "PRICE": 206.44,
    "QUANTITY": 652,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "799",
    "TOTAL_AMOUNT": 134598.88
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-06T17:04:14",
    "PRICE": 771.57,
    "QUANTITY": 9155,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "800",
    "TOTAL_AMOUNT": 7063723.42
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-01T11:33:11",
    "PRICE": 802.10,
    "QUANTITY": 5913,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "801",
    "TOTAL_AMOUNT": 4742817.16
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-10T23:28:41",
    "PRICE": 744.89,
    "QUANTITY": 3450,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "802",
    "TOTAL_AMOUNT": 2569870.55
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-08T15:51:49",
    "PRICE": 164.41,
    "QUANTITY": 4189,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "803",
    "TOTAL_AMOUNT": 688713.51
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-19T01:16:58",
    "PRICE": 267.83,
    "QUANTITY": 5017,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "804",
    "TOTAL_AMOUNT": 1343703.04
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-29T04:05:49",
    "PRICE": 288.88,
    "QUANTITY": 9034,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "805",
    "TOTAL_AMOUNT": 2609741.96
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-02T16:28:27",
    "PRICE": 925.51,
    "QUANTITY": 4380,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "806",
    "TOTAL_AMOUNT": 4053733.84
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-11T13:25:46",
    "PRICE": 805.78,
    "QUANTITY": 7563,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "807",
    "TOTAL_AMOUNT": 6094114.36
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-25T04:26:18",
    "PRICE": 429.92,
    "QUANTITY": 777,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "808",
    "TOTAL_AMOUNT": 334047.85
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-23T03:47:39",
    "PRICE": 782.73,
    "QUANTITY": 319,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "809",
    "TOTAL_AMOUNT": 249690.86
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-11T17:51:15",
    "PRICE": 484.92,
    "QUANTITY": 5015,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "810",
    "TOTAL_AMOUNT": 2431873.87
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-20T03:57:22",
    "PRICE": 74.62,
    "QUANTITY": 9457,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "811",
    "TOTAL_AMOUNT": 705681.37
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-16T01:14:36",
    "PRICE": 725.20,
    "QUANTITY": 447,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "812",
    "TOTAL_AMOUNT": 324164.41
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-10T12:12:57",
    "PRICE": 984.54,
    "QUANTITY": 4697,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "813",
    "TOTAL_AMOUNT": 4624384.28
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-11T00:33:50",
    "PRICE": 346.42,
    "QUANTITY": 5570,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "814",
    "TOTAL_AMOUNT": 1929559.47
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-28T20:28:05",
    "PRICE": 207.64,
    "QUANTITY": 5358,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "815",
    "TOTAL_AMOUNT": 1112535.12
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-30T21:11:15",
    "PRICE": 568.47,
    "QUANTITY": 1910,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "816",
    "TOTAL_AMOUNT": 1085777.64
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-22T18:17:35",
    "PRICE": 339.45,
    "QUANTITY": 4287,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "817",
    "TOTAL_AMOUNT": 1455222.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-09T20:28:37",
    "PRICE": 111.61,
    "QUANTITY": 4581,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "818",
    "TOTAL_AMOUNT": 511285.41
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-18T15:32:03",
    "PRICE": 717.86,
    "QUANTITY": 9390,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "819",
    "TOTAL_AMOUNT": 6740705.26
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-20T15:48:32",
    "PRICE": 715.16,
    "QUANTITY": 3162,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "820",
    "TOTAL_AMOUNT": 2261335.84
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-18T23:54:57",
    "PRICE": 84.41,
    "QUANTITY": 9169,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "821",
    "TOTAL_AMOUNT": 773955.32
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-22T00:11:44",
    "PRICE": 542.99,
    "QUANTITY": 2885,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "822",
    "TOTAL_AMOUNT": 1566526.12
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-17T19:08:44",
    "PRICE": 797.57,
    "QUANTITY": 907,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "823",
    "TOTAL_AMOUNT": 723396.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-12T04:16:28",
    "PRICE": 253.54,
    "QUANTITY": 7147,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "824",
    "TOTAL_AMOUNT": 1812050.33
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-16T04:03:39",
    "PRICE": 476.61,
    "QUANTITY": 3656,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "825",
    "TOTAL_AMOUNT": 1742486.11
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-05T05:24:40",
    "PRICE": 496.46,
    "QUANTITY": 7439,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "826",
    "TOTAL_AMOUNT": 3693165.88
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-06T05:47:22",
    "PRICE": 114.82,
    "QUANTITY": 583,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "827",
    "TOTAL_AMOUNT": 66940.06
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-31T01:18:59",
    "PRICE": 429.02,
    "QUANTITY": 8527,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "828",
    "TOTAL_AMOUNT": 3658253.45
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-31T02:40:36",
    "PRICE": 677.80,
    "QUANTITY": 3814,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "829",
    "TOTAL_AMOUNT": 2585129.15
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-30T23:39:21",
    "PRICE": 341.36,
    "QUANTITY": 8675,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "830",
    "TOTAL_AMOUNT": 2961297.87
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-19T05:30:16",
    "PRICE": 751.80,
    "QUANTITY": 9032,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "831",
    "TOTAL_AMOUNT": 6790257.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-06T07:05:51",
    "PRICE": 291.04,
    "QUANTITY": 4947,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "832",
    "TOTAL_AMOUNT": 1439774.92
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-25T02:41:48",
    "PRICE": 911.80,
    "QUANTITY": 2379,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "833",
    "TOTAL_AMOUNT": 2169172.17
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-19T06:23:29",
    "PRICE": 253.93,
    "QUANTITY": 537,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "834",
    "TOTAL_AMOUNT": 136360.41
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-04T14:25:35",
    "PRICE": 152.62,
    "QUANTITY": 3866,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "835",
    "TOTAL_AMOUNT": 590028.90
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-29T03:44:19",
    "PRICE": 344.25,
    "QUANTITY": 8300,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "836",
    "TOTAL_AMOUNT": 2857275.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-18T02:32:31",
    "PRICE": 818.02,
    "QUANTITY": 1227,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "837",
    "TOTAL_AMOUNT": 1003710.56
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-14T04:56:57",
    "PRICE": 276.25,
    "QUANTITY": 8504,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "838",
    "TOTAL_AMOUNT": 2349230.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-13T12:42:34",
    "PRICE": 256.36,
    "QUANTITY": 1719,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "839",
    "TOTAL_AMOUNT": 440682.81
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-24T18:50:15",
    "PRICE": 599.17,
    "QUANTITY": 6296,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "840",
    "TOTAL_AMOUNT": 3772374.21
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-23T07:43:28",
    "PRICE": 149.71,
    "QUANTITY": 2667,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "841",
    "TOTAL_AMOUNT": 399276.59
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-23T09:13:09",
    "PRICE": 654.01,
    "QUANTITY": 3358,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "842",
    "TOTAL_AMOUNT": 2196165.61
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-10T07:09:27",
    "PRICE": 135.53,
    "QUANTITY": 1634,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "843",
    "TOTAL_AMOUNT": 221456.02
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-04T09:18:04",
    "PRICE": 159.19,
    "QUANTITY": 9675,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "844",
    "TOTAL_AMOUNT": 1540163.27
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-19T18:39:10",
    "PRICE": 559.34,
    "QUANTITY": 4907,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "845",
    "TOTAL_AMOUNT": 2744681.51
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-30T18:35:04",
    "PRICE": 532.15,
    "QUANTITY": 9742,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "846",
    "TOTAL_AMOUNT": 5184205.54
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-27T10:26:08",
    "PRICE": 559.00,
    "QUANTITY": 2076,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "847",
    "TOTAL_AMOUNT": 1160484.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-04T06:16:40",
    "PRICE": 428.30,
    "QUANTITY": 3942,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "848",
    "TOTAL_AMOUNT": 1688358.55
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-28T15:46:24",
    "PRICE": 154.82,
    "QUANTITY": 3581,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "849",
    "TOTAL_AMOUNT": 554410.45
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-06T14:58:27",
    "PRICE": 473.25,
    "QUANTITY": 5542,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "850",
    "TOTAL_AMOUNT": 2622751.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-26T16:48:36",
    "PRICE": 916.54,
    "QUANTITY": 4527,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "851",
    "TOTAL_AMOUNT": 4149176.48
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-22T01:48:06",
    "PRICE": 292.17,
    "QUANTITY": 4453,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "852",
    "TOTAL_AMOUNT": 1301033.07
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-29T07:06:13",
    "PRICE": 971.28,
    "QUANTITY": 7300,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "853",
    "TOTAL_AMOUNT": 7090344.21
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-25T08:49:06",
    "PRICE": 110.78,
    "QUANTITY": 8814,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "854",
    "TOTAL_AMOUNT": 976414.91
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-28T06:19:09",
    "PRICE": 238.23,
    "QUANTITY": 874,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "855",
    "TOTAL_AMOUNT": 208213.02
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-02T18:26:56",
    "PRICE": 306.98,
    "QUANTITY": 4789,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "856",
    "TOTAL_AMOUNT": 1470127.27
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-13T19:15:22",
    "PRICE": 446.15,
    "QUANTITY": 3462,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "857",
    "TOTAL_AMOUNT": 1544571.28
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-23T02:55:01",
    "PRICE": 353.76,
    "QUANTITY": 1349,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "858",
    "TOTAL_AMOUNT": 477222.25
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-26T00:23:09",
    "PRICE": 534.28,
    "QUANTITY": 6172,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "859",
    "TOTAL_AMOUNT": 3297576.34
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-01T19:13:29",
    "PRICE": 982.81,
    "QUANTITY": 8969,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "860",
    "TOTAL_AMOUNT": 8814822.87
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-10T21:38:49",
    "PRICE": 887.04,
    "QUANTITY": 3111,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "861",
    "TOTAL_AMOUNT": 2759581.37
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-27T20:54:42",
    "PRICE": 507.01,
    "QUANTITY": 6353,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "862",
    "TOTAL_AMOUNT": 3221034.59
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-23T00:37:40",
    "PRICE": 545.60,
    "QUANTITY": 6464,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "863",
    "TOTAL_AMOUNT": 3526758.24
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-03T14:30:36",
    "PRICE": 802.87,
    "QUANTITY": 9800,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "864",
    "TOTAL_AMOUNT": 7868125.95
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-03T19:28:42",
    "PRICE": 63.02,
    "QUANTITY": 2235,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "865",
    "TOTAL_AMOUNT": 140849.70
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-18T16:24:27",
    "PRICE": 490.04,
    "QUANTITY": 524,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "866",
    "TOTAL_AMOUNT": 256780.96
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-08T15:02:04",
    "PRICE": 251.37,
    "QUANTITY": 6235,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "867",
    "TOTAL_AMOUNT": 1567291.92
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-12T01:33:24",
    "PRICE": 899.76,
    "QUANTITY": 987,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "868",
    "TOTAL_AMOUNT": 888063.13
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-04T06:57:49",
    "PRICE": 435.61,
    "QUANTITY": 2556,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "869",
    "TOTAL_AMOUNT": 1113419.12
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-11T22:37:16",
    "PRICE": 494.25,
    "QUANTITY": 2292,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "870",
    "TOTAL_AMOUNT": 1132821.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-21T03:57:49",
    "PRICE": 925.77,
    "QUANTITY": 6072,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "871",
    "TOTAL_AMOUNT": 5621275.56
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-03T02:02:44",
    "PRICE": 86.59,
    "QUANTITY": 3093,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "872",
    "TOTAL_AMOUNT": 267822.86
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-27T21:19:03",
    "PRICE": 744.61,
    "QUANTITY": 9825,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "873",
    "TOTAL_AMOUNT": 7315793.11
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-18T09:56:44",
    "PRICE": 900.85,
    "QUANTITY": 350,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "874",
    "TOTAL_AMOUNT": 315297.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-06T06:46:12",
    "PRICE": 104.41,
    "QUANTITY": 4300,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "875",
    "TOTAL_AMOUNT": 448963.02
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-05T06:15:55",
    "PRICE": 79.00,
    "QUANTITY": 3649,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "876",
    "TOTAL_AMOUNT": 288271.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-01T19:28:32",
    "PRICE": 534.13,
    "QUANTITY": 4862,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "877",
    "TOTAL_AMOUNT": 2596940.08
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-16T20:15:39",
    "PRICE": 662.17,
    "QUANTITY": 1180,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "878",
    "TOTAL_AMOUNT": 781360.58
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-26T17:52:28",
    "PRICE": 917.68,
    "QUANTITY": 4332,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "879",
    "TOTAL_AMOUNT": 3975389.73
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-31T18:04:15",
    "PRICE": 722.13,
    "QUANTITY": 559,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "880",
    "TOTAL_AMOUNT": 403670.67
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-23T13:03:19",
    "PRICE": 540.36,
    "QUANTITY": 3212,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "881",
    "TOTAL_AMOUNT": 1735636.27
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-22T22:15:01",
    "PRICE": 494.02,
    "QUANTITY": 1974,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "882",
    "TOTAL_AMOUNT": 975195.46
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-22T23:14:07",
    "PRICE": 63.08,
    "QUANTITY": 1585,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "883",
    "TOTAL_AMOUNT": 99981.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-14T06:34:33",
    "PRICE": 216.33,
    "QUANTITY": 6742,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "884",
    "TOTAL_AMOUNT": 1458496.87
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-30T18:32:16",
    "PRICE": 944.04,
    "QUANTITY": 8189,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "885",
    "TOTAL_AMOUNT": 7730743.38
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-28T02:35:35",
    "PRICE": 800.74,
    "QUANTITY": 3366,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "886",
    "TOTAL_AMOUNT": 2695290.81
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-10T05:20:43",
    "PRICE": 397.30,
    "QUANTITY": 3284,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "887",
    "TOTAL_AMOUNT": 1304733.16
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-11T06:04:17",
    "PRICE": 263.57,
    "QUANTITY": 1407,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "888",
    "TOTAL_AMOUNT": 370843.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-27T01:45:44",
    "PRICE": 230.59,
    "QUANTITY": 1287,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "889",
    "TOTAL_AMOUNT": 296769.33
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-22T18:09:10",
    "PRICE": 986.48,
    "QUANTITY": 1148,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "890",
    "TOTAL_AMOUNT": 1132479.02
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-19T07:24:02",
    "PRICE": 961.80,
    "QUANTITY": 5227,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "891",
    "TOTAL_AMOUNT": 5027328.54
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-18T17:54:30",
    "PRICE": 834.65,
    "QUANTITY": 3346,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "892",
    "TOTAL_AMOUNT": 2792738.98
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-17T08:16:23",
    "PRICE": 333.04,
    "QUANTITY": 7960,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "893",
    "TOTAL_AMOUNT": 2650998.47
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-27T19:51:23",
    "PRICE": 910.73,
    "QUANTITY": 1392,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "894",
    "TOTAL_AMOUNT": 1267736.13
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-08T17:22:25",
    "PRICE": 611.20,
    "QUANTITY": 3716,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "895",
    "TOTAL_AMOUNT": 2271219.25
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-18T18:21:04",
    "PRICE": 208.21,
    "QUANTITY": 287,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "896",
    "TOTAL_AMOUNT": 59756.27
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-12T18:59:53",
    "PRICE": 984.03,
    "QUANTITY": 6154,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "897",
    "TOTAL_AMOUNT": 6055720.80
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-22T03:12:55",
    "PRICE": 680.91,
    "QUANTITY": 5401,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "898",
    "TOTAL_AMOUNT": 3677594.76
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-28T07:32:00",
    "PRICE": 960.26,
    "QUANTITY": 3371,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "899",
    "TOTAL_AMOUNT": 3237036.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-27T11:56:43",
    "PRICE": 633.89,
    "QUANTITY": 9670,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "900",
    "TOTAL_AMOUNT": 6129716.44
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-25T01:17:11",
    "PRICE": 109.53,
    "QUANTITY": 4400,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "901",
    "TOTAL_AMOUNT": 481931.99
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-02T02:52:04",
    "PRICE": 690.65,
    "QUANTITY": 7903,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "902",
    "TOTAL_AMOUNT": 5458207.14
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-22T15:45:59",
    "PRICE": 552.96,
    "QUANTITY": 7834,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "903",
    "TOTAL_AMOUNT": 4331888.81
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-14T19:00:55",
    "PRICE": 751.63,
    "QUANTITY": 7195,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "904",
    "TOTAL_AMOUNT": 5407977.89
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-05T01:22:50",
    "PRICE": 625.15,
    "QUANTITY": 3967,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "905",
    "TOTAL_AMOUNT": 2479970.15
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-25T13:16:46",
    "PRICE": 294.05,
    "QUANTITY": 1967,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "906",
    "TOTAL_AMOUNT": 578396.33
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-30T12:52:24",
    "PRICE": 777.73,
    "QUANTITY": 3540,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "907",
    "TOTAL_AMOUNT": 2753164.13
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-05T14:08:43",
    "PRICE": 618.59,
    "QUANTITY": 1280,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "908",
    "TOTAL_AMOUNT": 791795.23
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-10T06:01:02",
    "PRICE": 481.04,
    "QUANTITY": 3454,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "909",
    "TOTAL_AMOUNT": 1661512.19
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-14T22:55:54",
    "PRICE": 449.61,
    "QUANTITY": 8405,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "910",
    "TOTAL_AMOUNT": 3778971.93
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-01T10:36:47",
    "PRICE": 411.01,
    "QUANTITY": 361,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "911",
    "TOTAL_AMOUNT": 148374.61
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-16T01:39:29",
    "PRICE": 852.53,
    "QUANTITY": 1448,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "912",
    "TOTAL_AMOUNT": 1234463.48
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-13T01:47:31",
    "PRICE": 892.03,
    "QUANTITY": 1317,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "913",
    "TOTAL_AMOUNT": 1174803.55
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-17T15:19:52",
    "PRICE": 653.69,
    "QUANTITY": 4028,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "914",
    "TOTAL_AMOUNT": 2633063.33
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-20T06:45:34",
    "PRICE": 573.25,
    "QUANTITY": 9668,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "915",
    "TOTAL_AMOUNT": 5542181.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-14T07:24:09",
    "PRICE": 131.09,
    "QUANTITY": 1467,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "916",
    "TOTAL_AMOUNT": 192309.02
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-31T03:51:35",
    "PRICE": 306.64,
    "QUANTITY": 1556,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "917",
    "TOTAL_AMOUNT": 477131.86
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-15T03:11:08",
    "PRICE": 207.83,
    "QUANTITY": 9344,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "918",
    "TOTAL_AMOUNT": 1941963.54
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-31T20:03:17",
    "PRICE": 497.95,
    "QUANTITY": 6899,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "919",
    "TOTAL_AMOUNT": 3435357.13
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-30T23:16:44",
    "PRICE": 993.58,
    "QUANTITY": 6154,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "920",
    "TOTAL_AMOUNT": 6114491.43
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-28T14:30:09",
    "PRICE": 957.06,
    "QUANTITY": 4249,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "921",
    "TOTAL_AMOUNT": 4066547.93
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-11T09:17:40",
    "PRICE": 198.52,
    "QUANTITY": 3368,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "922",
    "TOTAL_AMOUNT": 668615.37
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-23T05:16:34",
    "PRICE": 443.60,
    "QUANTITY": 6742,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "923",
    "TOTAL_AMOUNT": 2990751.24
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-18T15:59:11",
    "PRICE": 432.95,
    "QUANTITY": 752,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "924",
    "TOTAL_AMOUNT": 325578.41
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-11-09T12:15:07",
    "PRICE": 181.53,
    "QUANTITY": 8375,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "925",
    "TOTAL_AMOUNT": 1520313.74
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-28T18:56:00",
    "PRICE": 982.96,
    "QUANTITY": 8013,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "926",
    "TOTAL_AMOUNT": 7876458.66
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-11T10:05:15",
    "PRICE": 142.75,
    "QUANTITY": 8304,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "927",
    "TOTAL_AMOUNT": 1185396.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-25T10:15:45",
    "PRICE": 142.09,
    "QUANTITY": 2382,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "928",
    "TOTAL_AMOUNT": 338458.37
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-13T05:44:01",
    "PRICE": 238.03,
    "QUANTITY": 5498,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "929",
    "TOTAL_AMOUNT": 1308688.93
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-12T12:09:50",
    "PRICE": 635.96,
    "QUANTITY": 3198,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "930",
    "TOTAL_AMOUNT": 2033800.15
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-12T23:13:24",
    "PRICE": 728.08,
    "QUANTITY": 8776,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "931",
    "TOTAL_AMOUNT": 6389630.23
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-02T20:58:55",
    "PRICE": 578.17,
    "QUANTITY": 1250,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "932",
    "TOTAL_AMOUNT": 722712.48
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-08T12:55:03",
    "PRICE": 56.79,
    "QUANTITY": 2014,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "933",
    "TOTAL_AMOUNT": 114375.06
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-11T04:05:56",
    "PRICE": 478.12,
    "QUANTITY": 2282,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "934",
    "TOTAL_AMOUNT": 1091069.83
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-28T23:30:04",
    "PRICE": 575.55,
    "QUANTITY": 5250,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "935",
    "TOTAL_AMOUNT": 3021637.44
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-14T02:48:37",
    "PRICE": 56.14,
    "QUANTITY": 3746,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "936",
    "TOTAL_AMOUNT": 210300.44
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-07T07:39:12",
    "PRICE": 319.66,
    "QUANTITY": 3727,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "937",
    "TOTAL_AMOUNT": 1191372.83
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-20T00:00:45",
    "PRICE": 647.57,
    "QUANTITY": 7962,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "938",
    "TOTAL_AMOUNT": 5155952.40
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-30T01:43:56",
    "PRICE": 568.60,
    "QUANTITY": 9381,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "939",
    "TOTAL_AMOUNT": 5334036.37
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-25T11:16:19",
    "PRICE": 917.99,
    "QUANTITY": 7351,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "940",
    "TOTAL_AMOUNT": 6748144.42
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-04T21:32:45",
    "PRICE": 705.02,
    "QUANTITY": 7199,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "941",
    "TOTAL_AMOUNT": 5075439.12
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-02T12:47:55",
    "PRICE": 152.35,
    "QUANTITY": 2190,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "942",
    "TOTAL_AMOUNT": 333646.51
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-01T09:39:45",
    "PRICE": 362.58,
    "QUANTITY": 2217,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "943",
    "TOTAL_AMOUNT": 803839.83
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-11-07T17:36:18",
    "PRICE": 369.31,
    "QUANTITY": 6665,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "944",
    "TOTAL_AMOUNT": 2461451.13
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-01T14:32:00",
    "PRICE": 547.27,
    "QUANTITY": 330,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "945",
    "TOTAL_AMOUNT": 180599.11
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-18T16:55:29",
    "PRICE": 297.75,
    "QUANTITY": 5629,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "946",
    "TOTAL_AMOUNT": 1676034.75
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-30T04:32:04",
    "PRICE": 169.31,
    "QUANTITY": 5732,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "947",
    "TOTAL_AMOUNT": 970484.91
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-25T19:32:22",
    "PRICE": 588.15,
    "QUANTITY": 3218,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "948",
    "TOTAL_AMOUNT": 1892666.78
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-03T07:55:00",
    "PRICE": 882.99,
    "QUANTITY": 1476,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "949",
    "TOTAL_AMOUNT": 1303293.23
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-21T04:20:00",
    "PRICE": 571.72,
    "QUANTITY": 982,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "950",
    "TOTAL_AMOUNT": 561429.01
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-17T12:44:04",
    "PRICE": 399.55,
    "QUANTITY": 9762,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "951",
    "TOTAL_AMOUNT": 3900406.98
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-07T16:05:29",
    "PRICE": 709.55,
    "QUANTITY": 322,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "952",
    "TOTAL_AMOUNT": 228475.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-24T09:16:54",
    "PRICE": 885.62,
    "QUANTITY": 9459,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "953",
    "TOTAL_AMOUNT": 8377079.53
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-16T15:33:05",
    "PRICE": 844.80,
    "QUANTITY": 4363,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "954",
    "TOTAL_AMOUNT": 3685862.35
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-05T07:03:26",
    "PRICE": 442.53,
    "QUANTITY": 3550,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "955",
    "TOTAL_AMOUNT": 1570981.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-11T06:32:31",
    "PRICE": 632.92,
    "QUANTITY": 2537,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "956",
    "TOTAL_AMOUNT": 1605718.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-13T18:44:50",
    "PRICE": 542.23,
    "QUANTITY": 3432,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "957",
    "TOTAL_AMOUNT": 1860933.29
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-15T23:55:38",
    "PRICE": 870.44,
    "QUANTITY": 5937,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "958",
    "TOTAL_AMOUNT": 5167802.29
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-23T18:21:22",
    "PRICE": 533.78,
    "QUANTITY": 5022,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "959",
    "TOTAL_AMOUNT": 2680643.31
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-18T08:22:26",
    "PRICE": 621.75,
    "QUANTITY": 7945,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "960",
    "TOTAL_AMOUNT": 4939803.75
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-04T04:41:22",
    "PRICE": 302.21,
    "QUANTITY": 8855,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "961",
    "TOTAL_AMOUNT": 2676069.47
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-17T01:56:54",
    "PRICE": 330.60,
    "QUANTITY": 9790,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "962",
    "TOTAL_AMOUNT": 3236574.06
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-14T04:00:50",
    "PRICE": 453.39,
    "QUANTITY": 8850,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "963",
    "TOTAL_AMOUNT": 4012501.63
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-04T08:10:53",
    "PRICE": 700.83,
    "QUANTITY": 6927,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "964",
    "TOTAL_AMOUNT": 4854649.53
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-31T00:26:22",
    "PRICE": 187.27,
    "QUANTITY": 2332,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "965",
    "TOTAL_AMOUNT": 436713.65
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-01T06:49:45",
    "PRICE": 523.29,
    "QUANTITY": 4519,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "966",
    "TOTAL_AMOUNT": 2364747.41
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-25T00:07:04",
    "PRICE": 232.23,
    "QUANTITY": 5659,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "967",
    "TOTAL_AMOUNT": 1314189.55
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-12T23:29:51",
    "PRICE": 453.05,
    "QUANTITY": 5070,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "968",
    "TOTAL_AMOUNT": 2296963.44
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-20T07:06:06",
    "PRICE": 543.63,
    "QUANTITY": 4098,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "969",
    "TOTAL_AMOUNT": 2227795.76
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-14T05:37:58",
    "PRICE": 516.07,
    "QUANTITY": 5199,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "970",
    "TOTAL_AMOUNT": 2683047.97
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-26T08:00:02",
    "PRICE": 500.67,
    "QUANTITY": 1407,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "971",
    "TOTAL_AMOUNT": 704442.71
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-30T13:00:44",
    "PRICE": 123.39,
    "QUANTITY": 8208,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "972",
    "TOTAL_AMOUNT": 1012785.11
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-20T23:36:04",
    "PRICE": 875.48,
    "QUANTITY": 1454,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "973",
    "TOTAL_AMOUNT": 1272947.89
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-03T17:13:46",
    "PRICE": 473.06,
    "QUANTITY": 4570,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "974",
    "TOTAL_AMOUNT": 2161884.19
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-03T10:35:20",
    "PRICE": 333.18,
    "QUANTITY": 3097,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "975",
    "TOTAL_AMOUNT": 1031858.44
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-09-02T03:26:34",
    "PRICE": 627.02,
    "QUANTITY": 8527,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "976",
    "TOTAL_AMOUNT": 5346599.71
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-25T20:36:11",
    "PRICE": 630.77,
    "QUANTITY": 2340,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "977",
    "TOTAL_AMOUNT": 1476001.85
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-22T11:49:37",
    "PRICE": 461.48,
    "QUANTITY": 733,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "978",
    "TOTAL_AMOUNT": 338264.85
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-20T14:35:56",
    "PRICE": 388.08,
    "QUANTITY": 1176,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "979",
    "TOTAL_AMOUNT": 456382.06
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-11T19:05:03",
    "PRICE": 79.57,
    "QUANTITY": 2573,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "980",
    "TOTAL_AMOUNT": 204733.61
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-14T15:04:05",
    "PRICE": 322.83,
    "QUANTITY": 9005,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "981",
    "TOTAL_AMOUNT": 2907084.03
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-19T21:22:10",
    "PRICE": 864.20,
    "QUANTITY": 2279,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "982",
    "TOTAL_AMOUNT": 1969511.83
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-31T16:36:32",
    "PRICE": 797.63,
    "QUANTITY": 5753,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "983",
    "TOTAL_AMOUNT": 4588765.42
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-31T02:27:19",
    "PRICE": 324.97,
    "QUANTITY": 1523,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "984",
    "TOTAL_AMOUNT": 494929.31
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-01T07:24:40",
    "PRICE": 522.81,
    "QUANTITY": 3137,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "985",
    "TOTAL_AMOUNT": 1640054.96
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-26T23:20:55",
    "PRICE": 418.93,
    "QUANTITY": 116,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "986",
    "TOTAL_AMOUNT": 48595.88
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-01T12:17:22",
    "PRICE": 899.18,
    "QUANTITY": 3000,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "987",
    "TOTAL_AMOUNT": 2697539.98
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-06T15:32:20",
    "PRICE": 761.76,
    "QUANTITY": 4359,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "988",
    "TOTAL_AMOUNT": 3320511.88
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-24T02:00:45",
    "PRICE": 359.13,
    "QUANTITY": 6294,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "989",
    "TOTAL_AMOUNT": 2260364.25
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-04T05:11:26",
    "PRICE": 96.24,
    "QUANTITY": 9456,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "990",
    "TOTAL_AMOUNT": 910045.42
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-05T13:57:20",
    "PRICE": 492.50,
    "QUANTITY": 3191,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "991",
    "TOTAL_AMOUNT": 1571567.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-18T02:39:37",
    "PRICE": 718.57,
    "QUANTITY": 7306,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "992",
    "TOTAL_AMOUNT": 5249872.47
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-12-26T12:59:07",
    "PRICE": 641.62,
    "QUANTITY": 2036,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "993",
    "TOTAL_AMOUNT": 1306338.31
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-13T17:38:49",
    "PRICE": 843.55,
    "QUANTITY": 5739,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "994",
    "TOTAL_AMOUNT": 4841133.38
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-08-27T10:56:28",
    "PRICE": 693.33,
    "QUANTITY": 929,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "995",
    "TOTAL_AMOUNT": 644103.59
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-10T20:01:43",
    "PRICE": 666.91,
    "QUANTITY": 181,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "996",
    "TOTAL_AMOUNT": 120710.71
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-04T14:03:33",
    "PRICE": 466.34,
    "QUANTITY": 6961,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "997",
    "TOTAL_AMOUNT": 3246192.71
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-09T01:51:18",
    "PRICE": 77.34,
    "QUANTITY": 7616,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "998",
    "TOTAL_AMOUNT": 589021.41
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-18T06:20:41",
    "PRICE": 907.72,
    "QUANTITY": 5333,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "999",
    "TOTAL_AMOUNT": 4840870.60
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-09T19:00:00",
    "PRICE": 662.67,
    "QUANTITY": 628,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "1000",
    "TOTAL_AMOUNT": 416156.75
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-15T11:04:26",
    "PRICE": 190.59,
    "QUANTITY": 7351,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1001",
    "TOTAL_AMOUNT": 1401027.06
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-12T03:53:39",
    "PRICE": 328.18,
    "QUANTITY": 4394,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "1002",
    "TOTAL_AMOUNT": 1442022.89
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-04T03:00:59",
    "PRICE": 564.18,
    "QUANTITY": 6429,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1003",
    "TOTAL_AMOUNT": 3627113.17
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-24T10:21:52",
    "PRICE": 715.32,
    "QUANTITY": 623,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "1004",
    "TOTAL_AMOUNT": 445644.36
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-02T05:18:27",
    "PRICE": 327.97,
    "QUANTITY": 6897,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "1005",
    "TOTAL_AMOUNT": 2262009.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-19T11:53:56",
    "PRICE": 655.74,
    "QUANTITY": 2551,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1006",
    "TOTAL_AMOUNT": 1672792.72
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-25T11:56:48",
    "PRICE": 566.49,
    "QUANTITY": 259,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "1007",
    "TOTAL_AMOUNT": 146720.91
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-04T23:14:16",
    "PRICE": 327.83,
    "QUANTITY": 9281,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "1008",
    "TOTAL_AMOUNT": 3042590.11
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-22T02:48:02",
    "PRICE": 698.20,
    "QUANTITY": 4314,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1009",
    "TOTAL_AMOUNT": 3012034.85
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-22T07:07:57",
    "PRICE": 56.40,
    "QUANTITY": 6651,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "1010",
    "TOTAL_AMOUNT": 375116.41
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-19T05:49:28",
    "PRICE": 136.43,
    "QUANTITY": 4145,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "1011",
    "TOTAL_AMOUNT": 565502.32
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-18T02:01:27",
    "PRICE": 290.14,
    "QUANTITY": 1298,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "1012",
    "TOTAL_AMOUNT": 376601.74
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-14T20:46:02",
    "PRICE": 336.60,
    "QUANTITY": 779,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "1013",
    "TOTAL_AMOUNT": 262211.40
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-04T11:33:15",
    "PRICE": 682.13,
    "QUANTITY": 2032,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1014",
    "TOTAL_AMOUNT": 1386088.17
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-18T03:20:52",
    "PRICE": 609.70,
    "QUANTITY": 9582,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "1015",
    "TOTAL_AMOUNT": 5842145.52
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-24T02:35:02",
    "PRICE": 600.53,
    "QUANTITY": 8533,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "1016",
    "TOTAL_AMOUNT": 5124322.74
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-23T03:34:55",
    "PRICE": 743.04,
    "QUANTITY": 9485,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "1017",
    "TOTAL_AMOUNT": 7047734.19
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-30T17:23:23",
    "PRICE": 441.93,
    "QUANTITY": 5096,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "1018",
    "TOTAL_AMOUNT": 2252075.24
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-21T10:11:04",
    "PRICE": 565.91,
    "QUANTITY": 2097,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1019",
    "TOTAL_AMOUNT": 1186713.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-07-01T20:22:05",
    "PRICE": 376.66,
    "QUANTITY": 1253,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "1020",
    "TOTAL_AMOUNT": 471954.98
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-26T23:45:21",
    "PRICE": 502.76,
    "QUANTITY": 7717,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "1021",
    "TOTAL_AMOUNT": 3879799.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-21T20:19:09",
    "PRICE": 164.94,
    "QUANTITY": 984,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1022",
    "TOTAL_AMOUNT": 162300.96
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-10-28T16:48:28",
    "PRICE": 942.93,
    "QUANTITY": 6204,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "1023",
    "TOTAL_AMOUNT": 5849937.67
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-08T22:02:30",
    "PRICE": 821.68,
    "QUANTITY": 6409,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "1024",
    "TOTAL_AMOUNT": 5266147.07
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-17T01:03:49",
    "PRICE": 644.39,
    "QUANTITY": 4223,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "1025",
    "TOTAL_AMOUNT": 2721259.03
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-02T23:19:29",
    "PRICE": 295.38,
    "QUANTITY": 6684,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "1026",
    "TOTAL_AMOUNT": 1974319.95
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-09T06:59:03",
    "PRICE": 727.93,
    "QUANTITY": 1390,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "1027",
    "TOTAL_AMOUNT": 1011822.69
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-11-02T23:33:07",
    "PRICE": 281.67,
    "QUANTITY": 1044,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "1028",
    "TOTAL_AMOUNT": 294063.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-15T00:38:36",
    "PRICE": 431.27,
    "QUANTITY": 2942,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1029",
    "TOTAL_AMOUNT": 1268796.31
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-30T04:04:24",
    "PRICE": 983.64,
    "QUANTITY": 4731,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "1030",
    "TOTAL_AMOUNT": 4653600.91
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-12T10:16:03",
    "PRICE": 621.37,
    "QUANTITY": 4918,
    "STATUS": "Filled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1031",
    "TOTAL_AMOUNT": 3055897.64
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-02T01:37:51",
    "PRICE": 706.41,
    "QUANTITY": 5811,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "1032",
    "TOTAL_AMOUNT": 4104948.35
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-12-28T11:28:39",
    "PRICE": 460.16,
    "QUANTITY": 8734,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "1033",
    "TOTAL_AMOUNT": 4019037.47
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-05-27T20:13:46",
    "PRICE": 200.99,
    "QUANTITY": 4422,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "1034",
    "TOTAL_AMOUNT": 888777.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-05T17:48:02",
    "PRICE": 597.89,
    "QUANTITY": 6536,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "1035",
    "TOTAL_AMOUNT": 3907809.14
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-22T22:47:11",
    "PRICE": 607.34,
    "QUANTITY": 1109,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "1036",
    "TOTAL_AMOUNT": 673540.09
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-28T09:53:37",
    "PRICE": 666.99,
    "QUANTITY": 7352,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "1037",
    "TOTAL_AMOUNT": 4903710.41
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-14T18:58:03",
    "PRICE": 56.09,
    "QUANTITY": 5815,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "1038",
    "TOTAL_AMOUNT": 326163.35
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-11T10:36:40",
    "PRICE": 928.07,
    "QUANTITY": 7557,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "1039",
    "TOTAL_AMOUNT": 7013425.05
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-01-31T21:19:53",
    "PRICE": 91.89,
    "QUANTITY": 4568,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "1040",
    "TOTAL_AMOUNT": 419753.52
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-15T11:40:16",
    "PRICE": 342.88,
    "QUANTITY": 6425,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "1041",
    "TOTAL_AMOUNT": 2203004.03
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-06-27T13:52:48",
    "PRICE": 411.06,
    "QUANTITY": 784,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "1042",
    "TOTAL_AMOUNT": 322271.04
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-29T08:57:09",
    "PRICE": 480.80,
    "QUANTITY": 4415,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "1043",
    "TOTAL_AMOUNT": 2122731.95
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-04T04:36:18",
    "PRICE": 160.98,
    "QUANTITY": 3091,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "1044",
    "TOTAL_AMOUNT": 497589.17
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-08T16:14:02",
    "PRICE": 162.02,
    "QUANTITY": 5676,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "1045",
    "TOTAL_AMOUNT": 919625.54
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-13T06:17:51",
    "PRICE": 247.85,
    "QUANTITY": 385,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "1046",
    "TOTAL_AMOUNT": 95422.25
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-23T14:47:41",
    "PRICE": 462.45,
    "QUANTITY": 4490,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "1047",
    "TOTAL_AMOUNT": 2076400.55
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-28T12:09:43",
    "PRICE": 712.68,
    "QUANTITY": 8514,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "1048",
    "TOTAL_AMOUNT": 6067757.46
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-14T15:09:20",
    "PRICE": 658.54,
    "QUANTITY": 7819,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "1049",
    "TOTAL_AMOUNT": 5149124.09
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-08-02T05:01:05",
    "PRICE": 498.44,
    "QUANTITY": 490,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "1050",
    "TOTAL_AMOUNT": 244235.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-11T15:47:40",
    "PRICE": 221.63,
    "QUANTITY": 5620,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "1051",
    "TOTAL_AMOUNT": 1245560.63
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-10-23T00:34:48",
    "PRICE": 697.06,
    "QUANTITY": 2384,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "1052",
    "TOTAL_AMOUNT": 1661791.03
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-07T12:22:07",
    "PRICE": 263.93,
    "QUANTITY": 4353,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "1053",
    "TOTAL_AMOUNT": 1148887.26
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-23T19:33:51",
    "PRICE": 822.99,
    "QUANTITY": 2219,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "1054",
    "TOTAL_AMOUNT": 1826214.79
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-22T20:55:31",
    "PRICE": 495.69,
    "QUANTITY": 1997,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "1055",
    "TOTAL_AMOUNT": 989892.93
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 211.22,
    "QUANTITY": 9539,
    "STATUS": "Pending",
    "SYMBOL": "IWM",
    "TX_NUMBER": "1056",
    "TOTAL_AMOUNT": 2014827.59
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-09-04T14:03:28",
    "PRICE": 653.84,
    "QUANTITY": 5328,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1057",
    "TOTAL_AMOUNT": 3483659.66
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-02-25T15:43:40",
    "PRICE": 949.42,
    "QUANTITY": 7136,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1058",
    "TOTAL_AMOUNT": 6775061.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-10T00:01:07",
    "PRICE": 166.02,
    "QUANTITY": 2328,
    "STATUS": "Canceled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "1059",
    "TOTAL_AMOUNT": 386494.57
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-05T08:51:22",
    "PRICE": 629.25,
    "QUANTITY": 6328,
    "STATUS": "Canceled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "1060",
    "TOTAL_AMOUNT": 3981894.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-06-01T23:48:56",
    "PRICE": 240.28,
    "QUANTITY": 2508,
    "STATUS": "Canceled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "1061",
    "TOTAL_AMOUNT": 602622.24
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-01-11T11:14:26",
    "PRICE": 182.55,
    "QUANTITY": 7093,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "1062",
    "TOTAL_AMOUNT": 1294827.17
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2017-11-21T15:49:07",
    "PRICE": 153.08,
    "QUANTITY": 8665,
    "STATUS": "Filled",
    "SYMBOL": "IWM",
    "TX_NUMBER": "1063",
    "TOTAL_AMOUNT": 1326438.22
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-05-27T02:34:53",
    "PRICE": 317.55,
    "QUANTITY": 1617,
    "STATUS": "Canceled",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1064",
    "TOTAL_AMOUNT": 513478.33
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-01-27T00:00:00",
    "PRICE": 171.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "LHH",
    "TX_NUMBER": "1065",
    "TOTAL_AMOUNT": 17100.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-01-27T00:00:00",
    "PRICE": 171.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "LHH",
    "TX_NUMBER": "1066",
    "TOTAL_AMOUNT": 17100.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-01-27T00:00:00",
    "PRICE": 171.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "LHH",
    "TX_NUMBER": "1067",
    "TOTAL_AMOUNT": 17100.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-01-28T00:00:00",
    "PRICE": 171.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "LHH",
    "TX_NUMBER": "1068",
    "TOTAL_AMOUNT": 17100.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-03T01:57:18",
    "PRICE": 250.00,
    "QUANTITY": 25,
    "STATUS": "Pending",
    "SYMBOL": "JJJ",
    "TX_NUMBER": "1069",
    "TOTAL_AMOUNT": 6250.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-03T01:57:48",
    "PRICE": 250.00,
    "QUANTITY": 25,
    "STATUS": "Pending",
    "SYMBOL": "JJJ",
    "TX_NUMBER": "1070",
    "TOTAL_AMOUNT": 6250.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-03T02:12:39",
    "PRICE": 250.00,
    "QUANTITY": 25,
    "STATUS": "Pending",
    "SYMBOL": "JJJ",
    "TX_NUMBER": "1071",
    "TOTAL_AMOUNT": 6250.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-03T02:14:47",
    "PRICE": 250.00,
    "QUANTITY": 25,
    "STATUS": "Pending",
    "SYMBOL": "JLL",
    "TX_NUMBER": "1072",
    "TOTAL_AMOUNT": 6250.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-03T23:37:04",
    "PRICE": 99.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "JLL",
    "TX_NUMBER": "1073",
    "TOTAL_AMOUNT": 990.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-02-03T23:38:23",
    "PRICE": 11.00,
    "QUANTITY": 88,
    "STATUS": "Pending",
    "SYMBOL": "JLL",
    "TX_NUMBER": "1074",
    "TOTAL_AMOUNT": 968.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-03T23:42:26",
    "PRICE": 250.00,
    "QUANTITY": 25,
    "STATUS": "Pending",
    "SYMBOL": "JLL",
    "TX_NUMBER": "1075",
    "TOTAL_AMOUNT": 6250.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-02-03T23:42:40",
    "PRICE": 11.00,
    "QUANTITY": 88,
    "STATUS": "Pending",
    "SYMBOL": "JLL",
    "TX_NUMBER": "1076",
    "TOTAL_AMOUNT": 968.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-04T00:21:44",
    "PRICE": 11.00,
    "QUANTITY": 88,
    "STATUS": "Pending",
    "SYMBOL": "JLL",
    "TX_NUMBER": "1077",
    "TOTAL_AMOUNT": 968.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-02-04T00:24:16",
    "PRICE": 22.00,
    "QUANTITY": 77,
    "STATUS": "Pending",
    "SYMBOL": "JLL",
    "TX_NUMBER": "1078",
    "TOTAL_AMOUNT": 1694.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-08T02:11:42",
    "PRICE": 30.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "",
    "TX_NUMBER": "1079",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-08T02:37:17",
    "PRICE": 30.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "",
    "TX_NUMBER": "1080",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-08T02:39:58",
    "PRICE": 30.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "",
    "TX_NUMBER": "1081",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-08T02:42:03",
    "PRICE": 20.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "XYZ",
    "TX_NUMBER": "1082",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-08T12:06:20",
    "PRICE": 20.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "XYZ",
    "TX_NUMBER": "1083",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-08T12:20:42",
    "PRICE": 20.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "XYZ",
    "TX_NUMBER": "1084",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-08T15:01:24",
    "PRICE": 20.00,
    "QUANTITY": 500,
    "STATUS": "Pending",
    "SYMBOL": "ABC123",
    "TX_NUMBER": "1085",
    "TOTAL_AMOUNT": 10000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-19T17:42:03",
    "PRICE": 20.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "XYZ",
    "TX_NUMBER": "1086",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-19T17:43:27",
    "PRICE": 20.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "XYZ",
    "TX_NUMBER": "1087",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-19T17:45:03",
    "PRICE": 20.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "XYZ",
    "TX_NUMBER": "1088",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-19T17:46:03",
    "PRICE": 20.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "XYZ",
    "TX_NUMBER": "1089",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-19T17:49:21",
    "PRICE": 20.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "XYZ",
    "TX_NUMBER": "1090",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-19T17:51:45",
    "PRICE": 20.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "XYZ",
    "TX_NUMBER": "1091",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-19T17:53:42",
    "PRICE": 20.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "XYZ",
    "TX_NUMBER": "1092",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-02-19T18:39:20",
    "PRICE": 20.00,
    "QUANTITY": 500,
    "STATUS": "Pending",
    "SYMBOL": "ABC123",
    "TX_NUMBER": "1093",
    "TOTAL_AMOUNT": 10000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-07T13:28:15",
    "PRICE": 1008.00,
    "QUANTITY": 108,
    "STATUS": "Pending",
    "SYMBOL": "IND",
    "TX_NUMBER": "1094",
    "TOTAL_AMOUNT": 108864.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-07T13:36:05",
    "PRICE": 1009.00,
    "QUANTITY": 109,
    "STATUS": "Pending",
    "SYMBOL": "IND",
    "TX_NUMBER": "1095",
    "TOTAL_AMOUNT": 109981.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-07T13:41:02",
    "PRICE": 123.45,
    "QUANTITY": 108,
    "STATUS": "Pending",
    "SYMBOL": "IND",
    "TX_NUMBER": "1096",
    "TOTAL_AMOUNT": 13332.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-07T13:43:41",
    "PRICE": 123.45,
    "QUANTITY": 108,
    "STATUS": "Pending",
    "SYMBOL": "IND",
    "TX_NUMBER": "1097",
    "TOTAL_AMOUNT": 13332.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-07T13:52:43",
    "PRICE": 1234.45,
    "QUANTITY": 1008,
    "STATUS": "Pending",
    "SYMBOL": "MAA",
    "TX_NUMBER": "1098",
    "TOTAL_AMOUNT": 1244325.55
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-07T13:55:05",
    "PRICE": 1234.45,
    "QUANTITY": 1008,
    "STATUS": "Pending",
    "SYMBOL": "MAA",
    "TX_NUMBER": "1099",
    "TOTAL_AMOUNT": 1244325.55
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-07T13:56:36",
    "PRICE": 1234.45,
    "QUANTITY": 1008,
    "STATUS": "Pending",
    "SYMBOL": "MAA",
    "TX_NUMBER": "1100",
    "TOTAL_AMOUNT": 1244325.55
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-07T13:58:18",
    "PRICE": 1234.45,
    "QUANTITY": 1008,
    "STATUS": "Pending",
    "SYMBOL": "MAA",
    "TX_NUMBER": "1101",
    "TOTAL_AMOUNT": 1244325.55
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-07T14:27:49",
    "PRICE": 1234.45,
    "QUANTITY": 1008,
    "STATUS": "Pending",
    "SYMBOL": "MAA",
    "TX_NUMBER": "1102",
    "TOTAL_AMOUNT": 1244325.55
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T18:27:05",
    "PRICE": 12345.50,
    "QUANTITY": 2008,
    "STATUS": "Pending",
    "SYMBOL": "GOA",
    "TX_NUMBER": "1103",
    "TOTAL_AMOUNT": 24789764.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T18:27:05",
    "PRICE": 12345.50,
    "QUANTITY": 2008,
    "STATUS": "Pending",
    "SYMBOL": "GOA",
    "TX_NUMBER": "1104",
    "TOTAL_AMOUNT": 24789764.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T18:32:00",
    "PRICE": 12345.50,
    "QUANTITY": 2008,
    "STATUS": "Pending",
    "SYMBOL": "GOA2",
    "TX_NUMBER": "1105",
    "TOTAL_AMOUNT": 24789764.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T00:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 2008,
    "STATUS": "Pending",
    "SYMBOL": "GOA3",
    "TX_NUMBER": "1106",
    "TOTAL_AMOUNT": 24789764.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T00:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 2008,
    "STATUS": "Pending",
    "SYMBOL": "GOA4",
    "TX_NUMBER": "1107",
    "TOTAL_AMOUNT": 24789764.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 2008,
    "STATUS": "Pending",
    "SYMBOL": "GOA5",
    "TX_NUMBER": "1108",
    "TOTAL_AMOUNT": 24789764.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "GOA6",
    "TX_NUMBER": "1109",
    "TOTAL_AMOUNT": 1234550.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T22:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "GOA7",
    "TX_NUMBER": "1110",
    "TOTAL_AMOUNT": 1234550.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T22:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "GOA8",
    "TX_NUMBER": "1111",
    "TOTAL_AMOUNT": 1234550.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-04T10:48:01",
    "PRICE": 613.80,
    "QUANTITY": 6774,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "1112",
    "TOTAL_AMOUNT": 4157881.12
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T17:20:00",
    "PRICE": 613.80,
    "QUANTITY": 6774,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1113",
    "TOTAL_AMOUNT": 4157881.12
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T17:38:00",
    "PRICE": 123.23,
    "QUANTITY": 200,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1114",
    "TOTAL_AMOUNT": 24646.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T17:38:00",
    "PRICE": 123.23,
    "QUANTITY": 200,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1115",
    "TOTAL_AMOUNT": 24646.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T17:40:00",
    "PRICE": 456.78,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1116",
    "TOTAL_AMOUNT": 56183.94
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T18:05:00",
    "PRICE": 123.23,
    "QUANTITY": 200,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1117",
    "TOTAL_AMOUNT": 24646.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T18:04:00",
    "PRICE": 456.78,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1118",
    "TOTAL_AMOUNT": 56183.94
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T18:04:00",
    "PRICE": 456.78,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1119",
    "TOTAL_AMOUNT": 56183.94
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T22:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "GOA9",
    "TX_NUMBER": "1120",
    "TOTAL_AMOUNT": 1234550.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T22:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "GOA10",
    "TX_NUMBER": "1121",
    "TOTAL_AMOUNT": 1234550.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T22:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "GOA11",
    "TX_NUMBER": "1122",
    "TOTAL_AMOUNT": 1234550.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T18:38:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1123",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T18:38:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1124",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T19:31:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1125",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T19:32:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1126",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T19:35:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1127",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-07T22:36:35",
    "PRICE": 100.00,
    "QUANTITY": 99,
    "STATUS": "Pending",
    "SYMBOL": "FJS",
    "TX_NUMBER": "1128",
    "TOTAL_AMOUNT": 9900.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T19:35:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1129",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T19:35:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1130",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-07T23:02:31",
    "PRICE": 100.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FJS",
    "TX_NUMBER": "1131",
    "TOTAL_AMOUNT": 1000.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T22:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "GOA13",
    "TX_NUMBER": "1132",
    "TOTAL_AMOUNT": 1234550.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T22:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "GOA14",
    "TX_NUMBER": "1133",
    "TOTAL_AMOUNT": 1234550.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA15",
    "TX_NUMBER": "1134",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA16",
    "TX_NUMBER": "1135",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA17",
    "TX_NUMBER": "1136",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA19",
    "TX_NUMBER": "1137",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA20",
    "TX_NUMBER": "1138",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-07T23:41:15",
    "PRICE": 100.00,
    "QUANTITY": 20,
    "STATUS": "Pending",
    "SYMBOL": "FJS",
    "TX_NUMBER": "1139",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA21",
    "TX_NUMBER": "1140",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA22",
    "TX_NUMBER": "1141",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T23:45:46",
    "PRICE": 100.00,
    "QUANTITY": 30,
    "STATUS": "Pending",
    "SYMBOL": "FJS",
    "TX_NUMBER": "1142",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA23",
    "TX_NUMBER": "1143",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA25",
    "TX_NUMBER": "1144",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA26",
    "TX_NUMBER": "1145",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA27",
    "TX_NUMBER": "1146",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA29",
    "TX_NUMBER": "1147",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T21:08:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1148",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T21:12:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1149",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Canceled",
    "SYMBOL": "GOA30",
    "TX_NUMBER": "1150",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA31",
    "TX_NUMBER": "1151",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T21:25:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1152",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T21:29:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1153",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T21:29:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1154",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-04T23:00:00",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA32",
    "TX_NUMBER": "1155",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T01:56:31",
    "PRICE": 100.00,
    "QUANTITY": 122,
    "STATUS": "Pending",
    "SYMBOL": "FJS",
    "TX_NUMBER": "1156",
    "TOTAL_AMOUNT": 12200.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T23:02:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1157",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T23:02:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1158",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T23:02:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1159",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T23:02:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1160",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T23:02:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1161",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T23:56:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1162",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T23:59:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1163",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T23:59:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1164",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T23:59:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1165",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T23:59:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1166",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-07T23:59:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1167",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T00:19:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1168",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T00:19:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1169",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T00:19:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1170",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T00:19:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1171",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T00:19:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1172",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T00:19:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1173",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T00:19:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1174",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T00:19:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1175",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T00:19:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1176",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T00:19:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1177",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T00:19:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1178",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-08T10:20:52",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA34",
    "TX_NUMBER": "1179",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-08T10:24:03",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA35",
    "TX_NUMBER": "1180",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-08T07:31:30",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA36",
    "TX_NUMBER": "1181",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T18:54:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1182",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T18:57:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1183",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T19:05:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1184",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T19:09:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1185",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T19:11:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1186",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T19:26:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1187",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T19:37:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1188",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T19:46:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1189",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T19:48:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1190",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T19:51:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1191",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T19:53:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1192",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T19:56:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1193",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T20:01:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1194",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T20:09:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1195",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T20:12:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1196",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T20:14:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1197",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T20:17:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1198",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T20:19:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1199",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T20:23:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1200",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T20:28:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1201",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T20:32:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1202",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T20:33:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1203",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T20:44:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1204",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T20:46:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1205",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T20:48:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1206",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T20:50:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1207",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-08T20:51:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1208",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-08T21:17:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1209",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T22:22:20",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Canceled",
    "SYMBOL": "GOA31",
    "TX_NUMBER": "1210",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-08T22:22:40",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA36",
    "TX_NUMBER": "1211",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T22:29:47",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA37",
    "TX_NUMBER": "1212",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-09T01:30:06",
    "PRICE": 100.00,
    "QUANTITY": 2019,
    "STATUS": "Pending",
    "SYMBOL": "FJS",
    "TX_NUMBER": "1213",
    "TOTAL_AMOUNT": 201900.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-08T22:32:36",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Canceled",
    "SYMBOL": "GOA31",
    "TX_NUMBER": "1214",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-09T06:52:53",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA37",
    "TX_NUMBER": "1215",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-09T07:00:10",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA40",
    "TX_NUMBER": "1216",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-09T07:00:52",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA37",
    "TX_NUMBER": "1217",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-09T07:01:11",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA37",
    "TX_NUMBER": "1218",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-09T18:51:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1219",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-09T21:17:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1220",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-09T21:17:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1221",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-09T00:00:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1222",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-09T22:49:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1223",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-11T07:04:20",
    "PRICE": 12345.50,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "GOA37",
    "TX_NUMBER": "1224",
    "TOTAL_AMOUNT": 1246895.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-11T17:27:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1225",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-11T17:27:00",
    "PRICE": 4567.89,
    "QUANTITY": 123,
    "STATUS": "Pending",
    "SYMBOL": "ARS",
    "TX_NUMBER": "1226",
    "TOTAL_AMOUNT": 561850.49
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-04-30T16:38:26",
    "PRICE": 8504.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1227",
    "TOTAL_AMOUNT": 17008.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-30T17:00:28",
    "PRICE": 8504.00,
    "QUANTITY": 3,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1228",
    "TOTAL_AMOUNT": 25512.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-30T18:09:56",
    "PRICE": 8504.00,
    "QUANTITY": 3,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1229",
    "TOTAL_AMOUNT": 25512.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-04-30T20:14:38",
    "PRICE": 8504.00,
    "QUANTITY": 3,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1230",
    "TOTAL_AMOUNT": 25512.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-05-01T10:51:26",
    "PRICE": 8504.00,
    "QUANTITY": 3,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1231",
    "TOTAL_AMOUNT": 25512.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-05-01T13:33:41",
    "PRICE": 8504.00,
    "QUANTITY": 3,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1232",
    "TOTAL_AMOUNT": 25512.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-05-01T18:50:48",
    "PRICE": 8505.00,
    "QUANTITY": 3,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1233",
    "TOTAL_AMOUNT": 25515.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-05-02T01:57:44",
    "PRICE": 1985.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1234",
    "TOTAL_AMOUNT": 3970.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-05-02T09:43:05",
    "PRICE": 4000.00,
    "QUANTITY": 3,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1235",
    "TOTAL_AMOUNT": 12000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-05-02T09:46:52",
    "PRICE": 5000.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "USD",
    "TX_NUMBER": "1236",
    "TOTAL_AMOUNT": 10000.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-05-08T15:42:41",
    "PRICE": 1985.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "COP",
    "TX_NUMBER": "1237",
    "TOTAL_AMOUNT": 7940.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-05-08T16:35:42",
    "PRICE": 5000.00,
    "QUANTITY": 3,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "1238",
    "TOTAL_AMOUNT": 15000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 1.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10000",
    "TOTAL_AMOUNT": 6.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-06-02T22:54:59",
    "PRICE": 189.00,
    "QUANTITY": 20,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10001",
    "TOTAL_AMOUNT": 3780.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-06-02T22:56:55",
    "PRICE": 189.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "GGAL",
    "TX_NUMBER": "10002",
    "TOTAL_AMOUNT": 3780.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-06-04T01:45:27",
    "PRICE": 189.00,
    "QUANTITY": 20,
    "STATUS": "Pending",
    "SYMBOL": "GGAL",
    "TX_NUMBER": "10003",
    "TOTAL_AMOUNT": 3780.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-06-04T02:12:45",
    "PRICE": 189.00,
    "QUANTITY": 20,
    "STATUS": "Pending",
    "SYMBOL": "GGAL",
    "TX_NUMBER": "10004",
    "TOTAL_AMOUNT": 3780.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-11T20:21:51",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10005",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T09:12:58",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10006",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T09:17:48",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10007",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T09:21:12",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10008",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T09:29:57",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10009",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T09:36:39",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10010",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T09:38:44",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10011",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T09:41:19",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10012",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T11:06:19",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10013",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T11:17:29",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10014",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T11:28:25",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10015",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T11:29:43",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10016",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T11:43:19",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10017",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T12:20:30",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10018",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T18:09:47",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10019",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T18:10:17",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10020",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T18:11:01",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10021",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T20:16:29",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10022",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T20:18:18",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10023",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T20:18:31",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10024",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T21:27:32",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10025",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T21:30:48",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10026",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T21:33:20",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10027",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T21:36:09",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10028",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T21:36:39",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10029",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T21:37:01",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10030",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T21:44:16",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10031",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-12T21:44:19",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10032",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-13T00:48:23",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10033",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-13T01:09:29",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10034",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-13T16:04:01",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10035",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-18T02:31:00",
    "PRICE": 20.00,
    "QUANTITY": 1111,
    "STATUS": "Pending",
    "SYMBOL": "MAU",
    "TX_NUMBER": "10036",
    "TOTAL_AMOUNT": 22220.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-18T02:31:00",
    "PRICE": 20.00,
    "QUANTITY": 1111,
    "STATUS": "Pending",
    "SYMBOL": "MAU",
    "TX_NUMBER": "10037",
    "TOTAL_AMOUNT": 22220.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-18T03:31:00",
    "PRICE": 20.00,
    "QUANTITY": 2345,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10038",
    "TOTAL_AMOUNT": 46900.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-08T18:40:50",
    "PRICE": 107.85,
    "QUANTITY": 505,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10039",
    "TOTAL_AMOUNT": 54464.25
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-02-09T18:40:50",
    "PRICE": 107.85,
    "QUANTITY": 505,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10040",
    "TOTAL_AMOUNT": 54464.25
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-24T11:14:39",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10041",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-24T15:29:38",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10042",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-06-24T15:31:08",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Canceled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10043",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-08-18T16:50:21",
    "PRICE": 100.00,
    "QUANTITY": 99,
    "STATUS": "Pending",
    "SYMBOL": "TST",
    "TX_NUMBER": "10044",
    "TOTAL_AMOUNT": 9900.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-08-20T00:57:49",
    "PRICE": 99.50,
    "QUANTITY": 15,
    "STATUS": "Pending",
    "SYMBOL": "TST",
    "TX_NUMBER": "10045",
    "TOTAL_AMOUNT": 1492.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-08-21T15:17:22",
    "PRICE": 100.00,
    "QUANTITY": 99,
    "STATUS": "Pending",
    "SYMBOL": "TST",
    "TX_NUMBER": "10046",
    "TOTAL_AMOUNT": 9900.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-08-21T15:19:05",
    "PRICE": 99.50,
    "QUANTITY": 15,
    "STATUS": "Pending",
    "SYMBOL": "TST",
    "TX_NUMBER": "10047",
    "TOTAL_AMOUNT": 1492.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-08-22T23:10:07",
    "PRICE": 100.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "TST",
    "TX_NUMBER": "10048",
    "TOTAL_AMOUNT": 10000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 1.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10049",
    "TOTAL_AMOUNT": 2.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 1.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10050",
    "TOTAL_AMOUNT": 2.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10051",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10052",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10053",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10054",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10055",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10056",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10057",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10058",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10059",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10060",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10061",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10062",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10063",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10064",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10065",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10066",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10067",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10068",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10069",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10070",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10071",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10072",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10073",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10074",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 2.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10075",
    "TOTAL_AMOUNT": 8.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 3.00,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10076",
    "TOTAL_AMOUNT": 15.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 3.00,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10077",
    "TOTAL_AMOUNT": 15.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-09-16T09:44:45",
    "PRICE": 723.06,
    "QUANTITY": 6388,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "10078",
    "TOTAL_AMOUNT": 4618907.26
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-09-16T09:44:45",
    "PRICE": 723.06,
    "QUANTITY": 6388,
    "STATUS": "Pending",
    "SYMBOL": "FPP",
    "TX_NUMBER": "10079",
    "TOTAL_AMOUNT": 4618907.26
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-09-18T19:17:10",
    "PRICE": 123.00,
    "QUANTITY": 123,
    "STATUS": "Filled",
    "SYMBOL": "AXL",
    "TX_NUMBER": "10080",
    "TOTAL_AMOUNT": 15129.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-09-18T19:33:46",
    "PRICE": 234.00,
    "QUANTITY": 234,
    "STATUS": "Filled",
    "SYMBOL": "AXL",
    "TX_NUMBER": "10081",
    "TOTAL_AMOUNT": 54756.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-09-18T19:39:20",
    "PRICE": 345.00,
    "QUANTITY": 345,
    "STATUS": "Filled",
    "SYMBOL": "AXL",
    "TX_NUMBER": "10082",
    "TOTAL_AMOUNT": 119025.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-09-18T20:18:58",
    "PRICE": 456.00,
    "QUANTITY": 456,
    "STATUS": "Filled",
    "SYMBOL": "AXL",
    "TX_NUMBER": "10083",
    "TOTAL_AMOUNT": 207936.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2016-02-28T16:41:41",
    "PRICE": 3.00,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "JAP",
    "TX_NUMBER": "10084",
    "TOTAL_AMOUNT": 15.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-09-26T03:43:36",
    "PRICE": 567.00,
    "QUANTITY": 567,
    "STATUS": "Pending",
    "SYMBOL": "AXL",
    "TX_NUMBER": "10085",
    "TOTAL_AMOUNT": 321489.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T11:08:55",
    "PRICE": 100.00,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "PEP",
    "TX_NUMBER": "10086",
    "TOTAL_AMOUNT": 1000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T11:08:55",
    "PRICE": 100.00,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "PEP",
    "TX_NUMBER": "10087",
    "TOTAL_AMOUNT": 1000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T11:08:55",
    "PRICE": 100.00,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "PEP",
    "TX_NUMBER": "10088",
    "TOTAL_AMOUNT": 1000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T17:48:55",
    "PRICE": 10.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10089",
    "TOTAL_AMOUNT": 10.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T17:49:33",
    "PRICE": 10.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10090",
    "TOTAL_AMOUNT": 10.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T18:17:18",
    "PRICE": 10.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10091",
    "TOTAL_AMOUNT": 10.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T18:19:57",
    "PRICE": 100.24,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10092",
    "TOTAL_AMOUNT": 1002.40
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T19:36:50",
    "PRICE": 100.24,
    "QUANTITY": 11,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10093",
    "TOTAL_AMOUNT": 1102.64
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T19:40:57",
    "PRICE": 123.45,
    "QUANTITY": 67,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10094",
    "TOTAL_AMOUNT": 8271.15
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T19:46:16",
    "PRICE": 122.45,
    "QUANTITY": 987,
    "STATUS": "Filled",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10095",
    "TOTAL_AMOUNT": 120858.15
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T20:05:48",
    "PRICE": 100.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10096",
    "TOTAL_AMOUNT": 200.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T21:29:58",
    "PRICE": 100.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10097",
    "TOTAL_AMOUNT": 200.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T21:33:04",
    "PRICE": 100.00,
    "QUANTITY": 3,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10098",
    "TOTAL_AMOUNT": 300.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T21:37:18",
    "PRICE": 100.00,
    "QUANTITY": 3,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10099",
    "TOTAL_AMOUNT": 300.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T21:42:47",
    "PRICE": 100.00,
    "QUANTITY": 3,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10100",
    "TOTAL_AMOUNT": 300.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T21:57:25",
    "PRICE": 100.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10101",
    "TOTAL_AMOUNT": 400.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T22:26:19",
    "PRICE": 100.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10102",
    "TOTAL_AMOUNT": 400.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T22:28:26",
    "PRICE": 100.00,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10103",
    "TOTAL_AMOUNT": 400.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-10-05T22:29:27",
    "PRICE": 100.00,
    "QUANTITY": 55,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10104",
    "TOTAL_AMOUNT": 5500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-10-06T13:39:02",
    "PRICE": 100.00,
    "QUANTITY": 55,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10105",
    "TOTAL_AMOUNT": 5500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T13:39:16",
    "PRICE": 100.00,
    "QUANTITY": 55,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10106",
    "TOTAL_AMOUNT": 5500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-05T11:30:00",
    "PRICE": 100.00,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10107",
    "TOTAL_AMOUNT": 1000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T11:30:00",
    "PRICE": 100.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10108",
    "TOTAL_AMOUNT": 1000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T11:30:00",
    "PRICE": 100.00,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "LP",
    "TX_NUMBER": "10109",
    "TOTAL_AMOUNT": 1000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T15:58:16",
    "PRICE": 100.00,
    "QUANTITY": 55,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10110",
    "TOTAL_AMOUNT": 5500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T17:26:17",
    "PRICE": 100.00,
    "QUANTITY": 55,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10111",
    "TOTAL_AMOUNT": 5500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-10-06T17:26:31",
    "PRICE": 100.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10112",
    "TOTAL_AMOUNT": 200.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T18:00:36",
    "PRICE": 100.00,
    "QUANTITY": 55,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10113",
    "TOTAL_AMOUNT": 5500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T18:07:16",
    "PRICE": 100.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10114",
    "TOTAL_AMOUNT": 100.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T18:24:16",
    "PRICE": 100.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10115",
    "TOTAL_AMOUNT": 100.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T18:56:28",
    "PRICE": 100.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10116",
    "TOTAL_AMOUNT": 100.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T11:30:00",
    "PRICE": 100.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10117",
    "TOTAL_AMOUNT": 1000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T11:30:00",
    "PRICE": 100.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10118",
    "TOTAL_AMOUNT": 1000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T11:30:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10119",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T11:30:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10120",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T17:30:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10121",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T17:30:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10122",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-06T17:30:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10123",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-06T17:30:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10124",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-10-07T01:46:22",
    "PRICE": 100.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10125",
    "TOTAL_AMOUNT": 200.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-07T10:42:05",
    "PRICE": 100.00,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "LP",
    "TX_NUMBER": "10126",
    "TOTAL_AMOUNT": 1000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-07T10:42:05",
    "PRICE": 100.00,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "LP",
    "TX_NUMBER": "10127",
    "TOTAL_AMOUNT": 1000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-07T10:42:05",
    "PRICE": 1000.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10128",
    "TOTAL_AMOUNT": 10000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-07T10:42:05",
    "PRICE": 1000.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10129",
    "TOTAL_AMOUNT": 10000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-07T17:41:35",
    "PRICE": 723.06,
    "QUANTITY": 6388,
    "STATUS": "Pending",
    "SYMBOL": "FPP",
    "TX_NUMBER": "10130",
    "TOTAL_AMOUNT": 4618907.26
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-07T17:42:11",
    "PRICE": 723.06,
    "QUANTITY": 6388,
    "STATUS": "Pending",
    "SYMBOL": "CPP-10",
    "TX_NUMBER": "10131",
    "TOTAL_AMOUNT": 4618907.26
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-06T17:30:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10132",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-06T17:30:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10133",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-06T17:30:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10134",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-06T17:30:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10135",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-06T17:30:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10136",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-07T19:30:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "LP",
    "TX_NUMBER": "10137",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-07T23:22:37",
    "PRICE": 100.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10138",
    "TOTAL_AMOUNT": 200.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-08T01:12:37",
    "PRICE": 100.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "RDD",
    "TX_NUMBER": "10139",
    "TOTAL_AMOUNT": 100.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-08T01:21:51",
    "PRICE": 100.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "RDD",
    "TX_NUMBER": "10140",
    "TOTAL_AMOUNT": 100.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-10-08T01:23:07",
    "PRICE": 0.00,
    "QUANTITY": 487,
    "STATUS": "Pending",
    "SYMBOL": "RDD",
    "TX_NUMBER": "10141",
    "TOTAL_AMOUNT": 0.06
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-10-08T01:34:05",
    "PRICE": 0.00,
    "QUANTITY": 487000,
    "STATUS": "Pending",
    "SYMBOL": "RDD-1",
    "TX_NUMBER": "10142",
    "TOTAL_AMOUNT": 58.44
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-10-08T01:39:09",
    "PRICE": 0.00,
    "QUANTITY": 487000,
    "STATUS": "Pending",
    "SYMBOL": "RDD-1",
    "TX_NUMBER": "10143",
    "TOTAL_AMOUNT": 58.44
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-08T01:46:04",
    "PRICE": 100.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "GLD",
    "TX_NUMBER": "10144",
    "TOTAL_AMOUNT": 100.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-10-08T10:59:29",
    "PRICE": 0.00,
    "QUANTITY": 487000,
    "STATUS": "Pending",
    "SYMBOL": "RDD-1",
    "TX_NUMBER": "10148",
    "TOTAL_AMOUNT": 58.44
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-08T13:52:05",
    "PRICE": 723.06,
    "QUANTITY": 6388,
    "STATUS": "Pending",
    "SYMBOL": "CPP-10",
    "TX_NUMBER": "10150",
    "TOTAL_AMOUNT": 4618907.26
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-10-08T13:52:34",
    "PRICE": 723.06,
    "QUANTITY": 6388,
    "STATUS": "Pending",
    "SYMBOL": "CPP-11",
    "TX_NUMBER": "10151",
    "TOTAL_AMOUNT": 4618907.26
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-10-08T13:53:47",
    "PRICE": 12.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "CPP-12",
    "TX_NUMBER": "10152",
    "TOTAL_AMOUNT": 1200.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-10-15T00:34:57",
    "PRICE": 0.00,
    "QUANTITY": 487000,
    "STATUS": "Pending",
    "SYMBOL": "RDD-1",
    "TX_NUMBER": "10154",
    "TOTAL_AMOUNT": 58.44
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-30T00:00:00",
    "PRICE": 250.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10155",
    "TOTAL_AMOUNT": 12510.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-30T00:00:00",
    "PRICE": 250.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SOME",
    "TX_NUMBER": "10156",
    "TOTAL_AMOUNT": 12510.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-10-30T00:00:00",
    "PRICE": 2.50,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "SOME2",
    "TX_NUMBER": "10157",
    "TOTAL_AMOUNT": 5.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-30T00:00:00",
    "PRICE": 2.50,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "SOME3",
    "TX_NUMBER": "10158",
    "TOTAL_AMOUNT": 5.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-30T00:00:00",
    "PRICE": 2.50,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "SOME3",
    "TX_NUMBER": "10159",
    "TOTAL_AMOUNT": 5.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-10-31T00:00:00",
    "PRICE": 2.50,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "SOME3",
    "TX_NUMBER": "10160",
    "TOTAL_AMOUNT": 5.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-10-31T00:00:00",
    "PRICE": 2.50,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "SOME3",
    "TX_NUMBER": "10161",
    "TOTAL_AMOUNT": 5.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-31T00:00:00",
    "PRICE": 2.50,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "SOME3",
    "TX_NUMBER": "10162",
    "TOTAL_AMOUNT": 5.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-10-31T00:00:00",
    "PRICE": 2.50,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "SOME3",
    "TX_NUMBER": "10163",
    "TOTAL_AMOUNT": 5.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 500,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10164",
    "TOTAL_AMOUNT": 102605.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10165",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-11-09T21:19:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10166",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10167",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-11-10T13:01:00",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10168",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-11-10T13:10:00",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10169",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-11-10T13:11:00",
    "PRICE": 205.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10170",
    "TOTAL_AMOUNT": 10250.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-11-10T13:11:00",
    "PRICE": 200.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10171",
    "TOTAL_AMOUNT": 10000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-11-10T13:58:00",
    "PRICE": 200.20,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10172",
    "TOTAL_AMOUNT": 10010.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-11-10T14:15:00",
    "PRICE": 200.20,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10173",
    "TOTAL_AMOUNT": 10010.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-11-10T14:18:00",
    "PRICE": 200.20,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10174",
    "TOTAL_AMOUNT": 10010.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-11-10T14:18:00",
    "PRICE": 200.20,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10175",
    "TOTAL_AMOUNT": 10010.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-11-10T14:28:00",
    "PRICE": 200.20,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10176",
    "TOTAL_AMOUNT": 10010.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-11-10T14:28:00",
    "PRICE": 200.20,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10177",
    "TOTAL_AMOUNT": 10010.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-11-10T14:46:00",
    "PRICE": 200.20,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10178",
    "TOTAL_AMOUNT": 10010.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10179",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10180",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10181",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10182",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10183",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10184",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10185",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10186",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10187",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10188",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10189",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10190",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10191",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10192",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10193",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10194",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10195",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10196",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10197",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10198",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10199",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10200",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10201",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10202",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10203",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10204",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10205",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10206",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10207",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10208",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10210",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10211",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10212",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T06:45:34",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10213",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-11-20T06:45:34",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10214",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T18:22:19",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10217",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T18:29:21",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10218",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T19:34:47",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10219",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T19:52:11",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10220",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T19:52:18",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10221",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T20:03:29",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10222",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T20:08:55",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10223",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T20:20:30",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10225",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T20:29:37",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10226",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T20:31:55",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10227",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T20:33:49",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10228",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T20:35:51",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10229",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T22:29:02",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10230",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T22:29:27",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10231",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T22:55:20",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10232",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T23:36:27",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10233",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T23:53:28",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10234",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T23:53:43",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10235",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T23:55:20",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10236",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T23:56:08",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10237",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T23:56:48",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10238",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-01T23:58:41",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10239",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T00:00:04",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10240",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T00:02:33",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10241",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T00:04:20",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10242",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T00:07:44",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10243",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T00:09:51",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10244",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T00:11:45",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10245",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T00:13:52",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10246",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T00:14:55",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10247",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T00:18:15",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10248",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T00:19:28",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10249",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T00:20:23",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10250",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T00:21:47",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10251",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T00:22:29",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10252",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T00:23:29",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10253",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T14:51:06",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10254",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T15:05:51",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10255",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T15:06:25",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10256",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T15:19:48",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10257",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T16:15:49",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10258",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T16:15:54",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10259",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T16:29:07",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10260",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T16:34:44",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10261",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T16:34:55",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10262",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T16:35:25",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10263",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T16:38:41",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10264",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T16:39:53",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10265",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T16:49:44",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10266",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-02T16:51:30",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10267",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T17:24:51",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10268",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T17:24:59",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10269",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T17:25:04",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10270",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T17:25:12",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10271",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T17:32:03",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10272",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T17:32:47",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10273",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T17:33:01",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10274",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T17:33:05",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10275",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T17:50:19",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10276",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T17:51:23",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10277",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T17:56:51",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10278",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T18:03:24",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10279",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T18:03:50",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10280",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T18:03:50",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10281",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T18:03:50",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10282",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T18:03:50",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10283",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T18:03:50",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10284",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T18:03:59",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10285",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T18:03:57",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10286",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T18:03:55",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10287",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T18:03:53",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10288",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T18:04:01",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10289",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-02T18:59:33",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10290",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T09:02:52",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10291",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T09:05:29",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10293",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T09:09:40",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10294",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T09:26:13",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10297",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T09:28:32",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10298",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T09:34:11",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10299",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T09:41:23",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10300",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T09:43:41",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10301",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T09:53:25",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10302",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T10:06:08",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10303",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T10:45:42",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10304",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:31:07",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10305",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:31:07",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10306",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:31:07",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10307",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:31:07",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10308",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:31:07",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10309",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:31:18",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10310",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:31:16",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10311",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:31:13",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10312",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:31:12",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10313",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:31:21",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10314",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:33:05",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10315",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:33:05",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10316",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:33:05",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10317",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:33:05",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10318",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:33:05",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10319",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:33:17",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10320",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:33:15",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10321",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:33:13",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10322",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:33:11",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10323",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:33:21",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10324",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:35:14",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10325",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:35:14",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10326",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:35:14",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10327",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:35:14",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10328",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:35:14",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10329",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:35:26",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10330",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:35:23",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10331",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:35:21",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10332",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:35:19",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10333",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:35:28",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10334",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:39:37",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10335",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:39:56",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10336",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:41:25",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10337",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:42:53",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10338",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:44:31",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10339",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:46:46",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10340",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:49:54",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10341",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:53:11",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10342",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:54:34",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10343",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:56:54",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10344",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:56:54",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10345",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:56:59",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10346",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:57:01",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10347",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:57:03",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10348",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:57:06",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10349",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:57:08",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10350",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:57:11",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10351",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:57:13",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10352",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:59:08",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10353",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:59:08",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10354",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:59:13",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10355",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:59:16",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10356",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:59:18",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10357",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:59:21",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10358",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:59:23",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10359",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:59:27",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10360",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T12:59:29",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10361",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:01:15",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10362",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:01:15",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10363",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:01:20",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10364",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:01:22",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10365",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:01:24",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10366",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:01:27",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10367",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:01:29",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10368",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:01:32",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10369",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:01:34",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10370",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:02:32",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10371",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:02:32",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10372",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:02:32",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10373",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:02:39",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10374",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:02:36",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10375",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:02:43",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10376",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:02:41",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10377",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:04:56",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10378",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:04:57",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10379",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:04:56",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10380",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:05:03",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10381",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:05:01",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10382",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:05:09",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10383",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:05:07",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10384",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:05:15",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10385",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:05:12",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10386",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:05:20",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10387",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:05:18",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10388",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:14:23",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10389",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:14:23",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10390",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:14:23",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10391",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:14:30",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10392",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:14:27",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10393",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:14:34",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10394",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:14:32",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10395",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:14:39",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10396",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:14:37",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10397",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:14:45",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10398",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:14:41",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10399",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:14:46",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10400",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T13:49:23",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10401",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T14:08:23",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10402",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T14:10:19",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10403",
    "TOTAL_AMOUNT": 202.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T14:12:43",
    "PRICE": 20.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10404",
    "TOTAL_AMOUNT": 202.12
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-03T14:13:22",
    "PRICE": 20.21,
    "QUANTITY": 150,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10405",
    "TOTAL_AMOUNT": 3031.77
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-04T13:01:36",
    "PRICE": 12.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "CPP-13",
    "TX_NUMBER": "10406",
    "TOTAL_AMOUNT": 1200.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-04T13:06:32",
    "PRICE": 12.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "CPP-14",
    "TX_NUMBER": "10407",
    "TOTAL_AMOUNT": 1200.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-05T00:09:12",
    "PRICE": 20.21,
    "QUANTITY": 150,
    "STATUS": "Pending",
    "SYMBOL": "SPY1",
    "TX_NUMBER": "10408",
    "TOTAL_AMOUNT": 3031.77
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-05T00:09:24",
    "PRICE": 20.21,
    "QUANTITY": 150,
    "STATUS": "Pending",
    "SYMBOL": "SP'Y1",
    "TX_NUMBER": "10409",
    "TOTAL_AMOUNT": 3031.77
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-05T00:09:52",
    "PRICE": 20.21,
    "QUANTITY": 150,
    "STATUS": "Pending",
    "SYMBOL": "SP'\"Y1",
    "TX_NUMBER": "10410",
    "TOTAL_AMOUNT": 3031.77
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-05T00:10:12",
    "PRICE": 20.21,
    "QUANTITY": 150,
    "STATUS": "Pending",
    "SYMBOL": "SP'\"Y1 --",
    "TX_NUMBER": "10411",
    "TOTAL_AMOUNT": 3031.77
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-05T00:11:19",
    "PRICE": 20.21,
    "QUANTITY": 150,
    "STATUS": "Pending",
    "SYMBOL": "/*SP'\"Y1 --",
    "TX_NUMBER": "10412",
    "TOTAL_AMOUNT": 3031.77
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-05T23:02:04",
    "PRICE": 20.21,
    "QUANTITY": 150,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10413",
    "TOTAL_AMOUNT": 3031.77
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-06T11:01:59",
    "PRICE": 20.21,
    "QUANTITY": 150,
    "STATUS": "Pending",
    "SYMBOL": "QQQ",
    "TX_NUMBER": "10414",
    "TOTAL_AMOUNT": 3031.77
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-20T12:23:00",
    "PRICE": 100.50,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "CCC",
    "TX_NUMBER": "10415",
    "TOTAL_AMOUNT": 1005.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-20T12:28:10",
    "PRICE": 30.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "CCR",
    "TX_NUMBER": "10416",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-20T12:36:11",
    "PRICE": 30.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "RRR",
    "TX_NUMBER": "10417",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-20T12:37:45",
    "PRICE": 320.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "SSS",
    "TX_NUMBER": "10418",
    "TOTAL_AMOUNT": 32000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-20T12:47:32",
    "PRICE": 10.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TEST",
    "TX_NUMBER": "10419",
    "TOTAL_AMOUNT": 10.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-20T12:56:21",
    "PRICE": 10.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TEST",
    "TX_NUMBER": "10420",
    "TOTAL_AMOUNT": 10.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-20T17:36:14",
    "PRICE": 3210.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "APP",
    "TX_NUMBER": "10421",
    "TOTAL_AMOUNT": 32100.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-20T18:39:15",
    "PRICE": 3210.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SWT",
    "TX_NUMBER": "10422",
    "TOTAL_AMOUNT": 32100.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-20T18:49:50",
    "PRICE": 3210.50,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SWT",
    "TX_NUMBER": "10423",
    "TOTAL_AMOUNT": 32105.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-20T18:53:05",
    "PRICE": 5.50,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "PPP",
    "TX_NUMBER": "10424",
    "TOTAL_AMOUNT": 550.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-21T03:23:22",
    "PRICE": 5.50,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10425",
    "TOTAL_AMOUNT": 5.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-21T04:18:19",
    "PRICE": 1.50,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "BBBBB",
    "TX_NUMBER": "10426",
    "TOTAL_AMOUNT": 7.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-21T04:28:17",
    "PRICE": 1.50,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "CCCCC",
    "TX_NUMBER": "10427",
    "TOTAL_AMOUNT": 7.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-21T04:31:44",
    "PRICE": 1.50,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "DDD",
    "TX_NUMBER": "10428",
    "TOTAL_AMOUNT": 7.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-21T04:40:27",
    "PRICE": 1.50,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "FFF",
    "TX_NUMBER": "10429",
    "TOTAL_AMOUNT": 7.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-21T04:44:41",
    "PRICE": 11.50,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "GGG",
    "TX_NUMBER": "10430",
    "TOTAL_AMOUNT": 115.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-22T20:32:47",
    "PRICE": 1150.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10431",
    "TOTAL_AMOUNT": 115000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-22T21:31:56",
    "PRICE": 150.00,
    "QUANTITY": 40,
    "STATUS": "Pending",
    "SYMBOL": "POK",
    "TX_NUMBER": "10432",
    "TOTAL_AMOUNT": 6000.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-22T21:34:01",
    "PRICE": 1150.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10433",
    "TOTAL_AMOUNT": 115000.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-22T21:42:58",
    "PRICE": 1150.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10434",
    "TOTAL_AMOUNT": 115000.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-12-27T13:45:58",
    "PRICE": 1150.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10435",
    "TOTAL_AMOUNT": 115000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2019-12-27T14:12:13",
    "PRICE": 10.50,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "ABC",
    "TX_NUMBER": "10436",
    "TOTAL_AMOUNT": 1050.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-02-24T01:02:18",
    "PRICE": 100.00,
    "QUANTITY": 80,
    "STATUS": "Pending",
    "SYMBOL": "SLV",
    "TX_NUMBER": "10437",
    "TOTAL_AMOUNT": 8000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-02-24T21:13:07",
    "PRICE": 205.21,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10438",
    "TOTAL_AMOUNT": 1026.05
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-02-24T21:15:54",
    "PRICE": -205.21,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10439",
    "TOTAL_AMOUNT": -1026.05
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-02-24T21:16:05",
    "PRICE": -205.21,
    "QUANTITY": -5,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10440",
    "TOTAL_AMOUNT": 1026.05
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-02-24T21:39:35",
    "PRICE": 205.21,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10441",
    "TOTAL_AMOUNT": 1026.05
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-02-24T21:39:58",
    "PRICE": 205.21,
    "QUANTITY": 0,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10442",
    "TOTAL_AMOUNT": 0.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-02-24T21:40:17",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10443",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-02-24T23:56:15",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10444",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-02-24T23:57:45",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10445",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-02-25T00:28:47",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10446",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-01T20:48:04",
    "PRICE": 555.21,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10447",
    "TOTAL_AMOUNT": 5552.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-14T00:00:00",
    "PRICE": 34.21,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "JUL",
    "TX_NUMBER": "10448",
    "TOTAL_AMOUNT": 2052.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-14T18:11:58",
    "PRICE": 34.21,
    "QUANTITY": 61,
    "STATUS": "Pending",
    "SYMBOL": "JUL",
    "TX_NUMBER": "10449",
    "TOTAL_AMOUNT": 2086.81
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-14T18:33:49",
    "PRICE": 34.21,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "JUL",
    "TX_NUMBER": "10450",
    "TOTAL_AMOUNT": 2121.02
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-14T18:55:09",
    "PRICE": 34.21,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "JUL",
    "TX_NUMBER": "10451",
    "TOTAL_AMOUNT": 2121.02
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-14T19:00:07",
    "PRICE": 34.21,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "JUL",
    "TX_NUMBER": "10452",
    "TOTAL_AMOUNT": 2121.02
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-14T19:04:25",
    "PRICE": 34.21,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "JUL",
    "TX_NUMBER": "10453",
    "TOTAL_AMOUNT": 2121.02
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-14T19:08:16",
    "PRICE": 34.21,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "JUL",
    "TX_NUMBER": "10454",
    "TOTAL_AMOUNT": 2121.02
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-14T20:24:19",
    "PRICE": 50.21,
    "QUANTITY": 63,
    "STATUS": "Pending",
    "SYMBOL": "JUL",
    "TX_NUMBER": "10455",
    "TOTAL_AMOUNT": 3163.23
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-14T20:27:47",
    "PRICE": 50.21,
    "QUANTITY": 64,
    "STATUS": "Pending",
    "SYMBOL": "JUL",
    "TX_NUMBER": "10456",
    "TOTAL_AMOUNT": 3213.44
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-14T20:31:19",
    "PRICE": 50.21,
    "QUANTITY": 65,
    "STATUS": "Pending",
    "SYMBOL": "JUL",
    "TX_NUMBER": "10457",
    "TOTAL_AMOUNT": 3263.65
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-15T18:38:24",
    "PRICE": 99.21,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10458",
    "TOTAL_AMOUNT": 9921.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-15T18:43:53",
    "PRICE": 99.21,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10459",
    "TOTAL_AMOUNT": 9921.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-16T22:21:34",
    "PRICE": 99.21,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10460",
    "TOTAL_AMOUNT": 9921.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-16T19:27:30",
    "PRICE": 99.21,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10461",
    "TOTAL_AMOUNT": 9921.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-16T19:27:36",
    "PRICE": 99.21,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10462",
    "TOTAL_AMOUNT": 9921.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-03-16T23:58:06",
    "PRICE": 99.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10463",
    "TOTAL_AMOUNT": 9900.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-04-10T22:38:42",
    "PRICE": 1.50,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10464",
    "TOTAL_AMOUNT": 150.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-04-10T22:38:42",
    "PRICE": 1.50,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10465",
    "TOTAL_AMOUNT": 150.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-04-10T22:38:42",
    "PRICE": 1.50,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10466",
    "TOTAL_AMOUNT": 150.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-10T22:38:42",
    "PRICE": 1.50,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10467",
    "TOTAL_AMOUNT": 150.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-10T22:38:42",
    "PRICE": 50.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10468",
    "TOTAL_AMOUNT": 100.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-10T22:38:42",
    "PRICE": 50.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10469",
    "TOTAL_AMOUNT": 100.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-10T22:38:42",
    "PRICE": 50.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10470",
    "TOTAL_AMOUNT": 100.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-01-10T22:38:42",
    "PRICE": 50.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10471",
    "TOTAL_AMOUNT": 100.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-08T22:38:42",
    "PRICE": 50.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10472",
    "TOTAL_AMOUNT": 100.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-08T22:38:42",
    "PRICE": 70.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10473",
    "TOTAL_AMOUNT": 140.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-08T22:38:42",
    "PRICE": 70.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10474",
    "TOTAL_AMOUNT": 140.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-08T22:38:42",
    "PRICE": 60.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10475",
    "TOTAL_AMOUNT": 120.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-08T22:38:42",
    "PRICE": 60.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10476",
    "TOTAL_AMOUNT": 120.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-08T22:38:42",
    "PRICE": 60.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10477",
    "TOTAL_AMOUNT": 120.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-08T22:38:42",
    "PRICE": 20.50,
    "QUANTITY": 75,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10478",
    "TOTAL_AMOUNT": 1537.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-08T22:38:42",
    "PRICE": 60.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10479",
    "TOTAL_AMOUNT": 120.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-08T22:38:42",
    "PRICE": 60.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10480",
    "TOTAL_AMOUNT": 120.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-08T22:38:42",
    "PRICE": 60.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10481",
    "TOTAL_AMOUNT": 120.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-04-08T22:38:42",
    "PRICE": 60.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FIT",
    "TX_NUMBER": "10482",
    "TOTAL_AMOUNT": 120.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-05-10T06:45:34",
    "PRICE": 1.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TEST",
    "TX_NUMBER": "10483",
    "TOTAL_AMOUNT": 1.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-05-10T06:45:34",
    "PRICE": 1.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TEST",
    "TX_NUMBER": "10484",
    "TOTAL_AMOUNT": 1.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-05-10T22:53:02",
    "PRICE": 1.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TEST",
    "TX_NUMBER": "10485",
    "TOTAL_AMOUNT": 1.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-05-11T11:16:13",
    "PRICE": 1.12,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TEST",
    "TX_NUMBER": "10486",
    "TOTAL_AMOUNT": 1.12
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-05-11T11:47:48",
    "PRICE": 1.12,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TEST",
    "TX_NUMBER": "10487",
    "TOTAL_AMOUNT": 1.12
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-05-11T11:47:53",
    "PRICE": 1.12,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TEST",
    "TX_NUMBER": "10488",
    "TOTAL_AMOUNT": 1.12
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-05-11T16:36:25",
    "PRICE": 1.12,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TEST",
    "TX_NUMBER": "10489",
    "TOTAL_AMOUNT": 1.12
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-05-11T17:01:58",
    "PRICE": 1.12,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TEST",
    "TX_NUMBER": "10490",
    "TOTAL_AMOUNT": 1.12
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-05-29T01:27:44",
    "PRICE": 1.12,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TEST",
    "TX_NUMBER": "10491",
    "TOTAL_AMOUNT": 1.12
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-05-29T01:27:51",
    "PRICE": 1.12,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TEST",
    "TX_NUMBER": "10492",
    "TOTAL_AMOUNT": 1.12
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-07-04T13:48:01",
    "PRICE": 613.80,
    "QUANTITY": 6774,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10493",
    "TOTAL_AMOUNT": 4157881.12
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:31",
    "PRICE": 50.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10494",
    "TOTAL_AMOUNT": 2500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": 50.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10495",
    "TOTAL_AMOUNT": 2500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": 50.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10496",
    "TOTAL_AMOUNT": 2500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": 50.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10497",
    "TOTAL_AMOUNT": 2500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": 232.23,
    "QUANTITY": 23,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10498",
    "TOTAL_AMOUNT": 5341.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T00:00:00",
    "PRICE": 232.23,
    "QUANTITY": 23,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10499",
    "TOTAL_AMOUNT": 5341.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": 232.23,
    "QUANTITY": 23,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10500",
    "TOTAL_AMOUNT": 5341.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": 232.23,
    "QUANTITY": 23,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10501",
    "TOTAL_AMOUNT": 5341.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": 232.23,
    "QUANTITY": 23,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10502",
    "TOTAL_AMOUNT": 5341.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": 232.23,
    "QUANTITY": 23,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10503",
    "TOTAL_AMOUNT": 5341.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": 232.23,
    "QUANTITY": 23,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10504",
    "TOTAL_AMOUNT": 5341.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": -232.23,
    "QUANTITY": 23,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10505",
    "TOTAL_AMOUNT": -5341.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": 232.23,
    "QUANTITY": 23,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10506",
    "TOTAL_AMOUNT": 5341.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": -232.23,
    "QUANTITY": 23,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10507",
    "TOTAL_AMOUNT": -5341.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": 232.23,
    "QUANTITY": 23,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10508",
    "TOTAL_AMOUNT": 5341.29
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": 999.99,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10509",
    "TOTAL_AMOUNT": 49999.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T20:43:30",
    "PRICE": 999.99,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10510",
    "TOTAL_AMOUNT": 49999.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-08T20:43:30",
    "PRICE": 999.99,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10511",
    "TOTAL_AMOUNT": 49999.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-08T20:43:30",
    "PRICE": 999.99,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10512",
    "TOTAL_AMOUNT": 49999.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-08T20:43:30",
    "PRICE": 999.99,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10513",
    "TOTAL_AMOUNT": 49999.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10514",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10515",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10516",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-01-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10517",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-02-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10518",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-02-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10519",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-02-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10520",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-02-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10521",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-02-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10522",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2016-06-20T03:00:12",
    "PRICE": 207.85,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10523",
    "TOTAL_AMOUNT": 10392.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2016-06-20T03:00:12",
    "PRICE": 207.85,
    "QUANTITY": 40,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10524",
    "TOTAL_AMOUNT": 8314.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2008-07-24T08:04:54",
    "PRICE": 207.85,
    "QUANTITY": 30,
    "STATUS": "Pending",
    "SYMBOL": "AVG",
    "TX_NUMBER": "10525",
    "TOTAL_AMOUNT": 6235.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10526",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10527",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10528",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10529",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10530",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10531",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10532",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10533",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10534",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10535",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10536",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10537",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10538",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10539",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10540",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10541",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10542",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10543",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10544",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10545",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10546",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10547",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10548",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10549",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10550",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10551",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10552",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10553",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10554",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10555",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10556",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10557",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-27T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10558",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10559",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10560",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2022-04-29T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10561",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2022-04-29T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10562",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2008-07-24T08:04:54",
    "PRICE": 207.00,
    "QUANTITY": 30,
    "STATUS": "Pending",
    "SYMBOL": "AVG",
    "TX_NUMBER": "10563",
    "TOTAL_AMOUNT": 6210.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2022-04-29T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10564",
    "TOTAL_AMOUNT": 205.21
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 500,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10565",
    "TOTAL_AMOUNT": 102605.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 500,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10566",
    "TOTAL_AMOUNT": 102605.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 500,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10567",
    "TOTAL_AMOUNT": 102605.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-01-12T03:56:18",
    "PRICE": 123.00,
    "QUANTITY": 60,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10568",
    "TOTAL_AMOUNT": 7380.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2019-05-20T10:05:43",
    "PRICE": 123.85,
    "QUANTITY": 20,
    "STATUS": "Pending",
    "SYMBOL": "AKA",
    "TX_NUMBER": "10569",
    "TOTAL_AMOUNT": 2477.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-03-22T10:05:43",
    "PRICE": 123.85,
    "QUANTITY": 56,
    "STATUS": "Pending",
    "SYMBOL": "AKA",
    "TX_NUMBER": "10570",
    "TOTAL_AMOUNT": 6935.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-08T20:43:30",
    "PRICE": 999.99,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10571",
    "TOTAL_AMOUNT": 49999.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-08T20:43:30",
    "PRICE": 999.99,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10572",
    "TOTAL_AMOUNT": 49999.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-08T20:43:30",
    "PRICE": 999.99,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CHR",
    "TX_NUMBER": "10573",
    "TOTAL_AMOUNT": 49999.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-11T03:21:18",
    "PRICE": 965.00,
    "QUANTITY": 11,
    "STATUS": "Pending",
    "SYMBOL": "APL",
    "TX_NUMBER": "10574",
    "TOTAL_AMOUNT": 10615.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-11T03:30:29",
    "PRICE": 393.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "AMZ",
    "TX_NUMBER": "10575",
    "TOTAL_AMOUNT": 393.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-11T04:00:35",
    "PRICE": 311.00,
    "QUANTITY": 15,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10576",
    "TOTAL_AMOUNT": 4665.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-11T20:51:59",
    "PRICE": 300.00,
    "QUANTITY": 15,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10577",
    "TOTAL_AMOUNT": 4500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-11T21:25:40",
    "PRICE": 300.00,
    "QUANTITY": 15,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10578",
    "TOTAL_AMOUNT": 4500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-11T21:44:00",
    "PRICE": 300.00,
    "QUANTITY": 15,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10579",
    "TOTAL_AMOUNT": 4500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-11T22:06:13",
    "PRICE": 300.00,
    "QUANTITY": 15,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10580",
    "TOTAL_AMOUNT": 4500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 305.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10581",
    "TOTAL_AMOUNT": 15250.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T00:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10582",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T00:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10583",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T00:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10584",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T00:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10585",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T00:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10586",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T00:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10587",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T00:36:25",
    "PRICE": 15.00,
    "QUANTITY": 11,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10588",
    "TOTAL_AMOUNT": 165.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-12T15:36:25",
    "PRICE": 43.00,
    "QUANTITY": 22,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10589",
    "TOTAL_AMOUNT": 946.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-22T20:55:31",
    "PRICE": 495.69,
    "QUANTITY": 5,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "10590",
    "TOTAL_AMOUNT": 2478.45
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-22T20:55:31",
    "PRICE": 495.69,
    "QUANTITY": 5,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "10591",
    "TOTAL_AMOUNT": 2478.45
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-22T20:55:31",
    "PRICE": 495.69,
    "QUANTITY": 5,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "10592",
    "TOTAL_AMOUNT": 2478.45
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-22T20:55:31",
    "PRICE": 495.69,
    "QUANTITY": 5,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "10593",
    "TOTAL_AMOUNT": 2478.45
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-22T20:55:31",
    "PRICE": 495.69,
    "QUANTITY": 5,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "10594",
    "TOTAL_AMOUNT": 2478.45
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-22T20:55:31",
    "PRICE": 495.69,
    "QUANTITY": 5,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "10595",
    "TOTAL_AMOUNT": 2478.45
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T00:00:00",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10596",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T00:00:00",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10597",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T20:22:32",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10598",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T20:28:03",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10599",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10600",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10601",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10602",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10603",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10604",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10605",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10606",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10607",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10608",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10609",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10610",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-12T09:50:25",
    "PRICE": 228.76,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10611",
    "TOTAL_AMOUNT": 2287.60
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10612",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10613",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-13T12:01:25",
    "PRICE": 228.76,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10614",
    "TOTAL_AMOUNT": 2287.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-13T01:04:25",
    "PRICE": 228.76,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10615",
    "TOTAL_AMOUNT": 2287.60
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10616",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-13T01:04:25",
    "PRICE": 228.76,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10617",
    "TOTAL_AMOUNT": 2287.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-13T01:04:25",
    "PRICE": 228.76,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10618",
    "TOTAL_AMOUNT": 2287.60
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-05-12T12:36:25",
    "PRICE": 309.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10619",
    "TOTAL_AMOUNT": 15450.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10620",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10621",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10622",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-04-12T12:36:25",
    "PRICE": 307.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "AAM",
    "TX_NUMBER": "10623",
    "TOTAL_AMOUNT": 15350.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-05-12T12:36:25",
    "PRICE": 330.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10624",
    "TOTAL_AMOUNT": 16500.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-04-13T01:04:25",
    "PRICE": 111.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10625",
    "TOTAL_AMOUNT": 1110.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-02T15:25:56",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10626",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-02T15:26:58",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10627",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-02T16:20:56",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10628",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-03-06T17:43:31",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10629",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10630",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2023-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10631",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2023-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10632",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2022-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10633",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2023-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10634",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2023-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10635",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10638",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2022-12-28T17:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10639",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T14:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10640",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T16:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10641",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T16:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10642",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T10:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10643",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T07:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10644",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T20:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10645",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-04T11:38:08",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10646",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-05-04T11:43:10",
    "PRICE": 215.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10647",
    "TOTAL_AMOUNT": 10760.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-09T15:29:59",
    "PRICE": 29.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10648",
    "TOTAL_AMOUNT": 1460.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-09T21:53:25",
    "PRICE": 1500.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10649",
    "TOTAL_AMOUNT": 75010.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-05-12T23:16:12",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10650",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-13T14:17:28",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10652",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-23T15:48:13",
    "PRICE": 365.24,
    "QUANTITY": 6,
    "STATUS": "Pending",
    "SYMBOL": "DAY",
    "TX_NUMBER": "10653",
    "TOTAL_AMOUNT": 2191.44
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-23T16:20:56",
    "PRICE": 365.24,
    "QUANTITY": 6,
    "STATUS": "Pending",
    "SYMBOL": "OOO",
    "TX_NUMBER": "10654",
    "TOTAL_AMOUNT": 2191.44
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-23T16:32:18",
    "PRICE": 365.24,
    "QUANTITY": 6,
    "STATUS": "Pending",
    "SYMBOL": "OOO",
    "TX_NUMBER": "10655",
    "TOTAL_AMOUNT": 2191.44
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-05-23T16:42:12",
    "PRICE": 24.70,
    "QUANTITY": 6,
    "STATUS": "Pending",
    "SYMBOL": "OOO",
    "TX_NUMBER": "10656",
    "TOTAL_AMOUNT": 148.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-05-24T01:38:54",
    "PRICE": 24.70,
    "QUANTITY": 6,
    "STATUS": "Pending",
    "SYMBOL": "LUK",
    "TX_NUMBER": "10657",
    "TOTAL_AMOUNT": 148.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-05-24T04:40:17",
    "PRICE": 24.70,
    "QUANTITY": 6,
    "STATUS": "Pending",
    "SYMBOL": "WON",
    "TX_NUMBER": "10658",
    "TOTAL_AMOUNT": 148.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-05-24T05:08:18",
    "PRICE": 20.70,
    "QUANTITY": 207,
    "STATUS": "Pending",
    "SYMBOL": "CAM",
    "TX_NUMBER": "10659",
    "TOTAL_AMOUNT": 4284.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-24T18:19:16",
    "PRICE": 15.10,
    "QUANTITY": 151,
    "STATUS": "Pending",
    "SYMBOL": "BTS",
    "TX_NUMBER": "10661",
    "TOTAL_AMOUNT": 2280.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-24T18:23:33",
    "PRICE": 15.10,
    "QUANTITY": 151,
    "STATUS": "Pending",
    "SYMBOL": "BTS",
    "TX_NUMBER": "10662",
    "TOTAL_AMOUNT": 2280.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-24T18:26:00",
    "PRICE": 15.10,
    "QUANTITY": 151,
    "STATUS": "Pending",
    "SYMBOL": "BTS",
    "TX_NUMBER": "10663",
    "TOTAL_AMOUNT": 2280.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-24T18:35:23",
    "PRICE": 15.35,
    "QUANTITY": 153,
    "STATUS": "Pending",
    "SYMBOL": "LTH",
    "TX_NUMBER": "10665",
    "TOTAL_AMOUNT": 2348.55
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-31T12:30:18",
    "PRICE": 1.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "Example",
    "TX_NUMBER": "10667",
    "TOTAL_AMOUNT": 1.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-31T12:30:18",
    "PRICE": 1.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "Example",
    "TX_NUMBER": "10668",
    "TOTAL_AMOUNT": 1.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-31T12:30:18",
    "PRICE": 1.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "Example",
    "TX_NUMBER": "10669",
    "TOTAL_AMOUNT": 1.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-05-31T12:30:18",
    "PRICE": 1.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "Example",
    "TX_NUMBER": "10670",
    "TOTAL_AMOUNT": 1.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2022-05-31T12:30:18",
    "PRICE": 1.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "Example",
    "TX_NUMBER": "10672",
    "TOTAL_AMOUNT": 1.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2022-05-31T12:30:18",
    "PRICE": 1.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "Example",
    "TX_NUMBER": "10673",
    "TOTAL_AMOUNT": 1.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2022-06-30T12:30:18",
    "PRICE": 1.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "Example",
    "TX_NUMBER": "10674",
    "TOTAL_AMOUNT": 1.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2022-06-30T12:30:20",
    "PRICE": 129.29,
    "QUANTITY": 15,
    "STATUS": "Pending",
    "SYMBOL": "Example",
    "TX_NUMBER": "10675",
    "TOTAL_AMOUNT": 1939.35
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2022-06-30T12:30:20",
    "PRICE": 129.29,
    "QUANTITY": 15,
    "STATUS": "Pending",
    "SYMBOL": "Example",
    "TX_NUMBER": "10676",
    "TOTAL_AMOUNT": 1939.35
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2016-06-20T03:00:12",
    "PRICE": 207.85,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10677",
    "TOTAL_AMOUNT": 10392.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-05-06T20:55:31",
    "PRICE": 720.00,
    "QUANTITY": 5,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10678",
    "TOTAL_AMOUNT": 3600.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-05-06T20:55:31",
    "PRICE": 720.00,
    "QUANTITY": 5,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10679",
    "TOTAL_AMOUNT": 3600.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2018-03-22T20:55:31",
    "PRICE": 20.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10680",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-05T14:50:14",
    "PRICE": 20.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10681",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-06T21:00:16",
    "PRICE": 100.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10687",
    "TOTAL_AMOUNT": 10000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-06T21:02:22",
    "PRICE": 100.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10688",
    "TOTAL_AMOUNT": 10000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-06T21:02:31",
    "PRICE": 100.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10689",
    "TOTAL_AMOUNT": 10000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T01:14:53",
    "PRICE": 25.10,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10690",
    "TOTAL_AMOUNT": 251.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T02:18:24",
    "PRICE": 100.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10691",
    "TOTAL_AMOUNT": 10000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T02:18:28",
    "PRICE": 100.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10692",
    "TOTAL_AMOUNT": 10000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T09:53:07",
    "PRICE": 100.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10693",
    "TOTAL_AMOUNT": 10000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T13:14:10",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10694",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T13:37:33",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10695",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T13:55:30",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10696",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T14:12:40",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10697",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T14:16:07",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10698",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T14:17:57",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10699",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T14:20:26",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10700",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T14:35:32",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10701",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T14:51:25",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10702",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T14:52:24",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10703",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T15:04:32",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10704",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T15:16:11",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10705",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T15:26:43",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10706",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T18:41:16",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10707",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-07T15:46:07",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10708",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-03-22T20:55:31",
    "PRICE": 495.69,
    "QUANTITY": 5,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "10709",
    "TOTAL_AMOUNT": 2478.45
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-03-22T20:55:31",
    "PRICE": 495.69,
    "QUANTITY": 5,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "10710",
    "TOTAL_AMOUNT": 2478.45
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-03-22T20:55:31",
    "PRICE": 495.69,
    "QUANTITY": 5,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "10711",
    "TOTAL_AMOUNT": 2478.45
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-03-22T20:55:31",
    "PRICE": 100.00,
    "QUANTITY": 4,
    "STATUS": "Filled",
    "SYMBOL": "USD",
    "TX_NUMBER": "10712",
    "TOTAL_AMOUNT": 400.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-03-22T20:55:31",
    "PRICE": 100.00,
    "QUANTITY": 4,
    "STATUS": "Filled",
    "SYMBOL": "USD",
    "TX_NUMBER": "10713",
    "TOTAL_AMOUNT": 400.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-03-22T20:55:31",
    "PRICE": 100.00,
    "QUANTITY": 4,
    "STATUS": "Filled",
    "SYMBOL": "USD",
    "TX_NUMBER": "10714",
    "TOTAL_AMOUNT": 400.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-03-22T20:55:31",
    "PRICE": 100.00,
    "QUANTITY": 4,
    "STATUS": "Filled",
    "SYMBOL": "USD",
    "TX_NUMBER": "10715",
    "TOTAL_AMOUNT": 400.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-22T20:55:31",
    "PRICE": 10.00,
    "QUANTITY": 1,
    "STATUS": "Filled",
    "SYMBOL": "USD",
    "TX_NUMBER": "10716",
    "TOTAL_AMOUNT": 10.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-22T20:55:31",
    "PRICE": 10.00,
    "QUANTITY": 1,
    "STATUS": "Filled",
    "SYMBOL": "USD",
    "TX_NUMBER": "10717",
    "TOTAL_AMOUNT": 10.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-22T20:55:31",
    "PRICE": 10.00,
    "QUANTITY": 1,
    "STATUS": "Filled",
    "SYMBOL": "USD",
    "TX_NUMBER": "10718",
    "TOTAL_AMOUNT": 10.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-22T20:55:31",
    "PRICE": 10.00,
    "QUANTITY": 1,
    "STATUS": "Filled",
    "SYMBOL": "USD",
    "TX_NUMBER": "10719",
    "TOTAL_AMOUNT": 10.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-03-22T20:55:31",
    "PRICE": 100.00,
    "QUANTITY": 4,
    "STATUS": "Filled",
    "SYMBOL": "USD",
    "TX_NUMBER": "10720",
    "TOTAL_AMOUNT": 400.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2018-03-22T20:55:31",
    "PRICE": 10.00,
    "QUANTITY": 1,
    "STATUS": "Filled",
    "SYMBOL": "USD",
    "TX_NUMBER": "10722",
    "TOTAL_AMOUNT": 10.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-08T05:23:10",
    "PRICE": 20.15,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10723",
    "TOTAL_AMOUNT": 40.30
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-08T05:24:29",
    "PRICE": 20.15,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10724",
    "TOTAL_AMOUNT": 40.30
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-03-22T20:55:31",
    "PRICE": 495.69,
    "QUANTITY": 5,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "10725",
    "TOTAL_AMOUNT": 2478.45
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-03-22T20:55:31",
    "PRICE": 495.69,
    "QUANTITY": 5,
    "STATUS": "Filled",
    "SYMBOL": "GLD",
    "TX_NUMBER": "10726",
    "TOTAL_AMOUNT": 2478.45
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-08T09:20:41",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10727",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-08T12:48:09",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10728",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-09T03:44:30",
    "PRICE": 15.10,
    "QUANTITY": 7,
    "STATUS": "Pending",
    "SYMBOL": "FCO",
    "TX_NUMBER": "10729",
    "TOTAL_AMOUNT": 105.70
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-09T04:19:14",
    "PRICE": 10.11,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "RBM",
    "TX_NUMBER": "10730",
    "TOTAL_AMOUNT": 10.11
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-09T04:53:21",
    "PRICE": 10.11,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "RBM",
    "TX_NUMBER": "10731",
    "TOTAL_AMOUNT": 10.11
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-09T05:58:41",
    "PRICE": 32.47,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "IBM",
    "TX_NUMBER": "10732",
    "TOTAL_AMOUNT": 162.35
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-09T06:16:41",
    "PRICE": 3.30,
    "QUANTITY": 15,
    "STATUS": "Pending",
    "SYMBOL": "IBM",
    "TX_NUMBER": "10733",
    "TOTAL_AMOUNT": 49.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-09T06:16:58",
    "PRICE": 10.11,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "RBM",
    "TX_NUMBER": "10734",
    "TOTAL_AMOUNT": 10.11
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-09T06:19:07",
    "PRICE": 22.67,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "RBM1",
    "TX_NUMBER": "10735",
    "TOTAL_AMOUNT": 113.35
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-09T06:20:27",
    "PRICE": 32.67,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "RBM1",
    "TX_NUMBER": "10736",
    "TOTAL_AMOUNT": 163.35
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-10T11:06:12",
    "PRICE": 207.85,
    "QUANTITY": 30,
    "STATUS": "Pending",
    "SYMBOL": "APL",
    "TX_NUMBER": "10737",
    "TOTAL_AMOUNT": 6235.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2020-07-24T08:04:54",
    "PRICE": 207.85,
    "QUANTITY": 56,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10738",
    "TOTAL_AMOUNT": 11639.60
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-11T11:32:26",
    "PRICE": 1111.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10741",
    "TOTAL_AMOUNT": 111100.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-11T16:15:59",
    "PRICE": 25.52,
    "QUANTITY": 131,
    "STATUS": "Pending",
    "SYMBOL": "TXT",
    "TX_NUMBER": "10742",
    "TOTAL_AMOUNT": 3343.12
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-11T16:43:33",
    "PRICE": 25.52,
    "QUANTITY": 131,
    "STATUS": "Pending",
    "SYMBOL": "TXT",
    "TX_NUMBER": "10743",
    "TOTAL_AMOUNT": 3343.12
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-14T01:39:40",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10745",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-14T01:39:42",
    "PRICE": 150.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10746",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-14T03:00:16",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPO",
    "TX_NUMBER": "10747",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-14T03:00:16",
    "PRICE": 150.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "SPO",
    "TX_NUMBER": "10748",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-08-05T22:00:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Filled",
    "SYMBOL": "SPO",
    "TX_NUMBER": "10749",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-14T03:01:11",
    "PRICE": 150.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "SPO",
    "TX_NUMBER": "10750",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-08-05T22:00:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "SPO",
    "TX_NUMBER": "10751",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-14T03:02:25",
    "PRICE": 150.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "SPO",
    "TX_NUMBER": "10752",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-14T03:03:00",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10753",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-14T03:03:00",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10754",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2030-08-05T22:00:00",
    "PRICE": 100.00,
    "QUANTITY": 20,
    "STATUS": "Canceled",
    "SYMBOL": "NPARAMA",
    "TX_NUMBER": "10755",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-14T03:03:40",
    "PRICE": 150.00,
    "QUANTITY": 20,
    "STATUS": "Canceled",
    "SYMBOL": "DPS",
    "TX_NUMBER": "10756",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-14T03:56:45",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10757",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-14T03:56:45",
    "PRICE": 176.98,
    "QUANTITY": 60,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10758",
    "TOTAL_AMOUNT": 10618.80
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2030-08-05T22:00:00",
    "PRICE": 100.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "NPARAMA",
    "TX_NUMBER": "10759",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2030-08-05T22:00:00",
    "PRICE": 100.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "NPARAMA",
    "TX_NUMBER": "10760",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-08-05T22:00:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "TST1",
    "TX_NUMBER": "10761",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-14T08:26:23",
    "PRICE": 150.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "TST1",
    "TX_NUMBER": "10762",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-08-05T22:00:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "TST1",
    "TX_NUMBER": "10763",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-14T08:29:30",
    "PRICE": 150.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "tst",
    "TX_NUMBER": "10764",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-08-05T22:00:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "TST1",
    "TX_NUMBER": "10765",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-14T08:32:04",
    "PRICE": 150.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "tt1",
    "TX_NUMBER": "10766",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-08-05T22:00:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "TST1",
    "TX_NUMBER": "10767",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-14T09:57:31",
    "PRICE": 150.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "tt1",
    "TX_NUMBER": "10768",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-08-05T22:00:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "TST1",
    "TX_NUMBER": "10769",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-15T06:49:14",
    "PRICE": 150.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "tt1",
    "TX_NUMBER": "10770",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-08-05T22:00:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "TST1",
    "TX_NUMBER": "10771",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-15T07:16:19",
    "PRICE": 150.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "tt1",
    "TX_NUMBER": "10772",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-16T15:03:14",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10773",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-16T19:14:13",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10774",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-16T17:57:34",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10775",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-06-17T19:30:50",
    "PRICE": 10.00,
    "QUANTITY": 3,
    "STATUS": "Pending",
    "SYMBOL": "MULE",
    "TX_NUMBER": "10776",
    "TOTAL_AMOUNT": 30.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-08-05T22:00:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "TST1",
    "TX_NUMBER": "10777",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2030-08-05T22:00:00",
    "PRICE": 100.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "NPARAMA",
    "TX_NUMBER": "10778",
    "TOTAL_AMOUNT": 2000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2020-08-05T22:00:00",
    "PRICE": 150.00,
    "QUANTITY": 10,
    "STATUS": "Canceled",
    "SYMBOL": "TST1",
    "TX_NUMBER": "10779",
    "TOTAL_AMOUNT": 1500.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-24T03:13:41",
    "PRICE": 150.00,
    "QUANTITY": 20,
    "STATUS": "Filled",
    "SYMBOL": "tt1",
    "TX_NUMBER": "10780",
    "TOTAL_AMOUNT": 3000.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-25T15:44:32",
    "PRICE": 10.33,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "RBM2",
    "TX_NUMBER": "10781",
    "TOTAL_AMOUNT": 41.32
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-25T18:29:57",
    "PRICE": 10.33,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "RBM2",
    "TX_NUMBER": "10782",
    "TOTAL_AMOUNT": 41.32
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-25T18:31:32",
    "PRICE": 10.33,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "RBM",
    "TX_NUMBER": "10783",
    "TOTAL_AMOUNT": 41.32
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-25T18:32:15",
    "PRICE": 10.55,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "RBM",
    "TX_NUMBER": "10784",
    "TOTAL_AMOUNT": 42.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-06-25T19:09:02",
    "PRICE": 10.70,
    "QUANTITY": 4,
    "STATUS": "Pending",
    "SYMBOL": "RBM",
    "TX_NUMBER": "10785",
    "TOTAL_AMOUNT": 42.80
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-07-25T00:00:00",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10786",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-07-25T00:00:00",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10787",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-07-25T00:00:00",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10788",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-07-25T00:00:00",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10789",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-07-25T00:00:00",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10790",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-07-25T00:00:00",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10791",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-07-25T00:00:00",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10792",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-07-25T00:00:00",
    "PRICE": 6.30,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AAA",
    "TX_NUMBER": "10793",
    "TOTAL_AMOUNT": 31.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-08-22T11:22:57",
    "PRICE": 225.21,
    "QUANTITY": 25,
    "STATUS": "Pending",
    "SYMBOL": "AGS",
    "TX_NUMBER": "10794",
    "TOTAL_AMOUNT": 5630.25
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-08-22T12:40:40",
    "PRICE": 305.21,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AGS",
    "TX_NUMBER": "10795",
    "TOTAL_AMOUNT": 1526.05
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-08-22T12:41:51",
    "PRICE": 305.21,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AGS",
    "TX_NUMBER": "10796",
    "TOTAL_AMOUNT": 1526.05
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-08-22T12:42:08",
    "PRICE": 305.21,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "AGS",
    "TX_NUMBER": "10797",
    "TOTAL_AMOUNT": 1526.05
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-08-22T12:51:15",
    "PRICE": 205.21,
    "QUANTITY": 5,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10798",
    "TOTAL_AMOUNT": 1026.05
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-01T22:56:02",
    "PRICE": 210.10,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10799",
    "TOTAL_AMOUNT": 10505.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-01T22:57:06",
    "PRICE": 210.10,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10800",
    "TOTAL_AMOUNT": 10505.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-01T23:00:13",
    "PRICE": 211.10,
    "QUANTITY": 51,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10801",
    "TOTAL_AMOUNT": 10766.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-02T13:36:17",
    "PRICE": 212.10,
    "QUANTITY": 51,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10802",
    "TOTAL_AMOUNT": 10817.10
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-03T14:09:02",
    "PRICE": 214.10,
    "QUANTITY": 52,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10803",
    "TOTAL_AMOUNT": 11133.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-03T18:19:30",
    "PRICE": 214.10,
    "QUANTITY": 52,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10804",
    "TOTAL_AMOUNT": 11133.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-03T19:59:28",
    "PRICE": 214.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10805",
    "TOTAL_AMOUNT": 13274.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-03T20:23:48",
    "PRICE": 314.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10806",
    "TOTAL_AMOUNT": 19474.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-04T12:17:59",
    "PRICE": 414.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10807",
    "TOTAL_AMOUNT": 25674.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-04T12:19:51",
    "PRICE": 514.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10808",
    "TOTAL_AMOUNT": 31874.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-04T12:54:32",
    "PRICE": 514.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10809",
    "TOTAL_AMOUNT": 31874.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-04T13:12:30",
    "PRICE": 514.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10810",
    "TOTAL_AMOUNT": 31874.20
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T13:39:15",
    "PRICE": 614.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10811",
    "TOTAL_AMOUNT": 38074.20
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T13:39:52",
    "PRICE": 714.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10812",
    "TOTAL_AMOUNT": 44274.20
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T13:42:03",
    "PRICE": 714.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "",
    "TX_NUMBER": "10813",
    "TOTAL_AMOUNT": 44274.20
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T13:50:24",
    "PRICE": 814.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10814",
    "TOTAL_AMOUNT": 50474.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-04T14:17:44",
    "PRICE": 524.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10815",
    "TOTAL_AMOUNT": 32494.20
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-04T14:21:28",
    "PRICE": 912.10,
    "QUANTITY": 51,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10816",
    "TOTAL_AMOUNT": 46517.10
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T18:35:17",
    "PRICE": 914.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10817",
    "TOTAL_AMOUNT": 56674.20
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T20:16:43",
    "PRICE": 100.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10818",
    "TOTAL_AMOUNT": 1000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T21:05:52",
    "PRICE": 101.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10819",
    "TOTAL_AMOUNT": 1010.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T21:39:44",
    "PRICE": 102.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10820",
    "TOTAL_AMOUNT": 1020.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T21:40:02",
    "PRICE": 102.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10821",
    "TOTAL_AMOUNT": 1020.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T21:40:10",
    "PRICE": 102.00,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10822",
    "TOTAL_AMOUNT": 1020.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T22:28:34",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10824",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T22:42:31",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10825",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T22:46:38",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10826",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T22:47:05",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10827",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T22:51:30",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": ":SYMBOL2",
    "TX_NUMBER": "10828",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T23:03:01",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10829",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-04T23:28:28",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": ":SYMBOL2",
    "TX_NUMBER": "10830",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T11:30:45",
    "PRICE": 1114.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10831",
    "TOTAL_AMOUNT": 69074.20
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T11:33:45",
    "PRICE": 1214.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10832",
    "TOTAL_AMOUNT": 75274.20
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T11:44:34",
    "PRICE": 1214.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10833",
    "TOTAL_AMOUNT": 75274.20
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T11:51:09",
    "PRICE": 1414.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10834",
    "TOTAL_AMOUNT": 87674.20
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T12:16:15",
    "PRICE": 1514.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10835",
    "TOTAL_AMOUNT": 93874.20
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T13:08:53",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10836",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T14:25:30",
    "PRICE": 1000.00,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10837",
    "TOTAL_AMOUNT": 101000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T14:31:25",
    "PRICE": 1000.00,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10838",
    "TOTAL_AMOUNT": 101000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T14:34:33",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Filled",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10839",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T18:10:29",
    "PRICE": 1000.00,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10840",
    "TOTAL_AMOUNT": 100000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T18:39:16",
    "PRICE": 1000.00,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10841",
    "TOTAL_AMOUNT": 101000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T20:20:44",
    "PRICE": 1000.00,
    "QUANTITY": 101,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10842",
    "TOTAL_AMOUNT": 101000.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T20:24:41",
    "PRICE": 1114.00,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10843",
    "TOTAL_AMOUNT": 69068.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T20:26:38",
    "PRICE": 1114.00,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10844",
    "TOTAL_AMOUNT": 69068.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T20:38:45",
    "PRICE": 1314.00,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10845",
    "TOTAL_AMOUNT": 81468.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T20:41:59",
    "PRICE": 1315.00,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10846",
    "TOTAL_AMOUNT": 81530.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T20:44:05",
    "PRICE": 1315.00,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10847",
    "TOTAL_AMOUNT": 81530.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-05T20:46:34",
    "PRICE": 1315.00,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10848",
    "TOTAL_AMOUNT": 81530.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-05T21:08:51",
    "PRICE": 2315.00,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10849",
    "TOTAL_AMOUNT": 143530.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-05T21:11:50",
    "PRICE": 2415.00,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10850",
    "TOTAL_AMOUNT": 149730.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-10-06T11:22:42",
    "PRICE": 2415.00,
    "QUANTITY": 32,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10851",
    "TOTAL_AMOUNT": 77280.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-06T11:53:58",
    "PRICE": 2015.00,
    "QUANTITY": 32,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10852",
    "TOTAL_AMOUNT": 64480.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-16T11:38:35",
    "PRICE": 1614.10,
    "QUANTITY": 62,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10853",
    "TOTAL_AMOUNT": 100074.20
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-16T11:38:50",
    "PRICE": 3015.00,
    "QUANTITY": 32,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10854",
    "TOTAL_AMOUNT": 96480.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-18T14:42:04",
    "PRICE": 4015.00,
    "QUANTITY": 32,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10855",
    "TOTAL_AMOUNT": 128480.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-10-18T16:26:41",
    "PRICE": 5015.00,
    "QUANTITY": 32,
    "STATUS": "Pending",
    "SYMBOL": "DIA",
    "TX_NUMBER": "10856",
    "TOTAL_AMOUNT": 160480.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.10,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10857",
    "TOTAL_AMOUNT": 551.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.10,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10858",
    "TOTAL_AMOUNT": 551.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.10,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10859",
    "TOTAL_AMOUNT": 551.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.10,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10860",
    "TOTAL_AMOUNT": 551.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.10,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10861",
    "TOTAL_AMOUNT": 551.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.10,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10862",
    "TOTAL_AMOUNT": 551.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.19,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10863",
    "TOTAL_AMOUNT": 551.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.19,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10864",
    "TOTAL_AMOUNT": 551.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.19,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10865",
    "TOTAL_AMOUNT": 551.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.19,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10866",
    "TOTAL_AMOUNT": 551.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.19,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10867",
    "TOTAL_AMOUNT": 551.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.19,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10868",
    "TOTAL_AMOUNT": 551.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.19,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10869",
    "TOTAL_AMOUNT": 551.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.19,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10870",
    "TOTAL_AMOUNT": 551.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.19,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10871",
    "TOTAL_AMOUNT": 551.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-11-05T20:50:57",
    "PRICE": 5.50,
    "QUANTITY": 12,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10872",
    "TOTAL_AMOUNT": 66.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-11-05T20:50:57",
    "PRICE": 5.50,
    "QUANTITY": 12,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10873",
    "TOTAL_AMOUNT": 66.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-11-05T20:50:57",
    "PRICE": 5.50,
    "QUANTITY": 12,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10874",
    "TOTAL_AMOUNT": 66.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-11-05T20:50:57",
    "PRICE": 5.50,
    "QUANTITY": 12,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10875",
    "TOTAL_AMOUNT": 66.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-11-05T20:50:57",
    "PRICE": 5.50,
    "QUANTITY": 12,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10876",
    "TOTAL_AMOUNT": 66.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-11-05T20:50:57",
    "PRICE": 5.50,
    "QUANTITY": 12,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10877",
    "TOTAL_AMOUNT": 66.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-11-05T20:50:57",
    "PRICE": 5.50,
    "QUANTITY": 12,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10878",
    "TOTAL_AMOUNT": 66.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-11-05T20:50:57",
    "PRICE": 5.50,
    "QUANTITY": 12,
    "STATUS": "Canceled",
    "SYMBOL": "DWN",
    "TX_NUMBER": "10879",
    "TOTAL_AMOUNT": 66.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-11-05T20:50:57",
    "PRICE": 5.20,
    "QUANTITY": 12,
    "STATUS": "Canceled",
    "SYMBOL": "YYY",
    "TX_NUMBER": "10880",
    "TOTAL_AMOUNT": 62.40
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-11-05T20:50:57",
    "PRICE": 5.20,
    "QUANTITY": 12,
    "STATUS": "Filled",
    "SYMBOL": "YYY",
    "TX_NUMBER": "10881",
    "TOTAL_AMOUNT": 62.40
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-11-05T20:50:57",
    "PRICE": 5.20,
    "QUANTITY": 12,
    "STATUS": "Canceled",
    "SYMBOL": "YYY",
    "TX_NUMBER": "10882",
    "TOTAL_AMOUNT": 62.40
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-11-05T20:50:57",
    "PRICE": 4.00,
    "QUANTITY": 12,
    "STATUS": "Canceled",
    "SYMBOL": "YYY",
    "TX_NUMBER": "10883",
    "TOTAL_AMOUNT": 48.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-11-09T20:50:57",
    "PRICE": 4.00,
    "QUANTITY": 12,
    "STATUS": "Filled",
    "SYMBOL": "YYY",
    "TX_NUMBER": "10884",
    "TOTAL_AMOUNT": 48.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-11-09T20:50:57",
    "PRICE": 4.00,
    "QUANTITY": 12,
    "STATUS": "Pending",
    "SYMBOL": "YYY",
    "TX_NUMBER": "10885",
    "TOTAL_AMOUNT": 48.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-11-09T20:50:57",
    "PRICE": 4.00,
    "QUANTITY": 12,
    "STATUS": "Filled",
    "SYMBOL": "YYY",
    "TX_NUMBER": "10886",
    "TOTAL_AMOUNT": 48.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-11-09T20:50:57",
    "PRICE": 4.00,
    "QUANTITY": 12,
    "STATUS": "Pending",
    "SYMBOL": "YYY",
    "TX_NUMBER": "10887",
    "TOTAL_AMOUNT": 48.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.19,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10888",
    "TOTAL_AMOUNT": 551.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.19,
    "QUANTITY": 109,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10889",
    "TOTAL_AMOUNT": 6015.71
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2017-11-19T15:20:21",
    "PRICE": 55.19,
    "QUANTITY": 10,
    "STATUS": "Pending",
    "SYMBOL": "FED",
    "TX_NUMBER": "10890",
    "TOTAL_AMOUNT": 551.90
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-11-09T20:50:57",
    "PRICE": 4.00,
    "QUANTITY": 12,
    "STATUS": "Filled",
    "SYMBOL": "YYY",
    "TX_NUMBER": "10891",
    "TOTAL_AMOUNT": 48.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2021-11-19T17:10:57",
    "PRICE": 4.00,
    "QUANTITY": 12,
    "STATUS": "Filled",
    "SYMBOL": "YYY",
    "TX_NUMBER": "10892",
    "TOTAL_AMOUNT": 48.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-12-05T18:25:04",
    "PRICE": 100.80,
    "QUANTITY": 6774,
    "STATUS": "Pending",
    "SYMBOL": "TST",
    "TX_NUMBER": "10893",
    "TOTAL_AMOUNT": 682819.22
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-12-05T18:26:33",
    "PRICE": 101.80,
    "QUANTITY": 100,
    "STATUS": "Pending",
    "SYMBOL": "TST",
    "TX_NUMBER": "10894",
    "TOTAL_AMOUNT": 10180.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-12-05T18:52:26",
    "PRICE": 2.00,
    "QUANTITY": 2,
    "STATUS": "Pending",
    "SYMBOL": "TST",
    "TX_NUMBER": "10895",
    "TOTAL_AMOUNT": 4.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-12-05T19:11:31",
    "PRICE": 101.80,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TST",
    "TX_NUMBER": "10896",
    "TOTAL_AMOUNT": 101.80
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-12-06T18:51:31",
    "PRICE": 100.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TST",
    "TX_NUMBER": "10897",
    "TOTAL_AMOUNT": 100.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-12-06T18:59:00",
    "PRICE": 100.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TST",
    "TX_NUMBER": "10898",
    "TOTAL_AMOUNT": 100.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-12-06T21:13:47",
    "PRICE": 100.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TST",
    "TX_NUMBER": "10899",
    "TOTAL_AMOUNT": 100.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2021-12-06T21:54:31",
    "PRICE": 100.00,
    "QUANTITY": 1,
    "STATUS": "Pending",
    "SYMBOL": "TST",
    "TX_NUMBER": "10900",
    "TOTAL_AMOUNT": 100.00
  }
])