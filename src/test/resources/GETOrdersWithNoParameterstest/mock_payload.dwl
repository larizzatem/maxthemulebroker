[
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 1,
    "PRICE": 1.2999999523162842,
    "ACTION": "SELL",
    "TX_NUMBER": 1,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 1,
    "PRICE": 1.2999999523162842,
    "ACTION": "SELL",
    "TX_NUMBER": 2,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 1,
    "PRICE": 1.2999999523162842,
    "ACTION": "SELL",
    "TX_NUMBER": 3,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 1,
    "PRICE": 1.2999999523162842,
    "ACTION": "BUY",
    "TX_NUMBER": 4,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 25.100000381469727,
    "ACTION": "BUY",
    "TX_NUMBER": 5,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 6,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 11,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 13,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-04T13:48:01|,
    "SYMBOL": "SPY",
    "QUANTITY": 6774,
    "PRICE": 613.7999877929688,
    "ACTION": "SELL",
    "TX_NUMBER": 65,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-11-07T21:05:17|,
    "SYMBOL": "DIA",
    "QUANTITY": 4935,
    "PRICE": 224.11000061035156,
    "ACTION": "BUY",
    "TX_NUMBER": 66,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-05T07:22:56|,
    "SYMBOL": "DIA",
    "QUANTITY": 2247,
    "PRICE": 815.1500244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 67,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-08T01:27:19|,
    "SYMBOL": "SPY",
    "QUANTITY": 7120,
    "PRICE": 380.44000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 68,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-08T18:33:28|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3055,
    "PRICE": 478.6000061035156,
    "ACTION": "SELL",
    "TX_NUMBER": 69,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-10T12:56:36|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7009,
    "PRICE": 200.69000244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 70,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-01T19:34:14|,
    "SYMBOL": "DIA",
    "QUANTITY": 6932,
    "PRICE": 274.4800109863281,
    "ACTION": "BUY",
    "TX_NUMBER": 71,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-04T06:24:13|,
    "SYMBOL": "IWM",
    "QUANTITY": 3602,
    "PRICE": 284.010009765625,
    "ACTION": "SELL",
    "TX_NUMBER": 72,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-09T14:00:34|,
    "SYMBOL": "DIA",
    "QUANTITY": 2484,
    "PRICE": 361.1300048828125,
    "ACTION": "SELL",
    "TX_NUMBER": 73,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-31T12:42:32|,
    "SYMBOL": "SPY",
    "QUANTITY": 4858,
    "PRICE": 336.4599914550781,
    "ACTION": "BUY",
    "TX_NUMBER": 74,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-21T16:28:05|,
    "SYMBOL": "IWM",
    "QUANTITY": 5155,
    "PRICE": 635.3099975585938,
    "ACTION": "BUY",
    "TX_NUMBER": 75,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-07T13:09:28|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2700,
    "PRICE": 402.9200134277344,
    "ACTION": "BUY",
    "TX_NUMBER": 76,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-18T11:48:32|,
    "SYMBOL": "GLD",
    "QUANTITY": 9952,
    "PRICE": 559.530029296875,
    "ACTION": "BUY",
    "TX_NUMBER": 77,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-22T11:44:15|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1743,
    "PRICE": 344.9200134277344,
    "ACTION": "BUY",
    "TX_NUMBER": 78,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-09T16:37:39|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8883,
    "PRICE": 150.1999969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 79,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-11-08T15:12:04|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6482,
    "PRICE": 853.1400146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 80,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-14T02:25:02|,
    "SYMBOL": "SLV",
    "QUANTITY": 3234,
    "PRICE": 961.7100219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 81,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-08T21:40:50|,
    "SYMBOL": "DIA",
    "QUANTITY": 505,
    "PRICE": 107.8499984741211,
    "ACTION": "BUY",
    "TX_NUMBER": 82,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-30T04:06:04|,
    "SYMBOL": "SPY",
    "QUANTITY": 6369,
    "PRICE": 641.8499755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 83,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-15T00:07:04|,
    "SYMBOL": "GLD",
    "QUANTITY": 9384,
    "PRICE": 837.030029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 84,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-26T14:05:35|,
    "SYMBOL": "DIA",
    "QUANTITY": 6835,
    "PRICE": 523.5499877929688,
    "ACTION": "SELL",
    "TX_NUMBER": 85,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-23T18:20:47|,
    "SYMBOL": "QQQ",
    "QUANTITY": 480,
    "PRICE": 284.8800048828125,
    "ACTION": "SELL",
    "TX_NUMBER": 86,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-13T09:55:10|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5946,
    "PRICE": 742.9099731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 87,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-15T12:15:08|,
    "SYMBOL": "SPY",
    "QUANTITY": 3136,
    "PRICE": 277.42999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 88,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-08T21:29:49|,
    "SYMBOL": "SLV",
    "QUANTITY": 8657,
    "PRICE": 911.1699829101562,
    "ACTION": "BUY",
    "TX_NUMBER": 89,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-17T08:59:00|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3503,
    "PRICE": 294.17999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 90,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-21T02:26:14|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7463,
    "PRICE": 374.1600036621094,
    "ACTION": "SELL",
    "TX_NUMBER": 91,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-11-07T02:59:20|,
    "SYMBOL": "DIA",
    "QUANTITY": 4675,
    "PRICE": 482.1400146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 92,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-07T03:03:11|,
    "SYMBOL": "SPY",
    "QUANTITY": 230,
    "PRICE": 891.1900024414062,
    "ACTION": "BUY",
    "TX_NUMBER": 93,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-10T05:23:46|,
    "SYMBOL": "GLD",
    "QUANTITY": 2319,
    "PRICE": 189.30999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 94,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-15T22:47:30|,
    "SYMBOL": "DIA",
    "QUANTITY": 3015,
    "PRICE": 989.2100219726562,
    "ACTION": "SELL",
    "TX_NUMBER": 95,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-05T05:01:13|,
    "SYMBOL": "GLD",
    "QUANTITY": 3943,
    "PRICE": 451.67999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 96,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-04T10:09:59|,
    "SYMBOL": "GLD",
    "QUANTITY": 8682,
    "PRICE": 493.67999267578125,
    "ACTION": "SELL",
    "TX_NUMBER": 97,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-19T02:10:40|,
    "SYMBOL": "DIA",
    "QUANTITY": 5829,
    "PRICE": 604.4400024414062,
    "ACTION": "BUY",
    "TX_NUMBER": 98,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-06T08:25:16|,
    "SYMBOL": "SLV",
    "QUANTITY": 7818,
    "PRICE": 110.44000244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 99,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-04T15:13:56|,
    "SYMBOL": "SPY",
    "QUANTITY": 6298,
    "PRICE": 221.07000732421875,
    "ACTION": "SELL",
    "TX_NUMBER": 100,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-07T01:58:30|,
    "SYMBOL": "DIA",
    "QUANTITY": 3526,
    "PRICE": 899.72998046875,
    "ACTION": "SELL",
    "TX_NUMBER": 101,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-13T20:37:56|,
    "SYMBOL": "DIA",
    "QUANTITY": 7792,
    "PRICE": 735.3599853515625,
    "ACTION": "BUY",
    "TX_NUMBER": 102,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-16T17:01:48|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1941,
    "PRICE": 310.55999755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 103,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-24T03:09:34|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9146,
    "PRICE": 286.4599914550781,
    "ACTION": "SELL",
    "TX_NUMBER": 104,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-06T13:54:54|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5562,
    "PRICE": 524.030029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 105,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-01T10:30:46|,
    "SYMBOL": "DIA",
    "QUANTITY": 7203,
    "PRICE": 272.5400085449219,
    "ACTION": "SELL",
    "TX_NUMBER": 106,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-31T11:19:56|,
    "SYMBOL": "SLV",
    "QUANTITY": 3191,
    "PRICE": 270.9100036621094,
    "ACTION": "BUY",
    "TX_NUMBER": 107,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-29T19:17:26|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7924,
    "PRICE": 317.9700012207031,
    "ACTION": "SELL",
    "TX_NUMBER": 108,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-24T02:36:43|,
    "SYMBOL": "SLV",
    "QUANTITY": 2540,
    "PRICE": 462.7200012207031,
    "ACTION": "BUY",
    "TX_NUMBER": 109,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-26T22:49:53|,
    "SYMBOL": "IWM",
    "QUANTITY": 9063,
    "PRICE": 614.4500122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 110,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-13T10:42:28|,
    "SYMBOL": "IWM",
    "QUANTITY": 8647,
    "PRICE": 884.510009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 111,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-07T04:29:54|,
    "SYMBOL": "GLD",
    "QUANTITY": 7403,
    "PRICE": 774.6400146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 112,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-11T22:15:22|,
    "SYMBOL": "DIA",
    "QUANTITY": 1581,
    "PRICE": 566.1300048828125,
    "ACTION": "SELL",
    "TX_NUMBER": 113,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-09T09:09:43|,
    "SYMBOL": "DIA",
    "QUANTITY": 5502,
    "PRICE": 884.469970703125,
    "ACTION": "BUY",
    "TX_NUMBER": 114,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-19T05:35:55|,
    "SYMBOL": "GLD",
    "QUANTITY": 2335,
    "PRICE": 975.8300170898438,
    "ACTION": "BUY",
    "TX_NUMBER": 115,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-03T10:38:00|,
    "SYMBOL": "GLD",
    "QUANTITY": 1740,
    "PRICE": 692.5900268554688,
    "ACTION": "SELL",
    "TX_NUMBER": 116,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-28T18:52:00|,
    "SYMBOL": "GLD",
    "QUANTITY": 1126,
    "PRICE": 616.469970703125,
    "ACTION": "BUY",
    "TX_NUMBER": 117,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-18T14:09:25|,
    "SYMBOL": "DIA",
    "QUANTITY": 1264,
    "PRICE": 933.2999877929688,
    "ACTION": "SELL",
    "TX_NUMBER": 118,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-16T22:44:48|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3070,
    "PRICE": 698.4199829101562,
    "ACTION": "BUY",
    "TX_NUMBER": 119,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-05T04:13:10|,
    "SYMBOL": "DIA",
    "QUANTITY": 2388,
    "PRICE": 843.6699829101562,
    "ACTION": "BUY",
    "TX_NUMBER": 120,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-10T19:48:01|,
    "SYMBOL": "SPY",
    "QUANTITY": 901,
    "PRICE": 723.8400268554688,
    "ACTION": "SELL",
    "TX_NUMBER": 121,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-09T20:24:43|,
    "SYMBOL": "SPY",
    "QUANTITY": 3880,
    "PRICE": 978.4099731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 122,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-14T11:54:06|,
    "SYMBOL": "GLD",
    "QUANTITY": 6125,
    "PRICE": 116.86000061035156,
    "ACTION": "SELL",
    "TX_NUMBER": 123,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-09T11:19:46|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4684,
    "PRICE": 450.8900146484375,
    "ACTION": "BUY",
    "TX_NUMBER": 124,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-05T21:51:38|,
    "SYMBOL": "SPY",
    "QUANTITY": 1118,
    "PRICE": 360.3299865722656,
    "ACTION": "SELL",
    "TX_NUMBER": 125,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-07T14:10:19|,
    "SYMBOL": "IWM",
    "QUANTITY": 8914,
    "PRICE": 418.1600036621094,
    "ACTION": "BUY",
    "TX_NUMBER": 126,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-20T21:22:46|,
    "SYMBOL": "DIA",
    "QUANTITY": 4072,
    "PRICE": 732.6799926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 127,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-29T16:19:13|,
    "SYMBOL": "SPY",
    "QUANTITY": 3309,
    "PRICE": 499.8800048828125,
    "ACTION": "SELL",
    "TX_NUMBER": 128,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-03T12:17:56|,
    "SYMBOL": "GLD",
    "QUANTITY": 7967,
    "PRICE": 577.1300048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 129,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-24T01:03:51|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9051,
    "PRICE": 832.3400268554688,
    "ACTION": "BUY",
    "TX_NUMBER": 130,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-28T09:52:51|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4685,
    "PRICE": 939.780029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 131,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-17T04:41:22|,
    "SYMBOL": "IWM",
    "QUANTITY": 2837,
    "PRICE": 419.92999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 132,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-05T16:15:54|,
    "SYMBOL": "SPY",
    "QUANTITY": 7089,
    "PRICE": 822.4299926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 133,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-21T14:42:58|,
    "SYMBOL": "GLD",
    "QUANTITY": 4027,
    "PRICE": 141.85000610351562,
    "ACTION": "BUY",
    "TX_NUMBER": 134,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-29T15:33:54|,
    "SYMBOL": "GLD",
    "QUANTITY": 8425,
    "PRICE": 724.3300170898438,
    "ACTION": "BUY",
    "TX_NUMBER": 135,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-18T18:43:08|,
    "SYMBOL": "IWM",
    "QUANTITY": 6283,
    "PRICE": 124.16000366210938,
    "ACTION": "BUY",
    "TX_NUMBER": 136,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-25T20:49:09|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8951,
    "PRICE": 653.7000122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 137,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-13T03:10:14|,
    "SYMBOL": "SLV",
    "QUANTITY": 2377,
    "PRICE": 217.0,
    "ACTION": "BUY",
    "TX_NUMBER": 138,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-26T10:58:53|,
    "SYMBOL": "SPY",
    "QUANTITY": 2743,
    "PRICE": 931.02001953125,
    "ACTION": "BUY",
    "TX_NUMBER": 139,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-18T22:25:45|,
    "SYMBOL": "SPY",
    "QUANTITY": 2073,
    "PRICE": 522.2100219726562,
    "ACTION": "SELL",
    "TX_NUMBER": 140,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-30T00:10:21|,
    "SYMBOL": "IWM",
    "QUANTITY": 7096,
    "PRICE": 376.8999938964844,
    "ACTION": "SELL",
    "TX_NUMBER": 141,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-11T22:36:23|,
    "SYMBOL": "DIA",
    "QUANTITY": 7301,
    "PRICE": 300.2900085449219,
    "ACTION": "BUY",
    "TX_NUMBER": 142,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-01T21:25:56|,
    "SYMBOL": "GLD",
    "QUANTITY": 796,
    "PRICE": 907.0599975585938,
    "ACTION": "SELL",
    "TX_NUMBER": 143,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-14T11:16:10|,
    "SYMBOL": "DIA",
    "QUANTITY": 2112,
    "PRICE": 161.27000427246094,
    "ACTION": "BUY",
    "TX_NUMBER": 144,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-23T16:27:46|,
    "SYMBOL": "DIA",
    "QUANTITY": 8674,
    "PRICE": 136.9199981689453,
    "ACTION": "SELL",
    "TX_NUMBER": 145,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-16T17:56:14|,
    "SYMBOL": "SLV",
    "QUANTITY": 1786,
    "PRICE": 78.66999816894531,
    "ACTION": "SELL",
    "TX_NUMBER": 146,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-17T14:53:13|,
    "SYMBOL": "SPY",
    "QUANTITY": 5828,
    "PRICE": 373.95001220703125,
    "ACTION": "BUY",
    "TX_NUMBER": 147,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-07T05:03:07|,
    "SYMBOL": "SPY",
    "QUANTITY": 6170,
    "PRICE": 747.52001953125,
    "ACTION": "BUY",
    "TX_NUMBER": 148,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-20T15:20:06|,
    "SYMBOL": "SLV",
    "QUANTITY": 3189,
    "PRICE": 579.4099731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 149,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-06T11:08:28|,
    "SYMBOL": "GLD",
    "QUANTITY": 8100,
    "PRICE": 168.75999450683594,
    "ACTION": "SELL",
    "TX_NUMBER": 150,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-18T15:33:01|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9615,
    "PRICE": 159.88999938964844,
    "ACTION": "SELL",
    "TX_NUMBER": 151,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-17T18:08:50|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6245,
    "PRICE": 781.3499755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 152,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-08T20:46:47|,
    "SYMBOL": "GLD",
    "QUANTITY": 1701,
    "PRICE": 652.9500122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 153,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-02T02:08:23|,
    "SYMBOL": "DIA",
    "QUANTITY": 8308,
    "PRICE": 169.91000366210938,
    "ACTION": "BUY",
    "TX_NUMBER": 154,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-14T20:52:30|,
    "SYMBOL": "GLD",
    "QUANTITY": 5351,
    "PRICE": 170.72000122070312,
    "ACTION": "SELL",
    "TX_NUMBER": 155,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-06T00:49:02|,
    "SYMBOL": "DIA",
    "QUANTITY": 8117,
    "PRICE": 106.6500015258789,
    "ACTION": "SELL",
    "TX_NUMBER": 156,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-06T10:38:18|,
    "SYMBOL": "GLD",
    "QUANTITY": 6930,
    "PRICE": 943.1900024414062,
    "ACTION": "SELL",
    "TX_NUMBER": 157,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-01T19:59:51|,
    "SYMBOL": "DIA",
    "QUANTITY": 3946,
    "PRICE": 439.489990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 158,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-23T21:11:49|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9074,
    "PRICE": 777.3499755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 159,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-11-07T17:19:02|,
    "SYMBOL": "GLD",
    "QUANTITY": 8132,
    "PRICE": 567.1300048828125,
    "ACTION": "SELL",
    "TX_NUMBER": 160,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-02T02:26:07|,
    "SYMBOL": "GLD",
    "QUANTITY": 3962,
    "PRICE": 627.72998046875,
    "ACTION": "SELL",
    "TX_NUMBER": 161,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-09T20:30:35|,
    "SYMBOL": "SLV",
    "QUANTITY": 2420,
    "PRICE": 676.4600219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 162,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-29T16:30:07|,
    "SYMBOL": "SLV",
    "QUANTITY": 7325,
    "PRICE": 990.219970703125,
    "ACTION": "SELL",
    "TX_NUMBER": 163,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-30T01:19:25|,
    "SYMBOL": "IWM",
    "QUANTITY": 9584,
    "PRICE": 890.5599975585938,
    "ACTION": "SELL",
    "TX_NUMBER": 164,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-07T12:02:43|,
    "SYMBOL": "IWM",
    "QUANTITY": 9282,
    "PRICE": 773.719970703125,
    "ACTION": "BUY",
    "TX_NUMBER": 165,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-11-01T15:41:28|,
    "SYMBOL": "DIA",
    "QUANTITY": 6424,
    "PRICE": 308.5799865722656,
    "ACTION": "SELL",
    "TX_NUMBER": 166,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-03T02:12:32|,
    "SYMBOL": "IWM",
    "QUANTITY": 4702,
    "PRICE": 752.1400146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 167,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-21T17:22:41|,
    "SYMBOL": "SPY",
    "QUANTITY": 6793,
    "PRICE": 356.4100036621094,
    "ACTION": "BUY",
    "TX_NUMBER": 168,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-17T23:05:22|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2813,
    "PRICE": 701.2100219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 169,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-02T10:20:52|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4915,
    "PRICE": 581.2899780273438,
    "ACTION": "SELL",
    "TX_NUMBER": 170,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-14T22:26:38|,
    "SYMBOL": "DIA",
    "QUANTITY": 2958,
    "PRICE": 74.16000366210938,
    "ACTION": "BUY",
    "TX_NUMBER": 171,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-30T00:08:20|,
    "SYMBOL": "DIA",
    "QUANTITY": 3069,
    "PRICE": 634.8300170898438,
    "ACTION": "SELL",
    "TX_NUMBER": 172,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-21T22:36:23|,
    "SYMBOL": "IWM",
    "QUANTITY": 951,
    "PRICE": 885.8699951171875,
    "ACTION": "SELL",
    "TX_NUMBER": 173,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-01T15:23:07|,
    "SYMBOL": "SPY",
    "QUANTITY": 5206,
    "PRICE": 950.6799926757812,
    "ACTION": "SELL",
    "TX_NUMBER": 174,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-25T06:22:27|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8698,
    "PRICE": 343.8999938964844,
    "ACTION": "BUY",
    "TX_NUMBER": 175,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-08T15:55:04|,
    "SYMBOL": "SLV",
    "QUANTITY": 9829,
    "PRICE": 382.04998779296875,
    "ACTION": "BUY",
    "TX_NUMBER": 176,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-11T17:44:16|,
    "SYMBOL": "GLD",
    "QUANTITY": 8066,
    "PRICE": 788.8200073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 177,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-18T11:02:28|,
    "SYMBOL": "SLV",
    "QUANTITY": 3527,
    "PRICE": 920.489990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 178,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-29T04:05:13|,
    "SYMBOL": "GLD",
    "QUANTITY": 9825,
    "PRICE": 520.4600219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 179,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-14T10:24:57|,
    "SYMBOL": "GLD",
    "QUANTITY": 5523,
    "PRICE": 295.0400085449219,
    "ACTION": "BUY",
    "TX_NUMBER": 180,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-03T04:57:24|,
    "SYMBOL": "DIA",
    "QUANTITY": 8816,
    "PRICE": 512.6199951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 181,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-18T01:45:46|,
    "SYMBOL": "IWM",
    "QUANTITY": 1075,
    "PRICE": 934.2899780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 182,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-24T02:50:43|,
    "SYMBOL": "SLV",
    "QUANTITY": 7540,
    "PRICE": 618.8699951171875,
    "ACTION": "SELL",
    "TX_NUMBER": 183,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-19T22:53:37|,
    "SYMBOL": "SPY",
    "QUANTITY": 9092,
    "PRICE": 691.1500244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 184,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-28T05:47:47|,
    "SYMBOL": "DIA",
    "QUANTITY": 5097,
    "PRICE": 692.969970703125,
    "ACTION": "BUY",
    "TX_NUMBER": 185,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-18T02:48:09|,
    "SYMBOL": "DIA",
    "QUANTITY": 883,
    "PRICE": 683.030029296875,
    "ACTION": "BUY",
    "TX_NUMBER": 186,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-21T19:23:34|,
    "SYMBOL": "GLD",
    "QUANTITY": 9703,
    "PRICE": 668.2000122070312,
    "ACTION": "SELL",
    "TX_NUMBER": 187,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-16T01:11:04|,
    "SYMBOL": "IWM",
    "QUANTITY": 5978,
    "PRICE": 518.0,
    "ACTION": "BUY",
    "TX_NUMBER": 188,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-21T13:30:25|,
    "SYMBOL": "DIA",
    "QUANTITY": 6454,
    "PRICE": 138.77000427246094,
    "ACTION": "SELL",
    "TX_NUMBER": 189,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-03T22:46:19|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9306,
    "PRICE": 270.05999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 190,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-30T11:49:42|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2875,
    "PRICE": 791.6500244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 191,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-13T11:24:59|,
    "SYMBOL": "IWM",
    "QUANTITY": 3389,
    "PRICE": 843.4000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 192,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-31T18:58:26|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9518,
    "PRICE": 570.5,
    "ACTION": "BUY",
    "TX_NUMBER": 193,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-08T12:24:35|,
    "SYMBOL": "DIA",
    "QUANTITY": 9586,
    "PRICE": 711.489990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 194,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-29T16:06:35|,
    "SYMBOL": "DIA",
    "QUANTITY": 7593,
    "PRICE": 492.1400146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 195,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-09T18:16:47|,
    "SYMBOL": "SPY",
    "QUANTITY": 2590,
    "PRICE": 437.20001220703125,
    "ACTION": "SELL",
    "TX_NUMBER": 196,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-18T21:06:41|,
    "SYMBOL": "SLV",
    "QUANTITY": 6443,
    "PRICE": 284.239990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 197,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-05T09:48:46|,
    "SYMBOL": "DIA",
    "QUANTITY": 1832,
    "PRICE": 501.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 198,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-22T13:44:54|,
    "SYMBOL": "GLD",
    "QUANTITY": 5443,
    "PRICE": 462.6400146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 199,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-10T21:25:26|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5000,
    "PRICE": 345.7200012207031,
    "ACTION": "SELL",
    "TX_NUMBER": 200,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-25T01:41:58|,
    "SYMBOL": "GLD",
    "QUANTITY": 943,
    "PRICE": 191.39999389648438,
    "ACTION": "SELL",
    "TX_NUMBER": 201,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-14T00:07:06|,
    "SYMBOL": "SPY",
    "QUANTITY": 1668,
    "PRICE": 406.2699890136719,
    "ACTION": "SELL",
    "TX_NUMBER": 202,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-07T18:49:54|,
    "SYMBOL": "SPY",
    "QUANTITY": 8376,
    "PRICE": 548.6199951171875,
    "ACTION": "SELL",
    "TX_NUMBER": 203,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-10T05:32:01|,
    "SYMBOL": "SLV",
    "QUANTITY": 3068,
    "PRICE": 870.6099853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 204,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-11-07T22:06:14|,
    "SYMBOL": "IWM",
    "QUANTITY": 5464,
    "PRICE": 189.80999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 205,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-06T02:03:12|,
    "SYMBOL": "GLD",
    "QUANTITY": 9033,
    "PRICE": 637.6699829101562,
    "ACTION": "BUY",
    "TX_NUMBER": 206,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-14T01:33:14|,
    "SYMBOL": "IWM",
    "QUANTITY": 7090,
    "PRICE": 369.8999938964844,
    "ACTION": "BUY",
    "TX_NUMBER": 207,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-15T14:53:10|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6847,
    "PRICE": 639.5700073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 208,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-17T12:30:07|,
    "SYMBOL": "SPY",
    "QUANTITY": 3548,
    "PRICE": 957.9299926757812,
    "ACTION": "SELL",
    "TX_NUMBER": 209,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-01T19:53:02|,
    "SYMBOL": "DIA",
    "QUANTITY": 9069,
    "PRICE": 273.3999938964844,
    "ACTION": "BUY",
    "TX_NUMBER": 210,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-03T14:43:14|,
    "SYMBOL": "SPY",
    "QUANTITY": 6527,
    "PRICE": 486.3500061035156,
    "ACTION": "SELL",
    "TX_NUMBER": 211,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-24T14:45:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 4550,
    "PRICE": 470.4800109863281,
    "ACTION": "SELL",
    "TX_NUMBER": 212,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-07T10:12:39|,
    "SYMBOL": "SPY",
    "QUANTITY": 5491,
    "PRICE": 615.3599853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 213,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-25T23:17:48|,
    "SYMBOL": "SPY",
    "QUANTITY": 4099,
    "PRICE": 262.1199951171875,
    "ACTION": "SELL",
    "TX_NUMBER": 214,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-27T13:26:00|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7726,
    "PRICE": 845.0999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 215,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-21T23:30:08|,
    "SYMBOL": "GLD",
    "QUANTITY": 9679,
    "PRICE": 504.4700012207031,
    "ACTION": "SELL",
    "TX_NUMBER": 216,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-24T12:13:34|,
    "SYMBOL": "DIA",
    "QUANTITY": 2657,
    "PRICE": 107.01000213623047,
    "ACTION": "BUY",
    "TX_NUMBER": 217,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-14T06:59:48|,
    "SYMBOL": "GLD",
    "QUANTITY": 8071,
    "PRICE": 363.4800109863281,
    "ACTION": "SELL",
    "TX_NUMBER": 218,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-30T16:48:13|,
    "SYMBOL": "SLV",
    "QUANTITY": 2713,
    "PRICE": 948.3699951171875,
    "ACTION": "SELL",
    "TX_NUMBER": 219,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-10T03:15:07|,
    "SYMBOL": "DIA",
    "QUANTITY": 3138,
    "PRICE": 494.7699890136719,
    "ACTION": "SELL",
    "TX_NUMBER": 220,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-02T18:38:49|,
    "SYMBOL": "GLD",
    "QUANTITY": 6575,
    "PRICE": 302.2300109863281,
    "ACTION": "SELL",
    "TX_NUMBER": 221,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-30T01:57:43|,
    "SYMBOL": "DIA",
    "QUANTITY": 6073,
    "PRICE": 884.2100219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 222,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-28T20:15:16|,
    "SYMBOL": "DIA",
    "QUANTITY": 3779,
    "PRICE": 500.2699890136719,
    "ACTION": "BUY",
    "TX_NUMBER": 223,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-03T11:33:45|,
    "SYMBOL": "SPY",
    "QUANTITY": 2432,
    "PRICE": 535.02001953125,
    "ACTION": "BUY",
    "TX_NUMBER": 224,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-25T03:09:45|,
    "SYMBOL": "SPY",
    "QUANTITY": 7840,
    "PRICE": 57.79999923706055,
    "ACTION": "BUY",
    "TX_NUMBER": 225,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-10T04:47:41|,
    "SYMBOL": "GLD",
    "QUANTITY": 6561,
    "PRICE": 706.9199829101562,
    "ACTION": "SELL",
    "TX_NUMBER": 226,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-17T07:23:11|,
    "SYMBOL": "GLD",
    "QUANTITY": 4070,
    "PRICE": 992.8099975585938,
    "ACTION": "BUY",
    "TX_NUMBER": 227,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-12T12:02:13|,
    "SYMBOL": "GLD",
    "QUANTITY": 8403,
    "PRICE": 665.489990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 228,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-29T07:49:48|,
    "SYMBOL": "DIA",
    "QUANTITY": 2636,
    "PRICE": 728.3599853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 229,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-10T13:40:30|,
    "SYMBOL": "SPY",
    "QUANTITY": 7475,
    "PRICE": 232.9499969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 230,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-10T17:27:18|,
    "SYMBOL": "IWM",
    "QUANTITY": 8938,
    "PRICE": 371.25,
    "ACTION": "BUY",
    "TX_NUMBER": 231,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-03T15:01:38|,
    "SYMBOL": "GLD",
    "QUANTITY": 8590,
    "PRICE": 579.1799926757812,
    "ACTION": "SELL",
    "TX_NUMBER": 232,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-17T09:44:45|,
    "SYMBOL": "SLV",
    "QUANTITY": 6388,
    "PRICE": 723.0599975585938,
    "ACTION": "BUY",
    "TX_NUMBER": 233,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-18T18:42:08|,
    "SYMBOL": "IWM",
    "QUANTITY": 3907,
    "PRICE": 562.3200073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 234,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-10T15:04:03|,
    "SYMBOL": "DIA",
    "QUANTITY": 2310,
    "PRICE": 366.0899963378906,
    "ACTION": "BUY",
    "TX_NUMBER": 235,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-07T03:08:23|,
    "SYMBOL": "SPY",
    "QUANTITY": 7906,
    "PRICE": 491.1300048828125,
    "ACTION": "SELL",
    "TX_NUMBER": 236,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-12T14:17:32|,
    "SYMBOL": "DIA",
    "QUANTITY": 5499,
    "PRICE": 708.4600219726562,
    "ACTION": "SELL",
    "TX_NUMBER": 237,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-28T00:38:04|,
    "SYMBOL": "IWM",
    "QUANTITY": 2910,
    "PRICE": 819.0499877929688,
    "ACTION": "SELL",
    "TX_NUMBER": 238,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-03T10:59:36|,
    "SYMBOL": "GLD",
    "QUANTITY": 7353,
    "PRICE": 626.3300170898438,
    "ACTION": "SELL",
    "TX_NUMBER": 239,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-04T17:52:14|,
    "SYMBOL": "SPY",
    "QUANTITY": 6639,
    "PRICE": 801.4400024414062,
    "ACTION": "BUY",
    "TX_NUMBER": 240,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-31T22:16:43|,
    "SYMBOL": "SPY",
    "QUANTITY": 6292,
    "PRICE": 266.75,
    "ACTION": "BUY",
    "TX_NUMBER": 241,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-08T11:36:50|,
    "SYMBOL": "GLD",
    "QUANTITY": 6155,
    "PRICE": 510.0299987792969,
    "ACTION": "SELL",
    "TX_NUMBER": 242,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-04T01:26:06|,
    "SYMBOL": "GLD",
    "QUANTITY": 9700,
    "PRICE": 132.16000366210938,
    "ACTION": "BUY",
    "TX_NUMBER": 243,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-20T15:02:06|,
    "SYMBOL": "SPY",
    "QUANTITY": 4005,
    "PRICE": 237.85000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 244,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-01T16:50:15|,
    "SYMBOL": "DIA",
    "QUANTITY": 5797,
    "PRICE": 615.6300048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 245,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-18T09:16:44|,
    "SYMBOL": "SPY",
    "QUANTITY": 7184,
    "PRICE": 422.05999755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 246,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-05T04:12:48|,
    "SYMBOL": "QQQ",
    "QUANTITY": 197,
    "PRICE": 534.5,
    "ACTION": "SELL",
    "TX_NUMBER": 247,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-02T02:57:44|,
    "SYMBOL": "DIA",
    "QUANTITY": 4149,
    "PRICE": 927.8400268554688,
    "ACTION": "SELL",
    "TX_NUMBER": 248,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-11T06:19:03|,
    "SYMBOL": "DIA",
    "QUANTITY": 4238,
    "PRICE": 625.9000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 249,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-27T12:24:10|,
    "SYMBOL": "IWM",
    "QUANTITY": 417,
    "PRICE": 881.780029296875,
    "ACTION": "BUY",
    "TX_NUMBER": 250,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-11-02T01:01:02|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3473,
    "PRICE": 411.7099914550781,
    "ACTION": "SELL",
    "TX_NUMBER": 251,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-17T18:40:55|,
    "SYMBOL": "IWM",
    "QUANTITY": 3478,
    "PRICE": 917.7899780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 252,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-14T04:44:51|,
    "SYMBOL": "SPY",
    "QUANTITY": 6430,
    "PRICE": 411.3599853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 253,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-27T23:00:53|,
    "SYMBOL": "DIA",
    "QUANTITY": 6383,
    "PRICE": 772.3699951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 254,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-14T04:22:04|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8756,
    "PRICE": 416.6099853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 255,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-26T08:18:14|,
    "SYMBOL": "SLV",
    "QUANTITY": 9464,
    "PRICE": 647.9299926757812,
    "ACTION": "SELL",
    "TX_NUMBER": 256,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-13T12:32:16|,
    "SYMBOL": "IWM",
    "QUANTITY": 506,
    "PRICE": 950.0900268554688,
    "ACTION": "BUY",
    "TX_NUMBER": 257,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-21T12:49:38|,
    "SYMBOL": "DIA",
    "QUANTITY": 2757,
    "PRICE": 128.0800018310547,
    "ACTION": "BUY",
    "TX_NUMBER": 258,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-11-05T01:18:03|,
    "SYMBOL": "IWM",
    "QUANTITY": 4939,
    "PRICE": 601.280029296875,
    "ACTION": "BUY",
    "TX_NUMBER": 259,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-04T03:28:34|,
    "SYMBOL": "IWM",
    "QUANTITY": 8825,
    "PRICE": 51.599998474121094,
    "ACTION": "BUY",
    "TX_NUMBER": 260,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-13T16:09:42|,
    "SYMBOL": "IWM",
    "QUANTITY": 3856,
    "PRICE": 472.4599914550781,
    "ACTION": "BUY",
    "TX_NUMBER": 261,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-23T19:58:32|,
    "SYMBOL": "IWM",
    "QUANTITY": 2532,
    "PRICE": 240.4499969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 262,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-06T22:32:39|,
    "SYMBOL": "SLV",
    "QUANTITY": 4207,
    "PRICE": 103.76000213623047,
    "ACTION": "BUY",
    "TX_NUMBER": 263,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-03T08:36:56|,
    "SYMBOL": "SPY",
    "QUANTITY": 715,
    "PRICE": 732.6799926757812,
    "ACTION": "SELL",
    "TX_NUMBER": 264,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-25T14:46:22|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3580,
    "PRICE": 511.75,
    "ACTION": "BUY",
    "TX_NUMBER": 265,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-28T08:14:41|,
    "SYMBOL": "GLD",
    "QUANTITY": 8356,
    "PRICE": 957.6599731445312,
    "ACTION": "SELL",
    "TX_NUMBER": 266,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-19T13:08:45|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6237,
    "PRICE": 804.1199951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 267,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-10T07:42:28|,
    "SYMBOL": "GLD",
    "QUANTITY": 6053,
    "PRICE": 840.530029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 268,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-04T08:43:05|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5770,
    "PRICE": 566.719970703125,
    "ACTION": "BUY",
    "TX_NUMBER": 269,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-26T10:53:15|,
    "SYMBOL": "GLD",
    "QUANTITY": 9086,
    "PRICE": 992.1699829101562,
    "ACTION": "SELL",
    "TX_NUMBER": 270,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-27T00:19:42|,
    "SYMBOL": "SPY",
    "QUANTITY": 5312,
    "PRICE": 522.8400268554688,
    "ACTION": "SELL",
    "TX_NUMBER": 271,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-14T05:15:07|,
    "SYMBOL": "IWM",
    "QUANTITY": 2920,
    "PRICE": 853.7999877929688,
    "ACTION": "SELL",
    "TX_NUMBER": 272,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-29T04:35:53|,
    "SYMBOL": "SLV",
    "QUANTITY": 1648,
    "PRICE": 908.3300170898438,
    "ACTION": "SELL",
    "TX_NUMBER": 273,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-10T10:50:53|,
    "SYMBOL": "SLV",
    "QUANTITY": 5480,
    "PRICE": 723.4000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 274,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-11-04T06:13:09|,
    "SYMBOL": "IWM",
    "QUANTITY": 6729,
    "PRICE": 71.1500015258789,
    "ACTION": "BUY",
    "TX_NUMBER": 275,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-29T22:47:51|,
    "SYMBOL": "QQQ",
    "QUANTITY": 468,
    "PRICE": 527.7899780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 276,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-03T00:59:14|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6990,
    "PRICE": 323.1400146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 277,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-08T16:12:51|,
    "SYMBOL": "GLD",
    "QUANTITY": 161,
    "PRICE": 237.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 278,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-06T00:20:13|,
    "SYMBOL": "DIA",
    "QUANTITY": 9624,
    "PRICE": 700.0999755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 279,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-20T04:54:58|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8676,
    "PRICE": 753.9500122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 280,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-31T20:26:28|,
    "SYMBOL": "IWM",
    "QUANTITY": 3578,
    "PRICE": 327.5199890136719,
    "ACTION": "SELL",
    "TX_NUMBER": 281,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-20T20:28:02|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5825,
    "PRICE": 866.760009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 282,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-17T22:04:35|,
    "SYMBOL": "SLV",
    "QUANTITY": 5281,
    "PRICE": 836.219970703125,
    "ACTION": "BUY",
    "TX_NUMBER": 283,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-20T23:38:09|,
    "SYMBOL": "GLD",
    "QUANTITY": 9503,
    "PRICE": 168.77999877929688,
    "ACTION": "SELL",
    "TX_NUMBER": 284,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-11T07:30:16|,
    "SYMBOL": "DIA",
    "QUANTITY": 9251,
    "PRICE": 605.219970703125,
    "ACTION": "SELL",
    "TX_NUMBER": 285,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-25T21:48:32|,
    "SYMBOL": "IWM",
    "QUANTITY": 7960,
    "PRICE": 219.60000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 286,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-17T03:38:46|,
    "SYMBOL": "DIA",
    "QUANTITY": 579,
    "PRICE": 289.4200134277344,
    "ACTION": "SELL",
    "TX_NUMBER": 287,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-26T17:40:10|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2663,
    "PRICE": 261.6000061035156,
    "ACTION": "SELL",
    "TX_NUMBER": 288,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-23T03:23:35|,
    "SYMBOL": "SPY",
    "QUANTITY": 5472,
    "PRICE": 141.07000732421875,
    "ACTION": "SELL",
    "TX_NUMBER": 289,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-05T23:19:53|,
    "SYMBOL": "GLD",
    "QUANTITY": 9295,
    "PRICE": 459.6000061035156,
    "ACTION": "BUY",
    "TX_NUMBER": 290,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-02T04:29:53|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2247,
    "PRICE": 570.3099975585938,
    "ACTION": "SELL",
    "TX_NUMBER": 291,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-20T12:27:56|,
    "SYMBOL": "SLV",
    "QUANTITY": 345,
    "PRICE": 954.8800048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 292,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-01T15:03:02|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8212,
    "PRICE": 255.86000061035156,
    "ACTION": "BUY",
    "TX_NUMBER": 293,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-19T09:10:53|,
    "SYMBOL": "IWM",
    "QUANTITY": 9134,
    "PRICE": 574.4099731445312,
    "ACTION": "SELL",
    "TX_NUMBER": 294,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-25T03:36:07|,
    "SYMBOL": "SLV",
    "QUANTITY": 2488,
    "PRICE": 189.66000366210938,
    "ACTION": "BUY",
    "TX_NUMBER": 295,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-12T10:44:10|,
    "SYMBOL": "GLD",
    "QUANTITY": 5572,
    "PRICE": 90.69999694824219,
    "ACTION": "SELL",
    "TX_NUMBER": 296,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-12T16:53:08|,
    "SYMBOL": "SLV",
    "QUANTITY": 7789,
    "PRICE": 137.52999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 297,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-24T20:01:10|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6304,
    "PRICE": 605.7100219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 298,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-26T00:54:23|,
    "SYMBOL": "QQQ",
    "QUANTITY": 396,
    "PRICE": 84.83999633789062,
    "ACTION": "BUY",
    "TX_NUMBER": 299,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-14T16:05:19|,
    "SYMBOL": "SPY",
    "QUANTITY": 2507,
    "PRICE": 306.54998779296875,
    "ACTION": "BUY",
    "TX_NUMBER": 300,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-29T19:29:43|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3990,
    "PRICE": 347.32000732421875,
    "ACTION": "SELL",
    "TX_NUMBER": 301,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-26T18:03:54|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3986,
    "PRICE": 488.1499938964844,
    "ACTION": "SELL",
    "TX_NUMBER": 302,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-22T20:18:58|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7280,
    "PRICE": 382.0899963378906,
    "ACTION": "BUY",
    "TX_NUMBER": 303,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-31T05:15:58|,
    "SYMBOL": "IWM",
    "QUANTITY": 5711,
    "PRICE": 658.72998046875,
    "ACTION": "SELL",
    "TX_NUMBER": 304,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-12T05:20:21|,
    "SYMBOL": "SPY",
    "QUANTITY": 7938,
    "PRICE": 860.7899780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 305,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-22T21:37:19|,
    "SYMBOL": "GLD",
    "QUANTITY": 892,
    "PRICE": 478.19000244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 306,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-15T07:30:09|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9289,
    "PRICE": 791.1400146484375,
    "ACTION": "BUY",
    "TX_NUMBER": 307,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-19T05:03:41|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4679,
    "PRICE": 135.0500030517578,
    "ACTION": "SELL",
    "TX_NUMBER": 308,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-19T20:32:49|,
    "SYMBOL": "SPY",
    "QUANTITY": 2317,
    "PRICE": 194.39999389648438,
    "ACTION": "SELL",
    "TX_NUMBER": 309,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-05T02:18:35|,
    "SYMBOL": "DIA",
    "QUANTITY": 1313,
    "PRICE": 336.1300048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 310,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-30T08:32:12|,
    "SYMBOL": "SPY",
    "QUANTITY": 8513,
    "PRICE": 525.969970703125,
    "ACTION": "BUY",
    "TX_NUMBER": 311,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-03T22:34:57|,
    "SYMBOL": "SPY",
    "QUANTITY": 5014,
    "PRICE": 387.8399963378906,
    "ACTION": "BUY",
    "TX_NUMBER": 312,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-30T06:54:44|,
    "SYMBOL": "SPY",
    "QUANTITY": 7250,
    "PRICE": 414.5400085449219,
    "ACTION": "BUY",
    "TX_NUMBER": 313,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-29T22:53:28|,
    "SYMBOL": "IWM",
    "QUANTITY": 9998,
    "PRICE": 325.6199951171875,
    "ACTION": "SELL",
    "TX_NUMBER": 314,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-04T05:24:56|,
    "SYMBOL": "SLV",
    "QUANTITY": 9805,
    "PRICE": 176.1300048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 315,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-04T01:13:10|,
    "SYMBOL": "SPY",
    "QUANTITY": 8392,
    "PRICE": 258.2900085449219,
    "ACTION": "BUY",
    "TX_NUMBER": 316,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-17T16:00:27|,
    "SYMBOL": "SPY",
    "QUANTITY": 5602,
    "PRICE": 859.8499755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 317,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-17T01:45:12|,
    "SYMBOL": "DIA",
    "QUANTITY": 3409,
    "PRICE": 521.9500122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 318,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-02T10:49:28|,
    "SYMBOL": "DIA",
    "QUANTITY": 4469,
    "PRICE": 631.2100219726562,
    "ACTION": "SELL",
    "TX_NUMBER": 319,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-11T19:59:24|,
    "SYMBOL": "SLV",
    "QUANTITY": 808,
    "PRICE": 654.8400268554688,
    "ACTION": "BUY",
    "TX_NUMBER": 320,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-26T14:14:04|,
    "SYMBOL": "SPY",
    "QUANTITY": 856,
    "PRICE": 302.79998779296875,
    "ACTION": "BUY",
    "TX_NUMBER": 321,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-15T23:05:13|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3886,
    "PRICE": 773.8900146484375,
    "ACTION": "BUY",
    "TX_NUMBER": 322,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-14T08:12:28|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1613,
    "PRICE": 173.8800048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 323,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-01T10:07:17|,
    "SYMBOL": "DIA",
    "QUANTITY": 9159,
    "PRICE": 580.760009765625,
    "ACTION": "SELL",
    "TX_NUMBER": 324,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-06T12:05:18|,
    "SYMBOL": "GLD",
    "QUANTITY": 6125,
    "PRICE": 89.52999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 325,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-19T19:59:41|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8814,
    "PRICE": 256.3299865722656,
    "ACTION": "BUY",
    "TX_NUMBER": 326,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-16T16:40:40|,
    "SYMBOL": "SPY",
    "QUANTITY": 1564,
    "PRICE": 236.75999450683594,
    "ACTION": "SELL",
    "TX_NUMBER": 327,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-05T23:27:08|,
    "SYMBOL": "IWM",
    "QUANTITY": 2881,
    "PRICE": 361.94000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 328,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-25T12:03:42|,
    "SYMBOL": "GLD",
    "QUANTITY": 6039,
    "PRICE": 769.2999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 329,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-07T12:22:35|,
    "SYMBOL": "GLD",
    "QUANTITY": 1062,
    "PRICE": 68.36000061035156,
    "ACTION": "BUY",
    "TX_NUMBER": 330,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-28T06:36:29|,
    "SYMBOL": "SLV",
    "QUANTITY": 2106,
    "PRICE": 869.5599975585938,
    "ACTION": "BUY",
    "TX_NUMBER": 331,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-23T06:18:25|,
    "SYMBOL": "DIA",
    "QUANTITY": 2780,
    "PRICE": 727.969970703125,
    "ACTION": "SELL",
    "TX_NUMBER": 332,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-23T09:07:14|,
    "SYMBOL": "SLV",
    "QUANTITY": 268,
    "PRICE": 269.4599914550781,
    "ACTION": "SELL",
    "TX_NUMBER": 333,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-12T19:40:20|,
    "SYMBOL": "IWM",
    "QUANTITY": 8375,
    "PRICE": 507.3599853515625,
    "ACTION": "BUY",
    "TX_NUMBER": 334,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-22T20:48:29|,
    "SYMBOL": "IWM",
    "QUANTITY": 5989,
    "PRICE": 867.1099853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 335,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-14T20:23:57|,
    "SYMBOL": "SLV",
    "QUANTITY": 3859,
    "PRICE": 553.239990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 336,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-10T09:30:22|,
    "SYMBOL": "SLV",
    "QUANTITY": 5922,
    "PRICE": 52.5,
    "ACTION": "BUY",
    "TX_NUMBER": 337,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-17T21:57:50|,
    "SYMBOL": "GLD",
    "QUANTITY": 2669,
    "PRICE": 391.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 338,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-11-09T21:16:12|,
    "SYMBOL": "IWM",
    "QUANTITY": 6915,
    "PRICE": 790.75,
    "ACTION": "BUY",
    "TX_NUMBER": 339,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-18T16:46:09|,
    "SYMBOL": "DIA",
    "QUANTITY": 7391,
    "PRICE": 556.47998046875,
    "ACTION": "BUY",
    "TX_NUMBER": 340,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-07T19:45:47|,
    "SYMBOL": "SLV",
    "QUANTITY": 9710,
    "PRICE": 251.6999969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 341,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-13T01:30:50|,
    "SYMBOL": "IWM",
    "QUANTITY": 4445,
    "PRICE": 786.239990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 342,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-27T08:54:39|,
    "SYMBOL": "SLV",
    "QUANTITY": 2921,
    "PRICE": 173.44000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 343,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-09T10:25:20|,
    "SYMBOL": "DIA",
    "QUANTITY": 6547,
    "PRICE": 888.4099731445312,
    "ACTION": "SELL",
    "TX_NUMBER": 344,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-08T16:03:21|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9861,
    "PRICE": 611.0900268554688,
    "ACTION": "SELL",
    "TX_NUMBER": 345,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-05T12:13:09|,
    "SYMBOL": "GLD",
    "QUANTITY": 3164,
    "PRICE": 123.69999694824219,
    "ACTION": "BUY",
    "TX_NUMBER": 346,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-06T02:00:06|,
    "SYMBOL": "IWM",
    "QUANTITY": 8630,
    "PRICE": 607.8599853515625,
    "ACTION": "BUY",
    "TX_NUMBER": 347,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-17T20:30:14|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3886,
    "PRICE": 876.510009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 348,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-20T12:52:06|,
    "SYMBOL": "DIA",
    "QUANTITY": 3122,
    "PRICE": 814.2000122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 349,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-15T04:52:53|,
    "SYMBOL": "SLV",
    "QUANTITY": 7820,
    "PRICE": 220.05999755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 350,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-11-05T19:38:34|,
    "SYMBOL": "QQQ",
    "QUANTITY": 936,
    "PRICE": 699.9500122070312,
    "ACTION": "SELL",
    "TX_NUMBER": 351,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-09T19:30:22|,
    "SYMBOL": "SLV",
    "QUANTITY": 6752,
    "PRICE": 938.5399780273438,
    "ACTION": "SELL",
    "TX_NUMBER": 352,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-09T07:18:37|,
    "SYMBOL": "SPY",
    "QUANTITY": 3374,
    "PRICE": 388.82000732421875,
    "ACTION": "SELL",
    "TX_NUMBER": 353,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-16T21:05:28|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1041,
    "PRICE": 528.3099975585938,
    "ACTION": "BUY",
    "TX_NUMBER": 354,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-10T15:46:58|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4203,
    "PRICE": 648.5700073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 355,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-30T04:44:08|,
    "SYMBOL": "SPY",
    "QUANTITY": 8717,
    "PRICE": 56.31999969482422,
    "ACTION": "SELL",
    "TX_NUMBER": 356,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-09T00:57:42|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9264,
    "PRICE": 845.5599975585938,
    "ACTION": "SELL",
    "TX_NUMBER": 357,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-15T06:40:42|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8488,
    "PRICE": 169.13999938964844,
    "ACTION": "BUY",
    "TX_NUMBER": 358,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-17T17:07:17|,
    "SYMBOL": "IWM",
    "QUANTITY": 2529,
    "PRICE": 629.7899780273438,
    "ACTION": "SELL",
    "TX_NUMBER": 359,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-12T08:56:48|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9921,
    "PRICE": 816.4199829101562,
    "ACTION": "SELL",
    "TX_NUMBER": 360,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-29T11:12:57|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9598,
    "PRICE": 674.280029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 361,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-13T19:44:19|,
    "SYMBOL": "GLD",
    "QUANTITY": 133,
    "PRICE": 74.37000274658203,
    "ACTION": "SELL",
    "TX_NUMBER": 362,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-28T07:19:22|,
    "SYMBOL": "SPY",
    "QUANTITY": 6674,
    "PRICE": 629.5800170898438,
    "ACTION": "SELL",
    "TX_NUMBER": 363,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-11T07:12:26|,
    "SYMBOL": "GLD",
    "QUANTITY": 2125,
    "PRICE": 905.8400268554688,
    "ACTION": "BUY",
    "TX_NUMBER": 364,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-02T22:15:36|,
    "SYMBOL": "GLD",
    "QUANTITY": 5748,
    "PRICE": 675.6599731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 365,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-07T11:07:48|,
    "SYMBOL": "GLD",
    "QUANTITY": 9520,
    "PRICE": 654.9299926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 366,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-23T14:42:36|,
    "SYMBOL": "SLV",
    "QUANTITY": 2567,
    "PRICE": 677.3699951171875,
    "ACTION": "SELL",
    "TX_NUMBER": 367,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-29T19:22:35|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6575,
    "PRICE": 657.9500122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 368,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-17T16:14:35|,
    "SYMBOL": "SLV",
    "QUANTITY": 4191,
    "PRICE": 787.3499755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 369,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-17T10:53:34|,
    "SYMBOL": "SPY",
    "QUANTITY": 4448,
    "PRICE": 144.25999450683594,
    "ACTION": "SELL",
    "TX_NUMBER": 370,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-10T04:09:26|,
    "SYMBOL": "GLD",
    "QUANTITY": 871,
    "PRICE": 923.4000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 371,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-31T16:01:19|,
    "SYMBOL": "IWM",
    "QUANTITY": 7028,
    "PRICE": 177.47999572753906,
    "ACTION": "SELL",
    "TX_NUMBER": 372,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-17T01:10:23|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4414,
    "PRICE": 242.88999938964844,
    "ACTION": "BUY",
    "TX_NUMBER": 373,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-06T23:40:48|,
    "SYMBOL": "DIA",
    "QUANTITY": 4446,
    "PRICE": 214.92999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 374,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-17T01:47:03|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2288,
    "PRICE": 159.02999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 375,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-26T04:50:45|,
    "SYMBOL": "QQQ",
    "QUANTITY": 451,
    "PRICE": 694.010009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 376,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-31T11:59:40|,
    "SYMBOL": "SPY",
    "QUANTITY": 9199,
    "PRICE": 934.97998046875,
    "ACTION": "SELL",
    "TX_NUMBER": 377,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-11-05T21:44:33|,
    "SYMBOL": "SPY",
    "QUANTITY": 9108,
    "PRICE": 981.989990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 378,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-22T01:20:48|,
    "SYMBOL": "SLV",
    "QUANTITY": 2988,
    "PRICE": 645.3599853515625,
    "ACTION": "BUY",
    "TX_NUMBER": 379,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-08T04:15:57|,
    "SYMBOL": "GLD",
    "QUANTITY": 280,
    "PRICE": 998.030029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 380,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-06T07:41:49|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1762,
    "PRICE": 825.5,
    "ACTION": "BUY",
    "TX_NUMBER": 381,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-13T15:45:45|,
    "SYMBOL": "GLD",
    "QUANTITY": 789,
    "PRICE": 458.6600036621094,
    "ACTION": "BUY",
    "TX_NUMBER": 382,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-14T18:48:37|,
    "SYMBOL": "IWM",
    "QUANTITY": 3695,
    "PRICE": 564.9299926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 383,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-29T09:55:17|,
    "SYMBOL": "DIA",
    "QUANTITY": 1139,
    "PRICE": 169.39999389648438,
    "ACTION": "BUY",
    "TX_NUMBER": 384,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-11T15:31:46|,
    "SYMBOL": "IWM",
    "QUANTITY": 7443,
    "PRICE": 486.2099914550781,
    "ACTION": "BUY",
    "TX_NUMBER": 385,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-03T06:19:46|,
    "SYMBOL": "SPY",
    "QUANTITY": 8018,
    "PRICE": 755.72998046875,
    "ACTION": "BUY",
    "TX_NUMBER": 386,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-03T21:20:07|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5353,
    "PRICE": 591.280029296875,
    "ACTION": "BUY",
    "TX_NUMBER": 387,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-25T01:50:02|,
    "SYMBOL": "SPY",
    "QUANTITY": 5440,
    "PRICE": 272.260009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 388,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-30T23:49:47|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6374,
    "PRICE": 753.9500122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 389,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-12T21:55:25|,
    "SYMBOL": "IWM",
    "QUANTITY": 7941,
    "PRICE": 155.2899932861328,
    "ACTION": "SELL",
    "TX_NUMBER": 390,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-11T11:42:35|,
    "SYMBOL": "IWM",
    "QUANTITY": 7773,
    "PRICE": 548.3599853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 391,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-30T06:33:01|,
    "SYMBOL": "DIA",
    "QUANTITY": 1676,
    "PRICE": 591.3499755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 392,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-11-04T10:27:57|,
    "SYMBOL": "IWM",
    "QUANTITY": 494,
    "PRICE": 290.7799987792969,
    "ACTION": "BUY",
    "TX_NUMBER": 393,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-18T16:04:36|,
    "SYMBOL": "IWM",
    "QUANTITY": 2257,
    "PRICE": 533.4199829101562,
    "ACTION": "BUY",
    "TX_NUMBER": 394,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-19T22:36:40|,
    "SYMBOL": "SPY",
    "QUANTITY": 9838,
    "PRICE": 986.0999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 395,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-09T12:11:24|,
    "SYMBOL": "SPY",
    "QUANTITY": 5488,
    "PRICE": 467.6499938964844,
    "ACTION": "BUY",
    "TX_NUMBER": 396,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-09T00:17:17|,
    "SYMBOL": "DIA",
    "QUANTITY": 9196,
    "PRICE": 900.1900024414062,
    "ACTION": "SELL",
    "TX_NUMBER": 397,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-16T02:21:55|,
    "SYMBOL": "SPY",
    "QUANTITY": 9140,
    "PRICE": 689.6599731445312,
    "ACTION": "SELL",
    "TX_NUMBER": 398,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-01T14:17:07|,
    "SYMBOL": "IWM",
    "QUANTITY": 3053,
    "PRICE": 974.0700073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 399,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-05T22:42:02|,
    "SYMBOL": "IWM",
    "QUANTITY": 1736,
    "PRICE": 465.04998779296875,
    "ACTION": "BUY",
    "TX_NUMBER": 400,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-27T17:14:23|,
    "SYMBOL": "IWM",
    "QUANTITY": 6528,
    "PRICE": 121.80000305175781,
    "ACTION": "SELL",
    "TX_NUMBER": 401,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-18T20:54:10|,
    "SYMBOL": "IWM",
    "QUANTITY": 2842,
    "PRICE": 153.60000610351562,
    "ACTION": "BUY",
    "TX_NUMBER": 402,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-08T10:17:06|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7853,
    "PRICE": 115.25,
    "ACTION": "BUY",
    "TX_NUMBER": 403,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-25T13:37:16|,
    "SYMBOL": "DIA",
    "QUANTITY": 7082,
    "PRICE": 453.7900085449219,
    "ACTION": "SELL",
    "TX_NUMBER": 404,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-06T18:07:55|,
    "SYMBOL": "SLV",
    "QUANTITY": 2817,
    "PRICE": 442.8599853515625,
    "ACTION": "BUY",
    "TX_NUMBER": 405,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-17T23:13:44|,
    "SYMBOL": "QQQ",
    "QUANTITY": 850,
    "PRICE": 697.219970703125,
    "ACTION": "BUY",
    "TX_NUMBER": 406,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-17T00:39:20|,
    "SYMBOL": "SLV",
    "QUANTITY": 2511,
    "PRICE": 59.25,
    "ACTION": "BUY",
    "TX_NUMBER": 407,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-18T00:01:16|,
    "SYMBOL": "IWM",
    "QUANTITY": 6746,
    "PRICE": 766.3200073242188,
    "ACTION": "BUY",
    "TX_NUMBER": 408,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-15T02:26:28|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3985,
    "PRICE": 987.8599853515625,
    "ACTION": "BUY",
    "TX_NUMBER": 409,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-29T06:29:43|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6202,
    "PRICE": 208.61000061035156,
    "ACTION": "SELL",
    "TX_NUMBER": 410,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-30T12:41:36|,
    "SYMBOL": "DIA",
    "QUANTITY": 2069,
    "PRICE": 277.4800109863281,
    "ACTION": "BUY",
    "TX_NUMBER": 411,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-19T11:28:48|,
    "SYMBOL": "GLD",
    "QUANTITY": 2732,
    "PRICE": 255.7100067138672,
    "ACTION": "SELL",
    "TX_NUMBER": 412,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-28T18:22:41|,
    "SYMBOL": "SPY",
    "QUANTITY": 7691,
    "PRICE": 465.239990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 413,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-20T17:34:19|,
    "SYMBOL": "GLD",
    "QUANTITY": 7652,
    "PRICE": 852.8599853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 414,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-08T08:03:37|,
    "SYMBOL": "SPY",
    "QUANTITY": 1965,
    "PRICE": 752.469970703125,
    "ACTION": "SELL",
    "TX_NUMBER": 415,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-09T16:54:21|,
    "SYMBOL": "DIA",
    "QUANTITY": 4805,
    "PRICE": 250.60000610351562,
    "ACTION": "BUY",
    "TX_NUMBER": 416,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-27T19:36:13|,
    "SYMBOL": "DIA",
    "QUANTITY": 9648,
    "PRICE": 210.14999389648438,
    "ACTION": "BUY",
    "TX_NUMBER": 417,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-13T15:07:05|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9406,
    "PRICE": 620.719970703125,
    "ACTION": "BUY",
    "TX_NUMBER": 418,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-14T13:11:58|,
    "SYMBOL": "IWM",
    "QUANTITY": 5712,
    "PRICE": 179.44000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 419,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-12T15:43:05|,
    "SYMBOL": "IWM",
    "QUANTITY": 7657,
    "PRICE": 382.79998779296875,
    "ACTION": "SELL",
    "TX_NUMBER": 420,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-25T13:56:40|,
    "SYMBOL": "SPY",
    "QUANTITY": 6721,
    "PRICE": 852.989990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 421,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-07T09:19:52|,
    "SYMBOL": "GLD",
    "QUANTITY": 4156,
    "PRICE": 245.4499969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 422,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-31T07:09:43|,
    "SYMBOL": "DIA",
    "QUANTITY": 5247,
    "PRICE": 908.0900268554688,
    "ACTION": "SELL",
    "TX_NUMBER": 423,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-04T05:31:43|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3217,
    "PRICE": 399.0,
    "ACTION": "BUY",
    "TX_NUMBER": 424,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-16T14:10:47|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1148,
    "PRICE": 904.9099731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 425,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-25T13:19:48|,
    "SYMBOL": "DIA",
    "QUANTITY": 2252,
    "PRICE": 497.3399963378906,
    "ACTION": "BUY",
    "TX_NUMBER": 426,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-10T23:54:55|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1618,
    "PRICE": 931.4000244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 427,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-10T20:56:01|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5476,
    "PRICE": 573.8300170898438,
    "ACTION": "SELL",
    "TX_NUMBER": 428,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-23T01:30:13|,
    "SYMBOL": "GLD",
    "QUANTITY": 8999,
    "PRICE": 516.3499755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 429,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-04T14:46:31|,
    "SYMBOL": "DIA",
    "QUANTITY": 7409,
    "PRICE": 986.7899780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 430,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-03T19:51:23|,
    "SYMBOL": "SPY",
    "QUANTITY": 2894,
    "PRICE": 226.6300048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 431,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-28T16:17:40|,
    "SYMBOL": "DIA",
    "QUANTITY": 5428,
    "PRICE": 659.989990234375,
    "ACTION": "SELL",
    "TX_NUMBER": 432,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-04T04:41:11|,
    "SYMBOL": "DIA",
    "QUANTITY": 8449,
    "PRICE": 65.22000122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 433,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-26T00:16:31|,
    "SYMBOL": "DIA",
    "QUANTITY": 6269,
    "PRICE": 389.989990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 434,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-13T21:33:33|,
    "SYMBOL": "GLD",
    "QUANTITY": 6339,
    "PRICE": 731.52001953125,
    "ACTION": "BUY",
    "TX_NUMBER": 435,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-25T17:12:16|,
    "SYMBOL": "IWM",
    "QUANTITY": 746,
    "PRICE": 280.92999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 436,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-14T09:18:55|,
    "SYMBOL": "DIA",
    "QUANTITY": 6259,
    "PRICE": 932.77001953125,
    "ACTION": "BUY",
    "TX_NUMBER": 437,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-18T23:39:32|,
    "SYMBOL": "SPY",
    "QUANTITY": 2143,
    "PRICE": 733.5999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 438,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-10T14:38:09|,
    "SYMBOL": "SLV",
    "QUANTITY": 6826,
    "PRICE": 344.1499938964844,
    "ACTION": "BUY",
    "TX_NUMBER": 439,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-11T04:34:56|,
    "SYMBOL": "IWM",
    "QUANTITY": 5420,
    "PRICE": 717.4099731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 440,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-15T17:29:54|,
    "SYMBOL": "GLD",
    "QUANTITY": 2333,
    "PRICE": 259.17999267578125,
    "ACTION": "SELL",
    "TX_NUMBER": 441,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-22T09:08:08|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8819,
    "PRICE": 671.0900268554688,
    "ACTION": "BUY",
    "TX_NUMBER": 442,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-13T06:50:45|,
    "SYMBOL": "SPY",
    "QUANTITY": 7706,
    "PRICE": 565.8200073242188,
    "ACTION": "BUY",
    "TX_NUMBER": 443,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-12T18:41:51|,
    "SYMBOL": "DIA",
    "QUANTITY": 2659,
    "PRICE": 263.0799865722656,
    "ACTION": "SELL",
    "TX_NUMBER": 444,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-28T07:11:36|,
    "SYMBOL": "GLD",
    "QUANTITY": 6969,
    "PRICE": 795.9199829101562,
    "ACTION": "BUY",
    "TX_NUMBER": 445,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-12T11:18:27|,
    "SYMBOL": "IWM",
    "QUANTITY": 8922,
    "PRICE": 282.0400085449219,
    "ACTION": "BUY",
    "TX_NUMBER": 446,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-11T03:42:47|,
    "SYMBOL": "IWM",
    "QUANTITY": 8612,
    "PRICE": 855.3499755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 447,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-13T09:57:47|,
    "SYMBOL": "DIA",
    "QUANTITY": 2040,
    "PRICE": 371.82000732421875,
    "ACTION": "BUY",
    "TX_NUMBER": 448,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-20T20:31:28|,
    "SYMBOL": "IWM",
    "QUANTITY": 638,
    "PRICE": 756.9299926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 449,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-23T07:33:22|,
    "SYMBOL": "SLV",
    "QUANTITY": 5483,
    "PRICE": 556.0800170898438,
    "ACTION": "BUY",
    "TX_NUMBER": 450,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-18T13:05:40|,
    "SYMBOL": "SPY",
    "QUANTITY": 2257,
    "PRICE": 640.9099731445312,
    "ACTION": "SELL",
    "TX_NUMBER": 451,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-15T00:05:06|,
    "SYMBOL": "IWM",
    "QUANTITY": 6465,
    "PRICE": 57.029998779296875,
    "ACTION": "SELL",
    "TX_NUMBER": 452,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-26T11:56:43|,
    "SYMBOL": "SLV",
    "QUANTITY": 6052,
    "PRICE": 517.02001953125,
    "ACTION": "SELL",
    "TX_NUMBER": 453,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-18T22:08:18|,
    "SYMBOL": "GLD",
    "QUANTITY": 2841,
    "PRICE": 679.6699829101562,
    "ACTION": "SELL",
    "TX_NUMBER": 454,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-11T04:08:17|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7124,
    "PRICE": 97.22000122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 455,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-12T02:02:29|,
    "SYMBOL": "DIA",
    "QUANTITY": 3226,
    "PRICE": 998.9099731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 456,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-22T16:01:53|,
    "SYMBOL": "IWM",
    "QUANTITY": 6827,
    "PRICE": 961.0499877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 457,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-17T11:11:00|,
    "SYMBOL": "SLV",
    "QUANTITY": 5899,
    "PRICE": 569.2100219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 458,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-10T13:39:15|,
    "SYMBOL": "IWM",
    "QUANTITY": 4872,
    "PRICE": 817.5700073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 459,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-06T18:58:46|,
    "SYMBOL": "DIA",
    "QUANTITY": 8504,
    "PRICE": 709.72998046875,
    "ACTION": "SELL",
    "TX_NUMBER": 460,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-11T19:06:06|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5719,
    "PRICE": 988.0700073242188,
    "ACTION": "BUY",
    "TX_NUMBER": 461,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-30T05:44:06|,
    "SYMBOL": "DIA",
    "QUANTITY": 9818,
    "PRICE": 530.5399780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 462,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-06T19:04:15|,
    "SYMBOL": "SLV",
    "QUANTITY": 4277,
    "PRICE": 83.0999984741211,
    "ACTION": "SELL",
    "TX_NUMBER": 463,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-04T06:53:53|,
    "SYMBOL": "DIA",
    "QUANTITY": 2728,
    "PRICE": 455.95001220703125,
    "ACTION": "SELL",
    "TX_NUMBER": 464,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-13T22:23:48|,
    "SYMBOL": "SPY",
    "QUANTITY": 7442,
    "PRICE": 237.1999969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 465,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-22T08:00:56|,
    "SYMBOL": "GLD",
    "QUANTITY": 3836,
    "PRICE": 726.75,
    "ACTION": "BUY",
    "TX_NUMBER": 466,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-10T20:05:11|,
    "SYMBOL": "QQQ",
    "QUANTITY": 567,
    "PRICE": 942.9299926757812,
    "ACTION": "SELL",
    "TX_NUMBER": 467,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-25T20:42:49|,
    "SYMBOL": "IWM",
    "QUANTITY": 8612,
    "PRICE": 190.75999450683594,
    "ACTION": "SELL",
    "TX_NUMBER": 468,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-01T16:01:58|,
    "SYMBOL": "SLV",
    "QUANTITY": 6742,
    "PRICE": 623.0700073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 469,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-07T11:48:45|,
    "SYMBOL": "DIA",
    "QUANTITY": 1346,
    "PRICE": 247.64999389648438,
    "ACTION": "BUY",
    "TX_NUMBER": 470,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-19T05:39:44|,
    "SYMBOL": "DIA",
    "QUANTITY": 5802,
    "PRICE": 188.75999450683594,
    "ACTION": "SELL",
    "TX_NUMBER": 471,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-10T04:54:27|,
    "SYMBOL": "SPY",
    "QUANTITY": 2035,
    "PRICE": 888.8300170898438,
    "ACTION": "SELL",
    "TX_NUMBER": 472,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-25T12:31:54|,
    "SYMBOL": "GLD",
    "QUANTITY": 6006,
    "PRICE": 617.47998046875,
    "ACTION": "BUY",
    "TX_NUMBER": 473,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-07T09:03:39|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8461,
    "PRICE": 530.5900268554688,
    "ACTION": "SELL",
    "TX_NUMBER": 474,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-08T14:18:44|,
    "SYMBOL": "IWM",
    "QUANTITY": 5427,
    "PRICE": 876.5399780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 475,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-07T16:30:37|,
    "SYMBOL": "SLV",
    "QUANTITY": 2866,
    "PRICE": 902.0700073242188,
    "ACTION": "BUY",
    "TX_NUMBER": 476,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-01T01:50:43|,
    "SYMBOL": "DIA",
    "QUANTITY": 4581,
    "PRICE": 270.1700134277344,
    "ACTION": "SELL",
    "TX_NUMBER": 477,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-01T18:17:05|,
    "SYMBOL": "IWM",
    "QUANTITY": 2240,
    "PRICE": 672.1300048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 478,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-19T06:29:36|,
    "SYMBOL": "IWM",
    "QUANTITY": 8556,
    "PRICE": 498.8699951171875,
    "ACTION": "SELL",
    "TX_NUMBER": 479,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-19T11:52:21|,
    "SYMBOL": "GLD",
    "QUANTITY": 519,
    "PRICE": 583.030029296875,
    "ACTION": "BUY",
    "TX_NUMBER": 480,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-15T21:08:48|,
    "SYMBOL": "SLV",
    "QUANTITY": 9302,
    "PRICE": 567.1799926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 481,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-30T07:55:42|,
    "SYMBOL": "GLD",
    "QUANTITY": 3312,
    "PRICE": 429.0199890136719,
    "ACTION": "SELL",
    "TX_NUMBER": 482,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-25T03:01:12|,
    "SYMBOL": "SLV",
    "QUANTITY": 7518,
    "PRICE": 789.2899780273438,
    "ACTION": "SELL",
    "TX_NUMBER": 483,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-10T10:13:48|,
    "SYMBOL": "IWM",
    "QUANTITY": 1925,
    "PRICE": 519.5700073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 484,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-20T11:41:50|,
    "SYMBOL": "SLV",
    "QUANTITY": 3249,
    "PRICE": 81.86000061035156,
    "ACTION": "BUY",
    "TX_NUMBER": 485,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-31T17:07:13|,
    "SYMBOL": "GLD",
    "QUANTITY": 6964,
    "PRICE": 441.3500061035156,
    "ACTION": "SELL",
    "TX_NUMBER": 486,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-07T07:52:54|,
    "SYMBOL": "DIA",
    "QUANTITY": 3155,
    "PRICE": 597.22998046875,
    "ACTION": "BUY",
    "TX_NUMBER": 487,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-23T19:52:11|,
    "SYMBOL": "SLV",
    "QUANTITY": 772,
    "PRICE": 608.9400024414062,
    "ACTION": "BUY",
    "TX_NUMBER": 488,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-13T02:02:05|,
    "SYMBOL": "SPY",
    "QUANTITY": 9121,
    "PRICE": 586.47998046875,
    "ACTION": "SELL",
    "TX_NUMBER": 489,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-07T07:55:24|,
    "SYMBOL": "SPY",
    "QUANTITY": 3510,
    "PRICE": 647.1300048828125,
    "ACTION": "SELL",
    "TX_NUMBER": 490,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-26T13:48:40|,
    "SYMBOL": "DIA",
    "QUANTITY": 359,
    "PRICE": 442.1600036621094,
    "ACTION": "SELL",
    "TX_NUMBER": 491,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-01T01:22:07|,
    "SYMBOL": "SPY",
    "QUANTITY": 1612,
    "PRICE": 857.9199829101562,
    "ACTION": "SELL",
    "TX_NUMBER": 492,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-13T12:00:31|,
    "SYMBOL": "GLD",
    "QUANTITY": 897,
    "PRICE": 434.1099853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 493,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-28T16:17:02|,
    "SYMBOL": "GLD",
    "QUANTITY": 7307,
    "PRICE": 969.27001953125,
    "ACTION": "BUY",
    "TX_NUMBER": 494,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-05T21:31:16|,
    "SYMBOL": "DIA",
    "QUANTITY": 8202,
    "PRICE": 99.2300033569336,
    "ACTION": "BUY",
    "TX_NUMBER": 495,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-17T08:49:03|,
    "SYMBOL": "SPY",
    "QUANTITY": 6158,
    "PRICE": 705.0700073242188,
    "ACTION": "BUY",
    "TX_NUMBER": 496,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-25T16:53:01|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2688,
    "PRICE": 212.14999389648438,
    "ACTION": "SELL",
    "TX_NUMBER": 497,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-19T18:09:09|,
    "SYMBOL": "GLD",
    "QUANTITY": 6686,
    "PRICE": 534.780029296875,
    "ACTION": "BUY",
    "TX_NUMBER": 498,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-28T22:37:28|,
    "SYMBOL": "IWM",
    "QUANTITY": 4182,
    "PRICE": 828.8300170898438,
    "ACTION": "SELL",
    "TX_NUMBER": 499,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-15T18:59:25|,
    "SYMBOL": "SLV",
    "QUANTITY": 5204,
    "PRICE": 752.6599731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 500,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-29T06:56:34|,
    "SYMBOL": "SLV",
    "QUANTITY": 4464,
    "PRICE": 980.010009765625,
    "ACTION": "SELL",
    "TX_NUMBER": 501,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-11-02T10:44:34|,
    "SYMBOL": "IWM",
    "QUANTITY": 6798,
    "PRICE": 747.3900146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 502,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-29T07:28:34|,
    "SYMBOL": "IWM",
    "QUANTITY": 3413,
    "PRICE": 971.4400024414062,
    "ACTION": "BUY",
    "TX_NUMBER": 503,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-20T19:12:20|,
    "SYMBOL": "SLV",
    "QUANTITY": 9116,
    "PRICE": 364.44000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 504,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-06T23:09:29|,
    "SYMBOL": "GLD",
    "QUANTITY": 7545,
    "PRICE": 505.1199951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 505,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-22T17:44:33|,
    "SYMBOL": "SPY",
    "QUANTITY": 1075,
    "PRICE": 712.3400268554688,
    "ACTION": "SELL",
    "TX_NUMBER": 506,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-07T12:13:20|,
    "SYMBOL": "SPY",
    "QUANTITY": 7866,
    "PRICE": 440.44000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 507,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-03T05:28:16|,
    "SYMBOL": "SLV",
    "QUANTITY": 9471,
    "PRICE": 494.7099914550781,
    "ACTION": "SELL",
    "TX_NUMBER": 508,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-09T16:16:11|,
    "SYMBOL": "GLD",
    "QUANTITY": 9886,
    "PRICE": 673.5,
    "ACTION": "SELL",
    "TX_NUMBER": 509,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-19T00:14:32|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6705,
    "PRICE": 83.12000274658203,
    "ACTION": "SELL",
    "TX_NUMBER": 510,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-19T04:01:36|,
    "SYMBOL": "DIA",
    "QUANTITY": 1147,
    "PRICE": 764.27001953125,
    "ACTION": "BUY",
    "TX_NUMBER": 511,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-02T18:24:37|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5540,
    "PRICE": 296.260009765625,
    "ACTION": "SELL",
    "TX_NUMBER": 512,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-11T18:46:34|,
    "SYMBOL": "SLV",
    "QUANTITY": 4022,
    "PRICE": 209.0500030517578,
    "ACTION": "BUY",
    "TX_NUMBER": 513,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-23T23:43:31|,
    "SYMBOL": "IWM",
    "QUANTITY": 1771,
    "PRICE": 955.760009765625,
    "ACTION": "SELL",
    "TX_NUMBER": 514,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-11T15:55:36|,
    "SYMBOL": "GLD",
    "QUANTITY": 2495,
    "PRICE": 174.14999389648438,
    "ACTION": "BUY",
    "TX_NUMBER": 515,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-03T18:24:14|,
    "SYMBOL": "SLV",
    "QUANTITY": 6391,
    "PRICE": 66.41000366210938,
    "ACTION": "SELL",
    "TX_NUMBER": 516,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-11T10:17:34|,
    "SYMBOL": "SLV",
    "QUANTITY": 9414,
    "PRICE": 513.2100219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 517,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-20T10:54:09|,
    "SYMBOL": "SPY",
    "QUANTITY": 3890,
    "PRICE": 56.84000015258789,
    "ACTION": "BUY",
    "TX_NUMBER": 518,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-03T13:51:33|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6851,
    "PRICE": 943.6599731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 519,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-23T00:03:56|,
    "SYMBOL": "GLD",
    "QUANTITY": 5543,
    "PRICE": 119.9000015258789,
    "ACTION": "SELL",
    "TX_NUMBER": 520,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-27T22:17:39|,
    "SYMBOL": "IWM",
    "QUANTITY": 4653,
    "PRICE": 574.1400146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 521,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-19T15:30:53|,
    "SYMBOL": "DIA",
    "QUANTITY": 303,
    "PRICE": 477.260009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 522,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-22T00:14:22|,
    "SYMBOL": "IWM",
    "QUANTITY": 4036,
    "PRICE": 197.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 523,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-10T17:40:08|,
    "SYMBOL": "DIA",
    "QUANTITY": 6405,
    "PRICE": 278.25,
    "ACTION": "SELL",
    "TX_NUMBER": 524,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-19T09:28:55|,
    "SYMBOL": "SLV",
    "QUANTITY": 4935,
    "PRICE": 190.4499969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 525,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-25T10:18:52|,
    "SYMBOL": "IWM",
    "QUANTITY": 7493,
    "PRICE": 618.9400024414062,
    "ACTION": "SELL",
    "TX_NUMBER": 526,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-17T19:21:03|,
    "SYMBOL": "DIA",
    "QUANTITY": 1824,
    "PRICE": 703.3300170898438,
    "ACTION": "BUY",
    "TX_NUMBER": 527,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-01T20:57:33|,
    "SYMBOL": "IWM",
    "QUANTITY": 269,
    "PRICE": 949.9099731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 528,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-05T01:12:29|,
    "SYMBOL": "DIA",
    "QUANTITY": 3925,
    "PRICE": 541.5399780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 529,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-25T05:42:42|,
    "SYMBOL": "IWM",
    "QUANTITY": 4124,
    "PRICE": 59.630001068115234,
    "ACTION": "SELL",
    "TX_NUMBER": 530,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-06T06:12:17|,
    "SYMBOL": "SPY",
    "QUANTITY": 4691,
    "PRICE": 928.4000244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 531,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-16T07:48:53|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9503,
    "PRICE": 630.2100219726562,
    "ACTION": "SELL",
    "TX_NUMBER": 532,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-19T01:12:51|,
    "SYMBOL": "DIA",
    "QUANTITY": 818,
    "PRICE": 636.8300170898438,
    "ACTION": "BUY",
    "TX_NUMBER": 533,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-19T00:58:33|,
    "SYMBOL": "GLD",
    "QUANTITY": 4698,
    "PRICE": 51.099998474121094,
    "ACTION": "SELL",
    "TX_NUMBER": 534,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-20T10:53:41|,
    "SYMBOL": "IWM",
    "QUANTITY": 4569,
    "PRICE": 583.5399780273438,
    "ACTION": "SELL",
    "TX_NUMBER": 535,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-10T05:57:14|,
    "SYMBOL": "SLV",
    "QUANTITY": 2945,
    "PRICE": 645.1500244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 536,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-06T08:38:46|,
    "SYMBOL": "SLV",
    "QUANTITY": 431,
    "PRICE": 817.0700073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 537,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-26T07:26:10|,
    "SYMBOL": "SLV",
    "QUANTITY": 7647,
    "PRICE": 412.82000732421875,
    "ACTION": "SELL",
    "TX_NUMBER": 538,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-01T04:55:00|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5786,
    "PRICE": 525.5499877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 539,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-31T21:44:35|,
    "SYMBOL": "SLV",
    "QUANTITY": 3901,
    "PRICE": 485.54998779296875,
    "ACTION": "SELL",
    "TX_NUMBER": 540,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-11T18:50:21|,
    "SYMBOL": "SLV",
    "QUANTITY": 4615,
    "PRICE": 968.0499877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 541,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-31T06:45:16|,
    "SYMBOL": "IWM",
    "QUANTITY": 6657,
    "PRICE": 860.7000122070312,
    "ACTION": "SELL",
    "TX_NUMBER": 542,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-27T15:47:45|,
    "SYMBOL": "SPY",
    "QUANTITY": 2253,
    "PRICE": 845.9199829101562,
    "ACTION": "BUY",
    "TX_NUMBER": 543,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-30T23:46:51|,
    "SYMBOL": "SPY",
    "QUANTITY": 2743,
    "PRICE": 303.7799987792969,
    "ACTION": "BUY",
    "TX_NUMBER": 544,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-20T14:28:52|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4698,
    "PRICE": 183.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 545,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-05T12:48:55|,
    "SYMBOL": "IWM",
    "QUANTITY": 9222,
    "PRICE": 831.4199829101562,
    "ACTION": "BUY",
    "TX_NUMBER": 546,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-02T02:01:10|,
    "SYMBOL": "GLD",
    "QUANTITY": 2235,
    "PRICE": 308.239990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 547,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-29T13:23:36|,
    "SYMBOL": "DIA",
    "QUANTITY": 7048,
    "PRICE": 804.25,
    "ACTION": "BUY",
    "TX_NUMBER": 548,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-14T23:44:18|,
    "SYMBOL": "DIA",
    "QUANTITY": 8300,
    "PRICE": 172.64999389648438,
    "ACTION": "SELL",
    "TX_NUMBER": 549,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-04T18:22:41|,
    "SYMBOL": "DIA",
    "QUANTITY": 3357,
    "PRICE": 121.04000091552734,
    "ACTION": "SELL",
    "TX_NUMBER": 550,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-07T13:44:11|,
    "SYMBOL": "DIA",
    "QUANTITY": 3339,
    "PRICE": 123.3499984741211,
    "ACTION": "SELL",
    "TX_NUMBER": 551,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-05T15:00:41|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1636,
    "PRICE": 620.75,
    "ACTION": "BUY",
    "TX_NUMBER": 552,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-16T17:17:14|,
    "SYMBOL": "IWM",
    "QUANTITY": 898,
    "PRICE": 379.3699951171875,
    "ACTION": "SELL",
    "TX_NUMBER": 553,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-05T08:22:40|,
    "SYMBOL": "GLD",
    "QUANTITY": 4372,
    "PRICE": 693.5399780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 554,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-18T15:35:39|,
    "SYMBOL": "SPY",
    "QUANTITY": 8890,
    "PRICE": 831.8200073242188,
    "ACTION": "BUY",
    "TX_NUMBER": 555,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-12T04:43:33|,
    "SYMBOL": "GLD",
    "QUANTITY": 9649,
    "PRICE": 826.8900146484375,
    "ACTION": "BUY",
    "TX_NUMBER": 556,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-30T17:28:26|,
    "SYMBOL": "SPY",
    "QUANTITY": 1885,
    "PRICE": 388.17999267578125,
    "ACTION": "SELL",
    "TX_NUMBER": 557,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-20T23:25:16|,
    "SYMBOL": "SPY",
    "QUANTITY": 3174,
    "PRICE": 82.05000305175781,
    "ACTION": "BUY",
    "TX_NUMBER": 558,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-02T06:29:46|,
    "SYMBOL": "SLV",
    "QUANTITY": 4344,
    "PRICE": 733.489990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 559,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-05T16:07:45|,
    "SYMBOL": "DIA",
    "QUANTITY": 3829,
    "PRICE": 830.2100219726562,
    "ACTION": "SELL",
    "TX_NUMBER": 560,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-01T00:53:49|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7216,
    "PRICE": 418.20001220703125,
    "ACTION": "BUY",
    "TX_NUMBER": 561,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-07T21:19:31|,
    "SYMBOL": "GLD",
    "QUANTITY": 3709,
    "PRICE": 911.1099853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 562,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-24T08:34:32|,
    "SYMBOL": "DIA",
    "QUANTITY": 3036,
    "PRICE": 952.8499755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 563,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-14T21:13:02|,
    "SYMBOL": "GLD",
    "QUANTITY": 3048,
    "PRICE": 255.10000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 564,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-29T05:14:46|,
    "SYMBOL": "SLV",
    "QUANTITY": 8277,
    "PRICE": 641.0700073242188,
    "ACTION": "BUY",
    "TX_NUMBER": 565,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-18T15:36:48|,
    "SYMBOL": "DIA",
    "QUANTITY": 5493,
    "PRICE": 61.790000915527344,
    "ACTION": "SELL",
    "TX_NUMBER": 566,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-24T10:46:57|,
    "SYMBOL": "SPY",
    "QUANTITY": 4254,
    "PRICE": 357.70001220703125,
    "ACTION": "BUY",
    "TX_NUMBER": 567,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-20T13:44:26|,
    "SYMBOL": "SPY",
    "QUANTITY": 2636,
    "PRICE": 482.6600036621094,
    "ACTION": "BUY",
    "TX_NUMBER": 568,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-25T16:01:58|,
    "SYMBOL": "DIA",
    "QUANTITY": 8393,
    "PRICE": 585.5499877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 569,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-30T12:31:01|,
    "SYMBOL": "GLD",
    "QUANTITY": 6761,
    "PRICE": 167.16000366210938,
    "ACTION": "SELL",
    "TX_NUMBER": 570,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-21T02:25:34|,
    "SYMBOL": "QQQ",
    "QUANTITY": 336,
    "PRICE": 938.1099853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 571,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-28T09:23:41|,
    "SYMBOL": "GLD",
    "QUANTITY": 2260,
    "PRICE": 263.2699890136719,
    "ACTION": "SELL",
    "TX_NUMBER": 572,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-07T21:18:57|,
    "SYMBOL": "SPY",
    "QUANTITY": 5588,
    "PRICE": 266.9800109863281,
    "ACTION": "SELL",
    "TX_NUMBER": 573,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-14T01:19:09|,
    "SYMBOL": "IWM",
    "QUANTITY": 1939,
    "PRICE": 646.4199829101562,
    "ACTION": "BUY",
    "TX_NUMBER": 574,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-06T16:40:41|,
    "SYMBOL": "SLV",
    "QUANTITY": 2704,
    "PRICE": 758.5999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 575,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-25T19:48:15|,
    "SYMBOL": "GLD",
    "QUANTITY": 2688,
    "PRICE": 947.9600219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 576,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-14T19:35:19|,
    "SYMBOL": "SLV",
    "QUANTITY": 8988,
    "PRICE": 832.4000244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 577,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-26T22:20:08|,
    "SYMBOL": "IWM",
    "QUANTITY": 5104,
    "PRICE": 848.52001953125,
    "ACTION": "BUY",
    "TX_NUMBER": 578,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-14T21:34:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 6402,
    "PRICE": 671.010009765625,
    "ACTION": "SELL",
    "TX_NUMBER": 579,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-08T11:04:48|,
    "SYMBOL": "GLD",
    "QUANTITY": 4800,
    "PRICE": 332.19000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 580,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-11T22:03:02|,
    "SYMBOL": "GLD",
    "QUANTITY": 9553,
    "PRICE": 950.3400268554688,
    "ACTION": "BUY",
    "TX_NUMBER": 581,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-08T05:47:51|,
    "SYMBOL": "DIA",
    "QUANTITY": 8570,
    "PRICE": 517.719970703125,
    "ACTION": "BUY",
    "TX_NUMBER": 582,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-03T15:12:05|,
    "SYMBOL": "IWM",
    "QUANTITY": 8875,
    "PRICE": 480.92999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 583,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-17T09:05:54|,
    "SYMBOL": "IWM",
    "QUANTITY": 8456,
    "PRICE": 850.5800170898438,
    "ACTION": "BUY",
    "TX_NUMBER": 584,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-18T21:31:22|,
    "SYMBOL": "IWM",
    "QUANTITY": 5209,
    "PRICE": 956.02001953125,
    "ACTION": "SELL",
    "TX_NUMBER": 585,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-09T15:26:04|,
    "SYMBOL": "SLV",
    "QUANTITY": 7062,
    "PRICE": 747.7000122070312,
    "ACTION": "SELL",
    "TX_NUMBER": 586,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-27T05:27:01|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9482,
    "PRICE": 243.72000122070312,
    "ACTION": "SELL",
    "TX_NUMBER": 587,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-17T18:35:52|,
    "SYMBOL": "SPY",
    "QUANTITY": 1848,
    "PRICE": 811.2000122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 588,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-23T17:10:12|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6398,
    "PRICE": 797.6799926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 589,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-30T13:49:56|,
    "SYMBOL": "DIA",
    "QUANTITY": 9794,
    "PRICE": 772.760009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 590,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-20T13:25:01|,
    "SYMBOL": "SPY",
    "QUANTITY": 5082,
    "PRICE": 75.75,
    "ACTION": "BUY",
    "TX_NUMBER": 591,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-21T17:28:32|,
    "SYMBOL": "SPY",
    "QUANTITY": 426,
    "PRICE": 268.67999267578125,
    "ACTION": "SELL",
    "TX_NUMBER": 592,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-02T09:00:16|,
    "SYMBOL": "DIA",
    "QUANTITY": 9828,
    "PRICE": 619.02001953125,
    "ACTION": "BUY",
    "TX_NUMBER": 593,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-11T20:51:32|,
    "SYMBOL": "IWM",
    "QUANTITY": 1432,
    "PRICE": 372.57000732421875,
    "ACTION": "SELL",
    "TX_NUMBER": 594,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-12T01:19:07|,
    "SYMBOL": "DIA",
    "QUANTITY": 7654,
    "PRICE": 459.989990234375,
    "ACTION": "SELL",
    "TX_NUMBER": 595,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-28T18:18:13|,
    "SYMBOL": "DIA",
    "QUANTITY": 9142,
    "PRICE": 150.5800018310547,
    "ACTION": "SELL",
    "TX_NUMBER": 596,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-18T21:34:16|,
    "SYMBOL": "SPY",
    "QUANTITY": 3105,
    "PRICE": 795.6400146484375,
    "ACTION": "BUY",
    "TX_NUMBER": 597,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-15T03:51:03|,
    "SYMBOL": "DIA",
    "QUANTITY": 5275,
    "PRICE": 176.52999877929688,
    "ACTION": "SELL",
    "TX_NUMBER": 598,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-19T14:44:29|,
    "SYMBOL": "GLD",
    "QUANTITY": 3633,
    "PRICE": 385.6199951171875,
    "ACTION": "SELL",
    "TX_NUMBER": 599,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-11-02T18:12:46|,
    "SYMBOL": "SPY",
    "QUANTITY": 6482,
    "PRICE": 190.07000732421875,
    "ACTION": "SELL",
    "TX_NUMBER": 600,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-29T04:27:42|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9136,
    "PRICE": 323.489990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 601,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-16T12:21:54|,
    "SYMBOL": "IWM",
    "QUANTITY": 7335,
    "PRICE": 227.75,
    "ACTION": "BUY",
    "TX_NUMBER": 602,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-29T00:07:15|,
    "SYMBOL": "IWM",
    "QUANTITY": 2801,
    "PRICE": 878.6799926757812,
    "ACTION": "SELL",
    "TX_NUMBER": 603,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-11T14:16:30|,
    "SYMBOL": "SLV",
    "QUANTITY": 9909,
    "PRICE": 441.1499938964844,
    "ACTION": "BUY",
    "TX_NUMBER": 604,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-20T01:27:57|,
    "SYMBOL": "DIA",
    "QUANTITY": 5179,
    "PRICE": 409.9200134277344,
    "ACTION": "BUY",
    "TX_NUMBER": 605,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-13T07:26:21|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6322,
    "PRICE": 167.35000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 606,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-07T05:14:37|,
    "SYMBOL": "SPY",
    "QUANTITY": 4806,
    "PRICE": 821.6699829101562,
    "ACTION": "BUY",
    "TX_NUMBER": 607,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-25T08:28:16|,
    "SYMBOL": "IWM",
    "QUANTITY": 9656,
    "PRICE": 915.8400268554688,
    "ACTION": "BUY",
    "TX_NUMBER": 608,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-10T16:45:33|,
    "SYMBOL": "GLD",
    "QUANTITY": 901,
    "PRICE": 361.2099914550781,
    "ACTION": "SELL",
    "TX_NUMBER": 609,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-08T12:30:44|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5458,
    "PRICE": 681.2999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 610,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-19T15:47:53|,
    "SYMBOL": "DIA",
    "QUANTITY": 8667,
    "PRICE": 655.489990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 611,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-10T23:36:50|,
    "SYMBOL": "QQQ",
    "QUANTITY": 269,
    "PRICE": 552.6900024414062,
    "ACTION": "SELL",
    "TX_NUMBER": 612,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-03T12:50:30|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1587,
    "PRICE": 171.00999450683594,
    "ACTION": "BUY",
    "TX_NUMBER": 613,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-16T12:17:22|,
    "SYMBOL": "SPY",
    "QUANTITY": 1766,
    "PRICE": 720.280029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 614,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-06T19:42:12|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4567,
    "PRICE": 805.8200073242188,
    "ACTION": "BUY",
    "TX_NUMBER": 615,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-01T03:58:59|,
    "SYMBOL": "SLV",
    "QUANTITY": 9355,
    "PRICE": 151.97999572753906,
    "ACTION": "SELL",
    "TX_NUMBER": 616,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-18T23:54:34|,
    "SYMBOL": "SPY",
    "QUANTITY": 6532,
    "PRICE": 265.239990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 617,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-03T12:20:49|,
    "SYMBOL": "IWM",
    "QUANTITY": 9052,
    "PRICE": 454.760009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 618,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-06T07:52:05|,
    "SYMBOL": "IWM",
    "QUANTITY": 2648,
    "PRICE": 829.7100219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 619,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-04T19:22:35|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2867,
    "PRICE": 586.0700073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 620,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-23T18:25:32|,
    "SYMBOL": "IWM",
    "QUANTITY": 8601,
    "PRICE": 623.7899780273438,
    "ACTION": "SELL",
    "TX_NUMBER": 621,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-04T09:34:09|,
    "SYMBOL": "QQQ",
    "QUANTITY": 610,
    "PRICE": 762.469970703125,
    "ACTION": "SELL",
    "TX_NUMBER": 622,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-20T15:41:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 7405,
    "PRICE": 836.6599731445312,
    "ACTION": "SELL",
    "TX_NUMBER": 623,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-14T03:30:39|,
    "SYMBOL": "SPY",
    "QUANTITY": 3980,
    "PRICE": 239.3000030517578,
    "ACTION": "BUY",
    "TX_NUMBER": 624,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-16T12:15:47|,
    "SYMBOL": "IWM",
    "QUANTITY": 3930,
    "PRICE": 756.4000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 625,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-30T04:07:09|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7649,
    "PRICE": 247.8300018310547,
    "ACTION": "SELL",
    "TX_NUMBER": 626,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-28T15:22:30|,
    "SYMBOL": "IWM",
    "QUANTITY": 8476,
    "PRICE": 510.0799865722656,
    "ACTION": "BUY",
    "TX_NUMBER": 627,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-14T04:36:07|,
    "SYMBOL": "SPY",
    "QUANTITY": 4605,
    "PRICE": 515.5399780273438,
    "ACTION": "SELL",
    "TX_NUMBER": 628,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-11-04T15:24:41|,
    "SYMBOL": "SPY",
    "QUANTITY": 8982,
    "PRICE": 651.5,
    "ACTION": "BUY",
    "TX_NUMBER": 629,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-27T04:40:43|,
    "SYMBOL": "SPY",
    "QUANTITY": 2787,
    "PRICE": 832.1300048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 630,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-17T17:44:54|,
    "SYMBOL": "SPY",
    "QUANTITY": 7516,
    "PRICE": 689.5800170898438,
    "ACTION": "SELL",
    "TX_NUMBER": 631,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-04T05:02:19|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5461,
    "PRICE": 234.89999389648438,
    "ACTION": "BUY",
    "TX_NUMBER": 632,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-16T09:15:42|,
    "SYMBOL": "SLV",
    "QUANTITY": 3016,
    "PRICE": 550.1699829101562,
    "ACTION": "BUY",
    "TX_NUMBER": 633,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-25T07:37:48|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2105,
    "PRICE": 316.67999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 634,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-24T16:31:59|,
    "SYMBOL": "QQQ",
    "QUANTITY": 413,
    "PRICE": 645.4099731445312,
    "ACTION": "SELL",
    "TX_NUMBER": 635,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-24T21:07:58|,
    "SYMBOL": "IWM",
    "QUANTITY": 4729,
    "PRICE": 822.0499877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 636,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-17T05:22:00|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3415,
    "PRICE": 520.2000122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 637,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-05T04:15:40|,
    "SYMBOL": "SLV",
    "QUANTITY": 4409,
    "PRICE": 108.06999969482422,
    "ACTION": "SELL",
    "TX_NUMBER": 638,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-24T15:12:13|,
    "SYMBOL": "SLV",
    "QUANTITY": 898,
    "PRICE": 422.54998779296875,
    "ACTION": "SELL",
    "TX_NUMBER": 639,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-22T05:43:19|,
    "SYMBOL": "SLV",
    "QUANTITY": 4810,
    "PRICE": 409.5899963378906,
    "ACTION": "SELL",
    "TX_NUMBER": 640,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-30T05:33:34|,
    "SYMBOL": "SPY",
    "QUANTITY": 7008,
    "PRICE": 359.5199890136719,
    "ACTION": "SELL",
    "TX_NUMBER": 641,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-09T20:00:11|,
    "SYMBOL": "DIA",
    "QUANTITY": 153,
    "PRICE": 583.1799926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 642,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-16T00:40:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 6051,
    "PRICE": 183.6300048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 643,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-26T14:34:03|,
    "SYMBOL": "SPY",
    "QUANTITY": 9783,
    "PRICE": 533.8800048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 644,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-13T18:47:52|,
    "SYMBOL": "DIA",
    "QUANTITY": 8133,
    "PRICE": 904.030029296875,
    "ACTION": "BUY",
    "TX_NUMBER": 645,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-02T22:33:02|,
    "SYMBOL": "SLV",
    "QUANTITY": 7023,
    "PRICE": 150.63999938964844,
    "ACTION": "SELL",
    "TX_NUMBER": 646,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-17T03:33:41|,
    "SYMBOL": "SPY",
    "QUANTITY": 6354,
    "PRICE": 52.7599983215332,
    "ACTION": "SELL",
    "TX_NUMBER": 647,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-06T05:19:39|,
    "SYMBOL": "DIA",
    "QUANTITY": 6881,
    "PRICE": 358.92999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 648,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-19T00:43:06|,
    "SYMBOL": "SLV",
    "QUANTITY": 3183,
    "PRICE": 403.1499938964844,
    "ACTION": "BUY",
    "TX_NUMBER": 649,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-21T00:53:14|,
    "SYMBOL": "IWM",
    "QUANTITY": 1259,
    "PRICE": 175.02999877929688,
    "ACTION": "SELL",
    "TX_NUMBER": 650,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-28T09:14:50|,
    "SYMBOL": "IWM",
    "QUANTITY": 7565,
    "PRICE": 223.27000427246094,
    "ACTION": "BUY",
    "TX_NUMBER": 651,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-07T16:59:01|,
    "SYMBOL": "GLD",
    "QUANTITY": 5648,
    "PRICE": 753.4099731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 652,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-22T18:08:57|,
    "SYMBOL": "SLV",
    "QUANTITY": 248,
    "PRICE": 693.8200073242188,
    "ACTION": "BUY",
    "TX_NUMBER": 653,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-26T19:11:18|,
    "SYMBOL": "SPY",
    "QUANTITY": 8279,
    "PRICE": 319.7900085449219,
    "ACTION": "SELL",
    "TX_NUMBER": 654,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-26T22:48:24|,
    "SYMBOL": "SPY",
    "QUANTITY": 6164,
    "PRICE": 215.92999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 655,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-23T14:08:58|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3565,
    "PRICE": 966.489990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 656,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-11-05T04:57:57|,
    "SYMBOL": "SPY",
    "QUANTITY": 1708,
    "PRICE": 218.22000122070312,
    "ACTION": "SELL",
    "TX_NUMBER": 657,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-10T14:50:32|,
    "SYMBOL": "SPY",
    "QUANTITY": 1077,
    "PRICE": 685.5499877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 658,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-26T19:45:39|,
    "SYMBOL": "SPY",
    "QUANTITY": 3367,
    "PRICE": 253.19000244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 659,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-30T10:21:39|,
    "SYMBOL": "SPY",
    "QUANTITY": 393,
    "PRICE": 799.97998046875,
    "ACTION": "SELL",
    "TX_NUMBER": 660,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-09T15:53:47|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5505,
    "PRICE": 678.77001953125,
    "ACTION": "SELL",
    "TX_NUMBER": 661,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-15T14:22:34|,
    "SYMBOL": "GLD",
    "QUANTITY": 5349,
    "PRICE": 489.3500061035156,
    "ACTION": "SELL",
    "TX_NUMBER": 662,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-06T15:03:00|,
    "SYMBOL": "GLD",
    "QUANTITY": 6475,
    "PRICE": 300.94000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 663,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-14T22:13:22|,
    "SYMBOL": "DIA",
    "QUANTITY": 3851,
    "PRICE": 768.0800170898438,
    "ACTION": "SELL",
    "TX_NUMBER": 664,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-07T08:14:51|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6872,
    "PRICE": 493.5299987792969,
    "ACTION": "BUY",
    "TX_NUMBER": 665,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-08T12:03:49|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6559,
    "PRICE": 551.77001953125,
    "ACTION": "BUY",
    "TX_NUMBER": 666,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-30T14:39:02|,
    "SYMBOL": "SPY",
    "QUANTITY": 9964,
    "PRICE": 661.5900268554688,
    "ACTION": "SELL",
    "TX_NUMBER": 667,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-03T02:59:02|,
    "SYMBOL": "GLD",
    "QUANTITY": 1845,
    "PRICE": 769.02001953125,
    "ACTION": "SELL",
    "TX_NUMBER": 668,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-23T00:15:19|,
    "SYMBOL": "DIA",
    "QUANTITY": 2199,
    "PRICE": 733.969970703125,
    "ACTION": "BUY",
    "TX_NUMBER": 669,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-22T18:34:56|,
    "SYMBOL": "IWM",
    "QUANTITY": 2750,
    "PRICE": 900.22998046875,
    "ACTION": "SELL",
    "TX_NUMBER": 670,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-07T06:20:04|,
    "SYMBOL": "GLD",
    "QUANTITY": 4591,
    "PRICE": 331.80999755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 671,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-30T09:25:43|,
    "SYMBOL": "IWM",
    "QUANTITY": 1288,
    "PRICE": 214.88999938964844,
    "ACTION": "SELL",
    "TX_NUMBER": 672,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-17T06:32:41|,
    "SYMBOL": "SLV",
    "QUANTITY": 9873,
    "PRICE": 583.780029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 673,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-26T12:14:05|,
    "SYMBOL": "IWM",
    "QUANTITY": 1349,
    "PRICE": 750.8400268554688,
    "ACTION": "BUY",
    "TX_NUMBER": 674,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-04T18:22:04|,
    "SYMBOL": "SLV",
    "QUANTITY": 3194,
    "PRICE": 291.20001220703125,
    "ACTION": "SELL",
    "TX_NUMBER": 675,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-25T20:01:12|,
    "SYMBOL": "DIA",
    "QUANTITY": 2274,
    "PRICE": 183.25999450683594,
    "ACTION": "BUY",
    "TX_NUMBER": 676,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-28T11:19:24|,
    "SYMBOL": "DIA",
    "QUANTITY": 6540,
    "PRICE": 847.8300170898438,
    "ACTION": "BUY",
    "TX_NUMBER": 677,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-28T12:27:19|,
    "SYMBOL": "SPY",
    "QUANTITY": 6160,
    "PRICE": 266.8299865722656,
    "ACTION": "SELL",
    "TX_NUMBER": 678,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-11T23:08:42|,
    "SYMBOL": "SLV",
    "QUANTITY": 2935,
    "PRICE": 434.510009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 679,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-18T08:32:54|,
    "SYMBOL": "GLD",
    "QUANTITY": 6411,
    "PRICE": 740.9400024414062,
    "ACTION": "BUY",
    "TX_NUMBER": 680,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-25T11:58:49|,
    "SYMBOL": "IWM",
    "QUANTITY": 6851,
    "PRICE": 888.02001953125,
    "ACTION": "SELL",
    "TX_NUMBER": 681,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-09T08:30:23|,
    "SYMBOL": "SPY",
    "QUANTITY": 4389,
    "PRICE": 496.2200012207031,
    "ACTION": "SELL",
    "TX_NUMBER": 682,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-24T03:50:18|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4405,
    "PRICE": 443.3699951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 683,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-04T10:13:45|,
    "SYMBOL": "SLV",
    "QUANTITY": 9652,
    "PRICE": 975.530029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 684,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-12T08:55:39|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5269,
    "PRICE": 860.2899780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 685,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-30T01:51:26|,
    "SYMBOL": "SLV",
    "QUANTITY": 5625,
    "PRICE": 888.4600219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 686,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-04T20:58:03|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1478,
    "PRICE": 191.30999755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 687,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-01T19:12:12|,
    "SYMBOL": "SPY",
    "QUANTITY": 1681,
    "PRICE": 63.97999954223633,
    "ACTION": "SELL",
    "TX_NUMBER": 688,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-25T01:16:15|,
    "SYMBOL": "SPY",
    "QUANTITY": 1066,
    "PRICE": 846.8300170898438,
    "ACTION": "SELL",
    "TX_NUMBER": 689,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-26T20:10:09|,
    "SYMBOL": "SLV",
    "QUANTITY": 8731,
    "PRICE": 502.6099853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 690,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-08T11:42:28|,
    "SYMBOL": "IWM",
    "QUANTITY": 1632,
    "PRICE": 912.780029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 691,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-17T00:34:52|,
    "SYMBOL": "IWM",
    "QUANTITY": 5656,
    "PRICE": 898.219970703125,
    "ACTION": "SELL",
    "TX_NUMBER": 692,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-11T12:54:52|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7661,
    "PRICE": 922.6199951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 693,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-09T17:29:09|,
    "SYMBOL": "SPY",
    "QUANTITY": 9079,
    "PRICE": 348.07000732421875,
    "ACTION": "BUY",
    "TX_NUMBER": 694,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-05T08:01:28|,
    "SYMBOL": "SLV",
    "QUANTITY": 951,
    "PRICE": 540.7100219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 695,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-04T22:41:57|,
    "SYMBOL": "SLV",
    "QUANTITY": 7366,
    "PRICE": 580.3699951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 696,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-07T08:23:53|,
    "SYMBOL": "SPY",
    "QUANTITY": 9167,
    "PRICE": 865.6300048828125,
    "ACTION": "SELL",
    "TX_NUMBER": 697,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-15T11:31:25|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2572,
    "PRICE": 107.6500015258789,
    "ACTION": "BUY",
    "TX_NUMBER": 698,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-12T16:46:22|,
    "SYMBOL": "IWM",
    "QUANTITY": 7282,
    "PRICE": 238.94000244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 699,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-10T06:47:12|,
    "SYMBOL": "SPY",
    "QUANTITY": 8367,
    "PRICE": 624.4400024414062,
    "ACTION": "SELL",
    "TX_NUMBER": 700,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-25T20:25:16|,
    "SYMBOL": "GLD",
    "QUANTITY": 1626,
    "PRICE": 152.99000549316406,
    "ACTION": "SELL",
    "TX_NUMBER": 701,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-23T00:16:55|,
    "SYMBOL": "SLV",
    "QUANTITY": 9536,
    "PRICE": 790.0599975585938,
    "ACTION": "BUY",
    "TX_NUMBER": 702,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-15T03:00:41|,
    "SYMBOL": "IWM",
    "QUANTITY": 9857,
    "PRICE": 400.5199890136719,
    "ACTION": "SELL",
    "TX_NUMBER": 703,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-08T22:25:50|,
    "SYMBOL": "SLV",
    "QUANTITY": 4856,
    "PRICE": 667.4299926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 704,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-23T16:31:44|,
    "SYMBOL": "DIA",
    "QUANTITY": 8771,
    "PRICE": 347.7900085449219,
    "ACTION": "SELL",
    "TX_NUMBER": 705,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-30T04:46:56|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7057,
    "PRICE": 181.0800018310547,
    "ACTION": "SELL",
    "TX_NUMBER": 706,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-01T16:11:23|,
    "SYMBOL": "IWM",
    "QUANTITY": 9344,
    "PRICE": 613.3599853515625,
    "ACTION": "BUY",
    "TX_NUMBER": 707,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-30T10:52:36|,
    "SYMBOL": "DIA",
    "QUANTITY": 5831,
    "PRICE": 123.88999938964844,
    "ACTION": "BUY",
    "TX_NUMBER": 708,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-08T16:55:35|,
    "SYMBOL": "GLD",
    "QUANTITY": 5610,
    "PRICE": 225.63999938964844,
    "ACTION": "BUY",
    "TX_NUMBER": 709,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-30T07:19:03|,
    "SYMBOL": "GLD",
    "QUANTITY": 4839,
    "PRICE": 710.4600219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 710,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-14T05:46:25|,
    "SYMBOL": "SLV",
    "QUANTITY": 448,
    "PRICE": 50.02000045776367,
    "ACTION": "SELL",
    "TX_NUMBER": 711,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-17T18:32:24|,
    "SYMBOL": "DIA",
    "QUANTITY": 6254,
    "PRICE": 953.1199951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 712,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-04T12:19:19|,
    "SYMBOL": "DIA",
    "QUANTITY": 294,
    "PRICE": 732.3499755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 713,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-10T20:27:09|,
    "SYMBOL": "DIA",
    "QUANTITY": 7883,
    "PRICE": 141.00999450683594,
    "ACTION": "SELL",
    "TX_NUMBER": 714,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-16T14:20:32|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6400,
    "PRICE": 567.8599853515625,
    "ACTION": "BUY",
    "TX_NUMBER": 715,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-11T05:04:30|,
    "SYMBOL": "DIA",
    "QUANTITY": 8982,
    "PRICE": 756.5399780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 716,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-11-01T20:58:28|,
    "SYMBOL": "DIA",
    "QUANTITY": 2496,
    "PRICE": 427.55999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 717,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-15T21:30:23|,
    "SYMBOL": "SLV",
    "QUANTITY": 593,
    "PRICE": 209.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 718,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-17T17:12:05|,
    "SYMBOL": "GLD",
    "QUANTITY": 7685,
    "PRICE": 859.4199829101562,
    "ACTION": "SELL",
    "TX_NUMBER": 719,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-26T06:30:01|,
    "SYMBOL": "QQQ",
    "QUANTITY": 819,
    "PRICE": 114.04000091552734,
    "ACTION": "SELL",
    "TX_NUMBER": 720,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-07T21:49:06|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7594,
    "PRICE": 709.5700073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 721,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-27T06:50:42|,
    "SYMBOL": "SPY",
    "QUANTITY": 9296,
    "PRICE": 561.760009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 722,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-02T10:44:10|,
    "SYMBOL": "SLV",
    "QUANTITY": 7395,
    "PRICE": 109.91000366210938,
    "ACTION": "SELL",
    "TX_NUMBER": 723,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-14T15:59:35|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8475,
    "PRICE": 735.030029296875,
    "ACTION": "BUY",
    "TX_NUMBER": 724,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-22T18:08:58|,
    "SYMBOL": "SPY",
    "QUANTITY": 9160,
    "PRICE": 565.469970703125,
    "ACTION": "BUY",
    "TX_NUMBER": 725,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-13T23:34:59|,
    "SYMBOL": "SPY",
    "QUANTITY": 1704,
    "PRICE": 688.3099975585938,
    "ACTION": "SELL",
    "TX_NUMBER": 726,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-21T07:40:09|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9677,
    "PRICE": 227.8300018310547,
    "ACTION": "SELL",
    "TX_NUMBER": 727,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-18T07:05:36|,
    "SYMBOL": "SLV",
    "QUANTITY": 5355,
    "PRICE": 311.8999938964844,
    "ACTION": "SELL",
    "TX_NUMBER": 728,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-11T05:45:53|,
    "SYMBOL": "GLD",
    "QUANTITY": 809,
    "PRICE": 618.8099975585938,
    "ACTION": "BUY",
    "TX_NUMBER": 729,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-10T19:45:12|,
    "SYMBOL": "SLV",
    "QUANTITY": 7974,
    "PRICE": 675.469970703125,
    "ACTION": "SELL",
    "TX_NUMBER": 730,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-07T04:31:10|,
    "SYMBOL": "SLV",
    "QUANTITY": 4710,
    "PRICE": 121.97000122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 731,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-03T15:47:54|,
    "SYMBOL": "DIA",
    "QUANTITY": 8262,
    "PRICE": 307.8500061035156,
    "ACTION": "BUY",
    "TX_NUMBER": 732,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-22T19:56:18|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9976,
    "PRICE": 50.66999816894531,
    "ACTION": "BUY",
    "TX_NUMBER": 733,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-18T06:06:14|,
    "SYMBOL": "GLD",
    "QUANTITY": 2619,
    "PRICE": 139.97999572753906,
    "ACTION": "SELL",
    "TX_NUMBER": 734,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-06T13:26:04|,
    "SYMBOL": "SPY",
    "QUANTITY": 2057,
    "PRICE": 219.22999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 735,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-24T06:18:26|,
    "SYMBOL": "DIA",
    "QUANTITY": 2732,
    "PRICE": 712.1400146484375,
    "ACTION": "BUY",
    "TX_NUMBER": 736,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-05T02:45:33|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3161,
    "PRICE": 258.7799987792969,
    "ACTION": "BUY",
    "TX_NUMBER": 737,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-05T22:43:57|,
    "SYMBOL": "GLD",
    "QUANTITY": 8230,
    "PRICE": 727.8599853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 738,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-21T13:25:34|,
    "SYMBOL": "SPY",
    "QUANTITY": 4971,
    "PRICE": 729.260009765625,
    "ACTION": "SELL",
    "TX_NUMBER": 739,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-22T19:51:41|,
    "SYMBOL": "SLV",
    "QUANTITY": 8745,
    "PRICE": 163.5399932861328,
    "ACTION": "BUY",
    "TX_NUMBER": 740,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-28T05:35:42|,
    "SYMBOL": "SPY",
    "QUANTITY": 8888,
    "PRICE": 606.1900024414062,
    "ACTION": "SELL",
    "TX_NUMBER": 741,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-26T21:36:32|,
    "SYMBOL": "GLD",
    "QUANTITY": 9465,
    "PRICE": 453.989990234375,
    "ACTION": "SELL",
    "TX_NUMBER": 742,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-10T00:46:37|,
    "SYMBOL": "IWM",
    "QUANTITY": 4037,
    "PRICE": 418.489990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 743,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-06T17:36:18|,
    "SYMBOL": "GLD",
    "QUANTITY": 9321,
    "PRICE": 708.8599853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 744,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-12T21:51:19|,
    "SYMBOL": "DIA",
    "QUANTITY": 3155,
    "PRICE": 450.94000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 745,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-24T10:05:28|,
    "SYMBOL": "IWM",
    "QUANTITY": 8833,
    "PRICE": 85.23999786376953,
    "ACTION": "BUY",
    "TX_NUMBER": 746,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-20T16:11:50|,
    "SYMBOL": "GLD",
    "QUANTITY": 2614,
    "PRICE": 481.29998779296875,
    "ACTION": "BUY",
    "TX_NUMBER": 747,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-09T10:10:44|,
    "SYMBOL": "SPY",
    "QUANTITY": 2145,
    "PRICE": 363.1000061035156,
    "ACTION": "BUY",
    "TX_NUMBER": 748,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-11-07T10:21:14|,
    "SYMBOL": "DIA",
    "QUANTITY": 9289,
    "PRICE": 888.030029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 749,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-17T20:17:47|,
    "SYMBOL": "SPY",
    "QUANTITY": 8545,
    "PRICE": 196.6199951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 750,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-02T01:44:13|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6628,
    "PRICE": 586.0,
    "ACTION": "BUY",
    "TX_NUMBER": 751,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-12T06:04:01|,
    "SYMBOL": "IWM",
    "QUANTITY": 4472,
    "PRICE": 810.0,
    "ACTION": "BUY",
    "TX_NUMBER": 752,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-05T18:50:46|,
    "SYMBOL": "SPY",
    "QUANTITY": 1876,
    "PRICE": 853.8200073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 753,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-08T23:56:04|,
    "SYMBOL": "SLV",
    "QUANTITY": 2149,
    "PRICE": 87.73999786376953,
    "ACTION": "SELL",
    "TX_NUMBER": 754,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-11-05T17:38:41|,
    "SYMBOL": "SPY",
    "QUANTITY": 9587,
    "PRICE": 472.8500061035156,
    "ACTION": "BUY",
    "TX_NUMBER": 755,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-02T10:11:38|,
    "SYMBOL": "SPY",
    "QUANTITY": 1357,
    "PRICE": 457.8500061035156,
    "ACTION": "SELL",
    "TX_NUMBER": 756,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-12T04:03:22|,
    "SYMBOL": "DIA",
    "QUANTITY": 8588,
    "PRICE": 810.8300170898438,
    "ACTION": "SELL",
    "TX_NUMBER": 757,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-10T19:38:07|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3521,
    "PRICE": 470.3299865722656,
    "ACTION": "SELL",
    "TX_NUMBER": 758,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-03T16:47:32|,
    "SYMBOL": "SLV",
    "QUANTITY": 5200,
    "PRICE": 829.97998046875,
    "ACTION": "SELL",
    "TX_NUMBER": 759,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-11-06T06:34:14|,
    "SYMBOL": "SLV",
    "QUANTITY": 9867,
    "PRICE": 845.280029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 760,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-23T16:51:29|,
    "SYMBOL": "IWM",
    "QUANTITY": 7605,
    "PRICE": 115.98999786376953,
    "ACTION": "SELL",
    "TX_NUMBER": 761,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-12T11:46:22|,
    "SYMBOL": "SLV",
    "QUANTITY": 6005,
    "PRICE": 900.8699951171875,
    "ACTION": "SELL",
    "TX_NUMBER": 762,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-17T06:37:28|,
    "SYMBOL": "SLV",
    "QUANTITY": 6430,
    "PRICE": 984.6599731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 763,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-07T02:12:29|,
    "SYMBOL": "SPY",
    "QUANTITY": 3638,
    "PRICE": 108.68000030517578,
    "ACTION": "SELL",
    "TX_NUMBER": 764,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-03T11:43:40|,
    "SYMBOL": "IWM",
    "QUANTITY": 4658,
    "PRICE": 580.0800170898438,
    "ACTION": "BUY",
    "TX_NUMBER": 765,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-22T20:08:02|,
    "SYMBOL": "SLV",
    "QUANTITY": 1732,
    "PRICE": 103.37000274658203,
    "ACTION": "BUY",
    "TX_NUMBER": 766,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-20T07:57:46|,
    "SYMBOL": "DIA",
    "QUANTITY": 8931,
    "PRICE": 428.2799987792969,
    "ACTION": "SELL",
    "TX_NUMBER": 767,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-15T15:38:58|,
    "SYMBOL": "IWM",
    "QUANTITY": 8491,
    "PRICE": 984.22998046875,
    "ACTION": "BUY",
    "TX_NUMBER": 768,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-04T10:20:18|,
    "SYMBOL": "GLD",
    "QUANTITY": 3758,
    "PRICE": 622.9199829101562,
    "ACTION": "SELL",
    "TX_NUMBER": 769,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-06T09:11:42|,
    "SYMBOL": "GLD",
    "QUANTITY": 1522,
    "PRICE": 820.9500122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 770,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-09T13:02:40|,
    "SYMBOL": "SPY",
    "QUANTITY": 2265,
    "PRICE": 669.4099731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 771,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-06T06:54:35|,
    "SYMBOL": "QQQ",
    "QUANTITY": 228,
    "PRICE": 160.5399932861328,
    "ACTION": "BUY",
    "TX_NUMBER": 772,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-19T16:10:01|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8214,
    "PRICE": 818.9600219726562,
    "ACTION": "SELL",
    "TX_NUMBER": 773,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-23T08:41:28|,
    "SYMBOL": "SPY",
    "QUANTITY": 1221,
    "PRICE": 570.8699951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 774,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-14T08:58:46|,
    "SYMBOL": "SPY",
    "QUANTITY": 6330,
    "PRICE": 320.6700134277344,
    "ACTION": "SELL",
    "TX_NUMBER": 775,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-30T00:13:42|,
    "SYMBOL": "SLV",
    "QUANTITY": 7662,
    "PRICE": 932.0399780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 776,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-30T17:01:54|,
    "SYMBOL": "SPY",
    "QUANTITY": 833,
    "PRICE": 749.3599853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 777,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-06T08:28:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 6377,
    "PRICE": 575.5,
    "ACTION": "BUY",
    "TX_NUMBER": 778,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-29T06:29:03|,
    "SYMBOL": "IWM",
    "QUANTITY": 6245,
    "PRICE": 640.47998046875,
    "ACTION": "BUY",
    "TX_NUMBER": 779,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-06T18:01:07|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8343,
    "PRICE": 843.4299926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 780,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-25T20:33:21|,
    "SYMBOL": "SPY",
    "QUANTITY": 9586,
    "PRICE": 648.9500122070312,
    "ACTION": "SELL",
    "TX_NUMBER": 781,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-27T14:02:50|,
    "SYMBOL": "IWM",
    "QUANTITY": 8512,
    "PRICE": 497.1600036621094,
    "ACTION": "BUY",
    "TX_NUMBER": 782,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-07T06:11:13|,
    "SYMBOL": "IWM",
    "QUANTITY": 5199,
    "PRICE": 99.37999725341797,
    "ACTION": "SELL",
    "TX_NUMBER": 783,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-07T00:18:57|,
    "SYMBOL": "SLV",
    "QUANTITY": 2654,
    "PRICE": 456.5,
    "ACTION": "BUY",
    "TX_NUMBER": 784,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-11-08T03:57:59|,
    "SYMBOL": "SPY",
    "QUANTITY": 2471,
    "PRICE": 674.5800170898438,
    "ACTION": "SELL",
    "TX_NUMBER": 785,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-19T08:30:54|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9026,
    "PRICE": 494.760009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 786,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-05T21:40:37|,
    "SYMBOL": "GLD",
    "QUANTITY": 9409,
    "PRICE": 423.6000061035156,
    "ACTION": "SELL",
    "TX_NUMBER": 787,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-06T14:42:09|,
    "SYMBOL": "SLV",
    "QUANTITY": 1215,
    "PRICE": 756.3300170898438,
    "ACTION": "BUY",
    "TX_NUMBER": 788,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-04T13:09:04|,
    "SYMBOL": "GLD",
    "QUANTITY": 5261,
    "PRICE": 657.530029296875,
    "ACTION": "BUY",
    "TX_NUMBER": 789,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-02T02:56:41|,
    "SYMBOL": "IWM",
    "QUANTITY": 1046,
    "PRICE": 579.6599731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 790,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-07T13:01:45|,
    "SYMBOL": "IWM",
    "QUANTITY": 9114,
    "PRICE": 927.5900268554688,
    "ACTION": "SELL",
    "TX_NUMBER": 791,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-11-06T18:17:38|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3474,
    "PRICE": 372.6099853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 792,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-26T10:48:43|,
    "SYMBOL": "IWM",
    "QUANTITY": 6412,
    "PRICE": 342.8800048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 793,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-09T18:46:09|,
    "SYMBOL": "SPY",
    "QUANTITY": 3881,
    "PRICE": 985.6500244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 794,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-09T16:55:32|,
    "SYMBOL": "GLD",
    "QUANTITY": 1878,
    "PRICE": 678.72998046875,
    "ACTION": "SELL",
    "TX_NUMBER": 795,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-30T09:31:49|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1521,
    "PRICE": 190.2899932861328,
    "ACTION": "BUY",
    "TX_NUMBER": 796,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-08T18:47:12|,
    "SYMBOL": "SLV",
    "QUANTITY": 1289,
    "PRICE": 478.6499938964844,
    "ACTION": "SELL",
    "TX_NUMBER": 797,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-23T16:52:10|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4108,
    "PRICE": 905.27001953125,
    "ACTION": "SELL",
    "TX_NUMBER": 798,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-08T09:21:27|,
    "SYMBOL": "QQQ",
    "QUANTITY": 652,
    "PRICE": 206.44000244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 799,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-06T17:04:14|,
    "SYMBOL": "DIA",
    "QUANTITY": 9155,
    "PRICE": 771.5700073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 800,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-01T11:33:11|,
    "SYMBOL": "SPY",
    "QUANTITY": 5913,
    "PRICE": 802.0999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 801,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-10T23:28:41|,
    "SYMBOL": "SLV",
    "QUANTITY": 3450,
    "PRICE": 744.8900146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 802,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-08T15:51:49|,
    "SYMBOL": "GLD",
    "QUANTITY": 4189,
    "PRICE": 164.41000366210938,
    "ACTION": "BUY",
    "TX_NUMBER": 803,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-19T01:16:58|,
    "SYMBOL": "SPY",
    "QUANTITY": 5017,
    "PRICE": 267.8299865722656,
    "ACTION": "BUY",
    "TX_NUMBER": 804,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-29T04:05:49|,
    "SYMBOL": "DIA",
    "QUANTITY": 9034,
    "PRICE": 288.8800048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 805,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-02T16:28:27|,
    "SYMBOL": "GLD",
    "QUANTITY": 4380,
    "PRICE": 925.510009765625,
    "ACTION": "SELL",
    "TX_NUMBER": 806,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-11T13:25:46|,
    "SYMBOL": "SLV",
    "QUANTITY": 7563,
    "PRICE": 805.780029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 807,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-25T04:26:18|,
    "SYMBOL": "DIA",
    "QUANTITY": 777,
    "PRICE": 429.9200134277344,
    "ACTION": "BUY",
    "TX_NUMBER": 808,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-23T03:47:39|,
    "SYMBOL": "QQQ",
    "QUANTITY": 319,
    "PRICE": 782.72998046875,
    "ACTION": "BUY",
    "TX_NUMBER": 809,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-11T17:51:15|,
    "SYMBOL": "SLV",
    "QUANTITY": 5015,
    "PRICE": 484.9200134277344,
    "ACTION": "BUY",
    "TX_NUMBER": 810,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-20T03:57:22|,
    "SYMBOL": "SLV",
    "QUANTITY": 9457,
    "PRICE": 74.62000274658203,
    "ACTION": "SELL",
    "TX_NUMBER": 811,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-16T01:14:36|,
    "SYMBOL": "QQQ",
    "QUANTITY": 447,
    "PRICE": 725.2000122070312,
    "ACTION": "SELL",
    "TX_NUMBER": 812,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-10T12:12:57|,
    "SYMBOL": "SLV",
    "QUANTITY": 4697,
    "PRICE": 984.5399780273438,
    "ACTION": "SELL",
    "TX_NUMBER": 813,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-11T00:33:50|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5570,
    "PRICE": 346.4200134277344,
    "ACTION": "BUY",
    "TX_NUMBER": 814,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-28T20:28:05|,
    "SYMBOL": "IWM",
    "QUANTITY": 5358,
    "PRICE": 207.63999938964844,
    "ACTION": "BUY",
    "TX_NUMBER": 815,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-30T21:11:15|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1910,
    "PRICE": 568.469970703125,
    "ACTION": "SELL",
    "TX_NUMBER": 816,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-22T18:17:35|,
    "SYMBOL": "GLD",
    "QUANTITY": 4287,
    "PRICE": 339.45001220703125,
    "ACTION": "BUY",
    "TX_NUMBER": 817,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-09T20:28:37|,
    "SYMBOL": "DIA",
    "QUANTITY": 4581,
    "PRICE": 111.61000061035156,
    "ACTION": "SELL",
    "TX_NUMBER": 818,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-18T15:32:03|,
    "SYMBOL": "GLD",
    "QUANTITY": 9390,
    "PRICE": 717.8599853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 819,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-20T15:48:32|,
    "SYMBOL": "SLV",
    "QUANTITY": 3162,
    "PRICE": 715.1599731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 820,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-18T23:54:57|,
    "SYMBOL": "IWM",
    "QUANTITY": 9169,
    "PRICE": 84.41000366210938,
    "ACTION": "SELL",
    "TX_NUMBER": 821,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-22T00:11:44|,
    "SYMBOL": "DIA",
    "QUANTITY": 2885,
    "PRICE": 542.989990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 822,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-17T19:08:44|,
    "SYMBOL": "SPY",
    "QUANTITY": 907,
    "PRICE": 797.5700073242188,
    "ACTION": "BUY",
    "TX_NUMBER": 823,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-12T04:16:28|,
    "SYMBOL": "SLV",
    "QUANTITY": 7147,
    "PRICE": 253.5399932861328,
    "ACTION": "BUY",
    "TX_NUMBER": 824,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-16T04:03:39|,
    "SYMBOL": "GLD",
    "QUANTITY": 3656,
    "PRICE": 476.6099853515625,
    "ACTION": "BUY",
    "TX_NUMBER": 825,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-05T05:24:40|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7439,
    "PRICE": 496.4599914550781,
    "ACTION": "BUY",
    "TX_NUMBER": 826,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-06T05:47:22|,
    "SYMBOL": "QQQ",
    "QUANTITY": 583,
    "PRICE": 114.81999969482422,
    "ACTION": "SELL",
    "TX_NUMBER": 827,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-31T01:18:59|,
    "SYMBOL": "DIA",
    "QUANTITY": 8527,
    "PRICE": 429.0199890136719,
    "ACTION": "BUY",
    "TX_NUMBER": 828,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-31T02:40:36|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3814,
    "PRICE": 677.7999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 829,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-30T23:39:21|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8675,
    "PRICE": 341.3599853515625,
    "ACTION": "BUY",
    "TX_NUMBER": 830,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-19T05:30:16|,
    "SYMBOL": "SPY",
    "QUANTITY": 9032,
    "PRICE": 751.7999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 831,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-06T07:05:51|,
    "SYMBOL": "IWM",
    "QUANTITY": 4947,
    "PRICE": 291.0400085449219,
    "ACTION": "SELL",
    "TX_NUMBER": 832,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-25T02:41:48|,
    "SYMBOL": "SLV",
    "QUANTITY": 2379,
    "PRICE": 911.7999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 833,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-19T06:23:29|,
    "SYMBOL": "DIA",
    "QUANTITY": 537,
    "PRICE": 253.92999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 834,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-04T14:25:35|,
    "SYMBOL": "SPY",
    "QUANTITY": 3866,
    "PRICE": 152.6199951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 835,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-29T03:44:19|,
    "SYMBOL": "GLD",
    "QUANTITY": 8300,
    "PRICE": 344.25,
    "ACTION": "SELL",
    "TX_NUMBER": 836,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-18T02:32:31|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1227,
    "PRICE": 818.02001953125,
    "ACTION": "BUY",
    "TX_NUMBER": 837,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-14T04:56:57|,
    "SYMBOL": "SPY",
    "QUANTITY": 8504,
    "PRICE": 276.25,
    "ACTION": "BUY",
    "TX_NUMBER": 838,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-13T12:42:34|,
    "SYMBOL": "SPY",
    "QUANTITY": 1719,
    "PRICE": 256.3599853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 839,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-24T18:50:15|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6296,
    "PRICE": 599.1699829101562,
    "ACTION": "BUY",
    "TX_NUMBER": 840,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-23T07:43:28|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2667,
    "PRICE": 149.7100067138672,
    "ACTION": "SELL",
    "TX_NUMBER": 841,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-23T09:13:09|,
    "SYMBOL": "IWM",
    "QUANTITY": 3358,
    "PRICE": 654.010009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 842,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-10T07:09:27|,
    "SYMBOL": "IWM",
    "QUANTITY": 1634,
    "PRICE": 135.52999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 843,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-04T09:18:04|,
    "SYMBOL": "SLV",
    "QUANTITY": 9675,
    "PRICE": 159.19000244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 844,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-19T18:39:10|,
    "SYMBOL": "SPY",
    "QUANTITY": 4907,
    "PRICE": 559.3400268554688,
    "ACTION": "SELL",
    "TX_NUMBER": 845,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-30T18:35:04|,
    "SYMBOL": "GLD",
    "QUANTITY": 9742,
    "PRICE": 532.1500244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 846,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-27T10:26:08|,
    "SYMBOL": "DIA",
    "QUANTITY": 2076,
    "PRICE": 559.0,
    "ACTION": "SELL",
    "TX_NUMBER": 847,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-04T06:16:40|,
    "SYMBOL": "GLD",
    "QUANTITY": 3942,
    "PRICE": 428.29998779296875,
    "ACTION": "BUY",
    "TX_NUMBER": 848,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-28T15:46:24|,
    "SYMBOL": "DIA",
    "QUANTITY": 3581,
    "PRICE": 154.82000732421875,
    "ACTION": "BUY",
    "TX_NUMBER": 849,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-06T14:58:27|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5542,
    "PRICE": 473.25,
    "ACTION": "SELL",
    "TX_NUMBER": 850,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-26T16:48:36|,
    "SYMBOL": "DIA",
    "QUANTITY": 4527,
    "PRICE": 916.5399780273438,
    "ACTION": "SELL",
    "TX_NUMBER": 851,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-22T01:48:06|,
    "SYMBOL": "DIA",
    "QUANTITY": 4453,
    "PRICE": 292.1700134277344,
    "ACTION": "SELL",
    "TX_NUMBER": 852,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-29T07:06:13|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7300,
    "PRICE": 971.280029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 853,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-25T08:49:06|,
    "SYMBOL": "QQQ",
    "QUANTITY": 8814,
    "PRICE": 110.77999877929688,
    "ACTION": "SELL",
    "TX_NUMBER": 854,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-28T06:19:09|,
    "SYMBOL": "GLD",
    "QUANTITY": 874,
    "PRICE": 238.22999572753906,
    "ACTION": "SELL",
    "TX_NUMBER": 855,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-02T18:26:56|,
    "SYMBOL": "SLV",
    "QUANTITY": 4789,
    "PRICE": 306.9800109863281,
    "ACTION": "BUY",
    "TX_NUMBER": 856,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-13T19:15:22|,
    "SYMBOL": "DIA",
    "QUANTITY": 3462,
    "PRICE": 446.1499938964844,
    "ACTION": "SELL",
    "TX_NUMBER": 857,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-23T02:55:01|,
    "SYMBOL": "SLV",
    "QUANTITY": 1349,
    "PRICE": 353.760009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 858,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-26T00:23:09|,
    "SYMBOL": "IWM",
    "QUANTITY": 6172,
    "PRICE": 534.280029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 859,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-11-01T19:13:29|,
    "SYMBOL": "GLD",
    "QUANTITY": 8969,
    "PRICE": 982.8099975585938,
    "ACTION": "SELL",
    "TX_NUMBER": 860,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-10T21:38:49|,
    "SYMBOL": "SLV",
    "QUANTITY": 3111,
    "PRICE": 887.0399780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 861,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-27T20:54:42|,
    "SYMBOL": "DIA",
    "QUANTITY": 6353,
    "PRICE": 507.010009765625,
    "ACTION": "SELL",
    "TX_NUMBER": 862,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-23T00:37:40|,
    "SYMBOL": "DIA",
    "QUANTITY": 6464,
    "PRICE": 545.5999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 863,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-03T14:30:36|,
    "SYMBOL": "SLV",
    "QUANTITY": 9800,
    "PRICE": 802.8699951171875,
    "ACTION": "SELL",
    "TX_NUMBER": 864,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-03T19:28:42|,
    "SYMBOL": "DIA",
    "QUANTITY": 2235,
    "PRICE": 63.02000045776367,
    "ACTION": "SELL",
    "TX_NUMBER": 865,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-18T16:24:27|,
    "SYMBOL": "GLD",
    "QUANTITY": 524,
    "PRICE": 490.0400085449219,
    "ACTION": "BUY",
    "TX_NUMBER": 866,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-08T15:02:04|,
    "SYMBOL": "SPY",
    "QUANTITY": 6235,
    "PRICE": 251.3699951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 867,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-12T01:33:24|,
    "SYMBOL": "SLV",
    "QUANTITY": 987,
    "PRICE": 899.760009765625,
    "ACTION": "SELL",
    "TX_NUMBER": 868,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-04T06:57:49|,
    "SYMBOL": "SPY",
    "QUANTITY": 2556,
    "PRICE": 435.6099853515625,
    "ACTION": "BUY",
    "TX_NUMBER": 869,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-11T22:37:16|,
    "SYMBOL": "SPY",
    "QUANTITY": 2292,
    "PRICE": 494.25,
    "ACTION": "BUY",
    "TX_NUMBER": 870,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-21T03:57:49|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6072,
    "PRICE": 925.77001953125,
    "ACTION": "SELL",
    "TX_NUMBER": 871,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-03T02:02:44|,
    "SYMBOL": "SLV",
    "QUANTITY": 3093,
    "PRICE": 86.58999633789062,
    "ACTION": "BUY",
    "TX_NUMBER": 872,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-27T21:19:03|,
    "SYMBOL": "IWM",
    "QUANTITY": 9825,
    "PRICE": 744.6099853515625,
    "ACTION": "BUY",
    "TX_NUMBER": 873,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-18T09:56:44|,
    "SYMBOL": "IWM",
    "QUANTITY": 350,
    "PRICE": 900.8499755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 874,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-06T06:46:12|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4300,
    "PRICE": 104.41000366210938,
    "ACTION": "BUY",
    "TX_NUMBER": 875,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-05T06:15:55|,
    "SYMBOL": "IWM",
    "QUANTITY": 3649,
    "PRICE": 79.0,
    "ACTION": "BUY",
    "TX_NUMBER": 876,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-01T19:28:32|,
    "SYMBOL": "SPY",
    "QUANTITY": 4862,
    "PRICE": 534.1300048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 877,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-16T20:15:39|,
    "SYMBOL": "SLV",
    "QUANTITY": 1180,
    "PRICE": 662.1699829101562,
    "ACTION": "SELL",
    "TX_NUMBER": 878,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-26T17:52:28|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4332,
    "PRICE": 917.6799926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 879,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-31T18:04:15|,
    "SYMBOL": "DIA",
    "QUANTITY": 559,
    "PRICE": 722.1300048828125,
    "ACTION": "SELL",
    "TX_NUMBER": 880,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-23T13:03:19|,
    "SYMBOL": "DIA",
    "QUANTITY": 3212,
    "PRICE": 540.3599853515625,
    "ACTION": "SELL",
    "TX_NUMBER": 881,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-22T22:15:01|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1974,
    "PRICE": 494.0199890136719,
    "ACTION": "BUY",
    "TX_NUMBER": 882,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-22T23:14:07|,
    "SYMBOL": "GLD",
    "QUANTITY": 1585,
    "PRICE": 63.08000183105469,
    "ACTION": "BUY",
    "TX_NUMBER": 883,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-14T06:34:33|,
    "SYMBOL": "GLD",
    "QUANTITY": 6742,
    "PRICE": 216.3300018310547,
    "ACTION": "BUY",
    "TX_NUMBER": 884,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-30T18:32:16|,
    "SYMBOL": "DIA",
    "QUANTITY": 8189,
    "PRICE": 944.0399780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 885,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-28T02:35:35|,
    "SYMBOL": "IWM",
    "QUANTITY": 3366,
    "PRICE": 800.739990234375,
    "ACTION": "SELL",
    "TX_NUMBER": 886,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-10T05:20:43|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3284,
    "PRICE": 397.29998779296875,
    "ACTION": "BUY",
    "TX_NUMBER": 887,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-11T06:04:17|,
    "SYMBOL": "DIA",
    "QUANTITY": 1407,
    "PRICE": 263.57000732421875,
    "ACTION": "BUY",
    "TX_NUMBER": 888,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-27T01:45:44|,
    "SYMBOL": "IWM",
    "QUANTITY": 1287,
    "PRICE": 230.58999633789062,
    "ACTION": "BUY",
    "TX_NUMBER": 889,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-22T18:09:10|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1148,
    "PRICE": 986.47998046875,
    "ACTION": "BUY",
    "TX_NUMBER": 890,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-19T07:24:02|,
    "SYMBOL": "GLD",
    "QUANTITY": 5227,
    "PRICE": 961.7999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 891,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-18T17:54:30|,
    "SYMBOL": "GLD",
    "QUANTITY": 3346,
    "PRICE": 834.6500244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 892,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-17T08:16:23|,
    "SYMBOL": "DIA",
    "QUANTITY": 7960,
    "PRICE": 333.0400085449219,
    "ACTION": "SELL",
    "TX_NUMBER": 893,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-27T19:51:23|,
    "SYMBOL": "SLV",
    "QUANTITY": 1392,
    "PRICE": 910.72998046875,
    "ACTION": "SELL",
    "TX_NUMBER": 894,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-08T17:22:25|,
    "SYMBOL": "DIA",
    "QUANTITY": 3716,
    "PRICE": 611.2000122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 895,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-18T18:21:04|,
    "SYMBOL": "GLD",
    "QUANTITY": 287,
    "PRICE": 208.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 896,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-12T18:59:53|,
    "SYMBOL": "GLD",
    "QUANTITY": 6154,
    "PRICE": 984.030029296875,
    "ACTION": "BUY",
    "TX_NUMBER": 897,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-22T03:12:55|,
    "SYMBOL": "DIA",
    "QUANTITY": 5401,
    "PRICE": 680.9099731445312,
    "ACTION": "SELL",
    "TX_NUMBER": 898,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-28T07:32:00|,
    "SYMBOL": "DIA",
    "QUANTITY": 3371,
    "PRICE": 960.260009765625,
    "ACTION": "SELL",
    "TX_NUMBER": 899,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-27T11:56:43|,
    "SYMBOL": "GLD",
    "QUANTITY": 9670,
    "PRICE": 633.8900146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 900,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-25T01:17:11|,
    "SYMBOL": "SLV",
    "QUANTITY": 4400,
    "PRICE": 109.52999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 901,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-02T02:52:04|,
    "SYMBOL": "SLV",
    "QUANTITY": 7903,
    "PRICE": 690.6500244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 902,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-22T15:45:59|,
    "SYMBOL": "IWM",
    "QUANTITY": 7834,
    "PRICE": 552.9600219726562,
    "ACTION": "SELL",
    "TX_NUMBER": 903,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-14T19:00:55|,
    "SYMBOL": "SLV",
    "QUANTITY": 7195,
    "PRICE": 751.6300048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 904,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-05T01:22:50|,
    "SYMBOL": "DIA",
    "QUANTITY": 3967,
    "PRICE": 625.1500244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 905,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-25T13:16:46|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1967,
    "PRICE": 294.04998779296875,
    "ACTION": "BUY",
    "TX_NUMBER": 906,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-30T12:52:24|,
    "SYMBOL": "SPY",
    "QUANTITY": 3540,
    "PRICE": 777.72998046875,
    "ACTION": "SELL",
    "TX_NUMBER": 907,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-05T14:08:43|,
    "SYMBOL": "IWM",
    "QUANTITY": 1280,
    "PRICE": 618.5900268554688,
    "ACTION": "BUY",
    "TX_NUMBER": 908,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-10T06:01:02|,
    "SYMBOL": "SLV",
    "QUANTITY": 3454,
    "PRICE": 481.0400085449219,
    "ACTION": "BUY",
    "TX_NUMBER": 909,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-14T22:55:54|,
    "SYMBOL": "SPY",
    "QUANTITY": 8405,
    "PRICE": 449.6099853515625,
    "ACTION": "BUY",
    "TX_NUMBER": 910,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-01T10:36:47|,
    "SYMBOL": "GLD",
    "QUANTITY": 361,
    "PRICE": 411.010009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 911,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-16T01:39:29|,
    "SYMBOL": "GLD",
    "QUANTITY": 1448,
    "PRICE": 852.530029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 912,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-13T01:47:31|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1317,
    "PRICE": 892.030029296875,
    "ACTION": "BUY",
    "TX_NUMBER": 913,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-17T15:19:52|,
    "SYMBOL": "SLV",
    "QUANTITY": 4028,
    "PRICE": 653.6900024414062,
    "ACTION": "SELL",
    "TX_NUMBER": 914,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-20T06:45:34|,
    "SYMBOL": "GLD",
    "QUANTITY": 9668,
    "PRICE": 573.25,
    "ACTION": "SELL",
    "TX_NUMBER": 915,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-14T07:24:09|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1467,
    "PRICE": 131.08999633789062,
    "ACTION": "SELL",
    "TX_NUMBER": 916,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-31T03:51:35|,
    "SYMBOL": "DIA",
    "QUANTITY": 1556,
    "PRICE": 306.6400146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 917,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-15T03:11:08|,
    "SYMBOL": "SPY",
    "QUANTITY": 9344,
    "PRICE": 207.8300018310547,
    "ACTION": "SELL",
    "TX_NUMBER": 918,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-31T20:03:17|,
    "SYMBOL": "DIA",
    "QUANTITY": 6899,
    "PRICE": 497.95001220703125,
    "ACTION": "BUY",
    "TX_NUMBER": 919,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-30T23:16:44|,
    "SYMBOL": "IWM",
    "QUANTITY": 6154,
    "PRICE": 993.5800170898438,
    "ACTION": "BUY",
    "TX_NUMBER": 920,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-28T14:30:09|,
    "SYMBOL": "IWM",
    "QUANTITY": 4249,
    "PRICE": 957.0599975585938,
    "ACTION": "BUY",
    "TX_NUMBER": 921,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-11T09:17:40|,
    "SYMBOL": "IWM",
    "QUANTITY": 3368,
    "PRICE": 198.52000427246094,
    "ACTION": "SELL",
    "TX_NUMBER": 922,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-23T05:16:34|,
    "SYMBOL": "IWM",
    "QUANTITY": 6742,
    "PRICE": 443.6000061035156,
    "ACTION": "SELL",
    "TX_NUMBER": 923,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-18T15:59:11|,
    "SYMBOL": "DIA",
    "QUANTITY": 752,
    "PRICE": 432.95001220703125,
    "ACTION": "SELL",
    "TX_NUMBER": 924,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-11-09T12:15:07|,
    "SYMBOL": "DIA",
    "QUANTITY": 8375,
    "PRICE": 181.52999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 925,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-28T18:56:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 8013,
    "PRICE": 982.9600219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 926,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-11T10:05:15|,
    "SYMBOL": "DIA",
    "QUANTITY": 8304,
    "PRICE": 142.75,
    "ACTION": "BUY",
    "TX_NUMBER": 927,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-25T10:15:45|,
    "SYMBOL": "IWM",
    "QUANTITY": 2382,
    "PRICE": 142.08999633789062,
    "ACTION": "BUY",
    "TX_NUMBER": 928,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-13T05:44:01|,
    "SYMBOL": "SLV",
    "QUANTITY": 5498,
    "PRICE": 238.02999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 929,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-12T12:09:50|,
    "SYMBOL": "DIA",
    "QUANTITY": 3198,
    "PRICE": 635.9600219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 930,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-12T23:13:24|,
    "SYMBOL": "SLV",
    "QUANTITY": 8776,
    "PRICE": 728.0800170898438,
    "ACTION": "BUY",
    "TX_NUMBER": 931,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-02T20:58:55|,
    "SYMBOL": "SLV",
    "QUANTITY": 1250,
    "PRICE": 578.1699829101562,
    "ACTION": "SELL",
    "TX_NUMBER": 932,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-08T12:55:03|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2014,
    "PRICE": 56.790000915527344,
    "ACTION": "BUY",
    "TX_NUMBER": 933,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-11T04:05:56|,
    "SYMBOL": "GLD",
    "QUANTITY": 2282,
    "PRICE": 478.1199951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 934,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-28T23:30:04|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5250,
    "PRICE": 575.5499877929688,
    "ACTION": "SELL",
    "TX_NUMBER": 935,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-14T02:48:37|,
    "SYMBOL": "IWM",
    "QUANTITY": 3746,
    "PRICE": 56.13999938964844,
    "ACTION": "BUY",
    "TX_NUMBER": 936,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-07T07:39:12|,
    "SYMBOL": "IWM",
    "QUANTITY": 3727,
    "PRICE": 319.6600036621094,
    "ACTION": "SELL",
    "TX_NUMBER": 937,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-20T00:00:45|,
    "SYMBOL": "GLD",
    "QUANTITY": 7962,
    "PRICE": 647.5700073242188,
    "ACTION": "BUY",
    "TX_NUMBER": 938,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-30T01:43:56|,
    "SYMBOL": "SPY",
    "QUANTITY": 9381,
    "PRICE": 568.5999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 939,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-25T11:16:19|,
    "SYMBOL": "IWM",
    "QUANTITY": 7351,
    "PRICE": 917.989990234375,
    "ACTION": "SELL",
    "TX_NUMBER": 940,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-04T21:32:45|,
    "SYMBOL": "GLD",
    "QUANTITY": 7199,
    "PRICE": 705.02001953125,
    "ACTION": "SELL",
    "TX_NUMBER": 941,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-02T12:47:55|,
    "SYMBOL": "SPY",
    "QUANTITY": 2190,
    "PRICE": 152.35000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 942,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-01T09:39:45|,
    "SYMBOL": "GLD",
    "QUANTITY": 2217,
    "PRICE": 362.5799865722656,
    "ACTION": "BUY",
    "TX_NUMBER": 943,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-11-07T17:36:18|,
    "SYMBOL": "GLD",
    "QUANTITY": 6665,
    "PRICE": 369.30999755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 944,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-01T14:32:00|,
    "SYMBOL": "SLV",
    "QUANTITY": 330,
    "PRICE": 547.27001953125,
    "ACTION": "BUY",
    "TX_NUMBER": 945,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-18T16:55:29|,
    "SYMBOL": "SPY",
    "QUANTITY": 5629,
    "PRICE": 297.75,
    "ACTION": "SELL",
    "TX_NUMBER": 946,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-30T04:32:04|,
    "SYMBOL": "SPY",
    "QUANTITY": 5732,
    "PRICE": 169.30999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 947,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-25T19:32:22|,
    "SYMBOL": "IWM",
    "QUANTITY": 3218,
    "PRICE": 588.1500244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 948,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-03T07:55:00|,
    "SYMBOL": "GLD",
    "QUANTITY": 1476,
    "PRICE": 882.989990234375,
    "ACTION": "SELL",
    "TX_NUMBER": 949,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-21T04:20:00|,
    "SYMBOL": "GLD",
    "QUANTITY": 982,
    "PRICE": 571.719970703125,
    "ACTION": "BUY",
    "TX_NUMBER": 950,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-17T12:44:04|,
    "SYMBOL": "SLV",
    "QUANTITY": 9762,
    "PRICE": 399.54998779296875,
    "ACTION": "SELL",
    "TX_NUMBER": 951,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-07T16:05:29|,
    "SYMBOL": "QQQ",
    "QUANTITY": 322,
    "PRICE": 709.5499877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 952,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-24T09:16:54|,
    "SYMBOL": "QQQ",
    "QUANTITY": 9459,
    "PRICE": 885.6199951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 953,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-16T15:33:05|,
    "SYMBOL": "GLD",
    "QUANTITY": 4363,
    "PRICE": 844.7999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 954,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-05T07:03:26|,
    "SYMBOL": "IWM",
    "QUANTITY": 3550,
    "PRICE": 442.5299987792969,
    "ACTION": "BUY",
    "TX_NUMBER": 955,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-11T06:32:31|,
    "SYMBOL": "DIA",
    "QUANTITY": 2537,
    "PRICE": 632.9199829101562,
    "ACTION": "BUY",
    "TX_NUMBER": 956,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-13T18:44:50|,
    "SYMBOL": "IWM",
    "QUANTITY": 3432,
    "PRICE": 542.22998046875,
    "ACTION": "SELL",
    "TX_NUMBER": 957,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-15T23:55:38|,
    "SYMBOL": "GLD",
    "QUANTITY": 5937,
    "PRICE": 870.4400024414062,
    "ACTION": "SELL",
    "TX_NUMBER": 958,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-23T18:21:22|,
    "SYMBOL": "SPY",
    "QUANTITY": 5022,
    "PRICE": 533.780029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 959,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-18T08:22:26|,
    "SYMBOL": "SPY",
    "QUANTITY": 7945,
    "PRICE": 621.75,
    "ACTION": "BUY",
    "TX_NUMBER": 960,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-04T04:41:22|,
    "SYMBOL": "IWM",
    "QUANTITY": 8855,
    "PRICE": 302.2099914550781,
    "ACTION": "BUY",
    "TX_NUMBER": 961,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-17T01:56:54|,
    "SYMBOL": "SPY",
    "QUANTITY": 9790,
    "PRICE": 330.6000061035156,
    "ACTION": "BUY",
    "TX_NUMBER": 962,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-11-14T04:00:50|,
    "SYMBOL": "DIA",
    "QUANTITY": 8850,
    "PRICE": 453.3900146484375,
    "ACTION": "BUY",
    "TX_NUMBER": 963,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-04T08:10:53|,
    "SYMBOL": "GLD",
    "QUANTITY": 6927,
    "PRICE": 700.8300170898438,
    "ACTION": "BUY",
    "TX_NUMBER": 964,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-31T00:26:22|,
    "SYMBOL": "SPY",
    "QUANTITY": 2332,
    "PRICE": 187.27000427246094,
    "ACTION": "SELL",
    "TX_NUMBER": 965,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-01T06:49:45|,
    "SYMBOL": "IWM",
    "QUANTITY": 4519,
    "PRICE": 523.2899780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 966,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-25T00:07:04|,
    "SYMBOL": "IWM",
    "QUANTITY": 5659,
    "PRICE": 232.22999572753906,
    "ACTION": "SELL",
    "TX_NUMBER": 967,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-12T23:29:51|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5070,
    "PRICE": 453.04998779296875,
    "ACTION": "SELL",
    "TX_NUMBER": 968,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-20T07:06:06|,
    "SYMBOL": "IWM",
    "QUANTITY": 4098,
    "PRICE": 543.6300048828125,
    "ACTION": "SELL",
    "TX_NUMBER": 969,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-14T05:37:58|,
    "SYMBOL": "GLD",
    "QUANTITY": 5199,
    "PRICE": 516.0700073242188,
    "ACTION": "BUY",
    "TX_NUMBER": 970,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-26T08:00:02|,
    "SYMBOL": "SPY",
    "QUANTITY": 1407,
    "PRICE": 500.6700134277344,
    "ACTION": "BUY",
    "TX_NUMBER": 971,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-30T13:00:44|,
    "SYMBOL": "IWM",
    "QUANTITY": 8208,
    "PRICE": 123.38999938964844,
    "ACTION": "SELL",
    "TX_NUMBER": 972,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-20T23:36:04|,
    "SYMBOL": "GLD",
    "QUANTITY": 1454,
    "PRICE": 875.47998046875,
    "ACTION": "BUY",
    "TX_NUMBER": 973,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-03T17:13:46|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4570,
    "PRICE": 473.05999755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 974,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-05-03T10:35:20|,
    "SYMBOL": "GLD",
    "QUANTITY": 3097,
    "PRICE": 333.17999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 975,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-02T03:26:34|,
    "SYMBOL": "SLV",
    "QUANTITY": 8527,
    "PRICE": 627.02001953125,
    "ACTION": "SELL",
    "TX_NUMBER": 976,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-25T20:36:11|,
    "SYMBOL": "DIA",
    "QUANTITY": 2340,
    "PRICE": 630.77001953125,
    "ACTION": "BUY",
    "TX_NUMBER": 977,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-22T11:49:37|,
    "SYMBOL": "SPY",
    "QUANTITY": 733,
    "PRICE": 461.4800109863281,
    "ACTION": "BUY",
    "TX_NUMBER": 978,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-20T14:35:56|,
    "SYMBOL": "GLD",
    "QUANTITY": 1176,
    "PRICE": 388.0799865722656,
    "ACTION": "SELL",
    "TX_NUMBER": 979,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-11T19:05:03|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2573,
    "PRICE": 79.56999969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 980,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-14T15:04:05|,
    "SYMBOL": "DIA",
    "QUANTITY": 9005,
    "PRICE": 322.8299865722656,
    "ACTION": "BUY",
    "TX_NUMBER": 981,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-19T21:22:10|,
    "SYMBOL": "DIA",
    "QUANTITY": 2279,
    "PRICE": 864.2000122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 982,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-31T16:36:32|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5753,
    "PRICE": 797.6300048828125,
    "ACTION": "SELL",
    "TX_NUMBER": 983,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-08-31T02:27:19|,
    "SYMBOL": "GLD",
    "QUANTITY": 1523,
    "PRICE": 324.9700012207031,
    "ACTION": "BUY",
    "TX_NUMBER": 984,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-02-01T07:24:40|,
    "SYMBOL": "SPY",
    "QUANTITY": 3137,
    "PRICE": 522.8099975585938,
    "ACTION": "SELL",
    "TX_NUMBER": 985,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-26T23:20:55|,
    "SYMBOL": "IWM",
    "QUANTITY": 116,
    "PRICE": 418.92999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 986,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-01-01T12:17:22|,
    "SYMBOL": "GLD",
    "QUANTITY": 3000,
    "PRICE": 899.1799926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 987,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-06T15:32:20|,
    "SYMBOL": "SPY",
    "QUANTITY": 4359,
    "PRICE": 761.760009765625,
    "ACTION": "BUY",
    "TX_NUMBER": 988,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-24T02:00:45|,
    "SYMBOL": "SLV",
    "QUANTITY": 6294,
    "PRICE": 359.1300048828125,
    "ACTION": "SELL",
    "TX_NUMBER": 989,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-04T05:11:26|,
    "SYMBOL": "SLV",
    "QUANTITY": 9456,
    "PRICE": 96.23999786376953,
    "ACTION": "BUY",
    "TX_NUMBER": 990,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-05T13:57:20|,
    "SYMBOL": "IWM",
    "QUANTITY": 3191,
    "PRICE": 492.5,
    "ACTION": "BUY",
    "TX_NUMBER": 991,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-18T02:39:37|,
    "SYMBOL": "SPY",
    "QUANTITY": 7306,
    "PRICE": 718.5700073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 992,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-12-26T12:59:07|,
    "SYMBOL": "GLD",
    "QUANTITY": 2036,
    "PRICE": 641.6199951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 993,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-13T17:38:49|,
    "SYMBOL": "SLV",
    "QUANTITY": 5739,
    "PRICE": 843.5499877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 994,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-27T10:56:28|,
    "SYMBOL": "IWM",
    "QUANTITY": 929,
    "PRICE": 693.3300170898438,
    "ACTION": "SELL",
    "TX_NUMBER": 995,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-10T20:01:43|,
    "SYMBOL": "IWM",
    "QUANTITY": 181,
    "PRICE": 666.9099731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 996,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-04T14:03:33|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6961,
    "PRICE": 466.3399963378906,
    "ACTION": "SELL",
    "TX_NUMBER": 997,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-09T01:51:18|,
    "SYMBOL": "DIA",
    "QUANTITY": 7616,
    "PRICE": 77.33999633789062,
    "ACTION": "BUY",
    "TX_NUMBER": 998,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-18T06:20:41|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5333,
    "PRICE": 907.719970703125,
    "ACTION": "BUY",
    "TX_NUMBER": 999,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-09T19:00:00|,
    "SYMBOL": "GLD",
    "QUANTITY": 628,
    "PRICE": 662.6699829101562,
    "ACTION": "SELL",
    "TX_NUMBER": 1000,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-03-15T11:04:26|,
    "SYMBOL": "SLV",
    "QUANTITY": 7351,
    "PRICE": 190.58999633789062,
    "ACTION": "SELL",
    "TX_NUMBER": 1001,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-12T03:53:39|,
    "SYMBOL": "IWM",
    "QUANTITY": 4394,
    "PRICE": 328.17999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 1002,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-04T03:00:59|,
    "SYMBOL": "SLV",
    "QUANTITY": 6429,
    "PRICE": 564.1799926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 1003,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-24T10:21:52|,
    "SYMBOL": "GLD",
    "QUANTITY": 623,
    "PRICE": 715.3200073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 1004,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-10-02T05:18:27|,
    "SYMBOL": "GLD",
    "QUANTITY": 6897,
    "PRICE": 327.9700012207031,
    "ACTION": "SELL",
    "TX_NUMBER": 1005,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-19T11:53:56|,
    "SYMBOL": "SLV",
    "QUANTITY": 2551,
    "PRICE": 655.739990234375,
    "ACTION": "SELL",
    "TX_NUMBER": 1006,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-25T11:56:48|,
    "SYMBOL": "GLD",
    "QUANTITY": 259,
    "PRICE": 566.489990234375,
    "ACTION": "SELL",
    "TX_NUMBER": 1007,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-04T23:14:16|,
    "SYMBOL": "SPY",
    "QUANTITY": 9281,
    "PRICE": 327.8299865722656,
    "ACTION": "BUY",
    "TX_NUMBER": 1008,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-22T02:48:02|,
    "SYMBOL": "SLV",
    "QUANTITY": 4314,
    "PRICE": 698.2000122070312,
    "ACTION": "SELL",
    "TX_NUMBER": 1009,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-22T07:07:57|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6651,
    "PRICE": 56.400001525878906,
    "ACTION": "SELL",
    "TX_NUMBER": 1010,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-19T05:49:28|,
    "SYMBOL": "SPY",
    "QUANTITY": 4145,
    "PRICE": 136.42999267578125,
    "ACTION": "BUY",
    "TX_NUMBER": 1011,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-01-18T02:01:27|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1298,
    "PRICE": 290.1400146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 1012,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-14T20:46:02|,
    "SYMBOL": "SPY",
    "QUANTITY": 779,
    "PRICE": 336.6000061035156,
    "ACTION": "BUY",
    "TX_NUMBER": 1013,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-04T11:33:15|,
    "SYMBOL": "SLV",
    "QUANTITY": 2032,
    "PRICE": 682.1300048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 1014,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-12-18T03:20:52|,
    "SYMBOL": "GLD",
    "QUANTITY": 9582,
    "PRICE": 609.7000122070312,
    "ACTION": "SELL",
    "TX_NUMBER": 1015,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-24T02:35:02|,
    "SYMBOL": "IWM",
    "QUANTITY": 8533,
    "PRICE": 600.530029296875,
    "ACTION": "SELL",
    "TX_NUMBER": 1016,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-09-23T03:34:55|,
    "SYMBOL": "IWM",
    "QUANTITY": 9485,
    "PRICE": 743.0399780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 1017,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-04-30T17:23:23|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5096,
    "PRICE": 441.92999267578125,
    "ACTION": "SELL",
    "TX_NUMBER": 1018,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-21T10:11:04|,
    "SYMBOL": "SLV",
    "QUANTITY": 2097,
    "PRICE": 565.9099731445312,
    "ACTION": "BUY",
    "TX_NUMBER": 1019,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-01T20:22:05|,
    "SYMBOL": "IWM",
    "QUANTITY": 1253,
    "PRICE": 376.6600036621094,
    "ACTION": "BUY",
    "TX_NUMBER": 1020,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-26T23:45:21|,
    "SYMBOL": "SPY",
    "QUANTITY": 7717,
    "PRICE": 502.760009765625,
    "ACTION": "SELL",
    "TX_NUMBER": 1021,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-21T20:19:09|,
    "SYMBOL": "SLV",
    "QUANTITY": 984,
    "PRICE": 164.94000244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 1022,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-28T16:48:28|,
    "SYMBOL": "SPY",
    "QUANTITY": 6204,
    "PRICE": 942.9299926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 1023,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-08T22:02:30|,
    "SYMBOL": "SPY",
    "QUANTITY": 6409,
    "PRICE": 821.6799926757812,
    "ACTION": "SELL",
    "TX_NUMBER": 1024,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-04-17T01:03:49|,
    "SYMBOL": "SPY",
    "QUANTITY": 4223,
    "PRICE": 644.3900146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 1025,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-02T23:19:29|,
    "SYMBOL": "IWM",
    "QUANTITY": 6684,
    "PRICE": 295.3800048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 1026,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-09T06:59:03|,
    "SYMBOL": "DIA",
    "QUANTITY": 1390,
    "PRICE": 727.9299926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 1027,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-11-02T23:33:07|,
    "SYMBOL": "GLD",
    "QUANTITY": 1044,
    "PRICE": 281.6700134277344,
    "ACTION": "BUY",
    "TX_NUMBER": 1028,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-15T00:38:36|,
    "SYMBOL": "SLV",
    "QUANTITY": 2942,
    "PRICE": 431.2699890136719,
    "ACTION": "SELL",
    "TX_NUMBER": 1029,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-07-30T04:04:24|,
    "SYMBOL": "DIA",
    "QUANTITY": 4731,
    "PRICE": 983.6400146484375,
    "ACTION": "SELL",
    "TX_NUMBER": 1030,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-12T10:16:03|,
    "SYMBOL": "SLV",
    "QUANTITY": 4918,
    "PRICE": 621.3699951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 1031,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-07-02T01:37:51|,
    "SYMBOL": "GLD",
    "QUANTITY": 5811,
    "PRICE": 706.4099731445312,
    "ACTION": "SELL",
    "TX_NUMBER": 1032,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2017-12-28T11:28:39|,
    "SYMBOL": "DIA",
    "QUANTITY": 8734,
    "PRICE": 460.1600036621094,
    "ACTION": "SELL",
    "TX_NUMBER": 1033,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-27T20:13:46|,
    "SYMBOL": "IWM",
    "QUANTITY": 4422,
    "PRICE": 200.99000549316406,
    "ACTION": "BUY",
    "TX_NUMBER": 1034,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-05T17:48:02|,
    "SYMBOL": "SPY",
    "QUANTITY": 6536,
    "PRICE": 597.8900146484375,
    "ACTION": "BUY",
    "TX_NUMBER": 1035,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-22T22:47:11|,
    "SYMBOL": "IWM",
    "QUANTITY": 1109,
    "PRICE": 607.3400268554688,
    "ACTION": "BUY",
    "TX_NUMBER": 1036,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-28T09:53:37|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7352,
    "PRICE": 666.989990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 1037,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-14T18:58:03|,
    "SYMBOL": "DIA",
    "QUANTITY": 5815,
    "PRICE": 56.09000015258789,
    "ACTION": "BUY",
    "TX_NUMBER": 1038,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-05-11T10:36:40|,
    "SYMBOL": "GLD",
    "QUANTITY": 7557,
    "PRICE": 928.0700073242188,
    "ACTION": "SELL",
    "TX_NUMBER": 1039,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-31T21:19:53|,
    "SYMBOL": "IWM",
    "QUANTITY": 4568,
    "PRICE": 91.88999938964844,
    "ACTION": "BUY",
    "TX_NUMBER": 1040,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-15T11:40:16|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6425,
    "PRICE": 342.8800048828125,
    "ACTION": "SELL",
    "TX_NUMBER": 1041,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-06-27T13:52:48|,
    "SYMBOL": "SPY",
    "QUANTITY": 784,
    "PRICE": 411.05999755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 1042,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-10-29T08:57:09|,
    "SYMBOL": "SPY",
    "QUANTITY": 4415,
    "PRICE": 480.79998779296875,
    "ACTION": "SELL",
    "TX_NUMBER": 1043,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-06-04T04:36:18|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3091,
    "PRICE": 160.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 1044,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-08T16:14:02|,
    "SYMBOL": "DIA",
    "QUANTITY": 5676,
    "PRICE": 162.02000427246094,
    "ACTION": "BUY",
    "TX_NUMBER": 1045,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-13T06:17:51|,
    "SYMBOL": "QQQ",
    "QUANTITY": 385,
    "PRICE": 247.85000610351562,
    "ACTION": "BUY",
    "TX_NUMBER": 1046,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-09-23T14:47:41|,
    "SYMBOL": "DIA",
    "QUANTITY": 4490,
    "PRICE": 462.45001220703125,
    "ACTION": "BUY",
    "TX_NUMBER": 1047,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-08-28T12:09:43|,
    "SYMBOL": "GLD",
    "QUANTITY": 8514,
    "PRICE": 712.6799926757812,
    "ACTION": "BUY",
    "TX_NUMBER": 1048,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-02-14T15:09:20|,
    "SYMBOL": "IWM",
    "QUANTITY": 7819,
    "PRICE": 658.5399780273438,
    "ACTION": "BUY",
    "TX_NUMBER": 1049,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-08-02T05:01:05|,
    "SYMBOL": "GLD",
    "QUANTITY": 490,
    "PRICE": 498.44000244140625,
    "ACTION": "BUY",
    "TX_NUMBER": 1050,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-11T15:47:40|,
    "SYMBOL": "QQQ",
    "QUANTITY": 5620,
    "PRICE": 221.6300048828125,
    "ACTION": "BUY",
    "TX_NUMBER": 1051,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-10-23T00:34:48|,
    "SYMBOL": "IWM",
    "QUANTITY": 2384,
    "PRICE": 697.0599975585938,
    "ACTION": "SELL",
    "TX_NUMBER": 1052,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-07T12:22:07|,
    "SYMBOL": "DIA",
    "QUANTITY": 4353,
    "PRICE": 263.92999267578125,
    "ACTION": "SELL",
    "TX_NUMBER": 1053,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-23T19:33:51|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2219,
    "PRICE": 822.989990234375,
    "ACTION": "SELL",
    "TX_NUMBER": 1054,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-22T20:55:31|,
    "SYMBOL": "GLD",
    "QUANTITY": 1997,
    "PRICE": 495.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 1055,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "IWM",
    "QUANTITY": 9539,
    "PRICE": 211.22000122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 1056,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-09-04T14:03:28|,
    "SYMBOL": "SLV",
    "QUANTITY": 5328,
    "PRICE": 653.8400268554688,
    "ACTION": "BUY",
    "TX_NUMBER": 1057,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-25T15:43:40|,
    "SYMBOL": "SLV",
    "QUANTITY": 7136,
    "PRICE": 949.4199829101562,
    "ACTION": "SELL",
    "TX_NUMBER": 1058,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-10T00:01:07|,
    "SYMBOL": "GLD",
    "QUANTITY": 2328,
    "PRICE": 166.02000427246094,
    "ACTION": "BUY",
    "TX_NUMBER": 1059,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-05T08:51:22|,
    "SYMBOL": "QQQ",
    "QUANTITY": 6328,
    "PRICE": 629.25,
    "ACTION": "SELL",
    "TX_NUMBER": 1060,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-06-01T23:48:56|,
    "SYMBOL": "IWM",
    "QUANTITY": 2508,
    "PRICE": 240.27999877929688,
    "ACTION": "BUY",
    "TX_NUMBER": 1061,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2018-01-11T11:14:26|,
    "SYMBOL": "QQQ",
    "QUANTITY": 7093,
    "PRICE": 182.5500030517578,
    "ACTION": "SELL",
    "TX_NUMBER": 1062,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2017-11-21T15:49:07|,
    "SYMBOL": "IWM",
    "QUANTITY": 8665,
    "PRICE": 153.0800018310547,
    "ACTION": "SELL",
    "TX_NUMBER": 1063,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-05-27T02:34:53|,
    "SYMBOL": "SLV",
    "QUANTITY": 1617,
    "PRICE": 317.54998779296875,
    "ACTION": "SELL",
    "TX_NUMBER": 1064,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-01-27T00:00:00|,
    "SYMBOL": "LHH",
    "QUANTITY": 100,
    "PRICE": 171.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1065,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-01-27T00:00:00|,
    "SYMBOL": "LHH",
    "QUANTITY": 100,
    "PRICE": 171.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1066,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-01-27T00:00:00|,
    "SYMBOL": "LHH",
    "QUANTITY": 100,
    "PRICE": 171.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1067,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-01-28T00:00:00|,
    "SYMBOL": "LHH",
    "QUANTITY": 100,
    "PRICE": 171.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1068,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-02-03T01:57:18|,
    "SYMBOL": "JJJ",
    "QUANTITY": 25,
    "PRICE": 250.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1069,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-03T01:57:48|,
    "SYMBOL": "JJJ",
    "QUANTITY": 25,
    "PRICE": 250.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1070,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-03T02:12:39|,
    "SYMBOL": "JJJ",
    "QUANTITY": 25,
    "PRICE": 250.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1071,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-03T02:14:47|,
    "SYMBOL": "JLL",
    "QUANTITY": 25,
    "PRICE": 250.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1072,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-03T23:37:04|,
    "SYMBOL": "JLL",
    "QUANTITY": 10,
    "PRICE": 99.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1073,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-03T23:38:23|,
    "SYMBOL": "JLL",
    "QUANTITY": 88,
    "PRICE": 11.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1074,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-03T23:42:26|,
    "SYMBOL": "JLL",
    "QUANTITY": 25,
    "PRICE": 250.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1075,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-03T23:42:40|,
    "SYMBOL": "JLL",
    "QUANTITY": 88,
    "PRICE": 11.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1076,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-04T00:21:44|,
    "SYMBOL": "JLL",
    "QUANTITY": 88,
    "PRICE": 11.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1077,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-04T00:24:16|,
    "SYMBOL": "JLL",
    "QUANTITY": 77,
    "PRICE": 22.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1078,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-08T02:11:42|,
    "SYMBOL": "",
    "QUANTITY": 50,
    "PRICE": 30.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1079,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-08T02:37:17|,
    "SYMBOL": "",
    "QUANTITY": 50,
    "PRICE": 30.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1080,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-08T02:39:58|,
    "SYMBOL": "",
    "QUANTITY": 50,
    "PRICE": 30.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1081,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-08T02:42:03|,
    "SYMBOL": "XYZ",
    "QUANTITY": 100,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1082,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-08T12:06:20|,
    "SYMBOL": "XYZ",
    "QUANTITY": 100,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1083,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-08T12:20:42|,
    "SYMBOL": "XYZ",
    "QUANTITY": 100,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1084,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-08T15:01:24|,
    "SYMBOL": "ABC123",
    "QUANTITY": 500,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1085,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-19T17:42:03|,
    "SYMBOL": "XYZ",
    "QUANTITY": 100,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1086,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-19T17:43:27|,
    "SYMBOL": "XYZ",
    "QUANTITY": 100,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1087,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-19T17:45:03|,
    "SYMBOL": "XYZ",
    "QUANTITY": 100,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1088,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-19T17:46:03|,
    "SYMBOL": "XYZ",
    "QUANTITY": 100,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1089,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-19T17:49:21|,
    "SYMBOL": "XYZ",
    "QUANTITY": 100,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1090,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-19T17:51:45|,
    "SYMBOL": "XYZ",
    "QUANTITY": 100,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1091,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-19T17:53:42|,
    "SYMBOL": "XYZ",
    "QUANTITY": 100,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1092,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-02-19T18:39:20|,
    "SYMBOL": "ABC123",
    "QUANTITY": 500,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1093,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T13:28:15|,
    "SYMBOL": "IND",
    "QUANTITY": 108,
    "PRICE": 1008.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1094,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T13:36:05|,
    "SYMBOL": "IND",
    "QUANTITY": 109,
    "PRICE": 1009.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1095,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T13:41:02|,
    "SYMBOL": "IND",
    "QUANTITY": 108,
    "PRICE": 123.44999694824219,
    "ACTION": "BUY",
    "TX_NUMBER": 1096,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T13:43:41|,
    "SYMBOL": "IND",
    "QUANTITY": 108,
    "PRICE": 123.44999694824219,
    "ACTION": "BUY",
    "TX_NUMBER": 1097,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T13:52:43|,
    "SYMBOL": "MAA",
    "QUANTITY": 1008,
    "PRICE": 1234.449951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 1098,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T13:55:05|,
    "SYMBOL": "MAA",
    "QUANTITY": 1008,
    "PRICE": 1234.449951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 1099,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T13:56:36|,
    "SYMBOL": "MAA",
    "QUANTITY": 1008,
    "PRICE": 1234.449951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 1100,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T13:58:18|,
    "SYMBOL": "MAA",
    "QUANTITY": 1008,
    "PRICE": 1234.449951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 1101,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T14:27:49|,
    "SYMBOL": "MAA",
    "QUANTITY": 1008,
    "PRICE": 1234.449951171875,
    "ACTION": "BUY",
    "TX_NUMBER": 1102,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T18:27:05|,
    "SYMBOL": "GOA",
    "QUANTITY": 2008,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1103,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T18:27:05|,
    "SYMBOL": "GOA",
    "QUANTITY": 2008,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1104,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T18:32:00|,
    "SYMBOL": "GOA2",
    "QUANTITY": 2008,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1105,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T00:00:00|,
    "SYMBOL": "GOA3",
    "QUANTITY": 2008,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1106,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T00:00:00|,
    "SYMBOL": "GOA4",
    "QUANTITY": 2008,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1107,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T23:00:00|,
    "SYMBOL": "GOA5",
    "QUANTITY": 2008,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1108,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T23:00:00|,
    "SYMBOL": "GOA6",
    "QUANTITY": 100,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1109,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T22:00:00|,
    "SYMBOL": "GOA7",
    "QUANTITY": 100,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1110,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T22:00:00|,
    "SYMBOL": "GOA8",
    "QUANTITY": 100,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1111,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-04T10:48:01|,
    "SYMBOL": "SPY",
    "QUANTITY": 6774,
    "PRICE": 613.7999877929688,
    "ACTION": "SELL",
    "TX_NUMBER": 1112,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T17:20:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 6774,
    "PRICE": 613.7999877929688,
    "ACTION": "SELL",
    "TX_NUMBER": 1113,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T17:38:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 200,
    "PRICE": 123.2300033569336,
    "ACTION": "SELL",
    "TX_NUMBER": 1114,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T17:38:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 200,
    "PRICE": 123.2300033569336,
    "ACTION": "SELL",
    "TX_NUMBER": 1115,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T17:40:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 456.7799987792969,
    "ACTION": "SELL",
    "TX_NUMBER": 1116,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T18:05:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 200,
    "PRICE": 123.2300033569336,
    "ACTION": "SELL",
    "TX_NUMBER": 1117,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T18:04:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 456.7799987792969,
    "ACTION": "SELL",
    "TX_NUMBER": 1118,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T18:04:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 456.7799987792969,
    "ACTION": "SELL",
    "TX_NUMBER": 1119,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T22:00:00|,
    "SYMBOL": "GOA9",
    "QUANTITY": 100,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1120,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T22:00:00|,
    "SYMBOL": "GOA10",
    "QUANTITY": 100,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1121,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T22:00:00|,
    "SYMBOL": "GOA11",
    "QUANTITY": 100,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1122,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T18:38:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1123,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T18:38:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1124,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T19:31:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1125,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T19:32:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1126,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T19:35:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1127,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T22:36:35|,
    "SYMBOL": "FJS",
    "QUANTITY": 99,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1128,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T19:35:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1129,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T19:35:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1130,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T23:02:31|,
    "SYMBOL": "FJS",
    "QUANTITY": 10,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1131,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T22:00:00|,
    "SYMBOL": "GOA13",
    "QUANTITY": 100,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1132,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T22:00:00|,
    "SYMBOL": "GOA14",
    "QUANTITY": 100,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1133,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T23:00:00|,
    "SYMBOL": "GOA15",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1134,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T23:00:00|,
    "SYMBOL": "GOA16",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "BUY",
    "TX_NUMBER": 1135,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T23:00:00|,
    "SYMBOL": "GOA17",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "BUY",
    "TX_NUMBER": 1136,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T23:00:00|,
    "SYMBOL": "GOA19",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "BUY",
    "TX_NUMBER": 1137,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T23:00:00|,
    "SYMBOL": "GOA20",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "BUY",
    "TX_NUMBER": 1138,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T23:41:15|,
    "SYMBOL": "FJS",
    "QUANTITY": 20,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1139,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T23:00:00|,
    "SYMBOL": "GOA21",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "BUY",
    "TX_NUMBER": 1140,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T23:00:00|,
    "SYMBOL": "GOA22",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "BUY",
    "TX_NUMBER": 1141,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T23:45:46|,
    "SYMBOL": "FJS",
    "QUANTITY": 30,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1142,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T23:00:00|,
    "SYMBOL": "GOA23",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1143,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T23:00:00|,
    "SYMBOL": "GOA25",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1144,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T23:00:00|,
    "SYMBOL": "GOA26",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1145,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-04-04T23:00:00|,
    "SYMBOL": "GOA27",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1146,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-04T23:00:00|,
    "SYMBOL": "GOA29",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1147,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T21:08:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1148,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T21:12:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1149,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-04T23:00:00|,
    "SYMBOL": "GOA30",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1150,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-04-04T23:00:00|,
    "SYMBOL": "GOA31",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "BUY",
    "TX_NUMBER": 1151,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T21:25:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1152,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T21:29:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1153,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T21:29:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1154,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-04T23:00:00|,
    "SYMBOL": "GOA32",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "BUY",
    "TX_NUMBER": 1155,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T01:56:31|,
    "SYMBOL": "FJS",
    "QUANTITY": 122,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1156,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T23:02:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1157,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T23:02:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1158,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T23:02:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1159,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T23:02:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1160,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T23:02:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1161,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T23:56:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1162,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T23:59:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1163,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T23:59:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1164,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T23:59:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1165,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T23:59:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1166,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-07T23:59:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1167,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T00:19:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1168,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T00:19:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1169,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T00:19:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1170,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T00:19:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1171,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T00:19:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1172,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T00:19:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1173,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T00:19:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1174,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T00:19:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1175,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T00:19:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1176,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T00:19:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1177,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T00:19:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1178,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T10:20:52|,
    "SYMBOL": "GOA34",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "BUY",
    "TX_NUMBER": 1179,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T10:24:03|,
    "SYMBOL": "GOA35",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "BUY",
    "TX_NUMBER": 1180,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T07:31:30|,
    "SYMBOL": "GOA36",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "BUY",
    "TX_NUMBER": 1181,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T18:54:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1182,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T18:57:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1183,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T19:05:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1184,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T19:09:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1185,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T19:11:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1186,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T19:26:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1187,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T19:37:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1188,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T19:46:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1189,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T19:48:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1190,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T19:51:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1191,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T19:53:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1192,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T19:56:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1193,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:01:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1194,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:09:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1195,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:12:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1196,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:14:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1197,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:17:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1198,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:19:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1199,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:23:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1200,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:28:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1201,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:32:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1202,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:33:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1203,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:44:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1204,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:46:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1205,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:48:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1206,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:50:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1207,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T20:51:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "BUY",
    "TX_NUMBER": 1208,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T21:17:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "BUY",
    "TX_NUMBER": 1209,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T22:22:20|,
    "SYMBOL": "GOA31",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1210,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-04-08T22:22:40|,
    "SYMBOL": "GOA36",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "BUY",
    "TX_NUMBER": 1211,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T22:29:47|,
    "SYMBOL": "GOA37",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1212,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-09T01:30:06|,
    "SYMBOL": "FJS",
    "QUANTITY": 2019,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1213,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-08T22:32:36|,
    "SYMBOL": "GOA31",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1214,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-04-09T06:52:53|,
    "SYMBOL": "GOA37",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1215,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-09T07:00:10|,
    "SYMBOL": "GOA40",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1216,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-09T07:00:52|,
    "SYMBOL": "GOA37",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "SELL",
    "TX_NUMBER": 1217,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-09T07:01:11|,
    "SYMBOL": "GOA37",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "BUY",
    "TX_NUMBER": 1218,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-09T18:51:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "BUY",
    "TX_NUMBER": 1219,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-09T21:17:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "BUY",
    "TX_NUMBER": 1220,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-09T21:17:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "BUY",
    "TX_NUMBER": 1221,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-09T00:00:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "BUY",
    "TX_NUMBER": 1222,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-09T22:49:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "BUY",
    "TX_NUMBER": 1223,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-11T07:04:20|,
    "SYMBOL": "GOA37",
    "QUANTITY": 101,
    "PRICE": 12345.5,
    "ACTION": "BUY",
    "TX_NUMBER": 1224,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-11T17:27:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1225,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-11T17:27:00|,
    "SYMBOL": "ARS",
    "QUANTITY": 123,
    "PRICE": 4567.89013671875,
    "ACTION": "SELL",
    "TX_NUMBER": 1226,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-30T16:38:26|,
    "SYMBOL": "SLV",
    "QUANTITY": 2,
    "PRICE": 8504.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1227,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-30T17:00:28|,
    "SYMBOL": "SLV",
    "QUANTITY": 3,
    "PRICE": 8504.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1228,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-30T18:09:56|,
    "SYMBOL": "SLV",
    "QUANTITY": 3,
    "PRICE": 8504.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1229,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-04-30T20:14:38|,
    "SYMBOL": "SLV",
    "QUANTITY": 3,
    "PRICE": 8504.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1230,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-05-01T10:51:26|,
    "SYMBOL": "SLV",
    "QUANTITY": 3,
    "PRICE": 8504.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1231,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-05-01T13:33:41|,
    "SYMBOL": "SLV",
    "QUANTITY": 3,
    "PRICE": 8504.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1232,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-05-01T18:50:48|,
    "SYMBOL": "SLV",
    "QUANTITY": 3,
    "PRICE": 8505.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1233,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-05-02T01:57:44|,
    "SYMBOL": "SLV",
    "QUANTITY": 2,
    "PRICE": 1985.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1234,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-05-02T09:43:05|,
    "SYMBOL": "SLV",
    "QUANTITY": 3,
    "PRICE": 4000.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1235,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-05-02T09:46:52|,
    "SYMBOL": "USD",
    "QUANTITY": 2,
    "PRICE": 5000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1236,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-05-08T15:42:41|,
    "SYMBOL": "COP",
    "QUANTITY": 4,
    "PRICE": 1985.0,
    "ACTION": "SELL",
    "TX_NUMBER": 1237,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-05-08T16:35:42|,
    "SYMBOL": "SLV",
    "QUANTITY": 3,
    "PRICE": 5000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 1238,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 1.2999999523162842,
    "ACTION": "BUY",
    "TX_NUMBER": 10000,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-02T22:54:59|,
    "SYMBOL": "QQQ",
    "QUANTITY": 20,
    "PRICE": 189.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10001,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-02T22:56:55|,
    "SYMBOL": "GGAL",
    "QUANTITY": 20,
    "PRICE": 189.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10002,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-06-04T01:45:27|,
    "SYMBOL": "GGAL",
    "QUANTITY": 20,
    "PRICE": 189.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10003,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-04T02:12:45|,
    "SYMBOL": "GGAL",
    "QUANTITY": 20,
    "PRICE": 189.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10004,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-11T20:21:51|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10005,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-12T09:12:58|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10006,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-12T09:17:48|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10007,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-12T09:21:12|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10008,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-12T09:29:57|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10009,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-06-12T09:36:39|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10010,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-06-12T09:38:44|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10011,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-06-12T09:41:19|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10012,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-06-12T11:06:19|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10013,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-06-12T11:17:29|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10014,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-06-12T11:28:25|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10015,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-12T11:29:43|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10016,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-06-12T11:43:19|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10017,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-06-12T12:20:30|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10018,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-06-12T18:09:47|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10019,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-12T18:10:17|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10020,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-06-12T18:11:01|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10021,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-06-12T20:16:29|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10022,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-12T20:18:18|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10023,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-12T20:18:31|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10024,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-06-12T21:27:32|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10025,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-12T21:30:48|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10026,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-12T21:33:20|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10027,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-12T21:36:09|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10028,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-12T21:36:39|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10029,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-12T21:37:01|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10030,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-06-12T21:44:16|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10031,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-12T21:44:19|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10032,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-06-13T00:48:23|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10033,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-13T01:09:29|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10034,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-13T16:04:01|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10035,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-06-18T02:31:00|,
    "SYMBOL": "MAU",
    "QUANTITY": 1111,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10036,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-18T02:31:00|,
    "SYMBOL": "MAU",
    "QUANTITY": 1111,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10037,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-18T03:31:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 2345,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10038,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-08T18:40:50|,
    "SYMBOL": "DIA",
    "QUANTITY": 505,
    "PRICE": 107.8499984741211,
    "ACTION": "BUY",
    "TX_NUMBER": 10039,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-02-09T18:40:50|,
    "SYMBOL": "DIA",
    "QUANTITY": 505,
    "PRICE": 107.8499984741211,
    "ACTION": "BUY",
    "TX_NUMBER": 10040,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-24T11:14:39|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10041,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-24T15:29:38|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10042,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-06-24T15:31:08|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10043,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-08-18T16:50:21|,
    "SYMBOL": "TST",
    "QUANTITY": 99,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10044,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-08-20T00:57:49|,
    "SYMBOL": "TST",
    "QUANTITY": 15,
    "PRICE": 99.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10045,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-08-21T15:17:22|,
    "SYMBOL": "TST",
    "QUANTITY": 99,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10046,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-08-21T15:19:05|,
    "SYMBOL": "TST",
    "QUANTITY": 15,
    "PRICE": 99.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10047,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-08-22T23:10:07|,
    "SYMBOL": "TST",
    "QUANTITY": 100,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10048,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 2,
    "PRICE": 1.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10049,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 2,
    "PRICE": 1.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10050,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10051,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10052,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10053,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10054,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10055,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10056,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10057,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10058,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10059,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10060,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10061,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10062,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10063,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10064,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10065,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10066,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10067,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10068,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10069,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10070,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10071,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10072,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10073,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10074,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 4,
    "PRICE": 2.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10075,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 5,
    "PRICE": 3.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10076,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 5,
    "PRICE": 3.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10077,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-09-16T09:44:45|,
    "SYMBOL": "SLV",
    "QUANTITY": 6388,
    "PRICE": 723.0599975585938,
    "ACTION": "BUY",
    "TX_NUMBER": 10078,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-09-16T09:44:45|,
    "SYMBOL": "FPP",
    "QUANTITY": 6388,
    "PRICE": 723.0599975585938,
    "ACTION": "BUY",
    "TX_NUMBER": 10079,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-09-18T19:17:10|,
    "SYMBOL": "AXL",
    "QUANTITY": 123,
    "PRICE": 123.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10080,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-09-18T19:33:46|,
    "SYMBOL": "AXL",
    "QUANTITY": 234,
    "PRICE": 234.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10081,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-09-18T19:39:20|,
    "SYMBOL": "AXL",
    "QUANTITY": 345,
    "PRICE": 345.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10082,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-09-18T20:18:58|,
    "SYMBOL": "AXL",
    "QUANTITY": 456,
    "PRICE": 456.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10083,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2016-02-28T16:41:41|,
    "SYMBOL": "JAP",
    "QUANTITY": 5,
    "PRICE": 3.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10084,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-09-26T03:43:36|,
    "SYMBOL": "AXL",
    "QUANTITY": 567,
    "PRICE": 567.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10085,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T11:08:55|,
    "SYMBOL": "PEP",
    "QUANTITY": 10,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10086,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-10-05T11:08:55|,
    "SYMBOL": "PEP",
    "QUANTITY": 10,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10087,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-10-05T11:08:55|,
    "SYMBOL": "PEP",
    "QUANTITY": 10,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10088,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-10-05T17:48:55|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1,
    "PRICE": 10.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10089,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T17:49:33|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1,
    "PRICE": 10.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10090,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T18:17:18|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1,
    "PRICE": 10.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10091,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T18:19:57|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 100.23999786376953,
    "ACTION": "BUY",
    "TX_NUMBER": 10092,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T19:36:50|,
    "SYMBOL": "SPY",
    "QUANTITY": 11,
    "PRICE": 100.23999786376953,
    "ACTION": "BUY",
    "TX_NUMBER": 10093,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T19:40:57|,
    "SYMBOL": "QQQ",
    "QUANTITY": 67,
    "PRICE": 123.44999694824219,
    "ACTION": "BUY",
    "TX_NUMBER": 10094,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-10-05T19:46:16|,
    "SYMBOL": "QQQ",
    "QUANTITY": 987,
    "PRICE": 122.44999694824219,
    "ACTION": "BUY",
    "TX_NUMBER": 10095,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-10-05T20:05:48|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10096,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T21:29:58|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10097,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T21:33:04|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10098,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T21:37:18|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10099,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T21:42:47|,
    "SYMBOL": "QQQ",
    "QUANTITY": 3,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10100,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T21:57:25|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10101,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T22:26:19|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10102,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T22:28:26|,
    "SYMBOL": "QQQ",
    "QUANTITY": 4,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10103,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T22:29:27|,
    "SYMBOL": "QQQ",
    "QUANTITY": 55,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10104,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T13:39:02|,
    "SYMBOL": "QQQ",
    "QUANTITY": 55,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10105,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T13:39:16|,
    "SYMBOL": "QQQ",
    "QUANTITY": 55,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10106,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-05T11:30:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10107,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-10-06T11:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10108,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T11:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10109,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-10-06T15:58:16|,
    "SYMBOL": "QQQ",
    "QUANTITY": 55,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10110,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T17:26:17|,
    "SYMBOL": "QQQ",
    "QUANTITY": 55,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10111,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T17:26:31|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10112,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T18:00:36|,
    "SYMBOL": "QQQ",
    "QUANTITY": 55,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10113,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T18:07:16|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10114,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T18:24:16|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10115,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T18:56:28|,
    "SYMBOL": "QQQ",
    "QUANTITY": 1,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10116,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T11:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10117,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T11:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10118,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T11:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10119,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T11:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10120,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T17:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10121,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T17:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10122,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-06T17:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10123,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-06T17:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10124,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-07T01:46:22|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10125,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-07T10:42:05|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10126,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-10-07T10:42:05|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10127,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-10-07T10:42:05|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10128,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-07T10:42:05|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10129,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-07T17:41:35|,
    "SYMBOL": "FPP",
    "QUANTITY": 6388,
    "PRICE": 723.0599975585938,
    "ACTION": "BUY",
    "TX_NUMBER": 10130,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-07T17:42:11|,
    "SYMBOL": "CPP-10",
    "QUANTITY": 6388,
    "PRICE": 723.0599975585938,
    "ACTION": "BUY",
    "TX_NUMBER": 10131,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-06T17:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10132,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-06T17:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10133,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-06T17:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10134,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-06T17:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10135,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-06T17:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10136,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-07T19:30:00|,
    "SYMBOL": "LP",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10137,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-07T23:22:37|,
    "SYMBOL": "QQQ",
    "QUANTITY": 2,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10138,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-08T01:12:37|,
    "SYMBOL": "RDD",
    "QUANTITY": 1,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10139,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-08T01:21:51|,
    "SYMBOL": "RDD",
    "QUANTITY": 1,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10140,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-08T01:23:07|,
    "SYMBOL": "RDD",
    "QUANTITY": 487,
    "PRICE": 1.3000000035390258E-4,
    "ACTION": "SELL",
    "TX_NUMBER": 10141,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-08T01:34:05|,
    "SYMBOL": "RDD-1",
    "QUANTITY": 487000,
    "PRICE": 1.1999999696854502E-4,
    "ACTION": "SELL",
    "TX_NUMBER": 10142,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-08T01:39:09|,
    "SYMBOL": "RDD-1",
    "QUANTITY": 487000,
    "PRICE": 1.1999999696854502E-4,
    "ACTION": "SELL",
    "TX_NUMBER": 10143,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-08T01:46:04|,
    "SYMBOL": "GLD",
    "QUANTITY": 1,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10144,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-08T10:59:29|,
    "SYMBOL": "RDD-1",
    "QUANTITY": 487000,
    "PRICE": 1.1999999696854502E-4,
    "ACTION": "SELL",
    "TX_NUMBER": 10148,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-08T13:52:05|,
    "SYMBOL": "CPP-10",
    "QUANTITY": 6388,
    "PRICE": 723.0599975585938,
    "ACTION": "BUY",
    "TX_NUMBER": 10150,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-08T13:52:34|,
    "SYMBOL": "CPP-11",
    "QUANTITY": 6388,
    "PRICE": 723.0599975585938,
    "ACTION": "SELL",
    "TX_NUMBER": 10151,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-08T13:53:47|,
    "SYMBOL": "CPP-12",
    "QUANTITY": 100,
    "PRICE": 12.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10152,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-15T00:34:57|,
    "SYMBOL": "RDD-1",
    "QUANTITY": 487000,
    "PRICE": 1.1999999696854502E-4,
    "ACTION": "SELL",
    "TX_NUMBER": 10154,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-30T00:00:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 250.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10155,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-30T00:00:00|,
    "SYMBOL": "SOME",
    "QUANTITY": 50,
    "PRICE": 250.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10156,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-30T00:00:00|,
    "SYMBOL": "SOME2",
    "QUANTITY": 2,
    "PRICE": 2.5,
    "ACTION": "SELL",
    "TX_NUMBER": 10157,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-30T00:00:00|,
    "SYMBOL": "SOME3",
    "QUANTITY": 2,
    "PRICE": 2.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10158,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-30T00:00:00|,
    "SYMBOL": "SOME3",
    "QUANTITY": 2,
    "PRICE": 2.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10159,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-31T00:00:00|,
    "SYMBOL": "SOME3",
    "QUANTITY": 2,
    "PRICE": 2.5,
    "ACTION": "SELL",
    "TX_NUMBER": 10160,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-31T00:00:00|,
    "SYMBOL": "SOME3",
    "QUANTITY": 2,
    "PRICE": 2.5,
    "ACTION": "SELL",
    "TX_NUMBER": 10161,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-31T00:00:00|,
    "SYMBOL": "SOME3",
    "QUANTITY": 2,
    "PRICE": 2.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10162,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-10-31T00:00:00|,
    "SYMBOL": "SOME3",
    "QUANTITY": 2,
    "PRICE": 2.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10163,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 500,
    "PRICE": 205.2100067138672,
    "ACTION": "SELL",
    "TX_NUMBER": 10164,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2015-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10165,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-11-09T21:19:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10166,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10167,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-11-10T13:01:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10168,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-11-10T13:10:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10169,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-11-10T13:11:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10170,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-11-10T13:11:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 200.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10171,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-11-10T13:58:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 200.1999969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 10172,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-11-10T14:15:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 200.1999969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 10173,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-11-10T14:18:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 200.1999969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 10174,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-11-10T14:18:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 200.1999969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 10175,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-11-10T14:28:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 200.1999969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 10176,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-11-10T14:28:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 200.1999969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 10177,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-11-10T14:46:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 200.1999969482422,
    "ACTION": "BUY",
    "TX_NUMBER": 10178,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10179,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10180,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10181,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10182,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10183,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10184,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10185,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10186,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10187,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10188,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10189,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10190,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10191,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10192,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10193,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10194,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10195,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10196,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10197,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10198,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10199,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10200,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10201,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10202,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10203,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10204,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10205,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10206,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10207,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10208,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10210,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10211,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10212,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T06:45:34|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10213,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-11-20T06:45:34|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10214,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-12-01T18:22:19|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10217,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-12-01T18:29:21|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10218,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-12-01T19:34:47|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10219,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-01T19:52:11|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10220,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-01T19:52:18|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10221,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-01T20:03:29|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10222,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-12-01T20:08:55|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10223,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-12-01T20:20:30|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10225,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-12-01T20:29:37|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10226,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-12-01T20:31:55|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10227,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-12-01T20:33:49|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10228,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-12-01T20:35:51|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10229,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-12-01T22:29:02|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10230,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2019-12-01T22:29:27|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10231,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-01T22:55:20|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10232,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-01T23:36:27|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10233,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-01T23:53:28|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10234,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-01T23:53:43|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10235,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-01T23:55:20|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10236,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-01T23:56:08|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10237,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-01T23:56:48|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10238,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-01T23:58:41|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10239,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T00:00:04|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10240,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T00:02:33|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10241,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T00:04:20|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10242,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T00:07:44|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10243,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T00:09:51|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10244,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T00:11:45|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10245,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T00:13:52|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10246,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T00:14:55|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10247,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T00:18:15|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10248,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T00:19:28|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10249,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T00:20:23|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10250,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T00:21:47|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10251,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T00:22:29|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10252,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T00:23:29|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10253,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T14:51:06|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10254,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T15:05:51|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10255,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10256,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T15:19:48|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10257,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T16:15:49|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10258,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T16:15:54|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10259,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T16:29:07|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10260,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T16:34:44|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10261,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T16:34:55|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10262,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T16:35:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10263,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T16:38:41|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10264,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T16:39:53|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10265,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T16:49:44|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10266,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T16:51:30|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10267,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-02T17:24:51|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10268,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T17:24:59|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10269,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T17:25:04|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10270,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T17:25:12|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10271,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T17:32:03|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10272,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T17:32:47|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10273,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T17:33:01|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10274,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T17:33:05|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10275,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T17:50:19|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10276,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T17:51:23|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10277,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T17:56:51|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10278,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T18:03:24|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10279,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T18:03:50|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10280,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T18:03:50|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10281,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T18:03:50|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10282,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T18:03:50|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10283,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T18:03:50|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10284,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T18:03:59|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10285,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T18:03:57|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10286,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T18:03:55|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10287,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T18:03:53|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10288,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T18:04:01|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10289,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-02T18:59:33|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10290,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T09:02:52|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10291,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T09:05:29|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10293,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T09:09:40|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10294,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T09:26:13|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10297,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T09:28:32|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10298,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T09:34:11|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10299,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T09:41:23|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10300,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T09:43:41|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10301,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T09:53:25|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10302,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T10:06:08|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10303,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T10:45:42|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10304,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:31:07|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10305,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:31:07|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10306,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:31:07|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10307,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:31:07|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10308,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:31:07|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10309,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:31:18|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10310,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:31:16|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10311,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:31:13|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10312,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:31:12|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10313,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:31:21|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10314,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:33:05|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10315,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:33:05|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10316,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:33:05|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10317,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:33:05|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10318,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:33:05|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10319,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:33:17|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10320,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:33:15|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10321,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:33:13|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10322,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:33:11|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10323,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:33:21|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10324,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:35:14|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10325,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:35:14|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10326,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:35:14|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10327,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:35:14|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10328,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:35:14|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10329,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:35:26|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10330,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:35:23|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10331,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:35:21|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10332,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:35:19|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10333,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:35:28|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10334,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:39:37|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10335,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:39:56|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10336,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:41:25|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10337,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:42:53|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10338,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:44:31|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10339,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:46:46|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10340,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:49:54|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10341,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:53:11|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10342,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:54:34|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10343,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:56:54|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10344,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:56:54|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10345,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:56:59|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10346,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:57:01|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10347,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:57:03|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10348,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:57:06|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10349,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:57:08|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10350,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:57:11|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10351,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:57:13|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10352,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:59:08|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10353,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:59:08|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10354,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:59:13|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10355,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:59:16|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10356,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:59:18|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10357,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:59:21|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10358,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:59:23|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10359,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:59:27|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10360,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T12:59:29|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10361,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:01:15|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10362,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:01:15|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10363,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:01:20|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10364,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:01:22|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10365,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:01:24|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10366,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:01:27|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10367,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:01:29|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10368,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:01:32|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10369,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:01:34|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10370,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:02:32|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10371,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:02:32|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10372,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:02:32|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10373,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:02:39|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10374,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:02:36|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10375,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:02:43|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10376,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:02:41|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10377,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:04:56|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10378,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:04:57|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10379,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:04:56|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10380,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:05:03|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10381,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:05:01|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10382,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:05:09|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10383,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:05:07|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10384,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:05:15|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10385,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:05:12|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10386,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:05:20|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10387,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:05:18|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10388,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:14:23|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10389,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:14:23|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10390,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:14:23|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10391,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:14:30|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10392,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:14:27|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10393,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:14:34|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10394,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:14:32|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10395,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:14:39|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10396,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:14:37|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10397,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:14:45|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10398,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:14:41|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10399,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:14:46|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10400,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T13:49:23|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10401,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T14:08:23|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10402,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T14:10:19|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.209999084472656,
    "ACTION": "SELL",
    "TX_NUMBER": 10403,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T14:12:43|,
    "SYMBOL": "SPY1",
    "QUANTITY": 10,
    "PRICE": 20.21179962158203,
    "ACTION": "SELL",
    "TX_NUMBER": 10404,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-03T14:13:22|,
    "SYMBOL": "SPY1",
    "QUANTITY": 150,
    "PRICE": 20.21179962158203,
    "ACTION": "SELL",
    "TX_NUMBER": 10405,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-04T13:01:36|,
    "SYMBOL": "CPP-13",
    "QUANTITY": 100,
    "PRICE": 12.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10406,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2019-12-04T13:06:32|,
    "SYMBOL": "CPP-14",
    "QUANTITY": 100,
    "PRICE": 12.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10407,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-05T00:09:12|,
    "SYMBOL": "SPY1",
    "QUANTITY": 150,
    "PRICE": 20.21179962158203,
    "ACTION": "SELL",
    "TX_NUMBER": 10408,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-05T00:09:24|,
    "SYMBOL": "SP'Y1",
    "QUANTITY": 150,
    "PRICE": 20.21179962158203,
    "ACTION": "SELL",
    "TX_NUMBER": 10409,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-05T00:09:52|,
    "SYMBOL": "SP'\"Y1",
    "QUANTITY": 150,
    "PRICE": 20.21179962158203,
    "ACTION": "SELL",
    "TX_NUMBER": 10410,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-05T00:10:12|,
    "SYMBOL": "SP'\"Y1 --",
    "QUANTITY": 150,
    "PRICE": 20.21179962158203,
    "ACTION": "SELL",
    "TX_NUMBER": 10411,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-05T00:11:19|,
    "SYMBOL": "/*SP'\"Y1 --",
    "QUANTITY": 150,
    "PRICE": 20.21179962158203,
    "ACTION": "SELL",
    "TX_NUMBER": 10412,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-05T23:02:04|,
    "SYMBOL": "QQQ",
    "QUANTITY": 150,
    "PRICE": 20.21179962158203,
    "ACTION": "SELL",
    "TX_NUMBER": 10413,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-06T11:01:59|,
    "SYMBOL": "QQQ",
    "QUANTITY": 150,
    "PRICE": 20.21179962158203,
    "ACTION": "SELL",
    "TX_NUMBER": 10414,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-20T12:23:00|,
    "SYMBOL": "CCC",
    "QUANTITY": 10,
    "PRICE": 100.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10415,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-20T12:28:10|,
    "SYMBOL": "CCR",
    "QUANTITY": 100,
    "PRICE": 30.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10416,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-20T12:36:11|,
    "SYMBOL": "RRR",
    "QUANTITY": 100,
    "PRICE": 30.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10417,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-20T12:37:45|,
    "SYMBOL": "SSS",
    "QUANTITY": 100,
    "PRICE": 320.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10418,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-20T12:47:32|,
    "SYMBOL": "TEST",
    "QUANTITY": 1,
    "PRICE": 10.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10419,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-20T12:56:21|,
    "SYMBOL": "TEST",
    "QUANTITY": 1,
    "PRICE": 10.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10420,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-20T17:36:14|,
    "SYMBOL": "APP",
    "QUANTITY": 10,
    "PRICE": 3210.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10421,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-20T18:39:15|,
    "SYMBOL": "SWT",
    "QUANTITY": 10,
    "PRICE": 3210.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10422,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-20T18:49:50|,
    "SYMBOL": "SWT",
    "QUANTITY": 10,
    "PRICE": 3210.5,
    "ACTION": "SELL",
    "TX_NUMBER": 10423,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-20T18:53:05|,
    "SYMBOL": "PPP",
    "QUANTITY": 100,
    "PRICE": 5.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10424,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-21T03:23:22|,
    "SYMBOL": "AAA",
    "QUANTITY": 1,
    "PRICE": 5.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10425,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-21T04:18:19|,
    "SYMBOL": "BBBBB",
    "QUANTITY": 5,
    "PRICE": 1.5,
    "ACTION": "SELL",
    "TX_NUMBER": 10426,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-21T04:28:17|,
    "SYMBOL": "CCCCC",
    "QUANTITY": 5,
    "PRICE": 1.5,
    "ACTION": "SELL",
    "TX_NUMBER": 10427,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-21T04:31:44|,
    "SYMBOL": "DDD",
    "QUANTITY": 5,
    "PRICE": 1.5,
    "ACTION": "SELL",
    "TX_NUMBER": 10428,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-21T04:40:27|,
    "SYMBOL": "FFF",
    "QUANTITY": 5,
    "PRICE": 1.5,
    "ACTION": "SELL",
    "TX_NUMBER": 10429,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-21T04:44:41|,
    "SYMBOL": "GGG",
    "QUANTITY": 10,
    "PRICE": 11.5,
    "ACTION": "SELL",
    "TX_NUMBER": 10430,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-22T20:32:47|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10431,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-22T21:31:56|,
    "SYMBOL": "POK",
    "QUANTITY": 40,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10432,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-22T21:34:01|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10433,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-22T21:42:58|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10434,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-27T13:45:58|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10435,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-12-27T14:12:13|,
    "SYMBOL": "ABC",
    "QUANTITY": 100,
    "PRICE": 10.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10436,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-02-24T01:02:18|,
    "SYMBOL": "SLV",
    "QUANTITY": 80,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10437,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-02-24T21:13:07|,
    "SYMBOL": "SPY",
    "QUANTITY": 5,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10438,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-02-24T21:15:54|,
    "SYMBOL": "SPY",
    "QUANTITY": 5,
    "PRICE": -205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10439,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-02-24T21:16:05|,
    "SYMBOL": "SPY",
    "QUANTITY": -5,
    "PRICE": -205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10440,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-02-24T21:39:35|,
    "SYMBOL": "SPY",
    "QUANTITY": 5,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10441,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-02-24T21:39:58|,
    "SYMBOL": "SPY",
    "QUANTITY": 0,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10442,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-02-24T21:40:17|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10443,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-02-24T23:56:15|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10444,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-02-24T23:57:45|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10445,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-02-25T00:28:47|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10446,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-01T20:48:04|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 555.2100219726562,
    "ACTION": "BUY",
    "TX_NUMBER": 10447,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-14T00:00:00|,
    "SYMBOL": "JUL",
    "QUANTITY": 60,
    "PRICE": 34.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10448,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-14T18:11:58|,
    "SYMBOL": "JUL",
    "QUANTITY": 61,
    "PRICE": 34.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10449,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-14T18:33:49|,
    "SYMBOL": "JUL",
    "QUANTITY": 62,
    "PRICE": 34.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10450,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-14T18:55:09|,
    "SYMBOL": "JUL",
    "QUANTITY": 62,
    "PRICE": 34.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10451,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-14T19:00:07|,
    "SYMBOL": "JUL",
    "QUANTITY": 62,
    "PRICE": 34.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10452,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-14T19:04:25|,
    "SYMBOL": "JUL",
    "QUANTITY": 62,
    "PRICE": 34.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10453,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-14T19:08:16|,
    "SYMBOL": "JUL",
    "QUANTITY": 62,
    "PRICE": 34.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10454,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-14T20:24:19|,
    "SYMBOL": "JUL",
    "QUANTITY": 63,
    "PRICE": 50.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10455,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-14T20:27:47|,
    "SYMBOL": "JUL",
    "QUANTITY": 64,
    "PRICE": 50.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10456,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-14T20:31:19|,
    "SYMBOL": "JUL",
    "QUANTITY": 65,
    "PRICE": 50.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10457,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-15T18:38:24|,
    "SYMBOL": "CRM",
    "QUANTITY": 100,
    "PRICE": 99.20999908447266,
    "ACTION": "BUY",
    "TX_NUMBER": 10458,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-15T18:43:53|,
    "SYMBOL": "CRM",
    "QUANTITY": 100,
    "PRICE": 99.20999908447266,
    "ACTION": "BUY",
    "TX_NUMBER": 10459,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-16T22:21:34|,
    "SYMBOL": "CRM",
    "QUANTITY": 100,
    "PRICE": 99.20999908447266,
    "ACTION": "BUY",
    "TX_NUMBER": 10460,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-16T19:27:30|,
    "SYMBOL": "CRM",
    "QUANTITY": 100,
    "PRICE": 99.20999908447266,
    "ACTION": "BUY",
    "TX_NUMBER": 10461,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-16T19:27:36|,
    "SYMBOL": "CRM",
    "QUANTITY": 100,
    "PRICE": 99.20999908447266,
    "ACTION": "BUY",
    "TX_NUMBER": 10462,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-16T23:58:06|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 99.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10463,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2020-04-10T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 100,
    "PRICE": 1.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10464,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-10T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 100,
    "PRICE": 1.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10465,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-10T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 100,
    "PRICE": 1.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10466,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-10T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 100,
    "PRICE": 1.5,
    "ACTION": "SELL",
    "TX_NUMBER": 10467,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-10T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 2,
    "PRICE": 50.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10468,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-10T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 2,
    "PRICE": 50.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10469,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-10T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 2,
    "PRICE": 50.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10470,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-01-10T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 2,
    "PRICE": 50.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10471,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-08T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 2,
    "PRICE": 50.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10472,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-08T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 2,
    "PRICE": 70.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10473,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-08T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 2,
    "PRICE": 70.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10474,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-08T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 2,
    "PRICE": 60.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10475,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-08T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 2,
    "PRICE": 60.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10476,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-08T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 2,
    "PRICE": 60.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10477,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-08T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 75,
    "PRICE": 20.5,
    "ACTION": "SELL",
    "TX_NUMBER": 10478,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-08T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 2,
    "PRICE": 60.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10479,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-08T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 2,
    "PRICE": 60.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10480,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-08T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 2,
    "PRICE": 60.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10481,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-04-08T22:38:42|,
    "SYMBOL": "FIT",
    "QUANTITY": 2,
    "PRICE": 60.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10482,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-05-10T06:45:34|,
    "SYMBOL": "TEST",
    "QUANTITY": 1,
    "PRICE": 1.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10483,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-05-10T06:45:34|,
    "SYMBOL": "TEST",
    "QUANTITY": 1,
    "PRICE": 1.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10484,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-05-10T22:53:02|,
    "SYMBOL": "TEST",
    "QUANTITY": 1,
    "PRICE": 1.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10485,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-05-11T11:16:13|,
    "SYMBOL": "TEST",
    "QUANTITY": 1,
    "PRICE": 1.1200000047683716,
    "ACTION": "BUY",
    "TX_NUMBER": 10486,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-05-11T11:47:48|,
    "SYMBOL": "TEST",
    "QUANTITY": 1,
    "PRICE": 1.1222000122070312,
    "ACTION": "BUY",
    "TX_NUMBER": 10487,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-05-11T11:47:53|,
    "SYMBOL": "TEST",
    "QUANTITY": 1,
    "PRICE": 1.1222200393676758,
    "ACTION": "BUY",
    "TX_NUMBER": 10488,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-05-11T16:36:25|,
    "SYMBOL": "TEST",
    "QUANTITY": 1,
    "PRICE": 1.1222200393676758,
    "ACTION": "BUY",
    "TX_NUMBER": 10489,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-05-11T17:01:58|,
    "SYMBOL": "TEST",
    "QUANTITY": 1,
    "PRICE": 1.1222200393676758,
    "ACTION": "BUY",
    "TX_NUMBER": 10490,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-05-29T01:27:44|,
    "SYMBOL": "TEST",
    "QUANTITY": 1,
    "PRICE": 1.1222200393676758,
    "ACTION": "BUY",
    "TX_NUMBER": 10491,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-05-29T01:27:51|,
    "SYMBOL": "TEST",
    "QUANTITY": 1,
    "PRICE": 1.1222200393676758,
    "ACTION": "BUY",
    "TX_NUMBER": 10492,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-07-04T13:48:01|,
    "SYMBOL": "SPY",
    "QUANTITY": 6774,
    "PRICE": 613.7999877929688,
    "ACTION": "SELL",
    "TX_NUMBER": 10493,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:31|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 50.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10494,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 50,
    "PRICE": 50.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10495,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 50,
    "PRICE": 50.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10496,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 50,
    "PRICE": 50.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10497,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 23,
    "PRICE": 232.22999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10498,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T00:00:00|,
    "SYMBOL": "CHR",
    "QUANTITY": 23,
    "PRICE": 232.22999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10499,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 23,
    "PRICE": 232.22999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10500,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 23,
    "PRICE": 232.22999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10501,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 23,
    "PRICE": 232.22999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10502,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 23,
    "PRICE": 232.22999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10503,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 23,
    "PRICE": 232.22999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10504,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 23,
    "PRICE": -232.22999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10505,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 23,
    "PRICE": 232.22999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10506,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 23,
    "PRICE": -232.22999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10507,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 23,
    "PRICE": 232.22999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10508,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 50,
    "PRICE": 999.989990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 10509,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-06T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 50,
    "PRICE": 999.989990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 10510,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-08T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 50,
    "PRICE": 999.989990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 10511,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-08T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 50,
    "PRICE": 999.989990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 10512,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-08T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 50,
    "PRICE": 999.989990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 10513,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10514,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2020-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10515,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2020-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10516,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-01-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10517,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-02-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10518,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-02-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "SELL",
    "TX_NUMBER": 10519,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-02-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10520,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-02-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10521,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-02-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10522,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-06-20T03:00:12|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 207.85000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 10523,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-06-20T03:00:12|,
    "SYMBOL": "SPY",
    "QUANTITY": 40,
    "PRICE": 207.85000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 10524,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2008-07-24T08:04:54|,
    "SYMBOL": "AVG",
    "QUANTITY": 30,
    "PRICE": 207.85000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 10525,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10526,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10527,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10528,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10529,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10530,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10531,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10532,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10533,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10534,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10535,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10536,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10537,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10538,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10539,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10540,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10541,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10542,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10543,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10544,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10545,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10546,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10547,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10548,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10549,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10550,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10551,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10552,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10553,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10554,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10555,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10556,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10557,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-27T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10558,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10559,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10560,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2022-04-29T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10561,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2022-04-29T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10562,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2008-07-24T08:04:54|,
    "SYMBOL": "AVG",
    "QUANTITY": 30,
    "PRICE": 207.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10563,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2022-04-29T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 1,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10564,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 500,
    "PRICE": 205.2100067138672,
    "ACTION": "SELL",
    "TX_NUMBER": 10565,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 500,
    "PRICE": 205.2100067138672,
    "ACTION": "SELL",
    "TX_NUMBER": 10566,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 500,
    "PRICE": 205.2100067138672,
    "ACTION": "SELL",
    "TX_NUMBER": 10567,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-01-12T03:56:18|,
    "SYMBOL": "CRM",
    "QUANTITY": 60,
    "PRICE": 123.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10568,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2019-05-20T10:05:43|,
    "SYMBOL": "AKA",
    "QUANTITY": 20,
    "PRICE": 123.8499984741211,
    "ACTION": "SELL",
    "TX_NUMBER": 10569,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-03-22T10:05:43|,
    "SYMBOL": "AKA",
    "QUANTITY": 56,
    "PRICE": 123.8499984741211,
    "ACTION": "SELL",
    "TX_NUMBER": 10570,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-08T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 50,
    "PRICE": 999.989990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 10571,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-08T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 50,
    "PRICE": 999.989990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 10572,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-08T20:43:30|,
    "SYMBOL": "CHR",
    "QUANTITY": 50,
    "PRICE": 999.989990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 10573,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-11T03:21:18|,
    "SYMBOL": "APL",
    "QUANTITY": 11,
    "PRICE": 965.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10574,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-11T03:30:29|,
    "SYMBOL": "AMZ",
    "QUANTITY": 1,
    "PRICE": 393.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10575,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-11T04:00:35|,
    "SYMBOL": "CRM",
    "QUANTITY": 15,
    "PRICE": 311.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10576,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-11T20:51:59|,
    "SYMBOL": "CRM",
    "QUANTITY": 15,
    "PRICE": 300.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10577,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-11T21:25:40|,
    "SYMBOL": "CRM",
    "QUANTITY": 15,
    "PRICE": 300.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10578,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-11T21:44:00|,
    "SYMBOL": "CRM",
    "QUANTITY": 15,
    "PRICE": 300.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10579,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-11T22:06:13|,
    "SYMBOL": "CRM",
    "QUANTITY": 15,
    "PRICE": 300.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10580,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T15:06:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 50,
    "PRICE": 305.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10581,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T00:36:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10582,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T00:36:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10583,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T00:36:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10584,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T00:36:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10585,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T00:36:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10586,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T00:36:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10587,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T00:36:25|,
    "SYMBOL": "AAA",
    "QUANTITY": 11,
    "PRICE": 15.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10588,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T15:36:25|,
    "SYMBOL": "AAA",
    "QUANTITY": 22,
    "PRICE": 43.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10589,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2018-03-22T20:55:31|,
    "SYMBOL": "GLD",
    "QUANTITY": 5,
    "PRICE": 495.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 10590,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-22T20:55:31|,
    "SYMBOL": "GLD",
    "QUANTITY": 5,
    "PRICE": 495.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 10591,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-22T20:55:31|,
    "SYMBOL": "GLD",
    "QUANTITY": 5,
    "PRICE": 495.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 10592,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-22T20:55:31|,
    "SYMBOL": "GLD",
    "QUANTITY": 5,
    "PRICE": 495.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 10593,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-22T20:55:31|,
    "SYMBOL": "GLD",
    "QUANTITY": 5,
    "PRICE": 495.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 10594,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-22T20:55:31|,
    "SYMBOL": "GLD",
    "QUANTITY": 5,
    "PRICE": 495.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 10595,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-04-12T00:00:00|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10596,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T00:00:00|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10597,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T20:22:32|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10598,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T20:28:03|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10599,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10600,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10601,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10602,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10603,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10604,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10605,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10606,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10607,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10608,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10609,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10610,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T09:50:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 10,
    "PRICE": 228.75999450683594,
    "ACTION": "BUY",
    "TX_NUMBER": 10611,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10612,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10613,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-13T12:01:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 10,
    "PRICE": 228.75999450683594,
    "ACTION": "BUY",
    "TX_NUMBER": 10614,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-13T01:04:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 10,
    "PRICE": 228.75999450683594,
    "ACTION": "BUY",
    "TX_NUMBER": 10615,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10616,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-13T01:04:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 10,
    "PRICE": 228.75999450683594,
    "ACTION": "BUY",
    "TX_NUMBER": 10617,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-13T01:04:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 10,
    "PRICE": 228.75999450683594,
    "ACTION": "BUY",
    "TX_NUMBER": 10618,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-12T12:36:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 50,
    "PRICE": 309.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10619,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10620,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10621,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10622,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-12T12:36:25|,
    "SYMBOL": "AAM",
    "QUANTITY": 50,
    "PRICE": 307.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10623,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-12T12:36:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 50,
    "PRICE": 330.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10624,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-04-13T01:04:25|,
    "SYMBOL": "CRM",
    "QUANTITY": 10,
    "PRICE": 111.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10625,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-02T15:25:56|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10626,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-05-02T15:26:58|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10627,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-05-02T16:20:56|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10628,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-03-06T17:43:31|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10629,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2015-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10630,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2023-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10631,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2023-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10632,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2022-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10633,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2023-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10634,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2023-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10635,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10638,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2022-12-28T17:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10639,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T14:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10640,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T16:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10641,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T16:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10642,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T10:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10643,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T07:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10644,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2015-12-28T20:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10645,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-04T11:38:08|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10646,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-04T11:43:10|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 215.2100067138672,
    "ACTION": "SELL",
    "TX_NUMBER": 10647,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-09T15:29:59|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 29.209999084472656,
    "ACTION": "BUY",
    "TX_NUMBER": 10648,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-09T21:53:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 1500.2099609375,
    "ACTION": "BUY",
    "TX_NUMBER": 10649,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-12T23:16:12|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "SELL",
    "TX_NUMBER": 10650,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-13T14:17:28|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10652,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-23T15:48:13|,
    "SYMBOL": "DAY",
    "QUANTITY": 6,
    "PRICE": 365.239990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 10653,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-23T16:20:56|,
    "SYMBOL": "OOO",
    "QUANTITY": 6,
    "PRICE": 365.239990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 10654,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-23T16:32:18|,
    "SYMBOL": "OOO",
    "QUANTITY": 6,
    "PRICE": 365.239990234375,
    "ACTION": "BUY",
    "TX_NUMBER": 10655,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-23T16:42:12|,
    "SYMBOL": "OOO",
    "QUANTITY": 6,
    "PRICE": 24.700000762939453,
    "ACTION": "SELL",
    "TX_NUMBER": 10656,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-24T01:38:54|,
    "SYMBOL": "LUK",
    "QUANTITY": 6,
    "PRICE": 24.700000762939453,
    "ACTION": "SELL",
    "TX_NUMBER": 10657,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-24T04:40:17|,
    "SYMBOL": "WON",
    "QUANTITY": 6,
    "PRICE": 24.700000762939453,
    "ACTION": "SELL",
    "TX_NUMBER": 10658,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-24T05:08:18|,
    "SYMBOL": "CAM",
    "QUANTITY": 207,
    "PRICE": 20.700000762939453,
    "ACTION": "SELL",
    "TX_NUMBER": 10659,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-24T18:19:16|,
    "SYMBOL": "BTS",
    "QUANTITY": 151,
    "PRICE": 15.100000381469727,
    "ACTION": "BUY",
    "TX_NUMBER": 10661,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-24T18:23:33|,
    "SYMBOL": "BTS",
    "QUANTITY": 151,
    "PRICE": 15.100000381469727,
    "ACTION": "BUY",
    "TX_NUMBER": 10662,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-24T18:26:00|,
    "SYMBOL": "BTS",
    "QUANTITY": 151,
    "PRICE": 15.100000381469727,
    "ACTION": "BUY",
    "TX_NUMBER": 10663,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-24T18:35:23|,
    "SYMBOL": "LTH",
    "QUANTITY": 153,
    "PRICE": 15.350000381469727,
    "ACTION": "BUY",
    "TX_NUMBER": 10665,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-31T12:30:18|,
    "SYMBOL": "Example",
    "QUANTITY": 1,
    "PRICE": 1.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10667,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-31T12:30:18|,
    "SYMBOL": "Example",
    "QUANTITY": 1,
    "PRICE": 1.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10668,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-31T12:30:18|,
    "SYMBOL": "Example",
    "QUANTITY": 1,
    "PRICE": 1.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10669,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-31T12:30:18|,
    "SYMBOL": "Example",
    "QUANTITY": 1,
    "PRICE": 1.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10670,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2022-05-31T12:30:18|,
    "SYMBOL": "Example",
    "QUANTITY": 1,
    "PRICE": 1.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10672,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2022-05-31T12:30:18|,
    "SYMBOL": "Example",
    "QUANTITY": 1,
    "PRICE": 1.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10673,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2022-06-30T12:30:18|,
    "SYMBOL": "Example",
    "QUANTITY": 1,
    "PRICE": 1.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10674,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2022-06-30T12:30:20|,
    "SYMBOL": "Example",
    "QUANTITY": 15,
    "PRICE": 129.2899932861328,
    "ACTION": "SELL",
    "TX_NUMBER": 10675,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2022-06-30T12:30:20|,
    "SYMBOL": "Example",
    "QUANTITY": 15,
    "PRICE": 129.2899932861328,
    "ACTION": "BUY",
    "TX_NUMBER": 10676,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2016-06-20T03:00:12|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 207.85000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 10677,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-05-06T20:55:31|,
    "SYMBOL": "MULE",
    "QUANTITY": 5,
    "PRICE": 720.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10678,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-05-06T20:55:31|,
    "SYMBOL": "MULE",
    "QUANTITY": 5,
    "PRICE": 720.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10679,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-22T20:55:31|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10680,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-05T14:50:14|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 20.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10681,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-06T21:00:16|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10687,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-06T21:02:22|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10688,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-06T21:02:31|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10689,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T01:14:53|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 25.100000381469727,
    "ACTION": "BUY",
    "TX_NUMBER": 10690,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-07T02:18:24|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10691,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T02:18:28|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10692,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T09:53:07|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10693,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T13:14:10|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10694,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T13:37:33|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10695,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T13:55:30|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10696,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T14:12:40|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10697,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T14:16:07|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10698,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T14:17:57|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10699,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T14:20:26|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10700,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T14:35:32|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10701,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T14:51:25|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10702,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T14:52:24|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10703,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T15:04:32|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10704,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T15:16:11|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10705,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T15:26:43|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10706,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T18:41:16|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10707,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-07T15:46:07|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10708,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-03-22T20:55:31|,
    "SYMBOL": "GLD",
    "QUANTITY": 5,
    "PRICE": 495.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 10709,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-03-22T20:55:31|,
    "SYMBOL": "GLD",
    "QUANTITY": 5,
    "PRICE": 495.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 10710,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-03-22T20:55:31|,
    "SYMBOL": "GLD",
    "QUANTITY": 5,
    "PRICE": 495.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 10711,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-03-22T20:55:31|,
    "SYMBOL": "USD",
    "QUANTITY": 4,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10712,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-03-22T20:55:31|,
    "SYMBOL": "USD",
    "QUANTITY": 4,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10713,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-03-22T20:55:31|,
    "SYMBOL": "USD",
    "QUANTITY": 4,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10714,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-03-22T20:55:31|,
    "SYMBOL": "USD",
    "QUANTITY": 4,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10715,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-22T20:55:31|,
    "SYMBOL": "USD",
    "QUANTITY": 1,
    "PRICE": 10.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10716,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-22T20:55:31|,
    "SYMBOL": "USD",
    "QUANTITY": 1,
    "PRICE": 10.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10717,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-22T20:55:31|,
    "SYMBOL": "USD",
    "QUANTITY": 1,
    "PRICE": 10.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10718,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-22T20:55:31|,
    "SYMBOL": "USD",
    "QUANTITY": 1,
    "PRICE": 10.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10719,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-03-22T20:55:31|,
    "SYMBOL": "USD",
    "QUANTITY": 4,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10720,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2018-03-22T20:55:31|,
    "SYMBOL": "USD",
    "QUANTITY": 1,
    "PRICE": 10.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10722,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-08T05:23:10|,
    "SYMBOL": "FED",
    "QUANTITY": 2,
    "PRICE": 20.149999618530273,
    "ACTION": "BUY",
    "TX_NUMBER": 10723,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-08T05:24:29|,
    "SYMBOL": "FED",
    "QUANTITY": 2,
    "PRICE": 20.149999618530273,
    "ACTION": "BUY",
    "TX_NUMBER": 10724,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-03-22T20:55:31|,
    "SYMBOL": "GLD",
    "QUANTITY": 5,
    "PRICE": 495.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 10725,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-03-22T20:55:31|,
    "SYMBOL": "GLD",
    "QUANTITY": 5,
    "PRICE": 495.69000244140625,
    "ACTION": "SELL",
    "TX_NUMBER": 10726,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-08T09:20:41|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10727,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-08T12:48:09|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10728,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-09T03:44:30|,
    "SYMBOL": "FCO",
    "QUANTITY": 7,
    "PRICE": 15.100000381469727,
    "ACTION": "BUY",
    "TX_NUMBER": 10729,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-09T04:19:14|,
    "SYMBOL": "RBM",
    "QUANTITY": 1,
    "PRICE": 10.109999656677246,
    "ACTION": "BUY",
    "TX_NUMBER": 10730,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-09T04:53:21|,
    "SYMBOL": "RBM",
    "QUANTITY": 1,
    "PRICE": 10.109999656677246,
    "ACTION": "BUY",
    "TX_NUMBER": 10731,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-09T05:58:41|,
    "SYMBOL": "IBM",
    "QUANTITY": 5,
    "PRICE": 32.470001220703125,
    "ACTION": "SELL",
    "TX_NUMBER": 10732,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-09T06:16:41|,
    "SYMBOL": "IBM",
    "QUANTITY": 15,
    "PRICE": 3.299999952316284,
    "ACTION": "SELL",
    "TX_NUMBER": 10733,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-09T06:16:58|,
    "SYMBOL": "RBM",
    "QUANTITY": 1,
    "PRICE": 10.109999656677246,
    "ACTION": "BUY",
    "TX_NUMBER": 10734,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-09T06:19:07|,
    "SYMBOL": "RBM1",
    "QUANTITY": 5,
    "PRICE": 22.670000076293945,
    "ACTION": "BUY",
    "TX_NUMBER": 10735,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-09T06:20:27|,
    "SYMBOL": "RBM1",
    "QUANTITY": 5,
    "PRICE": 32.66999816894531,
    "ACTION": "SELL",
    "TX_NUMBER": 10736,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-10T11:06:12|,
    "SYMBOL": "APL",
    "QUANTITY": 30,
    "PRICE": 207.85000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 10737,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-07-24T08:04:54|,
    "SYMBOL": "CRM",
    "QUANTITY": 56,
    "PRICE": 207.85000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 10738,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-11T11:32:26|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1111.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10741,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-11T16:15:59|,
    "SYMBOL": "TXT",
    "QUANTITY": 131,
    "PRICE": 25.520000457763672,
    "ACTION": "BUY",
    "TX_NUMBER": 10742,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-11T16:43:33|,
    "SYMBOL": "TXT",
    "QUANTITY": 131,
    "PRICE": 25.520000457763672,
    "ACTION": "BUY",
    "TX_NUMBER": 10743,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-14T01:39:40|,
    "SYMBOL": "SPY",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10745,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-14T01:39:42|,
    "SYMBOL": "SPY",
    "QUANTITY": 20,
    "PRICE": 150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10746,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-14T03:00:16|,
    "SYMBOL": "SPO",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10747,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-14T03:00:16|,
    "SYMBOL": "SPO",
    "QUANTITY": 20,
    "PRICE": 150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10748,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2020-08-05T22:00:00|,
    "SYMBOL": "SPO",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10749,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-14T03:01:11|,
    "SYMBOL": "SPO",
    "QUANTITY": 20,
    "PRICE": 150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10750,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2020-08-05T22:00:00|,
    "SYMBOL": "SPO",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10751,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2021-06-14T03:02:25|,
    "SYMBOL": "SPO",
    "QUANTITY": 20,
    "PRICE": 150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10752,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-14T03:03:00|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10753,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-14T03:03:00|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10754,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2030-08-05T22:00:00|,
    "SYMBOL": "NPARAMA",
    "QUANTITY": 20,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10755,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2021-06-14T03:03:40|,
    "SYMBOL": "DPS",
    "QUANTITY": 20,
    "PRICE": 150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10756,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2021-06-14T03:56:45|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10757,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-14T03:56:45|,
    "SYMBOL": "DIA",
    "QUANTITY": 60,
    "PRICE": 176.97999572753906,
    "ACTION": "BUY",
    "TX_NUMBER": 10758,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2030-08-05T22:00:00|,
    "SYMBOL": "NPARAMA",
    "QUANTITY": 20,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10759,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2030-08-05T22:00:00|,
    "SYMBOL": "NPARAMA",
    "QUANTITY": 20,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10760,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2020-08-05T22:00:00|,
    "SYMBOL": "TST1",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10761,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2021-06-14T08:26:23|,
    "SYMBOL": "TST1",
    "QUANTITY": 20,
    "PRICE": 150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10762,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2020-08-05T22:00:00|,
    "SYMBOL": "TST1",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10763,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2021-06-14T08:29:30|,
    "SYMBOL": "tst",
    "QUANTITY": 20,
    "PRICE": 150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10764,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2020-08-05T22:00:00|,
    "SYMBOL": "TST1",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10765,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2021-06-14T08:32:04|,
    "SYMBOL": "tt1",
    "QUANTITY": 20,
    "PRICE": 150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10766,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2020-08-05T22:00:00|,
    "SYMBOL": "TST1",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10767,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2021-06-14T09:57:31|,
    "SYMBOL": "tt1",
    "QUANTITY": 20,
    "PRICE": 150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10768,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2020-08-05T22:00:00|,
    "SYMBOL": "TST1",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10769,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2021-06-15T06:49:14|,
    "SYMBOL": "tt1",
    "QUANTITY": 20,
    "PRICE": 150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10770,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2020-08-05T22:00:00|,
    "SYMBOL": "TST1",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10771,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2021-06-15T07:16:19|,
    "SYMBOL": "tt1",
    "QUANTITY": 20,
    "PRICE": 150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10772,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-16T15:03:14|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10773,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-16T19:14:13|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10774,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-16T17:57:34|,
    "SYMBOL": "MULE",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10775,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-17T19:30:50|,
    "SYMBOL": "MULE",
    "QUANTITY": 3,
    "PRICE": 10.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10776,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2020-08-05T22:00:00|,
    "SYMBOL": "TST1",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10777,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2030-08-05T22:00:00|,
    "SYMBOL": "NPARAMA",
    "QUANTITY": 20,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10778,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2020-08-05T22:00:00|,
    "SYMBOL": "TST1",
    "QUANTITY": 10,
    "PRICE": 150.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10779,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2021-06-24T03:13:41|,
    "SYMBOL": "tt1",
    "QUANTITY": 20,
    "PRICE": 150.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10780,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-06-25T15:44:32|,
    "SYMBOL": "RBM2",
    "QUANTITY": 4,
    "PRICE": 10.329999923706055,
    "ACTION": "SELL",
    "TX_NUMBER": 10781,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-25T18:29:57|,
    "SYMBOL": "RBM2",
    "QUANTITY": 4,
    "PRICE": 10.329999923706055,
    "ACTION": "SELL",
    "TX_NUMBER": 10782,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-25T18:31:32|,
    "SYMBOL": "RBM",
    "QUANTITY": 4,
    "PRICE": 10.329999923706055,
    "ACTION": "SELL",
    "TX_NUMBER": 10783,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-25T18:32:15|,
    "SYMBOL": "RBM",
    "QUANTITY": 4,
    "PRICE": 10.550000190734863,
    "ACTION": "SELL",
    "TX_NUMBER": 10784,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-06-25T19:09:02|,
    "SYMBOL": "RBM",
    "QUANTITY": 4,
    "PRICE": 10.699999809265137,
    "ACTION": "SELL",
    "TX_NUMBER": 10785,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-07-25T00:00:00|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10786,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-07-25T00:00:00|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10787,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-07-25T00:00:00|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10788,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-07-25T00:00:00|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10789,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-07-25T00:00:00|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "BUY",
    "TX_NUMBER": 10790,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-07-25T00:00:00|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "SELL",
    "TX_NUMBER": 10791,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-07-25T00:00:00|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "SELL",
    "TX_NUMBER": 10792,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-07-25T00:00:00|,
    "SYMBOL": "AAA",
    "QUANTITY": 5,
    "PRICE": 6.300000190734863,
    "ACTION": "SELL",
    "TX_NUMBER": 10793,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-08-22T11:22:57|,
    "SYMBOL": "AGS",
    "QUANTITY": 25,
    "PRICE": 225.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10794,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-08-22T12:40:40|,
    "SYMBOL": "AGS",
    "QUANTITY": 5,
    "PRICE": 305.2099914550781,
    "ACTION": "BUY",
    "TX_NUMBER": 10795,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-08-22T12:41:51|,
    "SYMBOL": "AGS",
    "QUANTITY": 5,
    "PRICE": 305.2099914550781,
    "ACTION": "BUY",
    "TX_NUMBER": 10796,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-08-22T12:42:08|,
    "SYMBOL": "AGS",
    "QUANTITY": 5,
    "PRICE": 305.2099914550781,
    "ACTION": "BUY",
    "TX_NUMBER": 10797,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-08-22T12:51:15|,
    "SYMBOL": "SPY",
    "QUANTITY": 5,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10798,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-01T22:56:02|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 210.10000610351562,
    "ACTION": "BUY",
    "TX_NUMBER": 10799,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-01T22:57:06|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 210.10000610351562,
    "ACTION": "BUY",
    "TX_NUMBER": 10800,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-01T23:00:13|,
    "SYMBOL": "SPY",
    "QUANTITY": 51,
    "PRICE": 211.10000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 10801,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-02T13:36:17|,
    "SYMBOL": "SPY",
    "QUANTITY": 51,
    "PRICE": 212.10000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 10802,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-03T14:09:02|,
    "SYMBOL": "SPY",
    "QUANTITY": 52,
    "PRICE": 214.10000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 10803,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-03T18:19:30|,
    "SYMBOL": "SPY",
    "QUANTITY": 52,
    "PRICE": 214.10000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 10804,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-03T19:59:28|,
    "SYMBOL": "SPY",
    "QUANTITY": 62,
    "PRICE": 214.10000610351562,
    "ACTION": "SELL",
    "TX_NUMBER": 10805,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-03T20:23:48|,
    "SYMBOL": "SPY",
    "QUANTITY": 62,
    "PRICE": 314.1000061035156,
    "ACTION": "SELL",
    "TX_NUMBER": 10806,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T12:17:59|,
    "SYMBOL": "SPY",
    "QUANTITY": 62,
    "PRICE": 414.1000061035156,
    "ACTION": "SELL",
    "TX_NUMBER": 10807,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T12:19:51|,
    "SYMBOL": "SPY",
    "QUANTITY": 62,
    "PRICE": 514.0999755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 10808,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T12:54:32|,
    "SYMBOL": "SPY",
    "QUANTITY": 62,
    "PRICE": 514.0999755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 10809,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T13:12:30|,
    "SYMBOL": "SPY",
    "QUANTITY": 62,
    "PRICE": 514.0999755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 10810,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T13:39:15|,
    "SYMBOL": "SPY",
    "QUANTITY": 62,
    "PRICE": 614.0999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 10811,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T13:39:52|,
    "SYMBOL": "SPY",
    "QUANTITY": 62,
    "PRICE": 714.0999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 10812,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T13:42:03|,
    "SYMBOL": "",
    "QUANTITY": 62,
    "PRICE": 714.0999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 10813,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T13:50:24|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 814.0999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 10814,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T14:17:44|,
    "SYMBOL": "SPY",
    "QUANTITY": 62,
    "PRICE": 524.0999755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 10815,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T14:21:28|,
    "SYMBOL": "SPY",
    "QUANTITY": 51,
    "PRICE": 912.0999755859375,
    "ACTION": "SELL",
    "TX_NUMBER": 10816,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T18:35:17|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 914.0999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 10817,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T20:16:43|,
    "SYMBOL": "DIA",
    "QUANTITY": 10,
    "PRICE": 100.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10818,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T21:05:52|,
    "SYMBOL": "DIA",
    "QUANTITY": 10,
    "PRICE": 101.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10819,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T21:39:44|,
    "SYMBOL": "DIA",
    "QUANTITY": 10,
    "PRICE": 102.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10820,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T21:40:02|,
    "SYMBOL": "DIA",
    "QUANTITY": 10,
    "PRICE": 102.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10821,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T21:40:10|,
    "SYMBOL": "DIA",
    "QUANTITY": 10,
    "PRICE": 102.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10822,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-04T22:28:34|,
    "SYMBOL": "DIA",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10824,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-10-04T22:42:31|,
    "SYMBOL": "DIA",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10825,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-10-04T22:46:38|,
    "SYMBOL": "DIA",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10826,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-10-04T22:47:05|,
    "SYMBOL": "DIA",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10827,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-10-04T22:51:30|,
    "SYMBOL": ":SYMBOL2",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10828,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-10-04T23:03:01|,
    "SYMBOL": "DIA",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10829,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-10-04T23:28:28|,
    "SYMBOL": ":SYMBOL2",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10830,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-10-05T11:30:45|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 1114.0999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 10831,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T11:33:45|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 1214.0999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 10832,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T11:44:34|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 1214.0999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 10833,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T11:51:09|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 1414.0999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 10834,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T12:16:15|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 1514.0999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 10835,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T13:08:53|,
    "SYMBOL": "DIA",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10836,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-10-05T14:25:30|,
    "SYMBOL": "DIA",
    "QUANTITY": 101,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10837,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T14:31:25|,
    "SYMBOL": "DIA",
    "QUANTITY": 101,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10838,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T14:34:33|,
    "SYMBOL": "DIA",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10839,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-10-05T18:10:29|,
    "SYMBOL": "DIA",
    "QUANTITY": 100,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10840,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T18:39:16|,
    "SYMBOL": "DIA",
    "QUANTITY": 101,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10841,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T20:20:44|,
    "SYMBOL": "DIA",
    "QUANTITY": 101,
    "PRICE": 1000.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10842,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T20:24:41|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 1114.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10843,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T20:26:38|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 1114.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10844,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T20:38:45|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 1314.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10845,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T20:41:59|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 1315.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10846,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T20:44:05|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 1315.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10847,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T20:46:34|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 1315.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10848,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T21:08:51|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 2315.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10849,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-05T21:11:50|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 2415.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10850,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-06T11:22:42|,
    "SYMBOL": "DIA",
    "QUANTITY": 32,
    "PRICE": 2415.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10851,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-06T11:53:58|,
    "SYMBOL": "DIA",
    "QUANTITY": 32,
    "PRICE": 2015.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10852,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-16T11:38:35|,
    "SYMBOL": "DIA",
    "QUANTITY": 62,
    "PRICE": 1614.0999755859375,
    "ACTION": "BUY",
    "TX_NUMBER": 10853,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-16T11:38:50|,
    "SYMBOL": "DIA",
    "QUANTITY": 32,
    "PRICE": 3015.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10854,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-18T14:42:04|,
    "SYMBOL": "DIA",
    "QUANTITY": 32,
    "PRICE": 4015.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10855,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-10-18T16:26:41|,
    "SYMBOL": "DIA",
    "QUANTITY": 32,
    "PRICE": 5015.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10856,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.099998474121094,
    "ACTION": "BUY",
    "TX_NUMBER": 10857,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.099998474121094,
    "ACTION": "BUY",
    "TX_NUMBER": 10858,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.099998474121094,
    "ACTION": "BUY",
    "TX_NUMBER": 10859,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.099998474121094,
    "ACTION": "BUY",
    "TX_NUMBER": 10860,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.099998474121094,
    "ACTION": "BUY",
    "TX_NUMBER": 10861,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.099998474121094,
    "ACTION": "BUY",
    "TX_NUMBER": 10862,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.189998626708984,
    "ACTION": "BUY",
    "TX_NUMBER": 10863,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.189998626708984,
    "ACTION": "BUY",
    "TX_NUMBER": 10864,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.189998626708984,
    "ACTION": "BUY",
    "TX_NUMBER": 10865,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.189998626708984,
    "ACTION": "BUY",
    "TX_NUMBER": 10866,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.189998626708984,
    "ACTION": "BUY",
    "TX_NUMBER": 10867,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.189998626708984,
    "ACTION": "BUY",
    "TX_NUMBER": 10868,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.189998626708984,
    "ACTION": "BUY",
    "TX_NUMBER": 10869,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.189998626708984,
    "ACTION": "BUY",
    "TX_NUMBER": 10870,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.189998626708984,
    "ACTION": "BUY",
    "TX_NUMBER": 10871,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-11-05T20:50:57|,
    "SYMBOL": "SPY",
    "QUANTITY": 12,
    "PRICE": 5.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10872,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-11-05T20:50:57|,
    "SYMBOL": "SPY",
    "QUANTITY": 12,
    "PRICE": 5.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10873,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-11-05T20:50:57|,
    "SYMBOL": "SPY",
    "QUANTITY": 12,
    "PRICE": 5.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10874,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-11-05T20:50:57|,
    "SYMBOL": "SPY",
    "QUANTITY": 12,
    "PRICE": 5.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10875,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-11-05T20:50:57|,
    "SYMBOL": "SPY",
    "QUANTITY": 12,
    "PRICE": 5.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10876,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-11-05T20:50:57|,
    "SYMBOL": "SPY",
    "QUANTITY": 12,
    "PRICE": 5.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10877,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-11-05T20:50:57|,
    "SYMBOL": "SPY",
    "QUANTITY": 12,
    "PRICE": 5.5,
    "ACTION": "BUY",
    "TX_NUMBER": 10878,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-11-05T20:50:57|,
    "SYMBOL": "DWN",
    "QUANTITY": 12,
    "PRICE": 5.5,
    "ACTION": "SELL",
    "TX_NUMBER": 10879,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2021-11-05T20:50:57|,
    "SYMBOL": "YYY",
    "QUANTITY": 12,
    "PRICE": 5.199999809265137,
    "ACTION": "SELL",
    "TX_NUMBER": 10880,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2021-11-05T20:50:57|,
    "SYMBOL": "YYY",
    "QUANTITY": 12,
    "PRICE": 5.199999809265137,
    "ACTION": "SELL",
    "TX_NUMBER": 10881,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-11-05T20:50:57|,
    "SYMBOL": "YYY",
    "QUANTITY": 12,
    "PRICE": 5.199999809265137,
    "ACTION": "SELL",
    "TX_NUMBER": 10882,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2021-11-05T20:50:57|,
    "SYMBOL": "YYY",
    "QUANTITY": 12,
    "PRICE": 4.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10883,
    "STATUS": "Canceled"
  },
  {
    "ORDER_DATE": |2021-11-09T20:50:57|,
    "SYMBOL": "YYY",
    "QUANTITY": 12,
    "PRICE": 4.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10884,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-11-09T20:50:57|,
    "SYMBOL": "YYY",
    "QUANTITY": 12,
    "PRICE": 4.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10885,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-11-09T20:50:57|,
    "SYMBOL": "YYY",
    "QUANTITY": 12,
    "PRICE": 4.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10886,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-11-09T20:50:57|,
    "SYMBOL": "YYY",
    "QUANTITY": 12,
    "PRICE": 4.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10887,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.189998626708984,
    "ACTION": "BUY",
    "TX_NUMBER": 10888,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 109,
    "PRICE": 55.189998626708984,
    "ACTION": "BUY",
    "TX_NUMBER": 10889,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2017-11-19T15:20:21|,
    "SYMBOL": "FED",
    "QUANTITY": 10,
    "PRICE": 55.189998626708984,
    "ACTION": "BUY",
    "TX_NUMBER": 10890,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-11-09T20:50:57|,
    "SYMBOL": "YYY",
    "QUANTITY": 12,
    "PRICE": 4.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10891,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-11-19T17:10:57|,
    "SYMBOL": "YYY",
    "QUANTITY": 12,
    "PRICE": 4.0,
    "ACTION": "BUY",
    "TX_NUMBER": 10892,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2021-12-05T18:25:04|,
    "SYMBOL": "TST",
    "QUANTITY": 6774,
    "PRICE": 100.80000305175781,
    "ACTION": "SELL",
    "TX_NUMBER": 10893,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-12-05T18:26:33|,
    "SYMBOL": "TST",
    "QUANTITY": 100,
    "PRICE": 101.80000305175781,
    "ACTION": "SELL",
    "TX_NUMBER": 10894,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-12-05T18:52:26|,
    "SYMBOL": "TST",
    "QUANTITY": 2,
    "PRICE": 2.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10895,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-12-05T19:11:31|,
    "SYMBOL": "TST",
    "QUANTITY": 1,
    "PRICE": 101.80000305175781,
    "ACTION": "SELL",
    "TX_NUMBER": 10896,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-12-06T18:51:31|,
    "SYMBOL": "TST",
    "QUANTITY": 1,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10897,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-12-06T18:59:00|,
    "SYMBOL": "TST",
    "QUANTITY": 1,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10898,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-12-06T21:13:47|,
    "SYMBOL": "TST",
    "QUANTITY": 1,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10899,
    "STATUS": "Pending"
  },
  {
    "ORDER_DATE": |2021-12-06T21:54:31|,
    "SYMBOL": "TST",
    "QUANTITY": 1,
    "PRICE": 100.0,
    "ACTION": "SELL",
    "TX_NUMBER": 10900,
    "STATUS": "Pending"
  }
]