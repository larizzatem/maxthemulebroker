%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo([
  {
    "Action": "SELL",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 500,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10164",
    "TOTAL_AMOUNT": 102605.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10630",
    "TOTAL_AMOUNT": 10260.50
  }
])