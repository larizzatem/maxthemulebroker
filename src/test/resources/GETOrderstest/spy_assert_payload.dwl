%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo([
  {
    "ORDER_DATE": |2015-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 500,
    "PRICE": 205.2100067138672,
    "ACTION": "SELL",
    "TX_NUMBER": 10164,
    "STATUS": "Filled"
  },
  {
    "ORDER_DATE": |2015-12-28T15:06:25|,
    "SYMBOL": "SPY",
    "QUANTITY": 50,
    "PRICE": 205.2100067138672,
    "ACTION": "BUY",
    "TX_NUMBER": 10630,
    "STATUS": "Filled"
  }
])