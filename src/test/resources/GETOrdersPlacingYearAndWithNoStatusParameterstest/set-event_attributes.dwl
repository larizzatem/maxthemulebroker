{
  "headers": {
    "user-agent": "PostmanRuntime/7.26.8",
    "accept": "*/*",
    "cache-control": "no-cache",
    "host": "localhost:8081",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive",
    "cookie": "JSESSIONID=738A97564C6ED784DA53BF53CE879E41"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "http",
  "queryParams": {
    "year": "2015"
  },
  "requestUri": "/api/orders?year=2015",
  "queryString": "year=2015",
  "version": "HTTP/1.1",
  "maskedRequestPath": null,
  "listenerPath": "/api/orders",
  "relativePath": "/orders",
  "localAddress": "/127.0.0.1:8081",
  "uriParams": {},
  "rawRequestUri": "/api/orders?year=2015",
  "rawRequestPath": "/api/orders",
  "remoteAddress": "/127.0.0.1:42460",
  "requestPath": "/api/orders"
}