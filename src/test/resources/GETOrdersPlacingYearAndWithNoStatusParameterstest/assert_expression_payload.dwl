%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo([
  {
    "Action": "SELL",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 500,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10164",
    "TOTAL_AMOUNT": 102605.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10165",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10167",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10179",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10180",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 500,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10565",
    "TOTAL_AMOUNT": 102605.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 500,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10566",
    "TOTAL_AMOUNT": 102605.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 500,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10567",
    "TOTAL_AMOUNT": 102605.00
  },
  {
    "Action": "SELL",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 305.00,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "CRM",
    "TX_NUMBER": "10581",
    "TOTAL_AMOUNT": 15250.00
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Filled",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10630",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T15:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10638",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T14:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10640",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T16:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10641",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T16:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10642",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T10:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10643",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T07:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10644",
    "TOTAL_AMOUNT": 10260.50
  },
  {
    "Action": "BUY",
    "ORDER_DATE": "2015-12-28T20:06:25",
    "PRICE": 205.21,
    "QUANTITY": 50,
    "STATUS": "Pending",
    "SYMBOL": "SPY",
    "TX_NUMBER": "10645",
    "TOTAL_AMOUNT": 10260.50
  }
])